import {MigrationInterface, QueryRunner} from "typeorm";

export class InsertPermisos1619118169423 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
        INSERT INTO public.usuario_permiso ("fk_persona_id","fk_publicacion_id","tipoPermiso")
        SELECT fk_publicante_id as fk_persona_id, id as fk_publicacion_id, 'WR' as tipoPermiso
        FROM 
        public.publicacion;
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
