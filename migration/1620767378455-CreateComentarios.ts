import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateComentarios1620767378455 implements MigrationInterface {
  name = 'CreateComentarios1620767378455';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "comentario" ("id" SERIAL NOT NULL, "texto" character varying NOT NULL, "fecha_publicacion" TIMESTAMP NOT NULL DEFAULT now(), "fk_persona_id" integer NOT NULL, "fk_noticia_id" integer NOT NULL, CONSTRAINT "PK_c9014211e5fbf491b9e3543bb19" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(
      `ALTER TABLE "comentario" ADD CONSTRAINT "FK_450e862303a39af4f7d9392d08f" FOREIGN KEY ("fk_persona_id") REFERENCES "persona"("id") ON DELETE CASCADE ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "comentario" ADD CONSTRAINT "FK_ac7ff9b82cd53ae326361153bcd" FOREIGN KEY ("fk_noticia_id") REFERENCES "noticia"("id") ON DELETE CASCADE ON UPDATE NO ACTION`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "comentario" DROP CONSTRAINT "FK_ac7ff9b82cd53ae326361153bcd"`
    );
    await queryRunner.query(
      `ALTER TABLE "comentario" DROP CONSTRAINT "FK_450e862303a39af4f7d9392d08f"`
    );
    await queryRunner.query(`DROP TABLE "comentario"`);
  }
}
