import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateTareas1623591142581 implements MigrationInterface {
  name = 'CreateTareas1623591142581';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "tarea" ("id" SERIAL NOT NULL, "texto" character varying NOT NULL, "personaId" integer NOT NULL, CONSTRAINT "PK_52df0f8fc74f81d0531ad890f0e" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(
      `ALTER TABLE "tarea" ADD CONSTRAINT "FK_21cfe9045300c63ec3d2e352324" FOREIGN KEY ("personaId") REFERENCES "persona"("id") ON DELETE CASCADE ON UPDATE NO ACTION`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "tarea" DROP CONSTRAINT "FK_21cfe9045300c63ec3d2e352324"`);
    await queryRunner.query(`DROP TABLE "tarea"`);
  }
}
