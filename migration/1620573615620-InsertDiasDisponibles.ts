import { MigrationInterface, QueryRunner } from 'typeorm';

export class InsertDiasDisponibles1620573615620 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`INSERT INTO public.hist_dias_disp_vacaciones(
                 fecha_solicitud, dias_disponibles, fk_persona_id)
                 (SELECT NOW(), 15, id FROM persona);`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {}
}
