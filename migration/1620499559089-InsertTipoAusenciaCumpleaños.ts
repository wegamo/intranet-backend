import { MigrationInterface, QueryRunner } from 'typeorm';

export class InsertTipoAusenciaCumpleaños1620499559089 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`INSERT INTO public.motivo_ausencia(
            nombre)
            VALUES ('Cumpleaños');`);

    // Insert de Ausencias por dia
    await queryRunner.query(`INSERT INTO public.ausencia(
    fk_solicitud_id, fk_motivo_ausencia_id, fk_tipo_descuento_id)
    VALUES (3, 11, 2);`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {}
}
