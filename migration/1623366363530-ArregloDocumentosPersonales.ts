import { MigrationInterface, QueryRunner } from 'typeorm';

export class ArregloDocumentosPersonales1623366363530 implements MigrationInterface {
  name = 'ArregloDocumentosPersonales1623366363530';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "documento_personal" ADD CONSTRAINT "UQ_d3ac4577736852a28b34e001155" UNIQUE ("fk_persona_id", "fk_tipo_doc_personal_id")`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "documento_personal" DROP CONSTRAINT "UQ_d3ac4577736852a28b34e001155"`
    );
  }
}
