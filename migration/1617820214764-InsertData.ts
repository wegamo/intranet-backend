import { MigrationInterface, QueryRunner } from 'typeorm';

export class InsertData1617820214764 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    // INSERTS

    await queryRunner.query(`INSERT INTO public.empresa(
        nombre, correo, estatus)
        VALUES ('Celupago', 'celupago@gmail.com', 'activo'),
               ('Cervecentro', 'cervecentro@gmail.com', 'activo'),
               ('Conectium', 'conectium@gmail.com', 'activo'),
               ('Pangeatech', 'pangeatech@gmail.com', 'activo');`);

    await queryRunner.query(`INSERT INTO public.area(
        nombre, descripcion)
        VALUES ('Board of Directors', 'Directivos de la Compañia'),
               ('Finance', 'Finanzas'),
               ('Administration', 'Administracion'),
               ('Accounting and Costs', 'Contabilidad'),
               ('Human Talent', 'Talento Humano'),
               ('Development', 'Desarrollo computacional de soluciones'),
               ('Product', 'Producto'),
               ('Marketing', 'Mercadeo'),
               ('QA & Process', 'QA'),
               ('Project', 'Proyecto'),
               ('Information Technology', 'IT'),
               ('Ekiipago', 'Ekiipago'),
               ('Sales', 'Ventas'),
               ('Management', 'Gestión'),
               ('Quality', 'Calidad'),
               ('Production', 'Producción'),
               ('Business Direction', 'Dirección de Negocios'),
               ('Business Intelligence', 'Inteligencia de Negocios'),
               ('Personal A. Riveroll', 'Personal A. Riveroll');`);

    // Insert Board of Directors

    await queryRunner.query(`INSERT INTO public.cargo(
        nombre, responsabilidades, fk_parent_cargo_id, fk_area_id)
        VALUES ('Chief Executive Officer', 'Toma decisiones sobre estrategia empresarial.', 1, 1),
               ('Chief Operating Officer', 'Coordinar los procesos y operaciones de la empresa.', 1, 1),
               ('Chief Financial Officer', 'Gestionar las Finanzas', 1, 2),
               ('Director of Operations Andromeda', 'Dirigir las Operaciones de Andromeda', 2, 17),
               ('Operations Director Sinapsis', '', 2, 17),
               ('Commercial Director Sinapsis', '', 2, 17),
               ('Director of Red Subasta', '', 2, 17),
               ('Commercial Manager Ekiipago', '', 2, 17),
               ('Commercial Manager Cervecentro', '', 1, 13),
               ('Secretary Board of Directors', '', 1, 1),
               ('Human Talent Manager', 'Gestionar Personal', 1, 5);`);

    // Insert Financial
    await queryRunner.query(`INSERT INTO public.cargo(
      nombre, responsabilidades, fk_parent_cargo_id, fk_area_id)
      VALUES ('Administration Manager', '', 3, 3),
             ('Accounting and Costs Manager', '', 3, 4),
             ('Legal Adviser', '', 3, 2),
             ('Financial Coordinator', '', 3, 2),
             ('Financial Analyst', '', 12, 3),
             ('Administrative Assistant', '', 12, 3),
             ('Motorized Messenger', '', 12, 3),
             ('Accounting Analyst', '', 13, 4),
             ('Human Talent Specialist', '', 11, 5),
             ('Executive Assistant', '', 11, 5),
             ('Cleaning Operator', '', 11, 16);`);

    // Insert Factory - Development
    await queryRunner.query(`INSERT INTO public.cargo(
      nombre, responsabilidades, fk_parent_cargo_id, fk_area_id)
      VALUES  ('Development Manager', 'Supervisar el cumplimiento de los proyectos', 4, 6),
              ('Senior Developer', 'Desarrollar en las tecnologías asignadas', 23, 6),
              ('Semisenior Developer', 'Desarrollar en las tecnologías asignadas', 23, 6),
              ('Junior Developer', 'Desarrollar en las tecnologías asignadas', 23, 6),
              ('Trainee Developer', 'Desarrollar en las tecnologías asignadas', 23, 6);`);

    // Insert Factory - Ux Product
    await queryRunner.query(`INSERT INTO public.cargo(
    nombre, responsabilidades, fk_parent_cargo_id, fk_area_id)
    VALUES  ('UX Product Manager', 'Supervisar el cumplimiento de los proyectos', 4, 7),
            ('Senior UI Designer', '', 28, 7),
            ('Junior UI Designer', '', 28, 7),
            ('Senior UX Designer', '', 28, 7),
            ('Junior UX Designer', '', 28, 7),
            ('Wordpress Specialist', '', 28, 7);`);

    // Insert Factory - Advertising
    await queryRunner.query(`INSERT INTO public.cargo(
    nombre, responsabilidades, fk_parent_cargo_id, fk_area_id)
    VALUES  ('Digital Advertising Manager', 'Supervisar el cumplimiento de los proyectos', 4, 8),
            ('Digital Advertising Lider', '', 35, 8),
            ('Senior Graphic Designer', '', 35, 8),
            ('Junior Graphic Designer', '', 35, 8),
            ('Digital Advertising Coordinator', '', 35, 8),
            ('Web Platform Coordinator', '', 35, 8);`);

    // Insert Factory - QA
    await queryRunner.query(`INSERT INTO public.cargo(
    nombre, responsabilidades, fk_parent_cargo_id, fk_area_id)
    VALUES  ('QA & Process Manager', 'Supervisar el cumplimiento de los proyectos', 4, 9),
            ('QA Specialist', '', 41, 9),
            ('Process Specialist', '', 41, 9);`);

    // Insert Factory - Project - Scrum
    await queryRunner.query(`INSERT INTO public.cargo(
      nombre, responsabilidades, fk_parent_cargo_id, fk_area_id)
      VALUES  ('Scrum Master', 'Supervisar el cumplimiento de los proyectos', 4, 10),
              ('Junior Scrum Master', '', 44, 10);`);

    // Insert Factory - IT
    await queryRunner.query(`INSERT INTO public.cargo(
      nombre, responsabilidades, fk_parent_cargo_id, fk_area_id)
      VALUES  ('Information Technology (IT) Manager', 'Supervisar el cumplimiento de los proyectos', 4, 11),
              ('IT Operations Manager', '', 46, 11),
              ('Architect of Solutions', '', 46, 11),
              ('Devops Engineer', '', 46, 11),
              ('IT Operations Specialist', '', 46, 11);`);

    // Insert Ekiipago
    await queryRunner.query(`INSERT INTO public.cargo(
      nombre, responsabilidades, fk_parent_cargo_id, fk_area_id)
      VALUES  ('Growth Manager', 'Supervisar el cumplimiento de los proyectos', 8, 12);`);

    // Insert Sales
    await queryRunner.query(`INSERT INTO public.cargo(
      nombre, responsabilidades, fk_parent_cargo_id, fk_area_id)
      VALUES  ('Sales Administrator', '', 9, 13),
              ('Trade marketing', '', 9, 13),
              ('Sales Administrative Assistant', '', 9, 13),
              ('Commercial Advisor', '', 9, 13),
              ('Delivery', '', 9, 13),
              ('Cargo Driver', '', 9, 13);`);

    // Insert Management 14
    await queryRunner.query(`INSERT INTO public.cargo(
      nombre, responsabilidades, fk_parent_cargo_id, fk_area_id)
      VALUES  ('Product Management Specialist', '', 4, 14),
              ('Trainee Product Management', '', 4, 14);`);

    // Insert Production 16
    await queryRunner.query(`INSERT INTO public.cargo(
      nombre, responsabilidades, fk_parent_cargo_id, fk_area_id)
      VALUES  ('Gerente de Planta', '', 4, 16),
              ('Maestro Cervecero', '', 59, 15),
              ('Supervisor de Mantenimiento', '', 59, 16),
              ('Operario de Llenadora', '', 59, 16),
              ('Supervisor de Envasado', '', 59, 16),
              ('Auxiliar de Llenadora', '', 59, 16),
              ('Oficial de Seguridad', '', 59, 16),
              ('Ayudante de Maestro Cervecero', '', 59, 16);`);

    // Insert Personal A. Riveroll 19
    await queryRunner.query(`INSERT INTO public.cargo(
      nombre, responsabilidades, fk_parent_cargo_id, fk_area_id)
      VALUES  ('Nurse', '', 1, 19),
              ('Driver', '', 1, 19);`);

    // Insert Business Intelligence 18
    await queryRunner.query(`INSERT INTO public.cargo(
      nombre, responsabilidades, fk_parent_cargo_id, fk_area_id)
      VALUES  ('Business Developer', '', 4, 19);`);

    // Insert Administration
    await queryRunner.query(`INSERT INTO public.cargo(
      nombre, responsabilidades, fk_parent_cargo_id, fk_area_id)
      VALUES  ('Treasury Analyst', '', 12, 3);`);

    // Insert Marketing
    await queryRunner.query(`INSERT INTO public.cargo(
      nombre, responsabilidades, fk_parent_cargo_id, fk_area_id)
      VALUES  ('Trainee Digital Advertising', '', 34, 8),
              ('Trainee Finance', '', 13, 8);`);

    await queryRunner.query(`INSERT INTO public.rol(
                nombre)
                VALUES ('SUPER_ADMIN'),
                       ('ADMIN_TH'),
                       ('ADMIN_SUPERVISOR'),
                       ('USER');`);

    await queryRunner.query(`INSERT INTO public.persona(genero,primer_nombre,primer_apellido,cedula,fecha_nacimiento,fk_empresa_id,fk_rol_id,fecha_ingreso,email,pais,estado,direccion,password) VALUES
    ('masculino','ALFONSO','RIVEROLL','9882381','5/1/1970',4,1,'3/16/2000','ariveroll@conectium.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('femenino','DALIA','VETENCOURT','10795819','2/14/1971',3,4,'11/21/2016','dalia@conectium.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('masculino','ANGEL','SANCHEZ','6265558','10/30/1965',4,4,'7/17/2000','asanchez@conectium.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('masculino','RONALD','MARCANO','12410513','3/20/1974',4,3,'2/25/2004','rmarcano@conectium.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('femenino','MARY','GUTIERREZ','14365665','3/13/1979',4,3,'2/10/2005','mgutierrez@conectium.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('femenino','HEYDI','RIVAS','13711358','6/25/1979',4,3,'1/17/2006','hrivas@conectium.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('femenino','YADIRA','CAMERO','9918613','5/15/1969',4,4,'10/17/2006','ycamero@conectium.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('masculino','CARLOS','MENGELLE','13412794','6/3/1977',4,3,'4/9/2012','cmengelle@conectium.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('femenino','MARIA','BARRIOS','22025918','3/29/1994',4,4,'5/15/2014','mbarrios@conectium.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('femenino','YSNELDA','OSPINO','15947231','1/12/1984',4,4,'5/18/2015','yospino@conectium.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('masculino','RAFAEL','ROLINGSON','9968734','8/17/1970',4,4,'7/1/2015','rrolingson@conectium.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('masculino','LUIS','SALAZAR','17482827','6/17/1987',4,3,'3/28/2016','lsalazar@conectium.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('masculino','ALEJANDRO','VELAZCO','3982204','10/4/1953',2,4,'6/1/2016','avelazco@peregrinabirra.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('femenino','INES','PULIDO','10335587','10/4/1970',4,1,'6/16/2016','ipulido@conectium.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('femenino','ODALIS','PENA','14494497','8/4/1980',4,2,'7/11/2016','opena@conectium.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('femenino','YASMIN','LOPEZ','13419905','4/8/1976',4,4,'7/20/2016','ylopez@conectium.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('masculino','HOMMI','CORNIVEL','15713548','8/21/1982',2,4,'8/15/2016','hcornivel@peregrinabirra.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('masculino','DAVID','NESBIT','19014900','1/22/1990',2,4,'8/24/2016','dnesbit@peregrinabirra.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('masculino','MIGUEL','PARRA','24041039','7/29/1993',4,4,'10/31/2016','mparra@transamovil.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('femenino','VICTORIA','ALVAREZ','17148440','4/21/1987',4,4,'12/12/2016','valvarez@conectium.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('femenino','MERCEDES','BRANDY','4576646','7/26/1955',4,3,'3/1/2017','mbrandy@conectium.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('masculino','ALEXIS','JRODRIGUEZ','12065041','8/25/1974',4,4,'5/16/2017','ajrodriguez@conectium.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('masculino','JUAN','ZERPA','18588829','2/3/1987',4,4,'8/14/2017','jzerpa@transamovil.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('femenino','ANDREA','RIJANA','25234130','12/29/1994',1,4,'10/20/2017','arijana@transamovil.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('masculino','RICARDO','FERNANDES','24367233','1/28/1995',1,4,'10/20/2017','rfernandes@transamovil.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('masculino','CARLOS','ALONZO','13613542','10/30/1978',3,3,'1/1/2018','calonzo@conectium.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('femenino','MILAGROS','HIDALGO','23625718','1/14/1992',3,4,'1/15/2018','mhidalgo@conectium.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('femenino','ROSA','RUIZ','16030662','6/11/1984',3,4,'1/15/2018','rruiz@sinapsishealth.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('femenino','DIANA','ANAMARIMA','20,914,209','4/25/1994',3,4,'2/5/2018','danamarima@conectium.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('femenino','IRMAR','BENITEZ','17,051,174','8/30/1985',2,4,'2/5/2018','ibenitez@peregrinabirra.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('masculino','JOSE','PINTO','13,873,685','1/4/1980',3,4,'2/7/2018','jpinto@conectium.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('masculino','RICHARD','MUÑOZ','12,000,551','12/19/1970',2,4,'3/2/2018','rmunoz@peregrinabirra.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('femenino','FABIANA','ALCOCER','26,223,997','10/15/1997',3,4,'3/5/2018','falcocer@conectium.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('femenino','LINDA','MORANDI','8,773,101','12/10/1969',3,2,'3/5/2018','lmorandi@conectium.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('masculino','MARK','RODRIGUEZ','20,364,652','4/16/1992',3,4,'3/23/2018','mrodriguez@conectium.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('femenino','CYNTHIA','DELGADO','14,755,287','8/25/1980',2,4,'4/1/2018','cdelgado@transamovil.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('masculino','GIOVANNY','CARDENAS','14,942,202','7/14/1976',3,4,'4/2/2018','gcardenas@conectium.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('femenino','NAXELY','OSUNA','18,933,359','10/28/1989',3,4,'4/3/2018','nosuna@conectium.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('masculino','DAVID','VASS','14,868,531','11/5/1980',2,4,'5/9/2018','dvass@peregrinabirra.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('femenino','ORIANA','TORRES','19,314,290','5/4/1990',3,4,'5/22/2018','otorres@conectium.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('femenino','JUDITH','VALECILLOS','12,835,119','3/26/1977',2,4,'11/5/2018','jvalecillos@peregrinabirra.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('femenino','IRLUMAR','BENITEZ','17,051,175','8/30/1985',2,4,'12/1/2018','irlumarbenitez@gmail.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('femenino','JESIKA','FARFAN','21,413,460','10/4/1994',3,4,'3/18/2019','jfarfan@conectium.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('masculino','JESUS','GONZALEZ','25,252,739','9/3/1996',3,4,'4/29/2019','jgonzalez@conectium.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('femenino','ANAILEB','CHACON','20,801,435','12/21/1992',3,4,'4/29/2019','achacon@conectium.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('masculino','GABRIEL','RAMIREZ','14,216,427','5/4/1981',2,4,'5/14/2019','gramirez@peregrinabirra.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('femenino','MILAGROS','RAMIREZ','11,939,453','9/24/1973',3,4,'5/27/2019','mramirez@conectium.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('femenino','FABIANA','ACOSTA','19,557,485','3/18/1985',2,3,'6/27/2019','facosta@peregrinabirra.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('femenino','KRISMAR','PEREZ','21,117,883','3/4/1991',3,4,'7/1/2019','kperez@conectium.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('masculino','JUAN','DA SILVA','10,501,632','9/28/1969',2,3,'7/8/2019','jdasilva@peregrinabirra.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('masculino','STEVEN','DAVILA','26,194,049','4/28/1998',1,4,'7/22/2019','sdavila@conectium.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('masculino','PABLO','TERAN','28,525,446','9/12/1998',1,4,'9/9/2019','pteran@conectium.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('masculino','FELICIANO','GONZALEZ','27,624,764','2/21/2001',1,4,'9/17/2019','fgonzalez@conectium.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('masculino','FREDDY','TORRES','18,935,621','7/3/1988',4,4,'10/7/2019','davidtorresfm@gmail.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('masculino','WOLFGANG','CACERES','6,027,766','5/12/1959',4,4,'10/7/2019','wolfgang.a.caceres@hotmail.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('masculino','PEDRO','BRICEÑO','14,599,316','8/24/1977',4,4,'10/7/2019','pbriceño@conectium.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('masculino','LAWRENCE','DOW','21,804,005','3/16/1992',3,4,'10/16/2019','ldow@conectium.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('masculino','EDGAR','OCHOA','6,867,222','11/4/1967',2,4,'11/1/2019','eochoa@peregrinabirra.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('masculino','WILMAN','BENITEZ','14,389,957','7/15/1980',2,4,'11/1/2019','wbenitez@peregrinabirra.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('femenino','ROSA','FLORES','6,603,677','8/2/1977',3,4,'11/7/2019','rflores@conectium.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('femenino','KATIUSKA','ROJAS','6,504,712','5/4/1967',3,4,'11/11/2019','krojas@conectium.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('masculino','DARWIN','TINEO','27,040,144','5/23/1999',2,4,'1/20/2020','dtineo@peregrinabirra.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('masculino','EDGAR','MUÑOZ','13,520,268','1/19/1979',2,4,'1/22/2020','emunoz@peregrinabirra.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('masculino','RICHARD','SOTELDO','12,140,792','2/6/1974',2,4,'2/17/2020','rsoteldo@peregrinabirra.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('femenino','PAOLA','OUTEDA','24,722,596','8/9/1995',1,4,'3/4/2020','pouteda@conectium.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('femenino','PAOLA','GARCIA','27,647,924','6/19/1999',1,4,'8/17/2020','paogarcia@conectium.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('masculino','TOMAS','BATISTA','17,563,504','1/29/1987',3,4,'1/1/2021','tbatista@conectium.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('femenino','GRACE','ESCOBAR','10,800,330','9/10/1971',3,3,'1/13/2021','gescobar@conectium.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('femenino','SAIDMAR','CONTRERAS','21,116,978','7/19/1994',3,3,'2/17/2021','scontreras@conectium.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('masculino','JESUS','COELLO','12,485,330','5/3/1977',3,4,'2/25/2021','jcoello@sinapsishealth.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('masculino','ANDRY','RIVAS','16,331,599','6/14/1984',3,3,'3/1/2021','arivas@conectium.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322')
   ,('masculino','EDUARD','AGREDA','12,278,393','12/26/1974',2,4,'3/26/2021','eduard.agreda@gmail.com','Venezuela','Miranda','La Trinidad','ccb2036e25f8590fec7cdfbb5269406f8267f322');        
   `);

    await queryRunner.query(`INSERT INTO public.hist_cargo(fk_persona_id,fk_cargo_id,fecha_inicio) VALUES
    (1,1,'3/16/2000')
   ,(2,2,'11/21/2016')
   ,(3,24,'7/17/2000')
   ,(4,46,'2/25/2004')
   ,(5,23,'2/10/2005')
   ,(6,57,'1/17/2006')
   ,(7,10,'10/17/2006')
   ,(8,28,'4/9/2012')
   ,(9,19,'5/15/2014')
   ,(10,70,'5/18/2015')
   ,(11,3,'7/1/2015')
   ,(12,43,'3/28/2016')
   ,(13,60,'6/1/2016')
   ,(14,11,'6/16/2016')
   ,(15,20,'7/11/2016')
   ,(16,22,'7/20/2016')
   ,(17,59,'8/15/2016')
   ,(18,61,'8/24/2016')
   ,(19,48,'10/31/2016')
   ,(20,12,'12/12/2016')
   ,(21,40,'3/1/2017')
   ,(22,49,'5/16/2017')
   ,(23,7,'8/14/2017')
   ,(24,41,'10/20/2017')
   ,(25,25,'10/20/2017')
   ,(26,45,'1/1/2018')
   ,(27,14,'1/15/2018')
   ,(28,5,'1/15/2018')
   ,(29,35,'2/5/2018')
   ,(30,17,'2/5/2018')
   ,(31,47,'2/7/2018')
   ,(32,62,'3/2/2018')
   ,(33,30,'3/5/2018')
   ,(34,20,'3/5/2018')
   ,(35,41,'3/23/2018')
   ,(36,53,'4/1/2018')
   ,(37,18,'4/2/2018')
   ,(38,29,'4/3/2018')
   ,(39,63,'5/9/2018')
   ,(40,36,'5/22/2018')
   ,(41,51,'11/5/2018')
   ,(42,22,'12/1/2018')
   ,(43,41,'3/18/2019')
   ,(44,37,'4/29/2019')
   ,(45,57,'4/29/2019')
   ,(46,64,'5/14/2019')
   ,(47,21,'5/27/2019')
   ,(48,13,'6/27/2019')
   ,(49,31,'7/1/2019')
   ,(50,9,'7/8/2019')
   ,(51,27,'7/22/2019')
   ,(52,26,'9/9/2019')
   ,(53,71,'9/17/2019')
   ,(54,67,'10/7/2019')
   ,(55,67,'10/7/2019')
   ,(56,68,'10/7/2019')
   ,(57,69,'10/16/2019')
   ,(58,54,'11/1/2019')
   ,(59,54,'11/1/2019')
   ,(60,26,'11/7/2019')
   ,(61,4,'11/11/2019')
   ,(62,65,'1/20/2020')
   ,(63,65,'1/22/2020')
   ,(64,65,'2/17/2020')
   ,(65,27,'3/4/2020')
   ,(66,58,'8/17/2020')
   ,(67,25,'1/1/2021')
   ,(68,8,'1/13/2021')
   ,(69,50,'2/17/2021')
   ,(70,6,'2/25/2021')
   ,(71,50,'3/1/2021')
   ,(72,66,'3/26/2021');   
   `);

    await queryRunner.query(`INSERT INTO public.solicitud(
        "requiere_aprobacion", descripcion)
        VALUES ('0', 'Prestaciones'),
               ('1', 'Vacaciones'),
               ('1', 'Ausencias por días'),
               ('1', 'Ausencias por horas');`);

    await queryRunner.query(`INSERT INTO public.telefono_persona(fk_persona_id,numero) VALUES
    (1,'+1 (786) 295-0088')
   ,(2,'+1 (954) 505-6756')
   ,(3,'+58 424-1782985')
   ,(4,'+58 412-2359005')
   ,(5,'+58 412-2530701')
   ,(6,'+58 412-2820598')
   ,(7,'+58 414-3142267')
   ,(8,'+58 412-2670737')
   ,(9,'+58 412-2530697')
   ,(10,'+58 412-3136282')
   ,(11,'+58 412-2356550')
   ,(12,'+58 424-1310808')
   ,(13,'+58 412-6472904')
   ,(14,'+58 412-7264158')
   ,(15,'+58 414-1032601')
   ,(16,'+58 412-3134879')
   ,(17,'+58 424-3790270')
   ,(18,'+58 412-9544742')
   ,(19,'+58 424-2098786')
   ,(20,'+58 412-6248349')
   ,(21,'+58 414-1392663')
   ,(22,'+58 412-5911900')
   ,(23,'+58 412-0124903')
   ,(24,'+58 412-9702940')
   ,(25,'+58 414-3694118')
   ,(26,'+58 414-2613408')
   ,(27,'+58 412-9543901')
   ,(28,'+58 414-1065624')
   ,(29,'+58 424-1930203')
   ,(30,'+58 424-3348031')
   ,(31,'+58 412-2530700')
   ,(32,'+58 412-0335344')
   ,(33,'+58 424-2386921')
   ,(34,'+58 424-2388394')
   ,(35,'+58 412-0289944')
   ,(36,'+58 412-2690459')
   ,(37,'+58 424-1163136')
   ,(38,'+58 412-6144469')
   ,(39,'+58 412-6339864')
   ,(40,'+58 424-1850919')
   ,(41,'+58 414-1521039')
   ,(42,'+58 414-4896106')
   ,(43,'+58 416-4118398')
   ,(44,'+58 424-1856733')
   ,(45,'+58 414-2665444')
   ,(46,'+58 412-5637625')
   ,(47,'+58 414-2326177')
   ,(48,'+58 424-2017978')
   ,(49,'+58 414-2223812')
   ,(50,'+58 412-3120505')
   ,(51,'+58 414-2819076')
   ,(52,'+58 424-2639940')
   ,(53,'+58 412-3212139')
   ,(54,'+58 424-2398832')
   ,(55,'+58 424-2366878')
   ,(56,'+58 424-2403618')
   ,(57,'+58 414-9024410')
   ,(58,'+58 414-6663735')
   ,(59,'+58 414-3474858')
   ,(60,'+58 414-3138411')
   ,(61,'+58 414-2609954')
   ,(63,'+58 412-4241477')
   ,(65,'+58 412-2930321')
   ,(66,'+58 412-2578938')
   ,(67,'+58 424-2224734')
   ,(68,'+58 414-2556447')
   ,(69,'+58 424-2422027')
   ,(70,'+58 412-7001481')
   ,(71,'+58 412-9746924')
   ,(72,'+58 414-2266612');
   `);

    await queryRunner.query(`INSERT INTO public.curso(
      nombre, institucion, ciudad, pais, certificado, fecha_inicio, fecha_fin, fk_persona_id)
      VALUES ('Data Mining', 'Universidad Metropolitana', 'Caracas', 'Venezuela', 'sin certificado', '10/08/2018', '10/08/2019', 5);`);

    await queryRunner.query(`INSERT INTO public.tipo_documento_personal(
        nombre, requerido)
        VALUES ('Curriculum', '1'),
               ('Titulo', '1'),
               ('Cédula', '1'),
               ('Rif', '1'),
               ('Segundo Título', '0');`);

    await queryRunner.query(`INSERT INTO public.documento_personal(
      fk_persona_id, fk_tipo_doc_personal_id)
      (SELECT id, 1 FROM persona);`);

    await queryRunner.query(`INSERT INTO public.documento_personal(
      fk_persona_id, fk_tipo_doc_personal_id)
      (SELECT id, 2 FROM persona);`);

    await queryRunner.query(`INSERT INTO public.documento_personal(
      fk_persona_id, fk_tipo_doc_personal_id)
      (SELECT id, 3 FROM persona);`);

    await queryRunner.query(`INSERT INTO public.documento_personal(
      fk_persona_id, fk_tipo_doc_personal_id)
      (SELECT id, 4 FROM persona);`);

    await queryRunner.query(`INSERT INTO public.motivo_ausencia(
              nombre)
              VALUES ('Cita médica (Empleado)'),
                      ('Cita medica (Hijo(a)/Padres)'),
                      ('Trámite Emisión/Retiro Cédula'),
                      ('Trámite Emisión/Retiro Pasaporte'),
                      ('Reunión Escolar'),
                      ('Trámites Universitarios'),
                      ('Diligencia Personal'),
                      ('Licencia por Matrimonio'),
                      ('Fallecimiento familiar directo'),
                      ('Fallecimiento familiar indirecto');`);

    await queryRunner.query(`INSERT INTO public.tipo_descuento(
                    nombre)
                    VALUES ('A cuenta de vacaciones'),
                            ('No aplica'),
                        ('No remunerado');`);

    // Insert de Ausencias por dia
    await queryRunner.query(`INSERT INTO public.ausencia(
      fk_solicitud_id, fk_motivo_ausencia_id, fk_tipo_descuento_id)
      VALUES (3, 7, 1),
             (3, 7, 2),
           (3, 7, 3),
           (3, 8, 2),
           (3, 9, 2),
           (3, 10, 2);`);

    // Insert de Ausencias por hora
    await queryRunner.query(`INSERT INTO public.ausencia(
      fk_solicitud_id, fk_motivo_ausencia_id, fk_tipo_descuento_id)
      VALUES (4, 1, 2),
             (4, 2, 2),
           (4, 3, 2),
           (4, 4, 2),
           (4, 5, 2),
           (4, 6, 2),
           (4, 7, 2);`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {}
}
