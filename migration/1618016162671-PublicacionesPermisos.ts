import {MigrationInterface, QueryRunner} from "typeorm";

export class PublicacionesPermisos1618016162671 implements MigrationInterface {
    name = 'PublicacionesPermisos1618016162671'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "publicacion" DROP CONSTRAINT "FK_f63abd3196726b8109beaa121d9"`);
        await queryRunner.query(`ALTER TABLE "publicacion" DROP COLUMN "publicanteId"`);
        await queryRunner.query(`CREATE TYPE "usuario_permiso_tipopermiso_enum" AS ENUM('R', 'WR', 'N')`);
        await queryRunner.query(`ALTER TABLE "usuario_permiso" ADD "tipoPermiso" "usuario_permiso_tipopermiso_enum" NOT NULL DEFAULT 'N'`);
        await queryRunner.query(`ALTER TABLE "publicacion" ADD "ubicacion" character varying NOT NULL`);
        await queryRunner.query(`ALTER TABLE "publicacion" ADD "nombreObjeto" character varying NOT NULL`);
        await queryRunner.query(`CREATE TYPE "publicacion_tipoobjeto_enum" AS ENUM('A', 'C')`);
        await queryRunner.query(`ALTER TABLE "publicacion" ADD "tipoObjeto" "publicacion_tipoobjeto_enum" NOT NULL`);
        await queryRunner.query(`ALTER TABLE "publicacion" ADD "fecha_publicacion" TIMESTAMP NOT NULL DEFAULT now()`);
        await queryRunner.query(`ALTER TABLE "publicacion" ADD "actualizacion" character varying NOT NULL`);
        await queryRunner.query(`ALTER TABLE "publicacion" ADD "fk_publicante_id" integer NOT NULL`);
        await queryRunner.query(`COMMENT ON COLUMN "empresa"."mision" IS NULL`);
        await queryRunner.query(`ALTER TABLE "empresa" ALTER COLUMN "mision" SET DEFAULT null`);
        await queryRunner.query(`COMMENT ON COLUMN "empresa"."vision" IS NULL`);
        await queryRunner.query(`ALTER TABLE "empresa" ALTER COLUMN "vision" SET DEFAULT null`);
        await queryRunner.query(`COMMENT ON COLUMN "curso"."certificado" IS NULL`);
        await queryRunner.query(`ALTER TABLE "curso" ALTER COLUMN "certificado" SET DEFAULT null`);
        await queryRunner.query(`COMMENT ON COLUMN "documento_personal"."archivo" IS NULL`);
        await queryRunner.query(`ALTER TABLE "documento_personal" ALTER COLUMN "archivo" SET DEFAULT null`);
        await queryRunner.query(`COMMENT ON COLUMN "hist_ausencias"."fechaRevision" IS NULL`);
        await queryRunner.query(`ALTER TABLE "hist_ausencias" ALTER COLUMN "fechaRevision" SET DEFAULT null`);
        await queryRunner.query(`COMMENT ON COLUMN "hist_ausencias"."motivo_rechazo" IS NULL`);
        await queryRunner.query(`ALTER TABLE "hist_ausencias" ALTER COLUMN "motivo_rechazo" SET DEFAULT null`);
        await queryRunner.query(`COMMENT ON COLUMN "hist_ausencias"."firma_solicitante" IS NULL`);
        await queryRunner.query(`ALTER TABLE "hist_ausencias" ALTER COLUMN "firma_solicitante" SET DEFAULT null`);
        await queryRunner.query(`COMMENT ON COLUMN "hist_ausencias"."firma_th" IS NULL`);
        await queryRunner.query(`ALTER TABLE "hist_ausencias" ALTER COLUMN "firma_th" SET DEFAULT null`);
        await queryRunner.query(`COMMENT ON COLUMN "hist_ausencias"."firma_supervisor" IS NULL`);
        await queryRunner.query(`ALTER TABLE "hist_ausencias" ALTER COLUMN "firma_supervisor" SET DEFAULT null`);
        await queryRunner.query(`COMMENT ON COLUMN "hist_vacaciones"."fecha_revision" IS NULL`);
        await queryRunner.query(`ALTER TABLE "hist_vacaciones" ALTER COLUMN "fecha_revision" SET DEFAULT null`);
        await queryRunner.query(`COMMENT ON COLUMN "hist_vacaciones"."motivo_rechazo" IS NULL`);
        await queryRunner.query(`ALTER TABLE "hist_vacaciones" ALTER COLUMN "motivo_rechazo" SET DEFAULT null`);
        await queryRunner.query(`COMMENT ON COLUMN "hist_vacaciones"."firma_solicitante" IS NULL`);
        await queryRunner.query(`ALTER TABLE "hist_vacaciones" ALTER COLUMN "firma_solicitante" SET DEFAULT null`);
        await queryRunner.query(`COMMENT ON COLUMN "hist_vacaciones"."fecha_pago" IS NULL`);
        await queryRunner.query(`ALTER TABLE "hist_vacaciones" ALTER COLUMN "fecha_pago" SET DEFAULT null`);
        await queryRunner.query(`COMMENT ON COLUMN "hist_vacaciones"."firma_th" IS NULL`);
        await queryRunner.query(`ALTER TABLE "hist_vacaciones" ALTER COLUMN "firma_th" SET DEFAULT null`);
        await queryRunner.query(`COMMENT ON COLUMN "hist_vacaciones"."firma_supervisor" IS NULL`);
        await queryRunner.query(`ALTER TABLE "hist_vacaciones" ALTER COLUMN "firma_supervisor" SET DEFAULT null`);
        await queryRunner.query(`COMMENT ON COLUMN "hist_prestaciones"."fechaRevision" IS NULL`);
        await queryRunner.query(`ALTER TABLE "hist_prestaciones" ALTER COLUMN "fechaRevision" SET DEFAULT null`);
        await queryRunner.query(`COMMENT ON COLUMN "hist_prestaciones"."fecha_pago" IS NULL`);
        await queryRunner.query(`ALTER TABLE "hist_prestaciones" ALTER COLUMN "fecha_pago" SET DEFAULT null`);
        await queryRunner.query(`COMMENT ON COLUMN "hist_prestaciones"."motivo_rechazo" IS NULL`);
        await queryRunner.query(`ALTER TABLE "hist_prestaciones" ALTER COLUMN "motivo_rechazo" SET DEFAULT null`);
        await queryRunner.query(`COMMENT ON COLUMN "hist_prestaciones"."firma_solicitante" IS NULL`);
        await queryRunner.query(`ALTER TABLE "hist_prestaciones" ALTER COLUMN "firma_solicitante" SET DEFAULT null`);
        await queryRunner.query(`COMMENT ON COLUMN "hist_prestaciones"."presupuesto" IS NULL`);
        await queryRunner.query(`ALTER TABLE "hist_prestaciones" ALTER COLUMN "presupuesto" SET DEFAULT null`);
        await queryRunner.query(`COMMENT ON COLUMN "hist_prestaciones"."firma_th" IS NULL`);
        await queryRunner.query(`ALTER TABLE "hist_prestaciones" ALTER COLUMN "firma_th" SET DEFAULT null`);
        await queryRunner.query(`COMMENT ON COLUMN "hist_prestaciones"."monto_aprobado" IS NULL`);
        await queryRunner.query(`ALTER TABLE "hist_prestaciones" ALTER COLUMN "monto_aprobado" SET DEFAULT null`);
        await queryRunner.query(`COMMENT ON COLUMN "monto_prestaciones"."fecha_inicio" IS NULL`);
        await queryRunner.query(`ALTER TABLE "monto_prestaciones" ALTER COLUMN "fecha_inicio" SET DEFAULT null`);
        await queryRunner.query(`COMMENT ON COLUMN "monto_prestaciones"."fecha_fin" IS NULL`);
        await queryRunner.query(`ALTER TABLE "monto_prestaciones" ALTER COLUMN "fecha_fin" SET DEFAULT null`);
        await queryRunner.query(`COMMENT ON COLUMN "monto_prestaciones"."monto_disponible" IS NULL`);
        await queryRunner.query(`ALTER TABLE "monto_prestaciones" ALTER COLUMN "monto_disponible" SET DEFAULT null`);
        await queryRunner.query(`COMMENT ON COLUMN "hist_cargo"."fecha_fin" IS NULL`);
        await queryRunner.query(`ALTER TABLE "hist_cargo" ALTER COLUMN "fecha_fin" SET DEFAULT null`);
        await queryRunner.query(`ALTER TABLE "publicacion" ADD CONSTRAINT "FK_37ae5298c28dca5817306b1efe5" FOREIGN KEY ("fk_publicante_id") REFERENCES "persona"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "publicacion" DROP CONSTRAINT "FK_37ae5298c28dca5817306b1efe5"`);
        await queryRunner.query(`ALTER TABLE "hist_cargo" ALTER COLUMN "fecha_fin" DROP DEFAULT`);
        await queryRunner.query(`COMMENT ON COLUMN "hist_cargo"."fecha_fin" IS NULL`);
        await queryRunner.query(`ALTER TABLE "monto_prestaciones" ALTER COLUMN "monto_disponible" DROP DEFAULT`);
        await queryRunner.query(`COMMENT ON COLUMN "monto_prestaciones"."monto_disponible" IS NULL`);
        await queryRunner.query(`ALTER TABLE "monto_prestaciones" ALTER COLUMN "fecha_fin" DROP DEFAULT`);
        await queryRunner.query(`COMMENT ON COLUMN "monto_prestaciones"."fecha_fin" IS NULL`);
        await queryRunner.query(`ALTER TABLE "monto_prestaciones" ALTER COLUMN "fecha_inicio" DROP DEFAULT`);
        await queryRunner.query(`COMMENT ON COLUMN "monto_prestaciones"."fecha_inicio" IS NULL`);
        await queryRunner.query(`ALTER TABLE "hist_prestaciones" ALTER COLUMN "monto_aprobado" DROP DEFAULT`);
        await queryRunner.query(`COMMENT ON COLUMN "hist_prestaciones"."monto_aprobado" IS NULL`);
        await queryRunner.query(`ALTER TABLE "hist_prestaciones" ALTER COLUMN "firma_th" DROP DEFAULT`);
        await queryRunner.query(`COMMENT ON COLUMN "hist_prestaciones"."firma_th" IS NULL`);
        await queryRunner.query(`ALTER TABLE "hist_prestaciones" ALTER COLUMN "presupuesto" DROP DEFAULT`);
        await queryRunner.query(`COMMENT ON COLUMN "hist_prestaciones"."presupuesto" IS NULL`);
        await queryRunner.query(`ALTER TABLE "hist_prestaciones" ALTER COLUMN "firma_solicitante" DROP DEFAULT`);
        await queryRunner.query(`COMMENT ON COLUMN "hist_prestaciones"."firma_solicitante" IS NULL`);
        await queryRunner.query(`ALTER TABLE "hist_prestaciones" ALTER COLUMN "motivo_rechazo" DROP DEFAULT`);
        await queryRunner.query(`COMMENT ON COLUMN "hist_prestaciones"."motivo_rechazo" IS NULL`);
        await queryRunner.query(`ALTER TABLE "hist_prestaciones" ALTER COLUMN "fecha_pago" DROP DEFAULT`);
        await queryRunner.query(`COMMENT ON COLUMN "hist_prestaciones"."fecha_pago" IS NULL`);
        await queryRunner.query(`ALTER TABLE "hist_prestaciones" ALTER COLUMN "fechaRevision" DROP DEFAULT`);
        await queryRunner.query(`COMMENT ON COLUMN "hist_prestaciones"."fechaRevision" IS NULL`);
        await queryRunner.query(`ALTER TABLE "hist_vacaciones" ALTER COLUMN "firma_supervisor" DROP DEFAULT`);
        await queryRunner.query(`COMMENT ON COLUMN "hist_vacaciones"."firma_supervisor" IS NULL`);
        await queryRunner.query(`ALTER TABLE "hist_vacaciones" ALTER COLUMN "firma_th" DROP DEFAULT`);
        await queryRunner.query(`COMMENT ON COLUMN "hist_vacaciones"."firma_th" IS NULL`);
        await queryRunner.query(`ALTER TABLE "hist_vacaciones" ALTER COLUMN "fecha_pago" DROP DEFAULT`);
        await queryRunner.query(`COMMENT ON COLUMN "hist_vacaciones"."fecha_pago" IS NULL`);
        await queryRunner.query(`ALTER TABLE "hist_vacaciones" ALTER COLUMN "firma_solicitante" DROP DEFAULT`);
        await queryRunner.query(`COMMENT ON COLUMN "hist_vacaciones"."firma_solicitante" IS NULL`);
        await queryRunner.query(`ALTER TABLE "hist_vacaciones" ALTER COLUMN "motivo_rechazo" DROP DEFAULT`);
        await queryRunner.query(`COMMENT ON COLUMN "hist_vacaciones"."motivo_rechazo" IS NULL`);
        await queryRunner.query(`ALTER TABLE "hist_vacaciones" ALTER COLUMN "fecha_revision" DROP DEFAULT`);
        await queryRunner.query(`COMMENT ON COLUMN "hist_vacaciones"."fecha_revision" IS NULL`);
        await queryRunner.query(`ALTER TABLE "hist_ausencias" ALTER COLUMN "firma_supervisor" DROP DEFAULT`);
        await queryRunner.query(`COMMENT ON COLUMN "hist_ausencias"."firma_supervisor" IS NULL`);
        await queryRunner.query(`ALTER TABLE "hist_ausencias" ALTER COLUMN "firma_th" DROP DEFAULT`);
        await queryRunner.query(`COMMENT ON COLUMN "hist_ausencias"."firma_th" IS NULL`);
        await queryRunner.query(`ALTER TABLE "hist_ausencias" ALTER COLUMN "firma_solicitante" DROP DEFAULT`);
        await queryRunner.query(`COMMENT ON COLUMN "hist_ausencias"."firma_solicitante" IS NULL`);
        await queryRunner.query(`ALTER TABLE "hist_ausencias" ALTER COLUMN "motivo_rechazo" DROP DEFAULT`);
        await queryRunner.query(`COMMENT ON COLUMN "hist_ausencias"."motivo_rechazo" IS NULL`);
        await queryRunner.query(`ALTER TABLE "hist_ausencias" ALTER COLUMN "fechaRevision" DROP DEFAULT`);
        await queryRunner.query(`COMMENT ON COLUMN "hist_ausencias"."fechaRevision" IS NULL`);
        await queryRunner.query(`ALTER TABLE "documento_personal" ALTER COLUMN "archivo" DROP DEFAULT`);
        await queryRunner.query(`COMMENT ON COLUMN "documento_personal"."archivo" IS NULL`);
        await queryRunner.query(`ALTER TABLE "curso" ALTER COLUMN "certificado" DROP DEFAULT`);
        await queryRunner.query(`COMMENT ON COLUMN "curso"."certificado" IS NULL`);
        await queryRunner.query(`ALTER TABLE "empresa" ALTER COLUMN "vision" SET DEFAULT NULL`);
        await queryRunner.query(`COMMENT ON COLUMN "empresa"."vision" IS NULL`);
        await queryRunner.query(`ALTER TABLE "empresa" ALTER COLUMN "mision" SET DEFAULT NULL`);
        await queryRunner.query(`COMMENT ON COLUMN "empresa"."mision" IS NULL`);
        await queryRunner.query(`ALTER TABLE "publicacion" DROP COLUMN "fk_publicante_id"`);
        await queryRunner.query(`ALTER TABLE "publicacion" DROP COLUMN "actualizacion"`);
        await queryRunner.query(`ALTER TABLE "publicacion" DROP COLUMN "fecha_publicacion"`);
        await queryRunner.query(`ALTER TABLE "publicacion" DROP COLUMN "tipoObjeto"`);
        await queryRunner.query(`DROP TYPE "publicacion_tipoobjeto_enum"`);
        await queryRunner.query(`ALTER TABLE "publicacion" DROP COLUMN "nombreObjeto"`);
        await queryRunner.query(`ALTER TABLE "publicacion" DROP COLUMN "ubicacion"`);
        await queryRunner.query(`ALTER TABLE "usuario_permiso" DROP COLUMN "tipoPermiso"`);
        await queryRunner.query(`DROP TYPE "usuario_permiso_tipopermiso_enum"`);
        await queryRunner.query(`ALTER TABLE "publicacion" ADD "publicanteId" integer NOT NULL`);
        await queryRunner.query(`ALTER TABLE "publicacion" ADD CONSTRAINT "FK_f63abd3196726b8109beaa121d9" FOREIGN KEY ("publicanteId") REFERENCES "persona"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
