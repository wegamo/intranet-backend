import { MigrationInterface, QueryRunner } from 'typeorm';

export class ArregloFKPublicacionAreaVertical1630949905628 implements MigrationInterface {
  name = 'ArregloFKPublicacionAreaVertical1630949905628';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "publicacion" ADD "fk_vertical_id" integer`);
    await queryRunner.query(`ALTER TABLE "publicacion" ADD "fk_area_id" integer`);
    await queryRunner.query(
      `ALTER TABLE "publicacion" ADD CONSTRAINT "FK_d126bcbef8947d4714e445208ff" FOREIGN KEY ("fk_vertical_id") REFERENCES "vertical"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "publicacion" ADD CONSTRAINT "FK_97d64915ba34bc0c9ffb34f2b1e" FOREIGN KEY ("fk_area_id") REFERENCES "area"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "publicacion" DROP CONSTRAINT "FK_97d64915ba34bc0c9ffb34f2b1e"`
    );
    await queryRunner.query(
      `ALTER TABLE "publicacion" DROP CONSTRAINT "FK_d126bcbef8947d4714e445208ff"`
    );
    await queryRunner.query(`ALTER TABLE "publicacion" DROP COLUMN "fk_area_id"`);
    await queryRunner.query(`ALTER TABLE "publicacion" DROP COLUMN "fk_vertical_id"`);
  }
}
