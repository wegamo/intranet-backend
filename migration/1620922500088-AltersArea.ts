import {MigrationInterface, QueryRunner} from "typeorm";

export class AltersArea1620922500088 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            UPDATE public.area SET "espacioAws"='documentos/transversales/talento-humano/' 
            WHERE 
            id=5;
        `);
        await queryRunner.query(`
            UPDATE public.area SET "espacioAws"='documentos/transversales/finanzas/' 
            WHERE 
            id=2;
        `);
        await queryRunner.query(`
            UPDATE public.area SET "espacioAws"='documentos/transversales/gestión/' 
            WHERE 
            id=3;
        `);
        await queryRunner.query(`
            UPDATE public.area SET "espacioAws"='documentos/transversales/qa/' 
            WHERE 
            id=9;
        `);
        await queryRunner.query(`
        INSERT INTO public.area(nombre, descripcion,"espacioAws","img")
        VALUES ('Otros documentos','','documentos/transversales/otros-documentos/',null);
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
