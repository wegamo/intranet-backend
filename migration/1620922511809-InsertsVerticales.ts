import {MigrationInterface, QueryRunner} from "typeorm";

export class InsertsVerticales1620922511809 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
        INSERT INTO public.vertical(nombre,descripcion,"espacioAws","img","imgSvg")
        VALUES ('Andromeda','','documentos/verticales/andromeda/',null,null),
        ('Brainbox','','documentos/verticales/brainbox/',null,null),
        ('Cervecentro','','documentos/verticales/cervecentro/',null,null),
        ('Ekiipago','','documentos/verticales/ekiipago/',null,null),
        ('Evoke','','documentos/verticales/evoke/',null,null),
        ('Conectium','','documentos/verticales/conectium/',null,null),
        ('SomosHolaDoc','','documentos/verticales/holadoc/',null,null),
        ('Red Subasta','','documentos/verticales/red-subasta/',null,null);
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
