import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateNotificaciones1620934238463 implements MigrationInterface {
  name = 'CreateNotificaciones1620934238463';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "notificacion" ("id" SERIAL NOT NULL, "mensaje" character varying NOT NULL, "accion" character varying, "tipo" character varying NOT NULL, "fecha_publicacion" TIMESTAMP NOT NULL DEFAULT now(), "fk_persona_id" integer NOT NULL, CONSTRAINT "PK_b4402a54386266ca21a86420f77" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(
      `ALTER TABLE "notificacion" ADD CONSTRAINT "FK_35fe8a6424ac890b324ed6530e1" FOREIGN KEY ("fk_persona_id") REFERENCES "persona"("id") ON DELETE CASCADE ON UPDATE NO ACTION`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "notificacion" DROP CONSTRAINT "FK_35fe8a6424ac890b324ed6530e1"`
    );
    await queryRunner.query(`DROP TABLE "notificacion"`);
  }
}
