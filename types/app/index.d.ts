declare namespace App {
  namespace JWT {
    interface JWTPayload {
      username: string;
      sub: number;
    }
  }
  namespace Auth {
    interface LoginResponse {
      token: string;
      user: Partial<import('../../src/models/persona/persona.entity').Persona>;
    }

    interface Request {
      userId: number;
      username: string;
    }
  }
}
