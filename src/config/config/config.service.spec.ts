import { Test, TestingModule } from '@nestjs/testing';
import * as dotenv from 'dotenv';
import { readFileSync } from 'fs';

import { ConfigService } from './config.service';

jest.mock('fs', () => ({
  readFileSync: jest.fn()
}));
jest.mock('dotenv', () => ({
  parse: jest.fn()
}));

describe('Service: Config', () => {
  const SECRET_KEY = 'SECRET';
  const SECRET_CODE = 'my-secret-code';
  let service: ConfigService;
  let envConfig: Record<string, string>;

  beforeEach(() => {
    (readFileSync as jest.Mock).mockClear();
    (dotenv.parse as jest.Mock).mockClear();
  });

  beforeEach(() => {
    envConfig = { [SECRET_KEY]: SECRET_CODE };
    (dotenv.parse as jest.Mock).mockReturnValue(envConfig);
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ConfigService]
    }).compile();

    service = module.get<ConfigService>(ConfigService);
  });

  describe('constructor(filePath)', () => {
    it('should invoke fs.readFileSync()', () => {
      const testFilePath = '.env';

      expect(readFileSync).toHaveBeenCalledTimes(1);
      expect(readFileSync).toHaveBeenCalledWith(testFilePath);
    });

    it('should invoke dotenv.parse()', () => {
      expect(dotenv.parse).toHaveBeenCalledTimes(1);
    });
  });

  describe('get(key)', () => {
    let key: string;
    let result: string | undefined;

    describe('case: key exist', () => {
      beforeEach(() => {
        key = SECRET_KEY;
        result = service.get(key);
      });

      it('should return the value', () => {
        expect(result).toBe(SECRET_CODE);
      });
    });

    describe('case: key doesnt exist', () => {
      beforeEach(() => {
        key = 'UNKNOWN';
        result = service.get(key);
      });

      it('should return undefined', () => {
        expect(result).toBeUndefined();
      });
    });
  });

  describe('getAll()', () => {
    let result: Record<string, string>;

    beforeEach(() => {
      result = service.getAll();
    });

    it('should return the env config', () => {
      expect(result).toStrictEqual(envConfig);
    });
  });
});
