import { ApiModelProperty } from '@nestjs/swagger';
import { EmpresaAreaEstatus } from '../enums/empresa-area.enum';
export class IAreaType {
    @ApiModelProperty()
    id:number;
    @ApiModelProperty()
    nombre:string;
    @ApiModelProperty({pattern:'**/**/**.png,.jpg'})
    icono:string;
    @ApiModelProperty()
    descripcion:string;
    @ApiModelProperty({enum:EmpresaAreaEstatus})
    estatus:string;
    @ApiModelProperty({pattern:'**/**/**/'})
    espacio:string;
}