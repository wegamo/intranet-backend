import { ApiModelProperty } from "@nestjs/swagger";

export class IVerticalType {
    @ApiModelProperty()
    id:number;
    @ApiModelProperty()
    nombre:string;
    @ApiModelProperty()
    icono:string;
    @ApiModelProperty()
    descripcion:string;
    @ApiModelProperty()
    iconoSvg:string;
    @ApiModelProperty()
    espacio:string;
}