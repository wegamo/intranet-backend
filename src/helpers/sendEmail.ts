import * as nodemailer from 'nodemailer';
import * as aws from 'aws-sdk';
import ConfigService from '../config/config/config.service';

export const sendEmail = async (email: string, subject: string, text: string, html: string) => {
  aws.config.update({
    accessKeyId: ConfigService.get('EMAIL_ACCESS_KEY'),
    secretAccessKey: ConfigService.get('EMAIL_SECRET_ACCESS_KEY'),
    region: ConfigService.get('EMAIL_HOST')
  });

  const transporter = nodemailer.createTransport({
    SES: new aws.SES({
      apiVersion: '2010-12-01'
    })
  });

  // send mail with defined transport object

  try {
    const info = await transporter.sendMail({
      from: `"Andromeda" ${ConfigService.get('EMAIL')}`,
      to: email,
      subject,
      text,
      html
    });
    console.log(`Email sent service:: ${info.messageId}`);
  } catch (error) {
    console.log(`Email sent service error:: ${error}`);
  }
};
