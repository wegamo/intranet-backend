import * as nodemailer from 'nodemailer';

import configService from '../config/config/config.service';

export const sendEmailRecuperar = async (email: string, username: string, password: string) => {
  const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      type: 'OAuth2',
      user: configService.get('USER'),
      clientId: configService.get('CLIENT_ID'),
      clientSecret: configService.get('CLIENT_SECRET'),
      refreshToken: configService.get('REFRESH_TOKEN')
    }
  });

  const mailOptions = {
    from: configService.get('FROM'),
    to: email,
    subject: 'Recuperar Contraseña',
    text: 'Recuperar Contraseña',
    html: `
      <!DOCTYPE html>
        <html lang="en">
          <head>
            <meta charset="UTF-8">    
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta name="format-detection" content="telephone=no">
            <meta name="format-detection" content="email=no">
            <title>Recuperar Contraseña</title>
            <style>
                body {
                  font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,"Noto Sans",sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji"; 
                  font-weight: 400; 
                  line-height: 1.5;
                  color: #212529;
                  text-align: left;
                }

                .root {
                  margin: 0 auto;
                  width: 100%;
                }

                .titulo {
                  margin-bottom: 20px;
                }

                h1 {
                  text-align: center;
                }
            </style>
          </head>
          <body>
            <div class="root">
              <div class="titulo">
                <h1>Recuperar Contraseña</h1>
              </div>
              <p>Hola <b><i>${username}</i></b></p>
              <p>Tu contraseña es <b><i>${password}</i></b></p>
            </div>
          </body>
        </html>`
  };

  transporter.sendMail(mailOptions, (err, res) => {
    if (err) {
      console.log('Error: ', err);
    } else {
      console.log('Email Sent: ', res);
    }
  });
};
