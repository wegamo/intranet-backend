import { Module } from '@nestjs/common';
import { APP_FILTER } from '@nestjs/core';
import { MulterModule } from '@nestjs/platform-express';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ScheduleModule } from '@nestjs/schedule';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import configService from './config/config/config.service';
import { PostgreExceptionFilter } from './exceptions/postgre-exception.filter';
import { AuthModule } from './modules/auth/auth.module';
import { CargoModule } from './modules/cargo/cargo.module';
import { DocumentoPersonalModule } from './modules/documento-personal/documento-personal.module';
import { TipoDocumentoPersonalModule } from './modules/tipo-documento-personal/tipoDocumentoPersonal.module';
import { EmpresaModule } from './modules/empresa/empresa.module';
import { PersonaModule } from './modules/persona/persona.module';
import { RolModule } from './modules/rol/rol.module';
import { Aws3CntmModule } from './modules/aws3-cntm/aws3-cntm.module';
import { NoticiasModule } from './modules/noticia/noticias.module';
import { HistCargosModule } from './modules/hist-cargos/hist-cargos.module';
import { HistPrestacionesModule } from './modules/hist-prestaciones/hist-prestaciones.module';
import { HistAusenciaModule } from './modules/hist-ausencia/hist-ausencia.module';
import { HistVacacionesModule } from './modules/hist-vacaciones/hist-vacaciones.module';
import { CursoModule } from './modules/curso/curso.module';
import { TelefonoPersonaModule } from './modules/telefono-persona/telefono-persona.module';
import { TelefonoEmpresaModule } from './modules/telefono-empresa/telefono-empresa.module';
import { MontoDisponiblePrestacionesModule } from './modules/monto-disponible-prestaciones/monto-disponible-prestaciones.module';
import { SolicitudesModule } from './modules/solicitudes/solicitudes.module';
import { AreaModule } from './modules/area/area.module';
import { AusenciaModule } from './modules/ausencia/ausencia.module';
import { MotivoAusenciaModule } from './modules/motivo-ausencia/motivo-ausencia.module';
import { TipoDescuentoModule } from './modules/tipo-descuento/tipo-descuento.module';
import { PublicacionModule } from './modules/publicacion/publicacion.module';
import { UsuarioPermisoModule } from './modules/usuario-permiso/usuario-permiso.module';
import { VerticalModule } from './modules/vertical/vertical.module';
import { HistDiasDispVacacionesModule } from './modules/hist-dias-disp-vacaciones/hist-dias-disp-vacaciones.module';
import { ComentariosModule } from './modules/comentarios/comentarios.module';
import { NotificacionesModule } from './modules/notificaciones/notificaciones.module';
import { TareasModule } from './modules/tareas/tareas.module';
import { HelpersModule } from './modules/helpers/helpers.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: configService.get<'postgres'>('TYPEORM_CONNECTION'),
      host: configService.get('TYPEORM_HOST'),
      port: parseInt(configService.get('TYPEORM_PORT'), 10),
      username: configService.get('TYPEORM_USERNAME'),
      password: configService.get('TYPEORM_PASSWORD'),
      database: configService.get('TYPEORM_DATABASE'),
      synchronize: configService.get('TYPEORM_SYNCHRONIZE') === 'true',
      entities: [__dirname + configService.get('TYPEORM_ENTITIES')],
      logging: 'all',
      migrationsTableName: configService.get('TYPEORM_MIGRATIONS_TABLE_NAME'),
      migrations: [configService.get('TYPEORM_MIGRATIONS')],
      cli: {
        migrationsDir: configService.get('TYPEORM_MIGRATIONS_DIR')
      }
    }),
    //ScheduleModule.forRoot(),
    AreaModule,
    AusenciaModule,
    MotivoAusenciaModule,
    TipoDescuentoModule,
    PersonaModule,
    EmpresaModule,
    CargoModule,
    TelefonoPersonaModule,
    TelefonoEmpresaModule,
    CursoModule,
    TipoDocumentoPersonalModule,
    RolModule,
    HistCargosModule,
    HistAusenciaModule,
    HistPrestacionesModule,
    HistVacacionesModule,
    HistDiasDispVacacionesModule,
    MontoDisponiblePrestacionesModule,
    DocumentoPersonalModule,
    SolicitudesModule,
    AuthModule,
    ComentariosModule,
    NoticiasModule,
    NotificacionesModule,
    Aws3CntmModule,
    TareasModule,
    MulterModule.register({
      dest: './'
    }),
    PublicacionModule,
    UsuarioPermisoModule,
    VerticalModule,
    HelpersModule
  ],
  controllers: [AppController],
  providers: [
    AppService,
    {
      provide: APP_FILTER,
      useClass: PostgreExceptionFilter
    }
  ]
})
export class AppModule {}
