import { ApiModelProperty } from '@nestjs/swagger';
import { IsOptional, Matches } from 'class-validator';

import { Empresa } from '../../empresa/empresa.entity';

export class UpdateNormaDTO {
  @IsOptional()
  @ApiModelProperty()
  readonly nombre?: string;

  @IsOptional()
  @ApiModelProperty()
  readonly descripcion?: string;

  @IsOptional()
  @Matches(RegExp('COMPORTAMIENTO|VESTIMENTA'))
  @ApiModelProperty({
    required: false,
    default: 'COMPORTAMIENTO',
    pattern: 'COMPORTAMIENTO|VESTIMENTA'
  })
  readonly tipo?: string;

  /*@IsOptional()
  @ApiModelProperty({ required: false, type: 'number' })
  readonly empresa?: Empresa;*/
}
