import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, Matches } from 'class-validator';
export class CreateNormaDTO {
  @IsNotEmpty()
  @ApiModelProperty()
  readonly nombre: string;

  @IsNotEmpty()
  @ApiModelProperty()
  readonly descripcion: string;

  @IsNotEmpty()
  @Matches(RegExp('COMPORTAMIENTO|VESTIMENTA'))
  @ApiModelProperty({
    required: true,
    default: 'COMPORTAMIENTO',
    pattern: 'COMPORTAMIENTO|VESTIMENTA'
  })
  readonly tipo: string;

  /*@IsNotEmpty()
  @ApiModelProperty({ required: true, type: 'number' })
  readonly empresa: Empresa;*/
}
