import { Check, Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { NormaTipo } from './const/norma-tipo.enum';

@Entity()
@Check(`"tipo" = 'COMPORTAMIENTO' OR "tipo" = 'VESTIMENTA'`)
export class Norma {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  nombre: string;

  @Column()
  descripcion: string;

  @Column({ default: NormaTipo.COMPORTAMIENTO })
  tipo: string;
}
