import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn
} from 'typeorm';
import { Noticia } from '../noticias/noticia.entity';

import { Persona } from '../persona/persona.entity';

@Entity()
export class Comentario {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: false })
  texto: string;

  @ManyToOne((type) => Persona, (persona) => persona.comentarios, {
    nullable: false,
    onDelete: 'CASCADE'
  })
  @JoinColumn({ name: 'fk_persona_id' })
  persona: Persona;

  @ManyToOne((type) => Noticia, (noticia) => noticia.comentarios, {
    nullable: false,
    onDelete: 'CASCADE'
  })
  @JoinColumn({ name: 'fk_noticia_id' })
  noticia: Noticia;

  @CreateDateColumn({ type: 'timestamp', name: 'fecha_publicacion' })
  fechaPublicacion: Date;
}
