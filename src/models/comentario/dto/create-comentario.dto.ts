import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional, IsPositive, IsString } from 'class-validator';
import { Noticia } from '../../../models/noticias/noticia.entity';
import { Persona } from '../../../models/persona/persona.entity';

export class CreateComentarioDTO {
  @IsNotEmpty()
  @IsString()
  @ApiModelProperty()
  readonly texto: string;

  @ApiModelProperty()
  readonly persona: Persona;

  @IsNotEmpty()
  @ApiModelProperty()
  readonly noticia: Noticia;
}
