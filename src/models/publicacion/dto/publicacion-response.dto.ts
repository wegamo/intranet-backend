import { ApiModelProperty } from "@nestjs/swagger";
import { TipoObjeto } from "../../../enums/tipo-objeto.enum";
import { Persona } from "../../persona/persona.entity";
export class PublicacionResponseDTO {
  @ApiModelProperty()
  readonly id: string;

  @ApiModelProperty()
  readonly ubicacion: string;

  @ApiModelProperty()
  readonly nombreObjeto: string;

  @ApiModelProperty({ enum:TipoObjeto })
  readonly tipoObjeto: TipoObjeto;

  @ApiModelProperty()
  readonly actualizacion: string;

  @ApiModelProperty()
  readonly publicante: Persona;
}