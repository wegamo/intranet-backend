import { ApiModelProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsOptional } from "class-validator";
import { Area } from "../../area/area.entity";
import { TipoObjeto } from "../../../enums/tipo-objeto.enum";
import { Persona } from "../../persona/persona.entity";
import { Vertical } from "../../vertical/vertical.entity";
export class CreatePublicacionDTO {
  @IsNotEmpty()
  @ApiModelProperty()
  readonly ubicacion: string;

  @IsNotEmpty()
  @ApiModelProperty()
  readonly nombreObjeto: string;

  @IsNotEmpty()
  @ApiModelProperty()
  readonly tipoObjeto: TipoObjeto;

  @IsNotEmpty()
  @ApiModelProperty()
  readonly actualizacion: string;

  @IsNotEmpty()
  @ApiModelProperty()
  readonly publicante: Persona;

  @IsOptional()
  @ApiModelProperty()
  readonly area?: Area;

  @IsOptional()
  @ApiModelProperty()
  readonly vertical?: Vertical;
}