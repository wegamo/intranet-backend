import { ApiModelProperty } from "@nestjs/swagger";
import { IsNotEmpty } from "class-validator";

export class UpdatePublicacionDTO {
  @IsNotEmpty()
  @ApiModelProperty()
  readonly ubicacion: string;

  @IsNotEmpty()
  @ApiModelProperty()
  readonly actualizacion: string;
}