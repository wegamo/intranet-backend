import { TipoObjeto } from '../../enums/tipo-objeto.enum';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn
} from 'typeorm';
import { Persona } from '../persona/persona.entity';
import { UsuarioPermiso } from '../usuario-permiso/usuario-permiso.entity';
import { Vertical } from '../vertical/vertical.entity';
import { Area } from '../area/area.entity';

@Entity()
export class Publicacion {
  @PrimaryGeneratedColumn({ type: 'integer' })
  id: number;

  @Column({ nullable: false })
  ubicacion: string;

  @Column({ type: 'varchar', nullable: false })
  nombreObjeto: string;

  @Column({
    type: 'enum',
    enum: TipoObjeto
  })
  tipoObjeto: TipoObjeto;

  @CreateDateColumn({ type: 'timestamp', nullable: false, name: 'fecha_publicacion' })
  fechaPublicacion: Date;

  @Column({ type: 'varchar', nullable: false })
  actualizacion: string;

  @OneToMany(() => UsuarioPermiso, (usuarioPermiso) => usuarioPermiso.publicacion, {
    nullable: true,
    cascade: ['insert', 'update', 'remove']
  })
  permisos?: UsuarioPermiso[];

  @ManyToOne(() => Persona, (persona) => persona.publicaciones, {
    nullable: true,
    onDelete: 'SET NULL'
  })
  @JoinColumn({ name: 'fk_publicante_id' })
  publicante: Persona;

  @ManyToOne(() => Vertical, (vertical) => vertical.publicaciones, {
    nullable: true
  })
  @JoinColumn({ name: 'fk_vertical_id' })
  vertical: Vertical;

  @ManyToOne(() => Area, (area) => area.publicaciones, {
    nullable: true
  })
  @JoinColumn({ name: 'fk_area_id' })
  area: Area;
}
