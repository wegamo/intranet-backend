import { Column, Entity, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { Ausencia } from '../ausencia/ausencia.entity';

@Entity()
export class MotivoAusencia {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 255, nullable: false })
  nombre: string;

  @OneToMany(() => Ausencia, (ausencia) => ausencia.motivo)
  ausencias: Ausencia[];
}
