import { Column, Entity, ManyToOne, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { DocumentoPersonal } from '../documento-personal/documento-personal.entity';

@Entity()
export class TipoDocumentoPersonal {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 255, nullable: false, unique: true })
  nombre: string;

  @Column({ type: 'varchar', length: 20, nullable: false })
  requerido: string;

  @OneToMany(
    () => DocumentoPersonal,
    (documentoPersonal) => documentoPersonal.tipoDocumentoPersonal,
    {
      nullable: true,
      cascade: ['insert', 'update', 'remove']
    }
  )
  documentosPersonales?: DocumentoPersonal[];
}
