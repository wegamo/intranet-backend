export enum DocumentoPersonalTipo {
  IMAGEN = 'IMAGEN',
  DOCUMENTO = 'DOCUMENTO',
  VIDEO = 'VIDEO'
}
