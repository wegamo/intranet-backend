import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsPositive, Matches, IsOptional } from 'class-validator';
export class CreateTipoDocumentoDTO {
  @IsNotEmpty()
  @ApiModelProperty({ required: true, nullable: false, type: 'varchar' })
  readonly nombre: string;

  @IsNotEmpty()
  @ApiModelProperty({ required: true, nullable: false, type: 'varchar' })
  readonly requerido: string;
}
