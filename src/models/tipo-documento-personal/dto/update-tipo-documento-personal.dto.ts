import { ApiModelProperty } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';

export class UpdateDocumentoDTO {
  @IsOptional()
  @ApiModelProperty({ type: 'varchar' })
  readonly nombre?: string;

  @IsOptional()
  @ApiModelProperty({ type: 'varchar' })
  readonly obligatorio?: string;

  @IsOptional()
  @ApiModelProperty({ type: 'varchar' })
  readonly archivo?: string;
}
