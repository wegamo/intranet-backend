import {
  Column,
  Entity,
  JoinColumn,
  CreateDateColumn,
  PrimaryGeneratedColumn,
  ManyToOne
} from 'typeorm';
import { Persona } from '../persona/persona.entity';
import { Solicitud } from '../solicitud/solicitud.entity';
import { SolicitudEstatus } from '../../enums/solicitud-estatus.enum';

@Entity()
export class HistPrestaciones {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'timestamp', nullable: true, default: null })
  fechaRevision?: Date;

  @CreateDateColumn({ type: 'timestamp', name: 'fecha_solicitud' })
  fechaSolicitud: Date;

  @Column({ type: 'timestamp', nullable: true, default: null, name: 'fecha_pago' })
  fechaPago?: Date;

  @Column({ nullable: false })
  motivo: string;

  @Column({ type: 'varchar', nullable: true, default: null, name: 'motivo_rechazo' })
  motivoRechazo?: string;

  @Column({ nullable: true, default: null, name: 'firma_solicitante' })
  firmaSolicitante: string;

  @Column({ nullable: true, default: null })
  presupuesto: string;

  @Column({ nullable: true, default: null, name: 'firma_th' })
  firmaTh?: string;

  @Column({ type: 'enum', enum: SolicitudEstatus, default: SolicitudEstatus.PENDIENTE })
  estatus: SolicitudEstatus;

  @Column({ nullable: false, name: 'monto_solicitado' })
  montoSolicitado: string;

  @Column({ nullable: false })
  acumulado75: string;

  @Column({ nullable: true, default: null, name: 'monto_aprobado' })
  montoAprobado?: string;

  @ManyToOne((type) => Solicitud, (solicitud) => solicitud.histPrestaciones, { nullable: false })
  @JoinColumn({ name: 'fk_solicitud_id' })
  solicitud: Solicitud;

  @ManyToOne((type) => Persona, (persona) => persona.histRevPresTH, {
    nullable: true
  })
  @JoinColumn({ name: 'fk_th_id' })
  talentoHumano?: Persona;

  @ManyToOne((type) => Persona, (persona) => persona.histPrestaciones, {
    nullable: false,
    onDelete: 'CASCADE'
  })
  @JoinColumn({ name: 'fk_persona_id' })
  persona: Persona;
}
