import { ApiModelProperty } from '@nestjs/swagger';
import { IsEnum, IsNotEmpty, IsOptional } from 'class-validator';
import { Persona } from '../../../models/persona/persona.entity';
import { SolicitudEstatus } from '../../../enums/solicitud-estatus.enum';

export class UpdateHistPrestacionesDTO {
  @IsNotEmpty()
  @ApiModelProperty()
  readonly fechaRevision: Date;

  @IsOptional()
  @ApiModelProperty()
  readonly fechaPago?: Date;

  @IsOptional()
  @ApiModelProperty()
  readonly motivoRechazo?: string;

  @IsNotEmpty()
  @ApiModelProperty()
  talentoHumano: Persona;

  @IsNotEmpty()
  @IsEnum(SolicitudEstatus)
  @ApiModelProperty({ required: true, type: 'string' })
  readonly estatus: SolicitudEstatus;
}
