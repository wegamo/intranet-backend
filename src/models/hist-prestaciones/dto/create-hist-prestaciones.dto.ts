import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { Persona } from '../../persona/persona.entity';
import { Solicitud } from '../../solicitud/solicitud.entity';

export class CreateHistPrestacionesDTO {
  @IsNotEmpty()
  @ApiModelProperty()
  readonly montoSolicitado: string;

  @IsNotEmpty()
  @ApiModelProperty()
  acumulado75: string;

  @IsNotEmpty()
  @ApiModelProperty()
  readonly motivo: string;

  @IsNotEmpty()
  @ApiModelProperty({ required: true, type: 'number' })
  readonly solicitud: Solicitud;

  @IsNotEmpty()
  @ApiModelProperty({ required: true, type: 'number' })
  persona: Persona;
}
