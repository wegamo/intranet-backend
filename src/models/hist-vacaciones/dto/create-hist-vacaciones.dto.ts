import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';
import { Persona } from '../../persona/persona.entity';
import { Solicitud } from '../../solicitud/solicitud.entity';

export class CreateHistVacacionesDTO {
  @IsNotEmpty()
  @ApiModelProperty()
  readonly fechaInicio: Date;

  @IsNotEmpty()
  @ApiModelProperty()
  readonly fechaFin: Date;

  @IsNotEmpty()
  @ApiModelProperty()
  readonly fechaReintegro: Date;

  @IsNotEmpty()
  @ApiModelProperty()
  readonly cantidadDias: number;

  @IsNotEmpty()
  @ApiModelProperty({ required: true, type: 'number' })
  readonly solicitud: Solicitud;

  @IsNotEmpty()
  @ApiModelProperty({ required: true, type: 'number' })
  readonly persona: Persona;

  @IsOptional()
  @ApiModelProperty({ required: false, type: 'number' })
  supervisor: Persona;
}
