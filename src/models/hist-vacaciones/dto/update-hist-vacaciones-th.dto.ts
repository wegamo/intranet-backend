import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';
import { SolicitudEstatus } from '../../../enums/solicitud-estatus.enum';
import { Persona } from '../../../models/persona/persona.entity';

export class UpdateHistVacacionesThDTO {
  @IsNotEmpty()
  @ApiModelProperty()
  readonly fechaRevision: Date;

  @IsNotEmpty()
  @ApiModelProperty()
  readonly fechaPago: Date;

  @IsOptional()
  @ApiModelProperty()
  readonly fechaExamenPreVacacional?: Date;

  @IsOptional()
  @ApiModelProperty()
  readonly motivoRechazo?: string;

  @IsNotEmpty()
  @ApiModelProperty()
  readonly periodoPagado: string;

  @IsNotEmpty()
  @ApiModelProperty()
  readonly periodoDisfrute: string;

  @IsNotEmpty()
  @ApiModelProperty()
  readonly diasPendientePago: number;

  @IsNotEmpty()
  @ApiModelProperty()
  readonly diasPendienteDisfrute: number;

  @IsNotEmpty()
  @ApiModelProperty()
  talentoHumano: Persona;

  @IsNotEmpty()
  @ApiModelProperty()
  readonly estatus: SolicitudEstatus;
}
