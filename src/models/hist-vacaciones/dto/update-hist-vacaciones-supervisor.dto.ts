import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';
import { SolicitudEstatus } from '../../../enums/solicitud-estatus.enum';

export class UpdateHistVacacionesSupervisorDTO {
  @IsNotEmpty()
  @ApiModelProperty()
  readonly fechaRevision: Date;

  @IsOptional()
  @ApiModelProperty()
  readonly motivoRechazo?: string;

  @IsNotEmpty()
  @ApiModelProperty()
  readonly estatus: SolicitudEstatus;
}
