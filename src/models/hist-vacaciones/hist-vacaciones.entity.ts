import {
  Column,
  Entity,
  JoinColumn,
  CreateDateColumn,
  PrimaryGeneratedColumn,
  ManyToOne
} from 'typeorm';
import { Persona } from '../persona/persona.entity';
import { Solicitud } from '../solicitud/solicitud.entity';
import { SolicitudEstatus } from '../../enums/solicitud-estatus.enum';
@Entity()
export class HistVacaciones {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'timestamp', nullable: true, default: null, name: 'fecha_revision' })
  fechaRevision: Date;

  @CreateDateColumn({ type: 'timestamp', name: 'fecha_solicitud' })
  fechaSolicitud: Date;

  @Column({ type: 'timestamp', nullable: true, name: 'fecha_examen_prevacacional' })
  fechaExamenPreVacacional?: Date;

  @Column({ type: 'timestamp', nullable: false, name: 'fecha_reintegro' })
  fechaReintegro: Date;

  @Column({ type: 'varchar', nullable: true, default: null, name: 'motivo_rechazo' })
  motivoRechazo: string;

  @Column({ nullable: true, name: 'periodo_pagado' })
  periodoPagado: string;

  @Column({ nullable: true, name: 'periodo_disfrute' })
  periodoDisfrute: string;

  @Column({ nullable: true, default: null, name: 'firma_solicitante' })
  firmaSolicitante: string;

  @Column({ nullable: false, name: 'cantidad_dias' })
  cantidadDias: number;

  @Column({ nullable: true, name: 'dias_pendiente_pago' })
  diasPendientePago: number;

  @Column({ nullable: true, name: 'dias_pendiente_disfrute' })
  diasPendienteDisfrute: number;

  @Column({ type: 'timestamp', nullable: false, name: 'fecha_inicio' })
  fechaInicio: Date;

  @Column({ type: 'timestamp', nullable: false, name: 'fecha_fin' })
  fechaFin: Date;

  @Column({ type: 'timestamp', nullable: true, default: null, name: 'fecha_pago' })
  fechaPago?: Date;

  @Column({ nullable: true, default: null, name: 'firma_th' })
  firmaTh?: string;

  @Column({ nullable: true, default: null, name: 'firma_supervisor' })
  firmaSupervisor?: string;

  @Column({ type: 'enum', enum: SolicitudEstatus, default: SolicitudEstatus.PENDIENTE })
  estatus: SolicitudEstatus;

  @ManyToOne((type) => Solicitud, (solicitud) => solicitud.histVacaciones, { nullable: false })
  @JoinColumn({ name: 'fk_solicitud_id' })
  solicitud: Solicitud;

  @ManyToOne((type) => Persona, (persona) => persona.histRevVacTH, {
    nullable: true
  })
  @JoinColumn({ name: 'fk_th_id' })
  talentoHumano?: Persona;

  @ManyToOne((type) => Persona, (persona) => persona.histVacaciones, {
    nullable: false,
    onDelete: 'CASCADE'
  })
  @JoinColumn({ name: 'fk_persona_id' })
  persona: Persona;

  @ManyToOne((type) => Persona, (persona) => persona.histRevVacSupervisor, { nullable: false })
  @JoinColumn({ name: 'fk_supervisor_id' })
  supervisor: Persona;
}
