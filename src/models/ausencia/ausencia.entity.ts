import { Entity, ManyToOne, PrimaryGeneratedColumn, JoinColumn, OneToMany } from 'typeorm';
import { HistAusencias } from '../hist-ausencias/hist-ausencias.entity';
import { MotivoAusencia } from '../motivo-ausencia/motivoAusencia.entity';
import { Solicitud } from '../solicitud/solicitud.entity';
import { TipoDescuento } from '../tipo-descuento/tipoDescuento.entity';

@Entity()
export class Ausencia {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne((type) => Solicitud, (solicitud) => solicitud.ausencias, { nullable: false })
  @JoinColumn({ name: 'fk_solicitud_id' })
  solicitud: Solicitud;

  @ManyToOne(() => MotivoAusencia, (motivoAusencia) => motivoAusencia.ausencias, {
    nullable: false
  })
  @JoinColumn({ name: 'fk_motivo_ausencia_id' })
  motivo: MotivoAusencia;

  @ManyToOne(() => TipoDescuento, (tipoDescuento) => tipoDescuento.ausencias, { nullable: false })
  @JoinColumn({ name: 'fk_tipo_descuento_id' })
  tipoDescuento: TipoDescuento;

  @OneToMany(() => HistAusencias, (histAusencias) => histAusencias.ausencia)
  histAusencias: HistAusencias[];
}
