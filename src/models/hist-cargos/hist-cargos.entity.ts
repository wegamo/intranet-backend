import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn, Check } from 'typeorm';

import { Cargo } from '../cargo/cargo.entity';
import { Persona } from '../persona/persona.entity';
//import { Empresa } from '../empresa/empresa.entity';

@Entity()
@Check(
  'check_percar_date',
  `("fecha_fin" > "fecha_inicio" AND "fecha_fin" < now()) OR "fecha_fin" is null`
)
export class HistCargo {
  @PrimaryGeneratedColumn({ type: 'integer' })
  id: number;

  @Column({ type: 'timestamp', nullable: false, name: 'fecha_inicio' })
  fechaInicio: Date;

  @Column({ type: 'timestamp', nullable: true, name: 'fecha_fin', default: null })
  fechaFin?: Date;

  @ManyToOne((type) => Cargo, (cargo) => cargo.id, { nullable: false })
  @JoinColumn({ name: 'fk_cargo_id' })
  cargo: Cargo;

  @ManyToOne((type) => Persona, (persona) => persona.id, { nullable: false, onDelete: 'CASCADE' })
  @JoinColumn({ name: 'fk_persona_id' })
  persona: Persona;
}
