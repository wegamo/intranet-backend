import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';
import { Cargo } from './../../../models/cargo/cargo.entity';
import { Persona } from './../../../models/persona/persona.entity';

export class CreateHistCargoDTO {
  @IsNotEmpty()
  @ApiModelProperty()
  readonly fechaInicio: Date;

  @IsOptional()
  @ApiModelProperty()
  readonly fechaFin?: Date;

  @IsNotEmpty()
  @ApiModelProperty({ required: true })
  readonly cargo: Cargo;

  @IsNotEmpty()
  @ApiModelProperty({ required: true })
  readonly persona: Persona;
}
