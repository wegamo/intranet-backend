import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';
import { Cargo } from '../../cargo/cargo.entity';

export class UpdateHistCargoDTO {
  @IsOptional()
  @ApiModelProperty()
  readonly fechaInicio: Date;

  @IsOptional()
  @ApiModelProperty()
  readonly fechaFin?: Date;

  @IsOptional()
  @ApiModelProperty({ required: false })
  readonly cargo: Cargo;
}
