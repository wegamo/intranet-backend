import { ApiModelProperty } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';
import { genero } from '../../../enums/genero.enum';
import { PersonaEstatus } from '../../../enums/persona-estatus.enum';
import { Curso } from '../../curso/curso.entity';
import { DocumentoPersonal } from '../../documento-personal/documento-personal.entity';
import { Empresa } from '../../empresa/empresa.entity';
import { HistAusencias } from '../../hist-ausencias/hist-ausencias.entity';
import { HistCargo } from '../../hist-cargos/hist-cargos.entity';
import { HistPrestaciones } from '../../hist-prestaciones/hist-prestaciones.entity';
import { HistVacaciones } from '../../hist-vacaciones/hist-vacaciones.entity';
import { MontoPrestaciones } from '../../monto-disponible-prestaciones/monto-disponible-prestaciones.entity';
import { Noticia } from '../../noticias/noticia.entity';
import { Rol } from '../../rol/rol.entity';
import { TelefonoPersona } from '../../telefono-persona/telefono-persona.entity';

export class UpdatePersonaDTO {
  @IsOptional()
  @ApiModelProperty({ required: false, type: 'string' })
  readonly cedula?: string;

  @IsOptional()
  @ApiModelProperty({ required: false, type: 'string' })
  readonly primerNombre?: string;

  @IsOptional()
  @ApiModelProperty({ required: false, type: 'string' })
  readonly segundoNombre?: string;

  @IsOptional()
  @ApiModelProperty({ required: false, type: 'string' })
  readonly primerApellido?: string;

  @IsOptional()
  @ApiModelProperty({ required: false, type: 'string' })
  readonly segundoApellido?: string;

  @IsOptional()
  @ApiModelProperty({ required: false, type: 'string' })
  readonly email?: string;

  @IsOptional()
  @ApiModelProperty({ required: false, type: 'date' })
  readonly fechaNacimiento?: Date;

  @IsOptional()
  @ApiModelProperty({ required: false, type: 'date' })
  readonly fechaIngreso?: Date;

  @IsOptional()
  @ApiModelProperty({ required: false, type: 'string' })
  readonly direccion?: string;

  @IsOptional()
  @ApiModelProperty({ required: false, type: 'string' })
  readonly estado?: string;

  @IsOptional()
  @ApiModelProperty({ required: false, type: 'string' })
  readonly pais?: string;

  @IsOptional()
  @ApiModelProperty({ required: false, type: 'enum' })
  readonly genero?: genero;

  @IsOptional()
  @ApiModelProperty({ required: false, type: 'string' })
  readonly imgPerfil?: string;

  @IsOptional()
  readonly estatus?: PersonaEstatus;

  @IsOptional()
  readonly rol?: Rol;

  @IsOptional()
  readonly empresa?: Empresa;

  @IsOptional()
  readonly cursos?: Curso[];

  @IsOptional()
  readonly telefonos?: TelefonoPersona[];

  @IsOptional()
  readonly histCargos?: HistCargo[];

  @IsOptional()
  readonly montoPresDisponible?: MontoPrestaciones[];

  @IsOptional()
  readonly noticias?: Noticia[];

  @IsOptional()
  readonly documentosPersonales?: DocumentoPersonal[];

  @IsOptional()
  readonly histPrestaciones?: HistPrestaciones[];

  @IsOptional()
  readonly histRevPresTH?: HistPrestaciones[];

  @IsOptional()
  readonly histVacaciones?: HistVacaciones[];

  @IsOptional()
  readonly histRevVacTH?: HistVacaciones[];

  @IsOptional()
  readonly histRevVacSupervisor?: HistVacaciones[];

  @IsOptional()
  readonly histAusencias?: HistAusencias[];

  @IsOptional()
  readonly histRevAusTH?: HistAusencias[];

  @IsOptional()
  readonly histRevAusSupervisor?: HistAusencias[];
}
