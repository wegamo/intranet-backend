import { ApiModelProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsOptional, Length } from 'class-validator';
import { genero } from '../../../enums/genero.enum';
import { Empresa } from '../../empresa/empresa.entity';
import { HistCargo } from '../../hist-cargos/hist-cargos.entity';
import { Rol } from '../../rol/rol.entity';
import { TelefonoPersona } from '../../telefono-persona/telefono-persona.entity';

//DECORADOR @IsDate muestra errores de compatibilidad con el tipo "timestamp"
// EVIDENTEMENTE NO SON TIPOS DE DATOS COMPARABLES ENTRE SI SEGUN TYPEORM
export class CreatePersonaDTO {
  @IsNotEmpty()
  @Length(1, 30)
  @ApiModelProperty({ required: true, type: 'string' })
  readonly cedula: string;

  @IsNotEmpty()
  @Length(1, 80)
  @ApiModelProperty({ required: true, type: 'string' })
  readonly primerNombre: string;

  @IsOptional()
  @Length(0, 80)
  @ApiModelProperty({ required: false, type: 'string' })
  readonly segundoNombre?: string;

  @IsNotEmpty()
  @Length(1, 80)
  @ApiModelProperty({ required: true, type: 'string' })
  readonly primerApellido: string;

  @IsOptional()
  @Length(0, 80)
  @ApiModelProperty({ required: false, type: 'string' })
  readonly segundoApellido?: string;

  @IsNotEmpty()
  @IsEmail()
  @Length(1, 256)
  @ApiModelProperty({ required: true, type: 'string' })
  readonly email: string;

  @IsNotEmpty()
  @Length(1, 25)
  @ApiModelProperty({ required: true })
  readonly genero: genero;

  @IsNotEmpty()
  @ApiModelProperty({ required: true, type: 'timestamp' })
  readonly fechaNacimiento: Date;

  @IsNotEmpty()
  @ApiModelProperty({ required: true, type: 'timestamp' })
  readonly fechaIngreso: Date;

  @IsNotEmpty()
  @ApiModelProperty({ required: true, type: 'number' })
  readonly histCargos: HistCargo[];

  @IsNotEmpty()
  @ApiModelProperty({ required: true, type: 'number' })
  rol: Rol;

  @IsNotEmpty()
  @ApiModelProperty({ required: true, type: 'number' })
  empresa: Empresa;
}
