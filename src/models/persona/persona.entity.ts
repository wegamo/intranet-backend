import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  JoinTable
} from 'typeorm';
import { Rol } from '../rol/rol.entity';
import { TelefonoPersona } from '../telefono-persona/telefono-persona.entity';
import { Empresa } from '../empresa/empresa.entity';
import { Curso } from '../curso/curso.entity';
import { HistCargo } from '../hist-cargos/hist-cargos.entity';
import { Noticia } from '../noticias/noticia.entity';
import { Comentario } from '../comentario/comentario.entity';
import { PersonaEstatus } from '../../enums/persona-estatus.enum';
import { DocumentoPersonal } from '../documento-personal/documento-personal.entity';
import { HistPrestaciones } from '../hist-prestaciones/hist-prestaciones.entity';
import { HistVacaciones } from '../hist-vacaciones/hist-vacaciones.entity';
import { HistAusencias } from '../hist-ausencias/hist-ausencias.entity';
import { MontoPrestaciones } from '../monto-disponible-prestaciones/monto-disponible-prestaciones.entity';
import { genero } from '../../enums/genero.enum';
import { Publicacion } from '../publicacion/publicacion.entity';
import { UsuarioPermiso } from '../usuario-permiso/usuario-permiso.entity';
import { HistDiasDispVacaciones } from '../hist-dias-disp-vacaciones/hist-dias-disp-vacaciones.entity';
import { Notificacion } from '../notificacion/notificacion.entity';
import { Tarea } from '../tarea/tarea.entity';
@Entity()
export class Persona {
  @PrimaryGeneratedColumn({ type: 'integer' })
  id: number;

  @Column({ type: 'varchar', length: 30, unique: true, nullable: false })
  cedula: string;

  @Column({ type: 'varchar', length: 80, nullable: false, name: 'primer_nombre' })
  primerNombre: string;

  @Column({ type: 'varchar', length: 80, nullable: true, name: 'segundo_nombre' })
  segundoNombre: string;

  @Column({ type: 'varchar', length: 80, nullable: false, name: 'primer_apellido' })
  primerApellido: string;

  @Column({ type: 'varchar', length: 80, nullable: true, name: 'segundo_apellido' })
  segundoApellido: string;

  @Column({ unique: true, type: 'varchar', length: 128, nullable: false })
  email: string;

  @Column({ type: 'date', nullable: false, name: 'fecha_nacimiento' })
  fechaNacimiento: Date;

  @Column({ type: 'date', nullable: false, name: 'fecha_ingreso' })
  fechaIngreso: Date;

  @Column({ type: 'varchar', length: 256, nullable: true })
  direccion: string;

  @Column({ type: 'varchar', length: 25, nullable: true })
  estado: string;

  @Column({ type: 'varchar', length: 25, nullable: true })
  pais: string;

  @Column({ type: 'enum', enum: genero, default: genero.MASCULINO })
  genero: genero;

  @Column({ type: 'varchar', nullable: true, name: 'img_perfil' })
  imgPerfil?: string;

  @Column({ select: false })
  password: string;

  @Column({
    type: 'enum',
    enum: PersonaEstatus,
    default: PersonaEstatus.ACTIVO
  })
  estatus?: PersonaEstatus;

  @ManyToOne((type) => Rol, (rol) => rol.personas, { nullable: false })
  @JoinColumn({ name: 'fk_rol_id' })
  rol: Rol;

  @ManyToOne((type) => Empresa, (empresa) => empresa.personas, { nullable: false })
  @JoinColumn({ name: 'fk_empresa_id' })
  empresa: Empresa;

  @OneToMany((type) => Curso, (curso) => curso.persona, {
    nullable: true,
    cascade: ['insert', 'remove']
  })
  cursos?: Curso[];

  @OneToMany((type) => TelefonoPersona, (telefono) => telefono.persona, {
    nullable: true,
    cascade: ['insert', 'update', 'remove']
  })
  telefonos?: TelefonoPersona[];

  @OneToMany(() => HistCargo, (histCargo) => histCargo.persona, {
    nullable: false,
    cascade: ['insert', 'update', 'remove']
  })
  histCargos: HistCargo[];

  @OneToMany(() => Noticia, (noticia) => noticia.persona, {
    nullable: true,
    cascade: ['insert', 'update', 'remove']
  })
  noticias?: Noticia[];

  @OneToMany(() => Comentario, (comentario) => comentario.persona, {
    nullable: true,
    cascade: ['insert', 'update', 'remove']
  })
  comentarios?: Comentario[];

  @OneToMany(() => Notificacion, (notificacion) => notificacion.persona, {
    nullable: true,
    cascade: ['insert', 'update', 'remove']
  })
  notificaciones?: Notificacion[];

  @OneToMany(() => MontoPrestaciones, (montoPrestaciones) => montoPrestaciones.persona, {
    nullable: true,
    cascade: ['insert', 'update', 'remove']
  })
  montoPresDisponible?: MontoPrestaciones[];

  @OneToMany(() => Publicacion, (publicacion) => publicacion.publicante, {
    nullable: true,
    cascade: ['insert', 'update', 'remove']
  })
  publicaciones?: Publicacion[];

  @OneToMany(() => UsuarioPermiso, (permiso) => permiso.persona, {
    nullable: true,
    cascade: ['insert', 'update', 'remove']
  })
  permisos?: UsuarioPermiso[];

  @OneToMany(() => DocumentoPersonal, (documento) => documento.persona, {
    nullable: true,
    cascade: ['insert', 'update', 'remove']
  })
  documentosPersonales?: DocumentoPersonal[];

  @OneToMany(() => HistPrestaciones, (histPrestaciones) => histPrestaciones.persona)
  histPrestaciones?: HistPrestaciones[];

  @OneToMany(() => HistPrestaciones, (histPrestaciones) => histPrestaciones.talentoHumano)
  histRevPresTH?: HistPrestaciones[];

  @OneToMany(() => HistVacaciones, (histVacaciones) => histVacaciones.persona)
  histVacaciones?: HistVacaciones[];

  @OneToMany(
    () => HistDiasDispVacaciones,
    (histDiasDispVacaciones) => histDiasDispVacaciones.persona
  )
  histDiasDispVacaciones?: HistDiasDispVacaciones[];

  @OneToMany(() => HistVacaciones, (histVacaciones) => histVacaciones.talentoHumano)
  histRevVacTH?: HistVacaciones[];

  @OneToMany(() => HistVacaciones, (histVacaciones) => histVacaciones.supervisor)
  histRevVacSupervisor?: HistVacaciones[];

  @OneToMany(() => HistAusencias, (histAusencias) => histAusencias.persona)
  histAusencias?: HistAusencias[];

  @OneToMany(() => HistAusencias, (histAusencias) => histAusencias.talentoHumano)
  histRevAusTH?: HistAusencias[];

  @OneToMany(() => HistAusencias, (histAusencias) => histAusencias.supervisor)
  histRevAusSupervisor?: HistAusencias[];

  @OneToMany(() => Tarea, (tarea) => tarea.persona)
  tareas?: Tarea[];
}
