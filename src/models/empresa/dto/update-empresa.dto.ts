import { ApiModelProperty } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';
import { TipoDocumentoPersonal } from '../../tipo-documento-personal/tipoDocumentoPersonal.entity';
import { Norma } from '../../../models/norma/norma.entity';
import { Valor } from '../../../models/valor/valor.entity';
import { Empresa } from '../empresa.entity';

export class UpdateEmpresaDTO {
  @IsOptional()
  @ApiModelProperty()
  readonly nombre?: string;

  @IsOptional()
  @ApiModelProperty()
  readonly mision?: string;

  @IsOptional()
  @ApiModelProperty()
  readonly vision?: string;

  @IsOptional()
  @ApiModelProperty()
  readonly correo?: string;
  /*
  @IsOptional()
  @ApiModelProperty({ required: false, type: 'number' })
  readonly parentEmpresa?: Empresa;*/

  /*@IsOptional()
  @IsPositive()
  @ApiModelProperty({ required: false, type: 'number' })
  readonly childEmpresas?: Empresa[];

  @IsOptional()
  @ApiModelProperty({ required: false, type: 'number' })
  readonly documentos?: Documento[];

  @IsOptional()
  @ApiModelProperty({ required: false, type: 'number' })
  readonly normas?: Norma[];

  @IsOptional()
  @ApiModelProperty({ required: false, type: 'number' })
  readonly valores?: Valor[];

  @IsOptional()
  @ApiModelProperty({ required: false, type: 'number' })
  readonly telefonos?: Telefono[];

  @IsOptional()
  @ApiModelProperty({ required: true, type: 'number' })
  readonly eventos?: (EmpresaEvento)[];*/
}
