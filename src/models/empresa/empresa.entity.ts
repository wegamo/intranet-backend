import { Column, Entity, ManyToMany, OneToMany, JoinTable, PrimaryGeneratedColumn } from 'typeorm';
import { Norma } from '../norma/norma.entity';
import { TelefonoEmpresa } from '../telefono-empresa/telefono-empresa.entity';
import { Valor } from '../valor/valor.entity';
import { Persona } from '../persona/persona.entity';

@Entity()
export class Empresa {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 60, nullable: false })
  nombre: string;

  @Column({ type: 'varchar', length: 60, nullable: true, default: null })
  mision?: string;

  @Column({ type: 'varchar', length: 60, nullable: true, default: null })
  vision?: string;

  @Column({ type: 'varchar', length: 60, nullable: false })
  correo: string;

  @Column({ type: 'varchar', length: 10, nullable: false })
  estatus: string;

  /*@OneToMany((type) => Norma, (normas) => normas.empresa, {
    nullable: true,
    cascade: ['insert', 'update']
  })
  @JoinTable({
    name: 'normas_de_empresa',
    joinColumn: { name: 'empresa_N_id', referencedColumnName: 'id' },
    inverseJoinColumn: { name: 'norma_id', referencedColumnName: 'empresa' }
  })
  normas?: (Norma)[];*/

  /*@OneToMany((type) => Valor, (valores) => valores.empresa, {
    nullable: true,
    cascade: ['insert', 'update']
  })
  @JoinTable({
    name: 'valores_de_empresa',
    joinColumn: { name: 'empresa_V_id', referencedColumnName: 'id' },
    inverseJoinColumn: { name: 'valor_id', referencedColumnName: 'empresa' }
  })
  valores?: (Valor)[];*/

  @OneToMany((type) => TelefonoEmpresa, (telefonos) => telefonos.empresa, {
    nullable: true,
    cascade: ['insert', 'update']
  })
  telefonos?: (TelefonoEmpresa)[];
  /*@JoinTable({
    name: 'telefonos_de_empresa',
    joinColumn: { name: 'empresa_T_id', referencedColumnName: 'id' },
    inverseJoinColumn: { name: 'telefono_id', referencedColumnName: 'empresa' }
  })*/

  @OneToMany((type) => Persona, (persona) => persona.empresa, {
    nullable: false,
    cascade: ['insert']
  })
  personas?: Persona[];
}
