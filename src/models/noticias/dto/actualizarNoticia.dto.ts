import { IsNotEmpty, IsOptional, IsString, IsNumber } from 'class-validator';
import { NoticiaPosicion } from '../../../enums/noticia.posicion.enum';
import { NoticiaTipo } from '../../../enums/noticia.tipo.enum';
import { Persona } from '../../persona/persona.entity';

export class ActualizarNoticiaDTO {
  @IsNotEmpty()
  id: number;

  @IsNotEmpty()
  tipo: NoticiaTipo;

  @IsNotEmpty()
  persona: Persona;

  @IsOptional()
  @IsNumber()
  posicion?: NoticiaPosicion;

  @IsOptional()
  @IsString()
  titulo?: string;

  @IsOptional()
  @IsString()
  descripcion?: string;

  @IsOptional()
  imagen?: Express.Multer.File;
}
