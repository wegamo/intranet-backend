import { IsNotEmpty, IsOptional, IsString, IsNumber, ValidateIf } from 'class-validator';
import { NoticiaTipo } from '../../../enums/noticia.tipo.enum';
import { NoticiaPosicion } from '../../../enums/noticia.posicion.enum';
import { Persona } from '../../persona/persona.entity';

export class GuardarNoticiaDTO {
  @IsNotEmpty()
  @IsString()
  tipo: NoticiaTipo;

  @IsNumber()
  @IsOptional()
  posicion: NoticiaPosicion;

  @IsNotEmpty()
  @IsString()
  titulo: string;

  @IsNotEmpty()
  @IsString()
  persona: Persona;

  @IsOptional()
  @IsString()
  descripcion: string;

  @IsOptional()
  @IsString()
  resumen: string;
}
