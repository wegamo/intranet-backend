import { IsNotEmpty, IsOptional, IsString, IsNumber, ValidateIf, Length } from 'class-validator';
import { NoticiaTipo } from '../../../enums/noticia.tipo.enum';
import { NoticiaPosicion } from '../../../enums/noticia.posicion.enum';
import { Persona } from '../../persona/persona.entity';
import { ApiModelProperty } from '@nestjs/swagger';

export class CrearNoticiaDTO {
  @ApiModelProperty({enum:NoticiaTipo})
  @IsNotEmpty()
  tipo: NoticiaTipo;

  @ApiModelProperty({enum:NoticiaPosicion})
  @IsOptional()
  posicion: NoticiaPosicion;

  @ApiModelProperty()
  @IsNotEmpty()
  @Length(1, 60)
  @IsString()
  titulo: string;

  @ApiModelProperty()
  @IsOptional()
  @IsString()
  descripcion: string;

  @ApiModelProperty()
  @IsOptional() 
  @Length(1, 100)
  @IsString()
  resumen: string;

  @ApiModelProperty()
  @IsNotEmpty()
  imagen: Express.Multer.File;

  @ApiModelProperty({type:Persona})
  @IsNotEmpty()
  persona: Persona;
}
