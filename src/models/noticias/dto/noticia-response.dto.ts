import { ApiModelProperty } from "@nestjs/swagger";
import { Persona } from "../../persona/persona.entity";
import { NoticiaPosicion } from "../../../enums/noticia.posicion.enum";
import { NoticiaTipo } from "../../../enums/noticia.tipo.enum";

export class NoticiaResponseDTO {
  @ApiModelProperty()
  id: number;

  @ApiModelProperty({ enum:NoticiaTipo })
  tipo: NoticiaTipo;

  @ApiModelProperty({
    enum: NoticiaPosicion,
    default: NoticiaPosicion.ONE
  })
  posicion: NoticiaPosicion;

  @ApiModelProperty({ type:Date })
  fechaPublicacion: Date;

  @ApiModelProperty()
  titulo: string;

  @ApiModelProperty()
  descripcion: string;

  @ApiModelProperty()
  resumen: string;

  @ApiModelProperty()
  imagen: string;

  @ApiModelProperty({ type:Persona })
  persona: Persona;
}