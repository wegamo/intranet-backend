import {
  Entity,
  Column,
  ManyToOne,
  JoinColumn,
  CreateDateColumn,
  PrimaryGeneratedColumn,
  OneToMany
} from 'typeorm';
import { NoticiaTipo } from '../../enums/noticia.tipo.enum';
import { NoticiaPosicion } from '../../enums/noticia.posicion.enum';
import { Persona } from '../persona/persona.entity';
import { Comentario } from '../comentario/comentario.entity';

@Entity()
export class Noticia {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: 'enum',
    enum: NoticiaTipo,
    default: NoticiaTipo.NORMAL
  })
  tipo: NoticiaTipo;

  @Column({
    type: 'enum',
    enum: NoticiaPosicion,
    default: NoticiaPosicion.ONE
  })
  posicion: NoticiaPosicion;

  @CreateDateColumn({ type: 'timestamp', name: 'fecha_publicacion' })
  fechaPublicacion: Date;

  @Column({ length: 100 })
  titulo: string;

  @Column({ nullable: true })
  descripcion: string;

  @Column({ nullable: true })
  resumen: string;

  @Column({ nullable: true })
  imagen: string;

  @ManyToOne((type) => Persona, (persona) => persona.noticias, {
    nullable: true,
    onDelete: 'SET NULL'
  })
  @JoinColumn({ name: 'fk_persona_id' })
  persona: Persona;

  @OneToMany(() => Comentario, (comentario) => comentario.noticia, {
    nullable: false,
    cascade: ['insert', 'update', 'remove']
  })
  comentarios?: Comentario[];
}
