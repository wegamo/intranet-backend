import { NoticiaTipo } from '../../enums/noticia.tipo.enum';

export class GuardarNoticia {
  id: string;
  tipo: NoticiaTipo;
  posicion?: number;
  titulo: string;
  descripcion?: string;
  fecha: string;
  mediaExt?: string;
  imagen?: string;
}
