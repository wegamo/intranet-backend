import { TipoGrado } from '../../enums/tipo-grado.enum';
import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Persona } from '../persona/persona.entity';

@Entity()
export class Curso {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'enum', enum: TipoGrado, default: TipoGrado.CURSO, name: 'tipo_grado' })
  tipoGrado: TipoGrado;

  @Column({ type: 'varchar', length: 147, nullable: false })
  nombre: string;

  @Column({ type: 'varchar', length: 147, nullable: false })
  institucion: string;

  @Column({ type: 'varchar', length: 147, nullable: false })
  ciudad: string;

  @Column({ type: 'varchar', length: 147, nullable: false })
  pais: string;

  @Column({ type: 'varchar', nullable: true, default: null })
  certificado: string;

  @Column({ type: 'timestamp', nullable: false, name: 'fecha_inicio' })
  fechaInicio: Date;

  @Column({ type: 'timestamp', nullable: false, name: 'fecha_fin' })
  fechaFin: Date;

  @ManyToOne((type) => Persona, (persona) => persona.cursos, {
    onDelete: 'CASCADE'
  })
  @JoinColumn({ name: 'fk_persona_id' })
  persona: Persona;
}
