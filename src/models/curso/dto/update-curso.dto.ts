import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';
import { TipoGrado } from '../../../enums/tipo-grado.enum';
import { Persona } from '../../persona/persona.entity';

export class UpdateCursoDTO {
  @IsOptional()
  @ApiModelProperty({ required: false })
  readonly tipoGrado?: TipoGrado;

  @IsOptional()
  @ApiModelProperty({ required: false, type: 'string' })
  readonly nombre?: string;

  @IsOptional()
  @ApiModelProperty({ required: false, type: 'string' })
  readonly institucion?: string;

  @IsOptional()
  @ApiModelProperty({ required: false, type: 'string' })
  readonly ciudad?: string;

  @IsOptional()
  @ApiModelProperty({ required: false, type: 'string' })
  readonly pais?: string;

  @IsOptional()
  @ApiModelProperty({ required: false, type: 'string' })
  readonly certificado?: string;

  @IsOptional()
  @ApiModelProperty({ required: false, type: 'timestamp' })
  readonly fechaInicio?: Date;

  @IsOptional()
  @ApiModelProperty({ required: false, type: 'timestamp' })
  readonly fechaFin?: Date;
}
