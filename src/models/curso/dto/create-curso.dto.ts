import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { TipoGrado } from '../../../enums/tipo-grado.enum';
import { Persona } from '../../persona/persona.entity';

export class CreateCursoDTO {
  @IsNotEmpty()
  @ApiModelProperty({ required: true })
  readonly tipoGrado: TipoGrado;

  @IsNotEmpty()
  @ApiModelProperty({ required: true, type: 'string' })
  readonly nombre: string;

  @IsNotEmpty()
  @ApiModelProperty({ required: true, type: 'string' })
  readonly institucion: string;

  @IsNotEmpty()
  @ApiModelProperty({ required: true, type: 'string' })
  ciudad: string;

  @IsNotEmpty()
  @ApiModelProperty({ required: true, type: 'string' })
  pais: string;

  @IsNotEmpty()
  @ApiModelProperty({ required: true, type: 'timestamp' })
  fechaInicio: Date;

  @IsNotEmpty()
  @ApiModelProperty({ required: true, type: 'timestamp' })
  fechaFin: Date;

  @IsNotEmpty()
  @ApiModelProperty({ required: true, type: 'number' })
  persona: Persona;
}
