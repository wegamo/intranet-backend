import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';
import { TipoDocumentoPersonal } from '../../tipo-documento-personal/tipoDocumentoPersonal.entity';
import { Persona } from '../../persona/persona.entity';
import { DocumentoPersonalEstatus } from '../../../enums/documento-personal-estatus.enum';

export class CreateDocumentoPersonalDTO {
  @IsOptional()
  @ApiModelProperty({ required: false })
  readonly estatus?: DocumentoPersonalEstatus;

  @IsOptional()
  @ApiModelProperty({ required: false })
  readonly nombre?: string;

  @IsOptional()
  @ApiModelProperty({ required: false })
  readonly requerido?: string;

  @IsOptional()
  @ApiModelProperty({ required: false })
  readonly tipoDocumentoPersonal?: TipoDocumentoPersonal;

  @IsNotEmpty()
  @ApiModelProperty({ required: true })
  readonly persona: Persona;
}
