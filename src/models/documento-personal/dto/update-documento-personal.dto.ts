import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';
import { DocumentoPersonalEstatus } from '../../../enums/documento-personal-estatus.enum';

export class UpdateDocumentoPersonalDTO {
  @IsOptional()
  @ApiModelProperty()
  archivo?: string;

  @IsNotEmpty()
  @ApiModelProperty()
  estatus: DocumentoPersonalEstatus;
}
