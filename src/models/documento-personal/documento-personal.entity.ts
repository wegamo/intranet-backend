import { Column, Entity, PrimaryGeneratedColumn, ManyToOne, JoinColumn, Unique } from 'typeorm';
import { TipoDocumentoPersonal } from '../tipo-documento-personal/tipoDocumentoPersonal.entity';
import { Persona } from '../persona/persona.entity';
import { DocumentoPersonalEstatus } from '../../enums/documento-personal-estatus.enum';

@Unique(['persona', 'tipoDocumentoPersonal'])
@Entity()
export class DocumentoPersonal {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: true, default: null })
  archivo?: string;

  @Column({
    type: 'enum',
    enum: DocumentoPersonalEstatus,
    default: DocumentoPersonalEstatus.PENDIENTE
  })
  estatus: DocumentoPersonalEstatus;

  @ManyToOne((type) => Persona, (persona) => persona.documentosPersonales, {
    nullable: false,
    onDelete: 'CASCADE'
  })
  @JoinColumn({ name: 'fk_persona_id' })
  persona: Persona;

  @ManyToOne((type) => TipoDocumentoPersonal, (documento) => documento.documentosPersonales, {
    nullable: false
  })
  @JoinColumn({ name: 'fk_tipo_doc_personal_id' })
  tipoDocumentoPersonal: TipoDocumentoPersonal;
}
