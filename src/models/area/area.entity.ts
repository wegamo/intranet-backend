import { EmpresaAreaEstatus } from '../../enums/empresa-area.enum';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Cargo } from '../cargo/cargo.entity';
import { Publicacion } from '../publicacion/publicacion.entity';

@Entity()
export class Area {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 128, nullable: false })
  nombre: string;

  @Column({ type: 'varchar', length: 256, nullable: false })
  descripcion: string;

  @Column({
    type: 'enum',
    enum: EmpresaAreaEstatus,
    default: EmpresaAreaEstatus.ACTIVA
  })
  estatus?: EmpresaAreaEstatus;

  //ESPACIO DE DATOS ?
  @Column({ type: 'varchar', nullable: true })
  espacioAws?: string;
  //IMG ?
  @Column({ type: 'varchar', nullable: true })
  img?: string;

  @OneToMany((type) => Cargo, (cargo) => cargo.area, {
    nullable: true,
    cascade: ['insert', 'update']
  })
  cargos?: Cargo[];

  @OneToMany(() => Publicacion, (publicacion) => publicacion.area, {
    nullable: true,
    cascade: ['insert', 'update', 'remove']
  })
  publicaciones?: Publicacion[];
}
