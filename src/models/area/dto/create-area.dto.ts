import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class CreateAreaDTO {
  @IsNotEmpty()
  @ApiModelProperty()
  readonly nombre: string;

  @IsNotEmpty()
  @ApiModelProperty()
  readonly descripcion: string;
}