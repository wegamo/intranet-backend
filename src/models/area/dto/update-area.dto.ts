import { ApiModelProperty } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';

export class UpdateAreaDTO {
  @IsOptional()
  @ApiModelProperty({required:false})
  readonly nombre?: string;

  @IsOptional()
  @ApiModelProperty({ required:false })
  readonly descripcion?: string;

  @IsOptional()
  @ApiModelProperty({ required:false })
  readonly img?: string;
}