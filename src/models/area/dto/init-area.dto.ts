import { ApiModelProperty } from "@nestjs/swagger";
import { CreateAreaDTO } from "./create-area.dto";
import { UpdateAreaDTO } from "./update-area.dto";

export class InitAreaDTO {
    @ApiModelProperty()
    data:CreateAreaDTO;
    @ApiModelProperty()
    user:number;
}

export class UpdAreaDTO {
    @ApiModelProperty()
    data:UpdateAreaDTO;
    @ApiModelProperty()
    user:number;
}