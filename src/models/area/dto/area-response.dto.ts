import { ApiModelProperty } from "@nestjs/swagger";
import { EmpresaAreaEstatus } from "../../../enums/empresa-area.enum";

export class AreaResponseDTO {
  @ApiModelProperty()
  id: number;
  @ApiModelProperty()
  nombre: string;
  @ApiModelProperty()
  descripcion: string;
  @ApiModelProperty({enum: EmpresaAreaEstatus})
  estatus: EmpresaAreaEstatus;
  @ApiModelProperty()
  espacioAws?: string;
  @ApiModelProperty()
  img?: string;
}