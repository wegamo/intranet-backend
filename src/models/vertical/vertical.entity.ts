import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Persona } from '../persona/persona.entity';
import { EmpresaAreaEstatus } from '../../enums/empresa-area.enum';
import { Publicacion } from '../publicacion/publicacion.entity';

@Entity()
export class Vertical {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 60, nullable: false })
  nombre: string;

  @Column({ type: 'varchar', nullable: false })
  descripcion: string;

  @Column({
    type: 'enum',
    enum: EmpresaAreaEstatus,
    default: EmpresaAreaEstatus.ACTIVA
  })
  estatus: EmpresaAreaEstatus;

  @Column({ type: 'varchar', nullable: true })
  espacioAws: string;

  @Column({ type: 'varchar', nullable: true })
  img: string;

  @Column({ type: 'varchar', nullable: true })
  imgSvg: string;

  @OneToMany(() => Publicacion, (publicacion) => publicacion.vertical, {
    nullable: true,
    cascade: ['insert', 'update', 'remove']
  })
  publicaciones?: Publicacion[];
}
