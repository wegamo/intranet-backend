import { ApiModelProperty } from "@nestjs/swagger";
import { CreateVerticalDTO } from "./create-vertical.dto";

export class InitVerticalDTO {
    @ApiModelProperty()
    data:CreateVerticalDTO;
    @ApiModelProperty()
    user:number;
}