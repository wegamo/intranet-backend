import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';
export class CreateVerticalDTO {
  @IsNotEmpty()
  @ApiModelProperty()
  readonly nombre: string;
  @IsNotEmpty()
  @ApiModelProperty()
  readonly descripcion: string;
}
