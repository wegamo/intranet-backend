import { ApiModelProperty } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';

export class UpdateVerticalDTO {
  @IsOptional()
  @ApiModelProperty()
  readonly nombre?: string;

  @IsOptional()
  @ApiModelProperty()
  readonly descripcion?: string;
  
  @IsOptional()
  @ApiModelProperty()
  readonly img?: string;

  @IsOptional()
  @ApiModelProperty()
  readonly imgSvg?: string;
}