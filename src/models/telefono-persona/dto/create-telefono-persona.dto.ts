import { ApiModelProperty } from "@nestjs/swagger";
import { IsNotEmpty } from "class-validator";
import { Persona } from "../../persona/persona.entity";

export class CreateTelefonoPersonaDTO {
  @IsNotEmpty()
  @ApiModelProperty({ required: true, type: 'string' })
  readonly numero: string;

  @IsNotEmpty()
  @ApiModelProperty({ required: true, type: 'number' })
  readonly persona: Persona;
}