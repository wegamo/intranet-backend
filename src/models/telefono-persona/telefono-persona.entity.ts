import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Persona } from '../persona/persona.entity';

@Entity()
export class TelefonoPersona {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  numero: string;

  @ManyToOne((type) => Persona, (persona) => persona.telefonos, {
    nullable: false,
    onDelete: 'CASCADE'
  })
  @JoinColumn({ name: 'fk_persona_id' })
  persona: Persona;
}
