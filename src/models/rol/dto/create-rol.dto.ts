import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';

export class CreateRolDTO {
  @IsNotEmpty()
  @ApiModelProperty()
  readonly nombre: string;

  /* @IsNotEmpty()
  @ApiModelProperty({ required: true, type: 'number' })
  readonly privilegios?: RolPrivilegio[] | number[];

  @IsNotEmpty()
  @ApiModelProperty({ required: true, type: 'number' })
  readonly usuarios?: Usuario[] | number[];*/
}
