import { IsOptional } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class UpdateRolDTO {
  @IsOptional()
  @ApiModelProperty()
  readonly nombre?: string;

  /*  @IsOptional()
  @ApiModelProperty({ required: false, type: 'number' })
  readonly privilegios?: RolPrivilegio[] | number[];

  @IsOptional()
  @ApiModelProperty({ required: false, type: 'number' })
  readonly usuarios?: Usuario[] | number[];*/
}
