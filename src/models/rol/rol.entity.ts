import { Column, Entity, OneToMany, PrimaryGeneratedColumn, JoinTable } from 'typeorm';
import { Persona } from '../persona/persona.entity';

@Entity()
export class Rol {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  nombre: string;

  @OneToMany((type) => Persona, (persona) => persona.rol, {
    nullable: false,
    cascade: ['insert']
  })
  personas?: Persona[];
}
