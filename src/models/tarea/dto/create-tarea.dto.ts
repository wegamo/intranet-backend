import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';

export class CreateTareaDTO {
  @IsNotEmpty()
  @ApiModelProperty()
  readonly texto: string;
}
