import { Column, Entity, OneToMany, PrimaryGeneratedColumn, JoinTable, ManyToOne } from 'typeorm';
import { Persona } from '../persona/persona.entity';

@Entity()
export class Tarea {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  texto: string;

  @ManyToOne((type) => Persona, (persona) => persona.tareas, {
    nullable: false,
    onDelete: 'CASCADE'
  })
  persona?: Persona;
}
