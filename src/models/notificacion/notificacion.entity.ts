import { TipoNotificacion } from '../../enums/tipo-notificacion.enum';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn
} from 'typeorm';

import { Persona } from '../persona/persona.entity';

@Entity()
export class Notificacion {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: false })
  mensaje: string;

  @Column({ nullable: true })
  accion: string;

  @Column({ nullable: false })
  tipo: TipoNotificacion;

  @ManyToOne((type) => Persona, (persona) => persona.notificaciones, {
    nullable: false,
    onDelete: 'CASCADE'
  })
  @JoinColumn({ name: 'fk_persona_id' })
  persona: Persona;

  @CreateDateColumn({ type: 'timestamp', name: 'fecha_publicacion' })
  fechaPublicacion: Date;
}
