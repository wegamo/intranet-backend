import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional, IsPositive, IsString } from 'class-validator';
import { TipoNotificacion } from '../../../enums/tipo-notificacion.enum';
import { Noticia } from '../../noticias/noticia.entity';
import { Persona } from '../../persona/persona.entity';

export class CreateNotificacionDTO {
  @IsNotEmpty()
  @IsString()
  @ApiModelProperty()
  readonly mensaje: string;

  @ApiModelProperty()
  readonly persona: Persona;

  @IsNotEmpty()
  @ApiModelProperty()
  readonly accion: string;

  @IsNotEmpty()
  @ApiModelProperty()
  readonly tipo: TipoNotificacion;
}
