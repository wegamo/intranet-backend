import { ApiModelProperty } from "@nestjs/swagger";
import { IsNotEmpty } from "class-validator";
import { TipoPermiso } from "../../../enums/tipo-permiso.enum";
import { Persona } from "../../persona/persona.entity";
import { Publicacion } from '../../publicacion/publicacion.entity';

export class UpdateUsuarioPermisoDTO {
    @IsNotEmpty()
    @ApiModelProperty({ enum:TipoPermiso })
    readonly tipoPermiso: TipoPermiso;

    @IsNotEmpty()
    @ApiModelProperty()
    readonly persona: Persona;

    @IsNotEmpty()
    @ApiModelProperty()
    readonly publicacion: Publicacion;
}