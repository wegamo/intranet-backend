import { TipoPermiso } from "../../../enums/tipo-permiso.enum";
import { ApiModelProperty } from '@nestjs/swagger';

export class InstancePermisoDTO {
    @ApiModelProperty()
    userId:number;
    @ApiModelProperty({ enum:TipoPermiso })
    tipoPermiso:TipoPermiso;
    @ApiModelProperty()
    fileId:number;
}