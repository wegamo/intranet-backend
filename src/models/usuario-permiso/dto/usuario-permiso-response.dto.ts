import { ApiModelProperty } from "@nestjs/swagger";
import { IsNotEmpty } from "class-validator";
import { PublicacionResponseDTO } from "../../publicacion/dto/publicacion-response.dto";
import { TipoPermiso } from "../../../enums/tipo-permiso.enum";
import { Persona } from "../../persona/persona.entity";
import { Publicacion } from '../../publicacion/publicacion.entity';

export class UsuarioPermisoResponseDTO {
    @ApiModelProperty()
    readonly id: number;

    @ApiModelProperty()
    readonly persona: Persona;

    @ApiModelProperty()
    readonly publicacion: PublicacionResponseDTO;

    @ApiModelProperty({ enum:TipoPermiso })
    readonly tipoPermiso: TipoPermiso;
}