import { TipoPermiso } from '../../enums/tipo-permiso.enum';
import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Persona } from '../persona/persona.entity';
import { Publicacion } from '../publicacion/publicacion.entity';

@Entity()
export class UsuarioPermiso {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: 'enum',
    enum: TipoPermiso,
    default: TipoPermiso.NONE
  })
  tipoPermiso: TipoPermiso;

  @ManyToOne((type) => Persona, (persona) => persona.permisos, {
    nullable: false,
    onDelete: 'CASCADE'
  })
  @JoinColumn({ name: 'fk_persona_id' })
  persona: Persona;

  @ManyToOne((type) => Publicacion, (publicacion) => publicacion.permisos, { nullable: false })
  @JoinColumn({ name: 'fk_publicacion_id' })
  publicacion: Publicacion;
}
