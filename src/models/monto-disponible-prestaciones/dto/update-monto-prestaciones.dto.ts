import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { SolicitudEstatus } from '../../../enums/solicitud-estatus.enum';

export class UpdateMontoPrestacionesDTO {
  @IsNotEmpty()
  @ApiModelProperty()
  readonly fechaInicio: Date;

  @IsNotEmpty()
  @ApiModelProperty()
  readonly fechaFin: Date;

  @IsNotEmpty()
  @ApiModelProperty()
  readonly estatus: SolicitudEstatus;

  @IsNotEmpty()
  @ApiModelProperty()
  readonly montoDisponible: string;
}
