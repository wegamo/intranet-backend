import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { SolicitudEstatus } from '../../../enums/solicitud-estatus.enum';
import { Persona } from '../../persona/persona.entity';

export class CreateMontoPrestacionesDTO {
    @IsNotEmpty()
    @ApiModelProperty()
    fechaSolicitud: Date;

    @IsNotEmpty()
    @ApiModelProperty()
    estatus: SolicitudEstatus;

    @IsNotEmpty()
    @ApiModelProperty()
    persona: Persona;
}   
