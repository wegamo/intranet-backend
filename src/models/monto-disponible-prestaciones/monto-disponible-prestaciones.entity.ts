import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  CreateDateColumn
} from 'typeorm';
import { Persona } from '../persona/persona.entity';
import { SolicitudEstatus } from '../../enums/solicitud-estatus.enum';

@Entity()
export class MontoPrestaciones {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'timestamp', nullable: true, default: null, name: 'fecha_inicio' })
  fechaInicio?: Date;

  @Column({ type: 'timestamp', nullable: true, default: null, name: 'fecha_fin' })
  fechaFin?: Date;

  @CreateDateColumn({ type: 'timestamp', name: 'fecha_solicitud' })
  fechaSolicitud: Date;

  @Column({ type: 'enum', enum: SolicitudEstatus, default: SolicitudEstatus.PENDIENTE })
  estatus: SolicitudEstatus;

  @Column({ nullable: true, default: null, name: 'monto_disponible' })
  montoDisponible?: string;

  @ManyToOne((type) => Persona, (persona) => persona.montoPresDisponible, {
    nullable: false,
    onDelete: 'CASCADE'
  })
  @JoinColumn({ name: 'fk_persona_id' })
  persona: Persona;
}
