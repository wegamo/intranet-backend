import { Column, Entity, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { Ausencia } from '../ausencia/ausencia.entity';

@Entity()
export class TipoDescuento {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 255, nullable: false })
  nombre: string;

  @OneToMany(() => Ausencia, (ausencia) => ausencia.tipoDescuento)
  ausencias: Ausencia[];
}
