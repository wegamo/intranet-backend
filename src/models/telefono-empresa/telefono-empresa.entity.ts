import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Empresa } from '../empresa/empresa.entity';

@Entity()
export class TelefonoEmpresa {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  numero: string;

  @ManyToOne(type => Empresa, (empresa) => empresa.telefonos, { nullable:false })
  @JoinColumn({ name: 'fk_empresa_id' })
  empresa: Empresa;
}
