import { ApiModelProperty } from "@nestjs/swagger";
import { IsNotEmpty } from "class-validator";
import { Empresa } from "../../empresa/empresa.entity";

export class CreateTelefonoEmpresaDTO {
  @IsNotEmpty()
  @ApiModelProperty({ required: true, type: 'string' })
  readonly numero: string;

  @IsNotEmpty()
  @ApiModelProperty({ required: true, type: 'number' })
  readonly empresa: Empresa;
}