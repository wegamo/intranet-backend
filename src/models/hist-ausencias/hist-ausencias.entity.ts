import {
  Column,
  Entity,
  Index,
  JoinColumn,
  CreateDateColumn,
  PrimaryGeneratedColumn,
  ManyToOne
} from 'typeorm';
import { Persona } from '../persona/persona.entity';
import { Solicitud } from '../solicitud/solicitud.entity';
import { SolicitudEstatus } from '../../enums/solicitud-estatus.enum';
import { Ausencia } from '../ausencia/ausencia.entity';
import { MotivoAusencia } from '../motivo-ausencia/motivoAusencia.entity';
import { TipoDescuento } from '../tipo-descuento/tipoDescuento.entity';
@Entity()
export class HistAusencias {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'timestamp', nullable: true, default: null })
  fechaRevision: Date;

  @Column({ type: 'varchar', nullable: true, default: null, name: 'motivo_rechazo' })
  motivoRechazo?: string;

  @CreateDateColumn({ type: 'timestamp', name: 'fecha_solicitud' })
  fechaSolicitud: Date;

  @Column({ type: 'timestamp', nullable: false, name: 'fecha_inicio' })
  fechaInicio: Date;

  @Column({ type: 'timestamp', nullable: false, name: 'fecha_fin' })
  fechaFin: Date;

  @Column({ nullable: true, default: null, name: 'firma_solicitante' })
  firmaSolicitante: string;

  @Column({ nullable: true, default: null, name: 'firma_th' })
  firmaTh?: string;

  @Column({ nullable: true, default: null, name: 'firma_supervisor' })
  firmaSupervisor?: string;

  @Column({ type: 'enum', enum: SolicitudEstatus, default: SolicitudEstatus.PENDIENTE })
  estatus: SolicitudEstatus;

  @ManyToOne((type) => Ausencia, (ausencia) => ausencia.histAusencias, { nullable: false })
  @JoinColumn({ name: 'fk_ausencia_id' })
  ausencia: Ausencia;

  @ManyToOne((type) => Persona, (persona) => persona.histRevAusTH, {
    nullable: true
  })
  @JoinColumn({ name: 'fk_th_id' })
  talentoHumano?: Persona;

  @ManyToOne((type) => Persona, (persona) => persona.histAusencias, {
    nullable: false,
    onDelete: 'CASCADE'
  })
  @JoinColumn({ name: 'fk_persona_id' })
  persona: Persona;

  @ManyToOne((type) => Persona, (persona) => persona.histRevAusSupervisor, { nullable: false })
  @JoinColumn({ name: 'fk_supervisor_id' })
  supervisor: Persona;
}
