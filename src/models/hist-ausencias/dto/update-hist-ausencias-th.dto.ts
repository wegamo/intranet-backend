import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';
import { SolicitudEstatus } from '../../../enums/solicitud-estatus.enum';
import { Persona } from '../../persona/persona.entity';

export class UpdateHistAusenciasThDTO {
  @IsNotEmpty()
  @ApiModelProperty()
  readonly fechaRevision: Date;

  @IsOptional()
  @ApiModelProperty()
  readonly motivoRechazo?: string;

  @IsNotEmpty()
  @ApiModelProperty()
  talentoHumano: Persona;

  @IsNotEmpty()
  @ApiModelProperty()
  readonly estatus: SolicitudEstatus;
}
