import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';
import { SolicitudEstatus } from '../../../enums/solicitud-estatus.enum';
import { Persona } from '../../persona/persona.entity';

export class UpdateHistAusenciasSupervisorDTO {
  @IsNotEmpty()
  @ApiModelProperty()
  readonly fechaRevision: Date;

  @IsNotEmpty()
  @ApiModelProperty()
  readonly estatus: SolicitudEstatus;

  @IsOptional()
  @ApiModelProperty()
  readonly motivoRechazo?: string;
}
