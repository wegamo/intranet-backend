import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';
import { TipoDescuento } from '../../tipo-descuento/tipoDescuento.entity';
import { MotivoAusencia } from '../../motivo-ausencia/motivoAusencia.entity';
import { Persona } from '../../persona/persona.entity';
import { Solicitud } from '../../solicitud/solicitud.entity';
import { Ausencia } from '../../ausencia/ausencia.entity';

export class CreateHistAusenciasDTO {
  @IsNotEmpty()
  @ApiModelProperty()
  readonly fechaInicio: Date;

  @IsNotEmpty()
  @ApiModelProperty()
  readonly fechaFin: Date;

  @IsNotEmpty()
  @ApiModelProperty()
  readonly motivo: MotivoAusencia;

  @IsNotEmpty()
  @ApiModelProperty({ required: true })
  readonly solicitud: Solicitud;

  @IsNotEmpty()
  @ApiModelProperty({ required: true })
  readonly tipoDescuento: TipoDescuento;

  @IsNotEmpty()
  @ApiModelProperty({ required: true })
  readonly persona: Persona;

  @IsNotEmpty()
  @ApiModelProperty({ required: true })
  readonly supervisor: Persona;

  @IsNotEmpty()
  @ApiModelProperty({ required: true })
  ausencia: Ausencia;
}
