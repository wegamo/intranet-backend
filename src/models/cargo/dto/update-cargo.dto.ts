import { IsOptional, IsPositive } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class UpdateCargoDTO {
  @IsOptional()
  @ApiModelProperty()
  readonly nombre?: string;

  @IsOptional()
  @ApiModelProperty()
  readonly descripcion?: string;
}
