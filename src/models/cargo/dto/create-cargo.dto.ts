import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional, IsPositive } from 'class-validator';
import { Area } from '../../area/area.entity';

export class CreateCargoDTO {
  @IsNotEmpty()
  @ApiModelProperty()
  readonly nombre: string;

  @IsNotEmpty()
  @ApiModelProperty()
  readonly area: Area;

  @IsOptional()
  @ApiModelProperty()
  readonly descripcion?: string;
}
