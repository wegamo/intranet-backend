import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Area } from '../area/area.entity';
import { HistCargo } from '../hist-cargos/hist-cargos.entity';

@Entity()
export class Cargo {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 147, nullable: false })
  nombre: string;

  @Column({ type: 'varchar', nullable: false })
  responsabilidades: string;

  @ManyToOne((type) => Cargo, (cargo) => cargo.childCargos)
  @JoinColumn({ name: 'fk_parent_cargo_id' })
  parentCargo?: Cargo;

  @OneToMany((type) => Cargo, (cargo) => cargo.parentCargo, {
    nullable: true,
    cascade: ['insert', 'update']
  })
  childCargos?: Cargo[];

  @ManyToOne((type) => Area, (area) => area.cargos)
  @JoinColumn({ name: 'fk_area_id' })
  area: Area;

  @OneToMany((type) => HistCargo, (histCargo) => histCargo.cargo, {
    nullable: true,
    cascade: ['insert', 'update']
  })
  histCargos?: HistCargo[];
}
