import {
  Column,
  Entity,
  JoinColumn,
  CreateDateColumn,
  PrimaryGeneratedColumn,
  ManyToOne
} from 'typeorm';
import { Persona } from '../persona/persona.entity';
@Entity()
export class HistDiasDispVacaciones {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'timestamp', name: 'fecha_solicitud' })
  fechaSolicitud: Date;

  @Column({ nullable: false, name: 'dias_disponibles' })
  diasDisponibles: number;

  @ManyToOne((type) => Persona, (persona) => persona.histDiasDispVacaciones, {
    nullable: false,
    onDelete: 'CASCADE'
  })
  @JoinColumn({ name: 'fk_persona_id' })
  persona: Persona;
}
