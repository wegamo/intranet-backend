import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';
import { Persona } from '../../persona/persona.entity';

export class CreateHistDiasDispVacacionesDTO {
  @IsNotEmpty()
  @ApiModelProperty({ required: true })
  readonly fechaSolicitud: Date;

  @IsNotEmpty()
  @ApiModelProperty({ required: true })
  readonly diasDisponibles: number;

  @IsNotEmpty()
  @ApiModelProperty({ required: true })
  readonly persona: Persona;
}
