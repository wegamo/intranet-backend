import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Ausencia } from '../ausencia/ausencia.entity';
import { HistPrestaciones } from '../hist-prestaciones/hist-prestaciones.entity';
import { HistVacaciones } from '../hist-vacaciones/hist-vacaciones.entity';

@Entity()
export class Solicitud {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'bool', nullable: false, name: 'requiere_aprobacion' })
  requiereAprobacion: boolean;

  @Column({ type: 'varchar', length: 256, nullable: false })
  descripcion: string;

  @OneToMany(() => Ausencia, (ausencia) => ausencia.solicitud, { nullable: true })
  ausencias?: Ausencia[];

  @OneToMany((type) => HistPrestaciones, (histPrestaciones) => histPrestaciones.solicitud, {
    nullable: true,
    cascade: ['insert', 'update']
  })
  histPrestaciones?: HistPrestaciones[];

  @OneToMany((type) => HistVacaciones, (histVacaciones) => histVacaciones.solicitud, {
    nullable: true,
    cascade: ['insert', 'update']
  })
  histVacaciones?: HistVacaciones[];
}
