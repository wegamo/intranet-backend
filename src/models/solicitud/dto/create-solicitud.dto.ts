import { ApiModelProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsOptional } from "class-validator";

export class CreateSolicitudDTO {
   
  @IsNotEmpty()
  @ApiModelProperty({ required: true, type: 'boolean' })
  approval: boolean;

  @IsNotEmpty()
  @ApiModelProperty({ required: true, type: 'string' })
  descripcion: string;

  @IsOptional()
  @ApiModelProperty({ required: true, type: 'boolean' })
  tipo_descuento: boolean;

  @IsOptional()
  @ApiModelProperty({ required: true, type: 'boolean' })
  tipo_ausencia: boolean;
  /*
  @IsOptional()
  @ApiModelProperty({ required: true, type: 'number' })
  histAusencia?: HistAusencia;

  @IsOptional()
  @ApiModelProperty({ required: true, type: 'number' })
  histPrestaciones?: HistPrestaciones;

  @IsOptional()
  @ApiModelProperty({ required: true, type: 'number' })
  histVacaciones?: HistVacaciones;
  */
    
}