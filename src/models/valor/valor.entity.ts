import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
@Entity()
export class Valor {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 147, nullable: false })
  nombre: string;

  @Column({ type: 'varchar', length: 255, nullable: false })
  descripcion: string;
}
