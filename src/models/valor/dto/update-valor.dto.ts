import { ApiModelProperty } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';

export class UpdateValorDTO {
  @IsOptional()
  @ApiModelProperty()
  readonly nombre?: string;

  @IsOptional()
  @ApiModelProperty()
  readonly descripcion?: string;

  /*@IsOptional()
  @ApiModelProperty({ required: false, type: 'number' })
  readonly empresa?: Empresa;*/
}
