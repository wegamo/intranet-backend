import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
export class CreateValorDTO {
  @IsNotEmpty()
  @ApiModelProperty()
  readonly nombre: string;

  @IsNotEmpty()
  @ApiModelProperty()
  readonly descripcion: string;

  /*@IsNotEmpty()
  @ApiModelProperty({ required: true, type: 'number' })
  readonly empresa: Empresa;*/
}
