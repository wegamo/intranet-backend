import { Injectable, CanActivate, ExecutionContext, UnauthorizedException } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Persona } from '../models/persona/persona.entity';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private reflector: Reflector) {}

  canActivate(context: ExecutionContext): boolean {
    const roles = this.reflector.get<string[]>('roles', context.getHandler());
    if (!roles) {
      return true;
    }
    const request = context.switchToHttp().getRequest();
    const user: Persona = request.user;

    if (!this.matchRoles(roles, user.rol.nombre)) {
      throw new UnauthorizedException();
    }
    return true;
  }

  matchRoles(roles: string[], role: string): boolean {
    return roles.includes(role);
  }
}
