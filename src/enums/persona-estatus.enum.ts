export enum PersonaEstatus {
  ACTIVO = 'activo',
  INACTIVO = 'inactivo'
}
