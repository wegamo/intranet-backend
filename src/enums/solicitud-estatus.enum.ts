//RETORNA UNDEFINED
export enum SolicitudEstatus {
  APROBADA = 'aprobada',
  APROBADA_SUPERVISOR = 'aprobada_supervisor',
  RECHAZADA = 'rechazada',
  PENDIENTE = 'pendiente'
}
