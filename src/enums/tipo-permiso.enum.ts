export enum TipoPermiso {
    READ = 'R',
    READWRITE = 'WR',
    NONE = 'N',
}