export enum userRole {
  SUPER_ADMIN = 'superadmin',
  ADMIN = 'admin',
  USER = 'user'
}
