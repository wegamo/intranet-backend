export enum TipoGrado {
  MASTER = 'master',
  DOCTORADO = 'doctorado',
  CURSO = 'curso',
  ESPECIALIZACION = 'especializacion',
  TALLER = 'taller'
}
