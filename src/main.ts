import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import * as express from 'express';
import * as cookieParser from 'cookie-parser';
import * as helmet from 'helmet';
import { join } from 'path';
import { AppModule } from './app.module';
import ConfigService from './config/config/config.service';
import { PostgreExceptionFilter } from './exceptions/postgre-exception.filter';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.setGlobalPrefix('api');
  //app.use(cookieParser(ConfigService.get('SECRET')));
  app.use(helmet());
  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      transform: true
    })
  );
  app.useGlobalFilters(new PostgreExceptionFilter());
  // serve static assets from the client/dist folder, change this to the correct path for your project
  app.use(express.static(join(process.cwd(), './static')));

  const documentationOptions = new DocumentBuilder()
    .setBasePath('api')
    .setTitle('Andromeda Intranet Backend API')
    .setDescription('Backend made with NestJS for Andromeda Intranet')
    .setVersion('1.0')
    .build();

  const document = SwaggerModule.createDocument(app, documentationOptions);
  SwaggerModule.setup('docs', app, document);

  const corsOptions: import('@nestjs/common/interfaces/external/cors-options.interface').CorsOptions = {
    credentials: true,
    origin: '*',
    //origin: ConfigService.get('FRONTEND_URL'),
    methods: 'GET, HEAD, PUT, PATCH, POST, DELETE, OPTIONS',
    preflightContinue: false,
    optionsSuccessStatus: 200,
    allowedHeaders: ['Accept', 'Content-Type', 'Authorization']
  };

  app.enableCors(corsOptions);
  await app.listen(ConfigService.get('PORT'));
}
bootstrap();
