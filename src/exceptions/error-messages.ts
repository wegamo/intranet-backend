import { HttpStatus } from '@nestjs/common';

import { ErrorCollection, ErrorContent } from './error-messages.interface';

export const ERROR = Object.freeze<ErrorCollection>({
  DATABASE: Object.freeze<ErrorContent>({
    statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
    message: 'Internal Server Error: Problem with the database'
  }),
  DATABASE_FK: Object.freeze<ErrorContent>({
    statusCode: HttpStatus.BAD_REQUEST,
    message: 'Bad Request: Invalid payload content | DATABASE_FK '
  }),
  DATABASE_CHECK: Object.freeze<ErrorContent>({
    statusCode: HttpStatus.BAD_REQUEST,
    message: 'Bad Request: Invalid payload content | DATABASE_CHECK'
  })
});
