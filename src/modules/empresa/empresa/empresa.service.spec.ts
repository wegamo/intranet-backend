import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';

import { CreateEmpresaDTO } from '../../../models/empresa/dto/create-empresa.dto';
import { UpdateEmpresaDTO } from '../../../models/empresa/dto/update-empresa.dto';
import { Empresa } from '../../../models/empresa/empresa.entity';

import { EmpresaService } from './empresa.service';

import { SampleEmpresa } from './../../../../test/ObjectSamples';
import { EmpresaInt } from './../../dto_response/Empresa_Payload';
import { GlobalException } from './../../exception/global-exception';
import { TelefonoEmpresaService } from '../../telefono-empresa/telefono-empresa/telefono-empresa.service';
import { TelefonoEmpresa } from '../../../models/telefono-empresa/telefono-empresa.entity';

describe('EmpresaService', () => {
  let service: EmpresaService;
  let RepositoryMock: jest.Mock;
  let TelfRepositoryMock: jest.Mock;
  let repository: Repository<Empresa>;
  let getRawManySpy: jest.Mock;

  beforeEach(() => {
    const selectSpy = jest.fn().mockReturnThis();
    getRawManySpy = jest.fn().mockReturnThis();

    RepositoryMock = jest.fn(() => ({
      find: jest.fn(),
      findOne: jest.fn(),
      createQueryBuilder: jest.fn(() => ({
        select: selectSpy,
        getRawMany: getRawManySpy
      })),
      save: jest.fn(),
      update: jest.fn(),
      delete: jest.fn()
    }));

    TelfRepositoryMock = jest.fn(() => ({
      find: jest.fn(),
      save: jest.fn(),
      update: jest.fn(),
      delete: jest.fn()
    }));
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        EmpresaService,
        { provide: getRepositoryToken(Empresa), useValue: new RepositoryMock() },
        TelefonoEmpresaService,
        { provide: getRepositoryToken(TelefonoEmpresa), useValue: new TelfRepositoryMock() }
      ]
    }).compile();

    repository = module.get(getRepositoryToken(Empresa));
    service = module.get<EmpresaService>(EmpresaService);
  });

  describe('findAll()', () => {
    const {
      //estatus,
     // normas,
      telefonos,
     // valores,
      ...rest
    } = SampleEmpresa;
    let expectedEmpresas: Empresa[] | EmpresaInt[];
    let result: Empresa[] | EmpresaInt[];

    describe('case: success', () => {
      describe('when there are companies', () => {
        beforeEach(async () => {
          expectedEmpresas = [
            { 
              //estatus: estatus.nombre, 
              telefonos: [], 
              //normas: [], valores: [],
               ...rest }
          ];
          (repository.find as jest.Mock).mockResolvedValue(expectedEmpresas);
          result = await service.findAll();
        });

        it('should invoke repository.find()', () => {
          expect(repository.find).toHaveBeenCalledTimes(1);
        });

        it('should return an array of companies', () => {
          expect(result).toStrictEqual(expectedEmpresas);
        });
      });
    });

    describe('case: failure', () => {
      describe('when there are not companies', () => {
        beforeEach(async () => {
          expectedEmpresas = [];
          (repository.find as jest.Mock).mockResolvedValue(expectedEmpresas);
          result = await service.findAll();
        });

        it('should invoke repository.find()', () => {
          expect(repository.find).toHaveBeenCalledTimes(1);
        });

        it('should return an array empty', () => {
          expect(result).toStrictEqual(expectedEmpresas);
        });
      });
    });
  });

  describe('findOne(id)', () => {
    const {
     // estatus,
     // normas,
      telefonos,
     // valores,
      ...rest
    } = SampleEmpresa;
    let expectedEmpresa: Empresa | EmpresaInt;
    let result: Empresa | EmpresaInt;

    describe('case: success', () => {
      describe('when a company is found', () => {
        beforeEach(async () => {
          expectedEmpresa = { 
            //estatus: estatus.nombre, 
            telefonos: [], 
            //normas: [], valores: [],
             ...rest };
          (repository.findOne as jest.Mock).mockResolvedValue(expectedEmpresa);
          result = await service.findOne(1);
        });

        it('should invoke repository.findOne()', () => {
          expect(repository.findOne).toHaveBeenCalledTimes(1);
          expect(repository.findOne).toHaveBeenCalledWith({
            where: { id: 1 },
            relations: ['normas', 'telefonos', 'valores', 'childEmpresas']
          });
        });

        it('should return a company', () => {
          expect(result).toStrictEqual(expectedEmpresa);
        });
      });
    });

    describe('case: failure', () => {
      describe('when a company is not found', () => {
        beforeEach(async () => {
          expectedEmpresa = undefined;
          (repository.findOne as jest.Mock).mockResolvedValue(expectedEmpresa);
          result = await service.findOne(1);
        });

        it('should invoke repository.findOne()', () => {
          expect(repository.findOne).toHaveBeenCalledTimes(1);
          expect(repository.findOne).toHaveBeenCalledWith({
            where: { id: 1 },
            relations: ['normas', 'telefonos', 'valores', 'childEmpresas']
          });
        });

        it('should return undefined', () => {
          expect(result).toStrictEqual(expectedEmpresa);
        });
      });
    });
  });

  describe('findAllId()', () => {
    let expectedEmpresas: Partial<Empresa>[];
    let result: Partial<Empresa>[];

    describe('case : success', () => {
      describe('when there are companies', () => {
        beforeEach(async () => {
          expectedEmpresas = [{ id: 1, nombre: 'Test' }];
          getRawManySpy.mockResolvedValue(expectedEmpresas);
          result = await service.findAllId();
        });

        it('should invoke repository.createQueryBuilder()', () => {
          expect(repository.createQueryBuilder).toHaveBeenCalledTimes(1);
        });

        it('should return an array of companies', () => {
          expect(result).toStrictEqual(expectedEmpresas);
        });
      });
    });

    describe('case : failure', () => {
      describe('when there are not companies', () => {
        beforeEach(async () => {
          expectedEmpresas = [];
          getRawManySpy.mockResolvedValue(expectedEmpresas);
          result = await service.findAllId();
        });

        it('should invoke repository.createQueryBuilder()', () => {
          expect(repository.createQueryBuilder).toHaveBeenCalledTimes(1);
        });

        it('should return an empty array', () => {
          expect(result).toStrictEqual(expectedEmpresas);
        });
      });
    });
  });

  describe('create(empresa)', () => {
    const { id, ...rest } = SampleEmpresa;
    let body: CreateEmpresaDTO;

    describe('case: success', () => {
      let expectedEmpresa: Empresa | GlobalException;
      let result: Empresa | GlobalException;

      describe('when a company is created', () => {
        beforeEach(async () => {
          body = rest;
          expectedEmpresa = SampleEmpresa;
          (repository.save as jest.Mock).mockResolvedValue(expectedEmpresa);
          result = await service.create(body);
        });

        it('should invoke repository.save()', () => {
          expect(repository.save).toHaveBeenCalledTimes(1);
          expect(repository.save).toHaveBeenCalledWith(body);
        });

        it('should create a company', () => {
          expect(result).toStrictEqual(expectedEmpresa);
        });
      });
    });

    describe('case: failure', () => {
      let expectedError: TypeError;

      describe('when a company is not created', () => {
        beforeEach(async () => {
          body = undefined;
          expectedError = new TypeError();
          jest.spyOn(service, 'create').mockRejectedValue(expectedError);
        });

        it('should throw when the body is undefined', async () => {
          await expect(service.create(body)).rejects.toThrow(TypeError);
        });

        it('should not create a company', () => {
          expect(repository.save).not.toHaveBeenCalled();
        });
      });
    });
  });

  describe('update(id, empresa)', () => {
    let update: UpdateEmpresaDTO;

    describe('case: success', () => {
      let updateResult: UpdateResult | GlobalException;
      let result: UpdateResult | GlobalException;

      describe('when a company is updated', () => {
        beforeEach(async () => {
          update = {
            nombre: 'Test',
            correo: 'Test'
          };
          updateResult = new UpdateResult();
          (repository.update as jest.Mock).mockResolvedValue(updateResult);
          result = await service.update(1, update);
        });

        it('should invoke repository.update()', () => {
          expect(repository.update).toHaveBeenCalledTimes(1);
          expect(repository.update).toHaveBeenCalledWith(1, update);
        });

        it('should update a company', () => {
          expect(result).toStrictEqual(updateResult);
        });
      });
    });

    describe('case: failure', () => {
      let expectedError: TypeError;

      describe('when a company is not updated', () => {
        beforeEach(async () => {
          expectedError = new TypeError();
          update = undefined;
          jest.spyOn(service, 'update').mockRejectedValue(expectedError);
        });

        it('should throw when the body is undefined', async () => {
          await expect(service.update(1, update)).rejects.toThrow(TypeError);
        });

        it('should not update a company', () => {
          expect(repository.update).not.toHaveBeenCalled();
        });
      });
    });
  });

  describe('delete(id)', () => {
    let deleteResult: DeleteResult | GlobalException;
    let result: DeleteResult | GlobalException;

    describe('case: success', () => {
      describe('when a company is found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 1;
          (repository.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await service.delete(1);
        });

        it('should invoke repository.delete()', () => {
          expect(repository.delete).toHaveBeenCalledTimes(1);
          expect(repository.delete).toHaveBeenCalledWith(1);
        });

        it('should delete a company', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });

    describe('case: failure', () => {
      describe('when a company is not found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 0;
          (repository.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await service.delete(1);
        });

        it('should invoke repository.delete()', () => {
          expect(repository.delete).toHaveBeenCalledTimes(1);
          expect(repository.delete).toHaveBeenCalledWith(1);
        });

        it('should not delete a company', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });
  });
});
