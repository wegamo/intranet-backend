import { Body, Controller, Delete, Get, Param, Post, Put, UseFilters } from '@nestjs/common';
import { DeleteResult, UpdateResult } from 'typeorm';

import { CreateEmpresaDTO } from '../../../models/empresa/dto/create-empresa.dto';
import { UpdateEmpresaDTO } from '../../../models/empresa/dto/update-empresa.dto';
import { Empresa } from '../../../models/empresa/empresa.entity';

import { EmpresaService } from './empresa.service';
import { GlobalException } from './../../exception/global-exception';
import { GlobalExceptionFilter } from './../../exception/global-exception.filter';
import { EmpresaInt } from './../../dto_response/Empresa_Payload';
import { ApiUseTags } from '@nestjs/swagger';

@ApiUseTags('Empresa')
@Controller('empresa')
export class EmpresaController {
  constructor(private readonly empresaService: EmpresaService) {}

  @Get()
  findAll(): Promise<Empresa[]> {
    return this.empresaService.findAll();
  }
  
  @Get('id')
  findAllId(): Promise<Empresa[]> {
    return this.empresaService.findAllId();
  }

  @Get(':id')
  findOne(@Param('id') id: number): Promise<Empresa | EmpresaInt> {
    return this.empresaService.findOne(id);
  }

  @Post()
  async create(@Body() empresaData: CreateEmpresaDTO): Promise<Empresa> {
    return this.empresaService.create(empresaData);
  }

  @Put(':id')
  async update(
    @Param('id') id: number,
    @Body() empresaData: UpdateEmpresaDTO
  ): Promise<UpdateResult | GlobalException> {
    return this.empresaService.update(id, empresaData);
  }
  //TODA INTENCION DE ELIMINACION DE DATA SERA SUSTITUIDA POR UN CAMBIO A ESTADO INACTIVO
  @Delete(':id')
  async delete(@Param('id') id: number): Promise<DeleteResult | GlobalException> {
    return await this.empresaService.delete(id);
  }

  
}
