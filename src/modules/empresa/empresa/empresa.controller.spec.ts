import { Test, TestingModule } from '@nestjs/testing';
import { DeleteResult, UpdateResult } from 'typeorm';

import { CreateEmpresaDTO } from '../../../models/empresa/dto/create-empresa.dto';
import { UpdateEmpresaDTO } from '../../../models/empresa/dto/update-empresa.dto';
import { Empresa } from '../../../models/empresa/empresa.entity';

import { EmpresaController } from './empresa.controller';
import { EmpresaService } from './empresa.service';

import { SampleEmpresa } from './../../../../test/ObjectSamples';
import { EmpresaInt } from './../../dto_response/Empresa_Payload';
import { GlobalException } from './../../exception/global-exception';

describe('Empresa Controller', () => {
  let controller: EmpresaController;
  let EmpresaServiceMock: jest.Mock<Partial<EmpresaService>>;
  let empresaService: EmpresaService;

  beforeEach(() => {
    EmpresaServiceMock = jest.fn<Partial<EmpresaService>, EmpresaService[]>(() => ({
      findAll: jest.fn(),
      findOne: jest.fn(),
      findAllId: jest.fn(),
      create: jest.fn(),
      delete: jest.fn(),
      update: jest.fn()
    }));
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [EmpresaController],
      providers: [{ provide: EmpresaService, useValue: new EmpresaServiceMock() }]
    }).compile();

    empresaService = module.get<EmpresaService>(EmpresaService);
    controller = module.get<EmpresaController>(EmpresaController);
  });

  describe('findAll()', () => {
    let expectedEmpresas: Empresa[] | EmpresaInt[];
    let result: Empresa[] | EmpresaInt[];

    describe('case: success', () => {
      describe('when there are companies', () => {
        beforeEach(async () => {
          expectedEmpresas = [SampleEmpresa];
          (empresaService.findAll as jest.Mock).mockResolvedValue(expectedEmpresas);
          result = await controller.findAll();
        });

        it('should invoke EmpresaService.findAll()', () => {
          expect(empresaService.findAll).toHaveBeenCalledTimes(1);
        });

        it('should return an array of companies', () => {
          expect(result).toStrictEqual(expectedEmpresas);
        });
      });
    });

    describe('case: failure', () => {
      describe('when there are not companies', () => {
        beforeEach(async () => {
          expectedEmpresas = [];
          (empresaService.findAll as jest.Mock).mockResolvedValue(expectedEmpresas);
          result = await controller.findAll();
        });

        it('should invoke EmpresaService.findAll()', () => {
          expect(empresaService.findAll).toHaveBeenCalledTimes(1);
        });

        it('should return an array empty', () => {
          expect(result).toStrictEqual(expectedEmpresas);
        });
      });
    });
  });

  describe('findOne(id)', () => {
    let expectedEmpresa: Empresa | EmpresaInt;
    let result: Empresa | EmpresaInt;

    describe('case: success', () => {
      describe('when a company is found', () => {
        beforeEach(async () => {
          expectedEmpresa = SampleEmpresa;
          (empresaService.findOne as jest.Mock).mockResolvedValue(expectedEmpresa);
          result = await controller.findOne(1);
        });

        it('should invoke EmpresaService.findOne()', () => {
          expect(empresaService.findOne).toHaveBeenCalledTimes(1);
          expect(empresaService.findOne).toHaveBeenCalledWith(1);
        });

        it('should return a company', () => {
          expect(result).toStrictEqual(expectedEmpresa);
        });
      });
    });

    describe('case: failure', () => {
      describe('when a company is not found', () => {
        beforeEach(async () => {
          expectedEmpresa = undefined;
          (empresaService.findOne as jest.Mock).mockResolvedValue(expectedEmpresa);
          result = await controller.findOne(1);
        });

        it('should invoke EmpresaService.findOne()', () => {
          expect(empresaService.findOne).toHaveBeenCalledTimes(1);
          expect(empresaService.findOne).toHaveBeenCalledWith(1);
        });

        it('should return undefined', () => {
          expect(result).toStrictEqual(expectedEmpresa);
        });
      });
    });
  });

  describe('findAllId()', () => {
    let expectedEmpresas: Partial<Empresa>[];
    let result: Partial<Empresa>[];

    describe('case : success', () => {
      describe('when there are companies', () => {
        beforeEach(async () => {
          expectedEmpresas = [{ id: 1, nombre: 'Test' }];
          (empresaService.findAllId as jest.Mock).mockResolvedValue(expectedEmpresas);
          result = await controller.findAllId();
        });
        it('should invoke empresaService.findAllId()', () => {
          expect(empresaService.findAllId).toHaveBeenCalledTimes(1);
        });
        it('should return an array of companies', () => {
          expect(result).toStrictEqual(expectedEmpresas);
        });
      });
    });

    describe('case : failure', () => {
      describe('when there are no companies', () => {
        beforeEach(async () => {
          expectedEmpresas = [];
          (empresaService.findAllId as jest.Mock).mockResolvedValue(expectedEmpresas);
          result = await controller.findAllId();
        });

        it('should invoke empresaService.findAllId()', () => {
          expect(empresaService.findAllId).toHaveBeenCalledTimes(1);
        });

        it('should return an array empty', () => {
          expect(result).toStrictEqual(expectedEmpresas);
        });
      });
    });
  });

  describe('create(empresaData)', () => {
    const { id, ...rest } = SampleEmpresa;
    let body: CreateEmpresaDTO;

    describe('case: success', () => {
      let expectedEmpresa: Empresa;
      let result: Empresa;

      describe('when a company is created', () => {
        beforeEach(async () => {
          body = rest;
          expectedEmpresa = SampleEmpresa;
          (empresaService.create as jest.Mock).mockResolvedValue(expectedEmpresa);
          result = await controller.create(body);
        });

        it('should invoke EmpresaService.create()', () => {
          expect(empresaService.create).toHaveBeenCalledTimes(1);
          expect(empresaService.create).toHaveBeenCalledWith(body);
        });

        it('should create a company', () => {
          expect(result).toStrictEqual(expectedEmpresa);
        });
      });
    });

    describe('case: failure', () => {
      let expectedError: TypeError;

      describe('when a company is not created', () => {
        beforeEach(async () => {
          body = undefined;
          expectedError = new TypeError();
          jest.spyOn(controller, 'create').mockRejectedValue(expectedError);
        });

        it('should throw when the body is undefined', async () => {
          await expect(controller.create(body)).rejects.toThrow(TypeError);
        });

        it('should not create a company', () => {
          expect(empresaService.create).not.toHaveBeenCalled();
        });
      });
    });
  });

  describe('update(id, empresaData)', () => {
    let update: UpdateEmpresaDTO;

    describe('case: success', () => {
      let updateResult: UpdateResult | GlobalException;
      let result: UpdateResult | GlobalException;

      describe('when a company is updated', () => {
        beforeEach(async () => {
          update = {
            nombre: 'Test',
            correo: 'Test'
          };
          updateResult = new UpdateResult();
          (empresaService.update as jest.Mock).mockResolvedValue(updateResult);
          result = await controller.update(1, update);
        });

        it('should invoke EmpresaService.update()', () => {
          expect(empresaService.update).toHaveBeenCalledTimes(1);
          expect(empresaService.update).toHaveBeenCalledWith(1, update);
        });

        it('should update a company', () => {
          expect(result).toStrictEqual(updateResult);
        });
      });
    });

    describe('case: failure', () => {
      let expectedError: TypeError;

      describe('when a company is not updated', () => {
        beforeEach(async () => {
          expectedError = new TypeError();
          update = undefined;
          jest.spyOn(controller, 'update').mockRejectedValue(expectedError);
        });

        it('should throw when the body is undefined', async () => {
          await expect(controller.update(1, update)).rejects.toThrow(TypeError);
        });

        it('should not update a company', () => {
          expect(empresaService.update).not.toHaveBeenCalled();
        });
      });
    });
  });

  describe('delete(id)', () => {
    let deleteResult: DeleteResult | GlobalException;
    let result: DeleteResult | GlobalException;

    describe('case: success', () => {
      describe('when a company is found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 1;
          (empresaService.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await controller.delete(1);
        });

        it('should invoke EmpresaService.delete()', () => {
          expect(empresaService.delete).toHaveBeenCalledTimes(1);
          expect(empresaService.delete).toHaveBeenCalledWith(1);
        });

        it('should delete a company', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });

    describe('case: failure', () => {
      describe('when a company is not found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 0;
          (empresaService.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await controller.delete(1);
        });

        it('should invoke EmpresaService.delete()', () => {
          expect(empresaService.delete).toHaveBeenCalledTimes(1);
          expect(empresaService.delete).toHaveBeenCalledWith(1);
        });

        it('should not delete a company', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });
  });
});
