import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';

import { CreateEmpresaDTO } from '../../../models/empresa/dto/create-empresa.dto';
import { UpdateEmpresaDTO } from '../../../models/empresa/dto/update-empresa.dto';
import { Empresa } from '../../../models/empresa/empresa.entity';
import { GlobalException } from './../../exception/global-exception';

import { EmpresaInt } from './../../dto_response/Empresa_Payload';
import { TelefonoInt } from './../../dto_response/Telefono_Payload';
import { TelefonoEmpresaService } from '../../telefono-empresa/telefono-empresa/telefono-empresa.service';

@Injectable()
export class EmpresaService {
  constructor(
    @InjectRepository(Empresa)
    private readonly empresaRepository: Repository<Empresa>,
    private telefonoEmpresaService: TelefonoEmpresaService
  ) {}

  findAll(): Promise<Empresa[]> {
    return this.empresaRepository.find();
  }
  /*
  async findAll(): Promise<Empresa[] | EmpresaInt[]> {
    let getRes: Empresa[] = await this.empresaRepository.find({
      relations: ['normas', 'telefonos', 'estatus', 'valores', 'childEmpresas'],
      order: { id: 'ASC' }
    });
    let AdpRes: EmpresaInt[] = new Array<EmpresaInt>();

    for (const x of getRes) {
      const {
        telefonos,
        //estatus,
        //valores,
        //normas,
        ...rest
      } = x;

      var P_telf: TelefonoInt[] = new Array<TelefonoInt>();
      let P_norma: Array<String[2]>[] = new Array<String[2][]>();
      var P_valor: Array<String[2]>[] = new Array<String[2][]>();

      let P_emp: EmpresaInt;

      if (telefonos != undefined)
        for (const t of telefonos) {
          let P_telefono: TelefonoInt = <TelefonoInt>{};
          P_telefono.numero = t.numero;
          P_telf.push(P_telefono);
        }

     

      P_emp = {
        ...rest,
        //valores: P_valor,
        //normas: P_norma,
        //estatus: `${estatus}`,
        telefonos: P_telf
      };

      AdpRes.push(P_emp);
    }

    return AdpRes;
  }*/

  async findAllId(): Promise<Empresa[]> {
    const empresas: Empresa[] = await this.empresaRepository
      .createQueryBuilder()
      .select('id, nombre')
      .getRawMany();

    return empresas;
  }

  async findOne(id: number): Promise<Empresa | EmpresaInt> {
    let getRes: Empresa = await this.empresaRepository.findOne({
      where: { id },
      relations: ['normas', 'telefonos', 'estatus', 'valores', 'childEmpresas']
    });
    // let AdpRes: EmpresaInt = this.EmpresaConvert(getRes);
    if (getRes != undefined) {
      const {
        telefonos,
        //estatus,
        //valores,
        //normas,
        ...rest
      } = getRes;

      var P_telf: TelefonoInt[] = new Array<TelefonoInt>();
      let P_norma: Array<String[2]>[] = new Array<String[2][]>();
      var P_valor: Array<String[2]>[] = new Array<String[2][]>();

      if (telefonos != undefined)
        for (const t of telefonos) {
          let P_telefono: TelefonoInt = <TelefonoInt>{};
          P_telefono.numero = t.numero;
          P_telf.push(P_telefono);
        }
      /*if (normas != undefined)
        normas.forEach((item) => {
          P_norma.push([item.nombre, item.descripcion]);
        });
      if (valores != undefined)
        valores.forEach((item) => {
          P_valor.push([item.nombre, item.descripcion]);
        });
        */
      return {
        ...rest,
        //valores: P_valor,
        //normas: P_norma,
        //estatus: `${estatus}`,
        telefonos: P_telf
      };
    } else {
      return undefined;
    }
  }

  async create(empresa: CreateEmpresaDTO): Promise<Empresa> {
    return await this.empresaRepository.save(empresa);
  }

  async update(id: number, empresa: UpdateEmpresaDTO): Promise<UpdateResult | GlobalException> {
    try {
      let UpdRes: UpdateResult = await this.empresaRepository.update(id, empresa);

      return UpdRes;
    } catch (Err) {
      console.log(Err.message + '\n' + Err.query + '\n');
      throw new GlobalException('Error al actualizar empresa', 0);
    }
  }

  async delete(id: number): Promise<DeleteResult | GlobalException> {
    try {
      let DelRes: DeleteResult = await this.empresaRepository.delete(id);
      return DelRes;
    } catch (Err) {
      console.log(Err.message + '\n' + Err.query + '\n');
      if (Err.message.match('viola la llave foránea')) {
        throw new GlobalException(
          'Informacion vinculada, se debe eliminar la informacion dependiente primero',
          1
        );
      }
    }
  }
}
