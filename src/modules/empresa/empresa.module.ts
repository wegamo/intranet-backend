import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { Empresa } from '../../models/empresa/empresa.entity';
import { TelefonoEmpresaModule } from '../telefono-empresa/telefono-empresa.module';

import { EmpresaController } from './empresa/empresa.controller';
import { EmpresaService } from './empresa/empresa.service';

@Module({
  imports: [TypeOrmModule.forFeature([Empresa]),TelefonoEmpresaModule],
  controllers: [EmpresaController],
  providers: [EmpresaService]
})
export class EmpresaModule {}
