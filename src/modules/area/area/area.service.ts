import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Area } from '../../../models/area/area.entity';
import { CreateAreaDTO } from '../../../models/area/dto/create-area.dto';
import { UpdateAreaDTO } from '../../../models/area/dto/update-area.dto';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { IAreaType } from '../../../helpers/IAreaType';
import { EmpresaAreaEstatus } from '../../../enums/empresa-area.enum';
@Injectable()
export class AreaService {
  constructor(@InjectRepository(Area) private readonly areaRepository: Repository<Area>) {}

  async findAll(): Promise<Area[]> {
    return await this.areaRepository.find({ relations: ['cargos'] });
  }

  async findById(areaId: number): Promise<Area> {
    return await this.areaRepository.findOne({ where: { id: areaId } });
  }

  parseAreas (areas: Area[]) : IAreaType[] {
    let parsedRes: IAreaType[] = areas.map((item, ind) => {
      const { id, nombre, descripcion, img, estatus, espacioAws } = item;
      return { id, nombre, icono: img, descripcion, estatus, espacio: espacioAws };
    });

    return parsedRes;
  }

  async findParsedV2(): Promise<IAreaType[]> {
    let areaRes: Area[] = await this.areaRepository
      .createQueryBuilder('area')
      .distinctOn(["area.id"])
      .innerJoin('area.publicaciones', 'publicacion')
      .where("publicacion.tipoObjeto = 'A'")
      .orderBy('area.id', 'ASC')
      .getMany();
    return this.parseAreas(areaRes);
  }

  async findParsed(): Promise<IAreaType[]> {
    let areaRes : Area[] = await this.areaRepository.find({order:{id:"ASC"}});
    let redRes : IAreaType[] = areaRes.map((item,ind) => {
      const {id, nombre,descripcion,img, estatus,espacioAws} = item;
      return {id, nombre,icono:img,descripcion, estatus,espacio:espacioAws}
    })
    return this.parseAreas(areaRes);
  }

  async create(area: CreateAreaDTO, espacioAws: string, img: string): Promise<Area> {
    return await this.areaRepository.save({ ...area, espacioAws, img });
  }

  async update({ id, ...rest }: Area): Promise<UpdateResult> {
    return await this.areaRepository.update(id, rest);
  }

  async delete(id: number): Promise<DeleteResult> {
    let DelRes: DeleteResult = await this.areaRepository.delete(id);
    return DelRes;
  }

  async updateState(areaId: number, state: EmpresaAreaEstatus): Promise<UpdateResult> {
    return await this.areaRepository.update({ id: areaId }, { estatus: state });
  }
}
