import { Test, TestingModule } from '@nestjs/testing';
import { Area } from '../../../models/area/area.entity';
import { CreateAreaDTO } from '../../../models/area/dto/create-area.dto';
import { UpdateAreaDTO } from '../../../models/area/dto/update-area.dto';
import { SampleArea } from '../../../../test/ObjectSamples';
import { DeleteResult, UpdateResult } from 'typeorm';
import { AreaController } from './area.controller';
import { AreaService } from './area.service';

describe('Area Controller', () => {
  let controller: AreaController;
  let areaService: AreaService;
  let AreaServiceMock: jest.Mock<Partial<AreaService>>;

  beforeEach(() => {
    AreaServiceMock = jest.fn<Partial<AreaService>, AreaService[]>(() => ({
      findAll: jest.fn(),
      create: jest.fn(),
      delete: jest.fn(),
      update: jest.fn()
    }));
  });


  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AreaController],
      providers: [{ provide: AreaService, useValue: new AreaServiceMock() }]
    }).compile();

    areaService = module.get<AreaService>(AreaService);
    controller = module.get<AreaController>(AreaController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('findAll()', () => {
    let expectedAreas: Area[];
    let result: Area[];

    describe('case : success', () => {
      describe('when there are roles', () => {
        beforeEach(async () => {
          expectedAreas = [SampleArea];
          (areaService.findAll as jest.Mock).mockResolvedValue(expectedAreas);
          result = await controller.findAll();
        });
        it('should invoke areaService.findAll()', () => {
          expect(areaService.findAll).toHaveBeenCalledTimes(1);
        });
        it('should return an array of roles', () => {
          expect(result).toStrictEqual(expectedAreas);
        });
      });
    });

    describe('case : failure', () => {
      describe('when there are no roles', () => {
        beforeEach(async () => {
          expectedAreas = [];
          (areaService.findAll as jest.Mock).mockResolvedValue(expectedAreas);
          result = await controller.findAll();
        });

        it('should invoke areaService.findAll()', () => {
          expect(areaService.findAll).toHaveBeenCalledTimes(1);
        });

        it('should return an array empty', () => {
          expect(result).toStrictEqual(expectedAreas);
        });
      });
    });
  });
  
  //Test create()
  describe('create(CreateAreaDTO)', () => {
    const { id, ...rest } = SampleArea;
    let body: CreateAreaDTO;

    describe('case : success', () => {
      let expectedArea: Area;
      let result: Area;

      describe('when a role is created', () => {
        beforeEach(async () => {
          body = rest;
          expectedArea = SampleArea;
          (areaService.create as jest.Mock).mockResolvedValue(expectedArea);
          result = await controller.create(body);
        });
        it('should invoke areaService.create()', () => {
          expect(areaService.create).toHaveBeenCalledTimes(1);
          expect(areaService.create).toHaveBeenCalledWith(body);
        });
        it('should create a role', () => {
          expect(result).toStrictEqual(expectedArea);
        });
      });
    });

    describe('case : failure', () => {
      let expectedError: TypeError;
      describe('when a role is not created', () => {
        beforeEach(async () => {
          body = undefined;
          expectedError = new TypeError();
          jest.spyOn(controller, 'create').mockRejectedValue(expectedError);
        });
        it('should throw when the body is undefined', async () => {
          await expect(controller.create(body)).rejects.toThrow(TypeError);
        });
        it('should not create a role', () => {
          expect(areaService.create).not.toHaveBeenCalled();
        });
      });
    });
  });
  //Test update()
  describe('update(id, UpdateAreaDTO)', () => {
    let update: UpdateAreaDTO;

    describe('case : success', () => {
      let updateResult: UpdateResult;
      let result: UpdateResult;

      describe('when an role is found', () => {
        beforeEach(async () => {
          update = {
            nombre: 'testmod',
          };
          updateResult = new UpdateResult();
          (areaService.update as jest.Mock).mockResolvedValue(updateResult);
          result = await controller.update(1, update);
        });
        it('should invoke areaService.update()', () => {
          expect(areaService.update).toHaveBeenCalledTimes(1);
          expect(areaService.update).toHaveBeenCalledWith(1, update);
        });
        it('should update an role', () => {
          expect(result).toStrictEqual(updateResult);
        });
      });
    });

    describe('case : failure', () => {
      let expectedError: TypeError;

      describe('when an role is not found', () => {
        beforeEach(async () => {
          expectedError = new TypeError();
          update = undefined;
          jest.spyOn(controller, 'update').mockRejectedValue(expectedError);
        });
        it('should invoke areaService.update()', async () => {
          await expect(controller.update(1, update)).rejects.toThrow(TypeError);
        });
        it('should not update an role', () => {
          expect(areaService.update).not.toHaveBeenCalled();
        });
      });
    });
  });
  //Test delete()
  describe('delete(id)', () => {
    let deleteResult: DeleteResult;
    let result: DeleteResult;

    describe('case : success', () => {
      describe('when an role is found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 1;
          (areaService.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await controller.delete(1);
        });
        it('should invoke areaService.delete()', () => {
          expect(areaService.delete).toHaveBeenCalledTimes(1);
          expect(areaService.delete).toHaveBeenCalledWith(1);
        });
        it('should delete an role', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });

    describe('case : failure', () => {
      let expectedError: TypeError;

      describe('when an role is not found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 0;
          (areaService.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await controller.delete(1);
        });

        it('should invoke areaService.delete()', () => {
          expect(areaService.delete).toHaveBeenCalledTimes(1);
          expect(areaService.delete).toHaveBeenCalledWith(1);
        });

        it('should not delete an role', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });
  });

});
