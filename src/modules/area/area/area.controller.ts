import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Post, Put, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common';
import { Area } from '../../../models/area/area.entity';
import { CreateAreaDTO } from '../../../models/area/dto/create-area.dto';
import { UpdateAreaDTO } from '../../../models/area/dto/update-area.dto';
import { DeleteResult, UpdateResult } from 'typeorm';
import { AreaService } from './area.service';
import { IAreaType } from '../../../helpers/IAreaType';
import { PublicacionService } from '../../publicacion/publicacion/publicacion.service';
import { UsuarioPermisoService } from '../../usuario-permiso/usuario-permiso/usuario-permiso.service';
import { Aws3CntmService } from '../../aws3-cntm/aws3-cntm/aws3-cntm.service';
import { AuthGuard } from '@nestjs/passport';
import { FileInterceptor } from '@nestjs/platform-express';
import { Persona } from '../../../models/persona/persona.entity';
import { Publicacion } from '../../../models/publicacion/publicacion.entity';
import { TipoObjeto } from '../../../enums/tipo-objeto.enum';
import { UsuarioPermiso } from '../../..//models/usuario-permiso/usuario-permiso.entity';
import { TipoPermiso } from '../../../enums/tipo-permiso.enum';
import { ApiBearerAuth, ApiConsumes, ApiImplicitBody, ApiOperation, ApiResponse, ApiUseTags } from '@nestjs/swagger';
import { InitAreaDTO, UpdAreaDTO } from '../../../models/area/dto/init-area.dto';
import { PersonaService } from '../../persona/persona/persona.service';
import { EmpresaAreaEstatus } from '../../../enums/empresa-area.enum';


@ApiBearerAuth()
@ApiUseTags('Area')
@Controller('area')
export class AreaController {
  constructor(
    private readonly areaService: AreaService,
    private readonly publicacionService: PublicacionService,
    private readonly s3Service: Aws3CntmService,
    private readonly usuarioPermisoService: UsuarioPermisoService,
    private readonly personaService: PersonaService,
    ) {}
  
  @UseGuards(AuthGuard('jwt'))
  @Get()
  findAll(): Promise<IAreaType[]> {
    try {
      return this.areaService.findParsed();
    }
    catch(err){
      throw new HttpException(`Error al obtener areas ${err.message}`, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
  
  @ApiOperation({ title:'Obtener Areas', description:'Obtención de areas registradas' })
  @ApiResponse({
    status: 200,
    description: 'Areas registradas',
    type:IAreaType,
    isArray:true
  })
  @UseGuards(AuthGuard('jwt'))
  @Get('parsed')
  async findParsed(): Promise<IAreaType[]> {
    try{
      return this.areaService.findParsedV2();
    }
    catch(err){
      throw new HttpException(`Error al obtener areas ${err.message}`, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  
  @ApiOperation({ title:'Crear area', description:'Creacion de nueva area' })
  @ApiResponse({
    status: 200,
    description: 'Nueva area registrada',
    type:IAreaType,
  })
  @ApiConsumes('multipart/form-data')
  @ApiImplicitBody({ name: 'body', description: 'datos del area', type:InitAreaDTO  })
  @UseGuards(AuthGuard('jwt'))
  @Post()
  @UseInterceptors(FileInterceptor('imgs'))
  async create(
    @Body() areaData: { body: string },
    @UploadedFile() imgs: Express.Multer.File
    ): Promise<Area> {
    let areaDTO : CreateAreaDTO;
    let img: Express.Multer.File;
    let espacioAws: string;
    let data : InitAreaDTO = JSON.parse(areaData.body);
    let publicante : Persona = await this.personaService.findOne(data.user);
    try{
      areaDTO  = data.data;
      img = imgs;
      //INSTANCIAS CARPETA, IMG
      let toName: string = areaDTO.nombre.toLowerCase().replace(/ /g,"-");
      //let toName: string = areaDTO.nombre.replace(/ /g,"-");
      let espacioResp : { resp: AWS.S3.PutObjectOutput; nKey: string } = await  this.s3Service.addAwsObject(toName,'documentos/transversales/',null); 
      espacioAws  = espacioResp.nKey;
      let publicacionResp: Publicacion = await this.publicacionService.create({
        publicante:publicante, 
        ubicacion:espacioAws,
        tipoObjeto:TipoObjeto.CARPETA,
        nombreObjeto:toName,
        actualizacion:`${Date.now()} ${publicante.primerNombre+" "+publicante.primerApellido}`
      });
      //REVIEW
      let permisoResp: UsuarioPermiso = await this.usuarioPermisoService.create(
        {tipoPermiso:TipoPermiso.READWRITE,persona:publicante,publicacion:publicacionResp}
      );
      //REVIEW
      let imgResp:{ resp: AWS.S3.PutObjectOutput; nKey: string } = await  this.s3Service.addAwsObject(img.originalname,espacioAws,img); 
      //REVIEW
      return this.areaService.create(areaDTO,espacioAws,img.originalname);
    }
    catch(err){
      throw new HttpException(`Error al crear area ${err.message}`, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
  
  @ApiOperation({ title:'Actualizacion de area', description:'Actualizar area existente' })
  @ApiResponse({
    status: 200,
    description: 'Area actualizada',
    type:IAreaType,
  })
  @ApiConsumes('multipart/form-data')
  @ApiImplicitBody({ name: 'body', description: 'datos del area', type:UpdAreaDTO  })
  @UseGuards(AuthGuard('jwt'))
  @Put(':id')
  @UseInterceptors(FileInterceptor('imgs'))
  async update(
    @Param('id') id: number, 
    @Body() areaData: { body: string },
    @UploadedFile() imgs: Express.Multer.File
    ): Promise<UpdateResult> {
    let areaDTO : UpdateAreaDTO;
    let img: Express.Multer.File;
    let area: Area = await this.areaService.findById(id);
    let data : { data:UpdateAreaDTO,user:number } = JSON.parse(areaData.body);
    console.log(data.data, areaData);
    try{
      areaDTO  = data.data;
      img = imgs;
      if(img.originalname !== '*'){
        let prevImg:string = area.img; 
        let imgResp:{ resp: AWS.S3.PutObjectOutput; nKey: string } = await  this.s3Service.addAwsObject(img.originalname,area.espacioAws,img); 
        if(prevImg !== img.originalname){
          let multiDelete: AWS.S3.DeleteObjectsOutput = await  this.s3Service.paramAwsDelete(`${area.espacioAws}${prevImg}`,false);
        }
      }
      return this.areaService.update({...area,...areaDTO});
    }
    catch(err){
      throw new HttpException(`Error al actualizar areas ${err.message}`, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
  
  @ApiOperation({ title:'Deshabilitar area', description:'Deshabilitado de area' })
  @ApiResponse({
    status: 200,
    description: 'Areas deshabilitada',
    type:IAreaType,
  })
  @UseGuards(AuthGuard('jwt'))
  @Delete(':id')
  async delete(@Param('id') id: number): Promise<UpdateResult> {
    try{
      /*let area: Area = await this.areaService.findById(id);
      return this.areaService.disable(id);*/
      return await this.areaService.updateState(id,EmpresaAreaEstatus.INACTIVA);
    }
    catch(err){
      throw new HttpException(`Error al des area ${err.message}`, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
  
  @ApiOperation({ title:'Eliminar área', description:'Eliminación completa de área' })
  @ApiResponse({
    status: 200,
    description: 'Areas eliminada',
    type:IAreaType,
  })
  @UseGuards(AuthGuard('jwt'))
  @Delete('del/:id')
  async trueDelete(@Param('id') id: number): Promise<DeleteResult> {
    try{
      let area: Area = await this.areaService.findById(id);
      let deleteRes = await this.areaService.delete(id);
      let multiDelete: AWS.S3.DeleteObjectsOutput = await  this.s3Service.paramAwsDelete(area.espacioAws,true);
      return deleteRes;
    }
    catch(err){
      throw new HttpException(`Error al eliminar área ${err.message}`, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
  
  @ApiOperation({ title:'Habiltar área', description:'Habilitado de área' })
  @ApiResponse({
    status: 200,
    description: 'Areas habilitada',
    type:IAreaType,
  })
  @UseGuards(AuthGuard('jwt'))
  @Put('enable/:id')
  async enableArea(@Param('id') id: number): Promise<UpdateResult> {
    try{
      return await this.areaService.updateState(id,EmpresaAreaEstatus.ACTIVA);
    }
    catch(err){
      throw new HttpException(`Error al habilitar área ${err.message}`, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
