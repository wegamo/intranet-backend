import { forwardRef, Module } from '@nestjs/common';
import { AreaService } from './area/area.service';
import { AreaController } from './area/area.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Aws3CntmModule } from '../aws3-cntm/aws3-cntm.module';
import { PublicacionModule } from '../publicacion/publicacion.module';
import { UsuarioPermisoModule } from '../usuario-permiso/usuario-permiso.module';
import { Area } from '../../models/area/area.entity';
import { PersonaModule } from '../persona/persona.module';
import { PublicacionService } from '../publicacion/publicacion/publicacion.service';
import { Aws3CntmService } from '../aws3-cntm/aws3-cntm/aws3-cntm.service';
import { UsuarioPermisoService } from '../usuario-permiso/usuario-permiso/usuario-permiso.service';
import { PersonaService } from '../persona/persona/persona.service';

@Module({
  imports:[
    TypeOrmModule.forFeature([Area]),
    forwardRef(() => PublicacionModule),
    Aws3CntmModule,
    forwardRef(()=> UsuarioPermisoModule),
    PersonaModule,
  ],
  providers: [
    AreaService, 
    PublicacionService, 
    Aws3CntmService, 
    UsuarioPermisoService, 
    PersonaService
  ],
  controllers: [AreaController],
})
export class AreaModule {}
