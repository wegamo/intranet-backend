import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '../../config/config.module';
import { HistAusencias } from '../../models/hist-ausencias/hist-ausencias.entity';
import { AusenciaModule } from '../ausencia/ausencia.module';
import { AusenciaService } from '../ausencia/ausencia/ausencia.service';
import { Aws3CntmModule } from '../aws3-cntm/aws3-cntm.module';
import { Aws3CntmService } from '../aws3-cntm/aws3-cntm/aws3-cntm.service';
import { NotificacionesModule } from '../notificaciones/notificaciones.module';
import { PersonaModule } from '../persona/persona.module';
import { PersonaService } from '../persona/persona/persona.service';
import { HistAusenciaController } from './hist-ausencia/hist-ausencia.controller';
import { HistAusenciaService } from './hist-ausencia/hist-ausencia.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([HistAusencias]),
    forwardRef(() => PersonaModule),
    ConfigModule,
    Aws3CntmModule,
    NotificacionesModule,
    AusenciaModule
  ],
  providers: [HistAusenciaService, PersonaService, Aws3CntmService, AusenciaService],
  controllers: [HistAusenciaController],
  exports: [HistAusenciaService]
})
export class HistAusenciaModule {}
