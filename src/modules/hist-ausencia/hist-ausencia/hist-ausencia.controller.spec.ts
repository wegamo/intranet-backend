import { Test, TestingModule } from '@nestjs/testing';
import { CreateHistAusenciasDTO } from '../../../models/hist-ausencias/dto/create-hist-ausencias.dto';
import { UpdateHistAusenciasThDTO } from '../../../models/hist-ausencias/dto/update-hist-ausencias-th.dto';
import { HistAusencias } from '../../../models/hist-ausencias/hist-ausencias.entity';
import { SampleHistAusencias, SamplePersona } from '../../../../test/ObjectSamples';
import { DeleteResult, UpdateResult } from 'typeorm';
import { HistAusenciaController } from './hist-ausencia.controller';
import { HistAusenciaService } from './hist-ausencia.service';
import { SolicitudEstatus } from '../../../enums/solicitud-estatus.enum';
import { PersonaService } from '../../persona/persona/persona.service';
import { AusenciaService } from '../../ausencia/ausencia/ausencia.service';

describe('HistAusencia Controller', () => {
  let controller: HistAusenciaController;
  let histAusenciasService: HistAusenciaService;
  let HistAusenciasServiceMock: jest.Mock<Partial<HistAusenciaService>>;
  let PersonaServiceMock: jest.Mock<Partial<PersonaService>>;
  let AusenciaServiceMock: jest.Mock<Partial<AusenciaService>>;

  beforeEach(() => {
    HistAusenciasServiceMock = jest.fn<Partial<HistAusenciaService>, HistAusenciaService[]>(() => ({
      findAll: jest.fn(),
      findByPerson: jest.fn(),
      create: jest.fn(),
      delete: jest.fn(),
      update: jest.fn(),
      updateByTh:jest.fn(),
      findOneWithDetails:jest.fn(),
    }));
    PersonaServiceMock = jest.fn<Partial<PersonaService>, PersonaService[]>(() => ({
      /*findAll: jest.fn(),
      findByPerson: jest.fn(),
      create: jest.fn(),
      delete: jest.fn(),
      update: jest.fn()*/
    }));

    AusenciaServiceMock = jest.fn<Partial<AusenciaService>, AusenciaService[]>(() => ({
      /*findAll: jest.fn(),
      findByPerson: jest.fn(),
      create: jest.fn(),
      delete: jest.fn(),
      update: jest.fn()*/
    }));
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [HistAusenciaController],
      providers: [
        { provide: HistAusenciaService, useValue: new HistAusenciasServiceMock() },
        { provide: PersonaService, useValue: new PersonaServiceMock() },
        { provide: AusenciaService, useValue: new  AusenciaServiceMock() }
      ]
    }).compile();
    histAusenciasService = module.get<HistAusenciaService>(HistAusenciaService);
    controller = module.get<HistAusenciaController>(HistAusenciaController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
  /*
  describe('findAll()', () => {
    let expectedHistAusenciass: HistAusencias[];
    let result: HistAusencias[];

    describe('case : success', () => {
      describe('when there are roles', () => {
        beforeEach(async () => {
          expectedHistAusenciass = [SampleHistAusencias];
          (histAusenciasService.findAll as jest.Mock).mockResolvedValue(expectedHistAusenciass);
          result = await controller.findAll();
        });
        it('should invoke HistAusenciasService.findAll()', () => {
          expect(histAusenciasService.findAll).toHaveBeenCalledTimes(1);
        });
        it('should return an array of roles', () => {
          expect(result).toStrictEqual(expectedHistAusenciass);
        });
      });
    });

    describe('case : failure', () => {
      describe('when there are no roles', () => {
        beforeEach(async () => {
          expectedHistAusenciass = [];
          (histAusenciasService.findAll as jest.Mock).mockResolvedValue(expectedHistAusenciass);
          result = await controller.findAll();
        });

        it('should invoke HistAusenciasService.findAll()', () => {
          expect(histAusenciasService.findAll).toHaveBeenCalledTimes(1);
        });

        it('should return an array empty', () => {
          expect(result).toStrictEqual(expectedHistAusenciass);
        });
      });
    });
  });
  */
  //Test findByPerson()
  describe('findOneWithDetails(id)', () => {
    let expectedHistAusencias: HistAusencias;
    let result: HistAusencias;

    describe('case : success', () => {
      describe('when an role is found', () => {
        beforeEach(async () => {
          expectedHistAusencias = SampleHistAusencias;
          (histAusenciasService.findOneWithDetails as jest.Mock).mockResolvedValue(expectedHistAusencias);
          result = await controller.findById(SampleHistAusencias.persona.id);
        });
        it('should invoke HistAusenciasService.findOneWithDetails()', () => {
          expect(histAusenciasService.findOneWithDetails).toHaveBeenCalledTimes(1);
          expect(histAusenciasService.findOneWithDetails).toHaveBeenCalledWith(SampleHistAusencias.persona.id);
        });
        it('should return an role', () => {
          expect(result).toStrictEqual(expectedHistAusencias);
        });
      });
    });

    describe('case : failure', () => {
      describe('when an role is not found', () => {
        beforeEach(async () => {
          expectedHistAusencias = undefined;
          (histAusenciasService.findOneWithDetails as jest.Mock).mockResolvedValue(expectedHistAusencias);
          result = await controller.findById(SampleHistAusencias.persona.id);
        });
        it('should invoke HistAusenciasService.findOneWithDetails()', () => {
          expect(histAusenciasService.findOneWithDetails).toHaveBeenCalledTimes(1);
          expect(histAusenciasService.findOneWithDetails).toHaveBeenCalledWith(
            SampleHistAusencias.persona.id
          );
        });
        it('should return undefined', () => {
          expect(result).toStrictEqual(expectedHistAusencias);
        });
      });
    });
  });
  /*
  describe('create(CreateHistAusenciasDTO,File)', () => {
    const { id, ...rest } = SampleHistAusencias;
    let body: CreateHistAusenciasDTO;

    describe('case : success', () => {
      let expectedHistAusencias: HistAusencias;
      let result: HistAusencias;

      describe('when a role is created', () => {
        beforeEach(async () => {
          body = rest;
          expectedHistAusencias = SampleHistAusencias;
          (histAusenciasService.create as jest.Mock).mockResolvedValue(expectedHistAusencias);
          result = await controller.create(body);
        });
        it('should invoke HistAusenciasService.create()', () => {
          expect(histAusenciasService.create).toHaveBeenCalledTimes(1);
          expect(histAusenciasService.create).toHaveBeenCalledWith(body);
        });
        it('should create a role', () => {
          expect(result).toStrictEqual(expectedHistAusencias);
        });
      });
    });

    describe('case : failure', () => {
      let expectedError: TypeError;
      describe('when a role is not created', () => {
        beforeEach(async () => {
          body = undefined;
          expectedError = new TypeError();
          jest.spyOn(controller, 'create').mockRejectedValue(expectedError);
        });
        it('should throw when the body is undefined', async () => {
          await expect(controller.create(body)).rejects.toThrow(TypeError);
        });
        it('should not create a role', () => {
          expect(histAusenciasService.create).not.toHaveBeenCalled();
        });
      });
    });
  });
  */
  
  //Test update()
  describe('update(id, UpdateHistAusenciasDTO)', () => {
    let update:UpdateHistAusenciasThDTO;

    describe('case : success', () => {
      let updateResult: HistAusencias;
      let result: HistAusencias;

      describe('when an role is found', () => {
        beforeEach(async () => {
          update = {
            fechaRevision:new Date(),
            firmaTh:'SIGNOUTPUT',
            talentoHumano:SamplePersona,
            estatus:SolicitudEstatus.APROBADA
          };
          //updateResult = new UpdateResult();
          (histAusenciasService.updateByTh as jest.Mock).mockResolvedValue(updateResult);
          result = await controller.update(SampleHistAusencias.persona.id,update);
        });
        it('should invoke HistAusenciasService.updateByTh()', () => {
          expect(histAusenciasService.updateByTh).toHaveBeenCalledTimes(1);
          expect(histAusenciasService.updateByTh).toHaveBeenCalledWith(1, update);
        });
        it('should update an role', () => {
          expect(result).toStrictEqual(updateResult);
        });
      });
    });

    describe('case : failure', () => {
      let expectedError: TypeError;

      describe('when an role is not found', () => {
        beforeEach(async () => {
          expectedError = new TypeError();
          update = undefined;
          jest.spyOn(controller, 'update').mockRejectedValue(expectedError);
        });
        it('should invoke HistAusenciasService.updateByTh()', async () => {
          await expect(controller.update(1, update)).rejects.toThrow(TypeError);
        });
        it('should not update an role', () => {
          expect(histAusenciasService.updateByTh).not.toHaveBeenCalled();
        });
      });
    });
  });
  
  /*
  describe('delete(id)', () => {
    let deleteResult: DeleteResult;
    let result: DeleteResult;

    describe('case : success', () => {
      describe('when an role is found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 1;
          (histAusenciasService.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await controller.delete(1);
        });
        it('should invoke HistAusenciasService.delete()', () => {
          expect(histAusenciasService.delete).toHaveBeenCalledTimes(1);
          expect(histAusenciasService.delete).toHaveBeenCalledWith(1);
        });
        it('should delete an role', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });

    describe('case : failure', () => {
      let expectedError: TypeError;

      describe('when an role is not found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 0;
          (histAusenciasService.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await controller.delete(1);
        });

        it('should invoke HistAusenciasService.delete()', () => {
          expect(histAusenciasService.delete).toHaveBeenCalledTimes(1);
          expect(histAusenciasService.delete).toHaveBeenCalledWith(1);
        });

        it('should not delete an role', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });
  });
  */
});
