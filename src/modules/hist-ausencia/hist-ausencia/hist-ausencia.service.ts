import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateHistAusenciasDTO } from '../../../models/hist-ausencias/dto/create-hist-ausencias.dto';
import { HistAusencias } from '../../../models/hist-ausencias/hist-ausencias.entity';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { UpdateHistAusenciasThDTO } from '../../../models/hist-ausencias/dto/update-hist-ausencias-th.dto';
import { IPaginationOptions, paginate, Pagination } from 'nestjs-typeorm-paginate';
import configService from '../../../config/config/config.service';
import { Aws3CntmService } from '../../aws3-cntm/aws3-cntm/aws3-cntm.service';
import { SolicitudEstatus } from '../../../enums/solicitud-estatus.enum';

const AWS_S3_BUCKET_NAME = configService.get('AWS_S3_BUCKET_NAME');
@Injectable()
export class HistAusenciaService {
  constructor(
    @InjectRepository(HistAusencias)
    private readonly histAusenciasRepository: Repository<HistAusencias>,
    private readonly s3Service: Aws3CntmService
  ) {}

  findOne(id: number): Promise<HistAusencias> {
    return this.histAusenciasRepository.findOne(id, {
      relations: ['supervisor', 'persona']
    });
  }

  countByCurrentMonth(id: number): Promise<number> {
    return this.histAusenciasRepository
      .createQueryBuilder('histAusencias')
      .innerJoin('histAusencias.persona', 'persona')
      .where('histAusencias.persona.id = :id', { id: id })
      .where('EXTRACT(MONTH FROM histAusencias.fechaSolicitud) = EXTRACT(MONTH FROM NOW())')
      .getCount();
  }

  findAllTH(options: IPaginationOptions): Promise<Pagination<HistAusencias>> {
    return paginate<HistAusencias>(this.histAusenciasRepository, options, {
      relations: ['persona', 'ausencia', 'ausencia.solicitud'],
      order: { fechaSolicitud: 'DESC' }
    });
  }

  findAllSupervisor(
    idSupervisor: number,
    options: IPaginationOptions
  ): Promise<Pagination<HistAusencias>> {
    const queryBuilder = this.histAusenciasRepository.createQueryBuilder('histAusencias');
    queryBuilder
      .innerJoinAndSelect('histAusencias.persona', 'persona')
      .innerJoinAndSelect('histAusencias.ausencia', 'ausencia')
      .innerJoinAndSelect('ausencia.solicitud', 'solicitud')
      .where('histAusencias.supervisor.id = :id', { id: idSupervisor })
      .orderBy('histAusencias.fechaSolicitud', 'DESC');

    return paginate<HistAusencias>(queryBuilder, options);
  }

  findOneWithDetails(id: number): Promise<HistAusencias> {
    return this.histAusenciasRepository
      .createQueryBuilder('histAusencias')
      .innerJoinAndSelect('histAusencias.ausencia', 'ausencia')
      .innerJoinAndSelect('ausencia.solicitud', 'solicitud')
      .innerJoinAndSelect('ausencia.tipoDescuento', 'tipoDescuento')
      .innerJoinAndSelect('ausencia.motivo', 'motivo')
      .innerJoinAndSelect('histAusencias.persona', 'persona')
      .innerJoinAndSelect('persona.empresa', 'empresa')
      .innerJoinAndSelect('persona.histCargos', 'histCargos')
      .innerJoinAndSelect('histCargos.cargo', 'cargo')
      .innerJoinAndSelect('cargo.parentCargo', 'parentCargo')
      .innerJoinAndSelect('cargo.area', 'area')
      .where('histCargos.fechaFin IS NULL AND histAusencias.id = :id', { id })
      .getOne();
  }

  findAllByPersona(
    personaId: number,
    options: IPaginationOptions
  ): Promise<Pagination<HistAusencias>> {
    return paginate<HistAusencias>(this.histAusenciasRepository, options, {
      relations: ['ausencia', 'ausencia.solicitud'],
      where: { persona: personaId },
      order: { fechaSolicitud: 'DESC' }
    });
  }

  findAllCurrentYearByPersona(personaId: number): Promise<HistAusencias[]> {
    return this.histAusenciasRepository
      .createQueryBuilder('histAusencias')
      .where('histAusencias.persona = :personaId', { personaId })
      .andWhere('EXTRACT(YEAR FROM histAusencias.fechaSolicitud) = EXTRACT(YEAR FROM NOW())')
      .getMany();
  }

  async create(
    histAusenciasData: CreateHistAusenciasDTO,
    firmaSolicitante: Express.Multer.File
  ): Promise<HistAusencias> {
    let histAusencias = await this.histAusenciasRepository.save(histAusenciasData);
    let mediaExt = firmaSolicitante.originalname.replace(/(^.*\.)/gi, '.');
    let urlImagen = `https://${AWS_S3_BUCKET_NAME}/complex/hist-ausencias/firma-solicitante/ausencia_firma_empleado_${
      histAusencias.id
    }${mediaExt}`;
    await this.s3Service.uploadFirmaSolicitanteAusencias(
      histAusencias.id,
      firmaSolicitante,
      mediaExt
    );
    histAusencias.firmaSolicitante = urlImagen;
    return this.histAusenciasRepository.save(histAusencias);
  }

  async updateBySupervisor(
    histAusencias: HistAusencias,
    firmaSupervisor: Express.Multer.File
  ): Promise<HistAusencias> {
    let mediaExt = firmaSupervisor.originalname.replace(/(^.*\.)/gi, '.');
    let urlImagen = `https://${AWS_S3_BUCKET_NAME}/complex/hist-ausencias/firma-supervisor/ausencia_firma_supervisor_${
      histAusencias.id
    }${mediaExt}`;
    await this.s3Service.uploadFirmaSupervisorAusencias(
      histAusencias.id,
      firmaSupervisor,
      mediaExt
    );
    histAusencias.firmaSupervisor = urlImagen;
    return this.histAusenciasRepository.save(histAusencias);
  }

  async updateByTh(
    histAusencias: HistAusencias,
    firmaTh: Express.Multer.File
  ): Promise<HistAusencias> {
    let mediaExt = firmaTh.originalname.replace(/(^.*\.)/gi, '.');
    let urlImagen = `https://${AWS_S3_BUCKET_NAME}/complex/hist-ausencias/firma-th/ausencia_firma_th_${
      histAusencias.id
    }${mediaExt}`;
    await this.s3Service.uploadFirmaThAusencias(histAusencias.id, firmaTh, mediaExt);
    histAusencias.firmaTh = urlImagen;
    return this.histAusenciasRepository.save(histAusencias);
  }

  findAllCurrentMonthGroupedByMotivo(): Promise<
    {
      motivo_nombre: string;
      count: string;
    }[]
  > {
    return this.histAusenciasRepository
      .createQueryBuilder('histAusencias')
      .select('COUNT(histAusencias.id)')
      .addSelect('motivo.nombre')
      .innerJoin('histAusencias.ausencia', 'ausencia')
      .innerJoin('ausencia.motivo', 'motivo')
      .where('EXTRACT(MONTH FROM histAusencias.fechaSolicitud) = EXTRACT(MONTH FROM NOW())')
      .groupBy('motivo.id')
      .getRawMany();
  }

  findAllCurrentMonthByTipo(tipo: string): Promise<HistAusencias[]> {
    const descripcion = tipo === 'horas' ? 'Ausencias por horas' : 'Ausencias por días';
    return this.histAusenciasRepository
      .createQueryBuilder('histAusencias')
      .innerJoinAndSelect('histAusencias.ausencia', 'ausencia')
      .innerJoinAndSelect('ausencia.solicitud', 'solicitud')
      .where('EXTRACT(MONTH FROM histAusencias.fechaSolicitud) = EXTRACT(MONTH FROM NOW())')
      .andWhere(`solicitud.descripcion = '${descripcion}'`)
      .getMany();
  }
  findAllPendientes(): Promise<number> {
    return this.histAusenciasRepository.count({
      where: [
        {estatus: SolicitudEstatus.PENDIENTE},
        {estatus: SolicitudEstatus.APROBADA_SUPERVISOR}],
    });
  }
}
