import {
  Body,
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Put,
  Query,
  UploadedFile,
  UseGuards,
  UseInterceptors,
  Request
} from '@nestjs/common';
import { UpdateHistAusenciasThDTO } from '../../../models/hist-ausencias/dto/update-hist-ausencias-th.dto';
import { HistAusencias } from '../../../models/hist-ausencias/hist-ausencias.entity';
import { HistAusenciaService } from './hist-ausencia.service';
import { Pagination } from 'nestjs-typeorm-paginate';
import { PersonaService } from '../../persona/persona/persona.service';
import { FileInterceptor } from '@nestjs/platform-express';
import { AusenciaService } from '../../ausencia/ausencia/ausencia.service';
import { AuthGuard } from '@nestjs/passport';
import { Persona } from '../../../models/persona/persona.entity';
import { UpdateHistAusenciasSupervisorDTO } from '../../../models/hist-ausencias/dto/update-hist-ausencias-supervisor.dto';
import { SolicitudEstatus } from '../../../enums/solicitud-estatus.enum';
import { TipoNotificacion } from '../../../enums/tipo-notificacion.enum';
import { sendEmail } from '../../../helpers/sendEmail';
import { RolesGuard } from '../../../guards/roles.guard';
import { Roles } from '../../../decorators/roles.decorator';
import { ConfigService } from '../../../config/config/config.service';
import { NotificacionesService } from '../../../modules/notificaciones/notificaciones/notificaciones.service';
import { ApiUseTags } from '@nestjs/swagger';
import { HelpersService } from '../../helpers/helpers/helpers/helpers.service';
@ApiUseTags('Histórico de ausencias')
@Controller('hist-ausencias')
export class HistAusenciaController {
  constructor(
    private readonly histAusenciaService: HistAusenciaService,
    private readonly personaService: PersonaService,
    private readonly ausenciaService: AusenciaService,
    private readonly _configService: ConfigService,
    private readonly notificacionesService: NotificacionesService,
    private readonly helperService: HelpersService
  ) {}

  @Roles('ADMIN_TH', 'SUPER_ADMIN', 'ADMIN_SUPERVISOR')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Get('all')
  async findAll(
    @Request() { user }: { user: Persona },
    @Query('page') page = 1,
    @Query('limit') limit = 10
  ): Promise<Pagination<HistAusencias>> {
    limit = limit > 100 ? 100 : limit;
    let histAusencias;
    if (user.rol.nombre === 'ADMIN_SUPERVISOR') {
      histAusencias = await this.histAusenciaService.findAllSupervisor(user.id, {
        page,
        limit
      });
    } else {
      histAusencias = await this.histAusenciaService.findAllTH({
        page,
        limit
      });
    }

    return histAusencias;
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('dashboard/:type')
  async getDashboard(@Param('type') type: string): Promise<any> {
    const histPrestacionesPendientes = await this.histAusenciaService.findAllPendientes();
    if (type === 'motivo') {
      let estadisticas: any[] = [];

      const histAusencias = await this.histAusenciaService.findAllCurrentMonthGroupedByMotivo();
      estadisticas = histAusencias.map((histAusencias, index) => ({
        [`motivoNombre${index}`]: histAusencias['motivo_nombre'],
        count: parseInt(histAusencias.count, 10)
      }));

      return { estadisticas, pendientes: histPrestacionesPendientes };
    } else {
      if (type === 'horas') {
        let estadisticas = [
          { decision: 'Aprobadas', area: 0 },
          { decision: 'Rechazadas', area: 0 },
          { decision: 'Pendientes', area: 0 }
        ];
        const histAusencias = await this.histAusenciaService.findAllCurrentMonthByTipo(type);

        histAusencias.map((histAusencia) => {
          if (histAusencia.estatus === SolicitudEstatus.APROBADA) {
            estadisticas[0].area++;
          } else {
            if (histAusencia.estatus === SolicitudEstatus.RECHAZADA) {
              estadisticas[1].area++;
            } else {
              if (
                histAusencia.estatus === SolicitudEstatus.PENDIENTE ||
                histAusencia.estatus === SolicitudEstatus.APROBADA_SUPERVISOR
              ) {
                estadisticas[2].area++;
              }
            }
          }
        });

        return { estadisticas, pendientes: histPrestacionesPendientes };
      } else {
        let estadisticas = [
          {
            estatus0: 'pendientes',
            count: 0
          },

          {
            estatus1: 'rechazadas',
            count: 0
          },
          {
            estatus2: 'aprobadas',
            count: 0
          }
        ];
        const histAusencias = await this.histAusenciaService.findAllCurrentMonthByTipo(type);

        histAusencias.map((histAusencia) => {
          if (histAusencia.estatus === SolicitudEstatus.APROBADA) {
            estadisticas[2].count++;
          } else {
            if (histAusencia.estatus === SolicitudEstatus.RECHAZADA) {
              estadisticas[1].count++;
            } else {
              if (
                histAusencia.estatus === SolicitudEstatus.PENDIENTE ||
                histAusencia.estatus === SolicitudEstatus.APROBADA_SUPERVISOR
              ) {
                estadisticas[0].count++;
              }
            }
          }
        });

        return { estadisticas, pendientes: histPrestacionesPendientes };
      }
    }
  }

  @UseGuards(AuthGuard('jwt'))
  @Get(':id')
  async findById(@Param('id') id: number): Promise<HistAusencias> {
    let histAusencias: any = await this.histAusenciaService.findOneWithDetails(id);
    const cargoSupervisor = histAusencias.persona.histCargos[0].cargo.parentCargo;
    let supervisor = await this.personaService.findCurrentPersonaByCargo(cargoSupervisor.id);
    histAusencias.supervisor = supervisor;
    return histAusencias;
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('persona/:id/estadisticas')
  async getEstadisticas(@Param('id') id: number): Promise<any> {
    const histAusencias = await this.histAusenciaService.findAllCurrentYearByPersona(id);
    const estadisticas = [
      { month: '1', rejected: 0, approved: 0 },
      { month: '2', rejected: 0, approved: 0 },
      { month: '3', rejected: 0, approved: 0 },
      { month: '4', rejected: 0, approved: 0 },
      { month: '5', rejected: 0, approved: 0 },
      { month: '6', rejected: 0, approved: 0 },
      { month: '7', rejected: 0, approved: 0 },
      { month: '8', rejected: 0, approved: 0 },
      { month: '9', rejected: 0, approved: 0 },
      { month: '10', rejected: 0, approved: 0 },
      { month: '11', rejected: 0, approved: 0 },
      { month: '12', rejected: 0, approved: 0 }
    ];

    histAusencias.map((histAusencia) => {
      const mes = histAusencia.fechaSolicitud.getMonth();
      if (histAusencia.estatus === SolicitudEstatus.APROBADA) {
        estadisticas[mes] = { ...estadisticas[mes], approved: estadisticas[mes].approved + 1 };
      } else {
        if (histAusencia.estatus === SolicitudEstatus.RECHAZADA) {
          estadisticas[mes] = { ...estadisticas[mes], rejected: estadisticas[mes].rejected + 1 };
        }
      }
    });
    return estadisticas;
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('persona/:id')
  async findByPersona(
    @Param('id') id: number,
    @Query('page') page = 1,
    @Query('limit') limit = 10
  ): Promise<Pagination<HistAusencias>> {
    limit = limit > 100 ? 100 : limit;
    const histAusencias = await this.histAusenciaService.findAllByPersona(id, {
      page,
      limit
    });
    return histAusencias;
  }

  @UseGuards(AuthGuard('jwt'))
  @Post('create')
  @UseInterceptors(FileInterceptor('file'))
  async create(
    @Body() histAusenciaData: { body: string },
    @UploadedFile() file: Express.Multer.File
  ): Promise<HistAusencias> {
    try {
      let histAusencia = JSON.parse(histAusenciaData.body);
      const { solicitud, motivo, tipoDescuento } = histAusencia;
      histAusencia.ausencia = await this.ausenciaService.findBySolicitudMotivoAndTipoDescuento(
        solicitud.id,
        motivo.id,
        tipoDescuento.id
      );
      histAusencia.supervisor = await this.personaService.findSupervisor(histAusencia.persona.id);
      let firmaSolicitante = file;
      const histAusenciaCreada = await this.histAusenciaService.create(
        histAusencia,
        firmaSolicitante
      );
      histAusenciaCreada.supervisor = histAusencia.supervisor;
      // Send email to manager
      await this.helperService.createAusencia(histAusenciaCreada);
      return histAusenciaCreada;
    } catch (err) {
      console.log(err);
      throw new HttpException('Error al Crear el histórico de Ausencias', HttpStatus.BAD_REQUEST);
    }
  }

  @UseGuards(AuthGuard('jwt'))
  @Put('update/supervisor/:id')
  @UseInterceptors(FileInterceptor('file'))
  async updateBySupervisor(
    @Param('id') id: number,
    @Body() histAusenciaData: { body: string },
    @UploadedFile() file: Express.Multer.File,
    @Request() { user }: { user: Persona }
  ): Promise<HistAusencias> {
    let histAusenciaToChange = await this.histAusenciaService.findOne(id);
    if (!histAusenciaToChange) {
      throw new HttpException('El historial de ausencia no existe', HttpStatus.NOT_FOUND);
    }
    if (histAusenciaToChange.supervisor.id !== user.id) {
      throw new HttpException(
        'El usuario no es supervisor del empleado perteneciente a la solicitud de ausencia',
        HttpStatus.BAD_REQUEST
      );
    }

    const histAusenciaParse: UpdateHistAusenciasSupervisorDTO = JSON.parse(histAusenciaData.body);
    if (histAusenciaToChange.estatus !== SolicitudEstatus.PENDIENTE) {
      throw new HttpException(
        'La solicitud ya ha sido revisada, no puede editarse',
        HttpStatus.BAD_REQUEST
      );
    }
    if (
      histAusenciaParse.estatus === SolicitudEstatus.APROBADA ||
      histAusenciaParse.estatus === SolicitudEstatus.PENDIENTE
    ) {
      throw new HttpException(`Estatus Inválido`, HttpStatus.BAD_REQUEST);
    }
    if (
      histAusenciaParse.motivoRechazo &&
      histAusenciaParse.estatus === SolicitudEstatus.APROBADA_SUPERVISOR
    ) {
      throw new HttpException(`Solicitud Inválida`, HttpStatus.BAD_REQUEST);
    }
    if (
      histAusenciaParse.estatus === SolicitudEstatus.RECHAZADA &&
      !histAusenciaParse.motivoRechazo
    ) {
      throw new HttpException(`Solicitud Inválida`, HttpStatus.BAD_REQUEST);
    }
    try {
      histAusenciaToChange = {
        ...histAusenciaToChange,
        fechaRevision: histAusenciaParse.fechaRevision,
        motivoRechazo: histAusenciaParse.motivoRechazo,
        estatus: histAusenciaParse.estatus
      };

      const histAusencia = await this.histAusenciaService.updateBySupervisor(
        histAusenciaToChange,
        file
      );
      // SEND EMAIL AND NOTIFICATIONS
      await this.helperService.updateAusenciaBySup(histAusencia);
      return histAusencia;
    } catch (err) {
      throw new HttpException('Error al actualizar la Ausencia', HttpStatus.BAD_REQUEST);
    }
  }

  @UseGuards(AuthGuard('jwt'))
  @Put('update/talento-humano/:id')
  @UseInterceptors(FileInterceptor('file'))
  async updateByTh(
    @Param('id') id: number,
    @Body() histAusenciaData: { body: string },
    @UploadedFile() file: Express.Multer.File,
    @Request() { user }: { user: Persona }
  ): Promise<HistAusencias> {
    // Se verifica que la persona del request pertenece a talento humano
    let histAusenciaToChange = await this.histAusenciaService.findOne(id);

    if (!histAusenciaToChange) {
      throw new HttpException('El historial de ausencias no existe', HttpStatus.NOT_FOUND);
    }
    const talentoHumano = await this.personaService.findOneHumanTalent(user.id);
    if (!talentoHumano) {
      throw new HttpException(
        'La persona que está tratando de modificar la solicitud no pertenece a talento humano',
        HttpStatus.BAD_REQUEST
      );
    }
    const histAusenciaParse: UpdateHistAusenciasThDTO = JSON.parse(histAusenciaData.body);
    if (
      histAusenciaToChange.estatus === SolicitudEstatus.APROBADA ||
      histAusenciaToChange.estatus === SolicitudEstatus.RECHAZADA
    ) {
      throw new HttpException(
        'La solicitud ya ha sido revisada, no puede editarse',
        HttpStatus.BAD_REQUEST
      );
    }
    if (histAusenciaToChange.estatus === SolicitudEstatus.PENDIENTE) {
      throw new HttpException(
        'No puedes editar la solicitud hasta que el supervisor lo autorice',
        HttpStatus.BAD_REQUEST
      );
    }
    if (
      histAusenciaParse.estatus === SolicitudEstatus.APROBADA_SUPERVISOR ||
      histAusenciaParse.estatus === SolicitudEstatus.PENDIENTE
    ) {
      throw new HttpException(`Estatus Inválido`, HttpStatus.BAD_REQUEST);
    }
    if (
      histAusenciaParse.motivoRechazo &&
      histAusenciaParse.estatus === SolicitudEstatus.APROBADA
    ) {
      throw new HttpException(`Solicitud Inválida`, HttpStatus.BAD_REQUEST);
    }
    if (
      histAusenciaParse.estatus === SolicitudEstatus.RECHAZADA &&
      !histAusenciaParse.motivoRechazo
    ) {
      throw new HttpException(`Solicitud Inválida`, HttpStatus.BAD_REQUEST);
    }
    try {
      histAusenciaParse.talentoHumano = user;
      histAusenciaToChange = {
        ...histAusenciaToChange,
        fechaRevision: histAusenciaParse.fechaRevision,
        motivoRechazo: histAusenciaParse.motivoRechazo,
        talentoHumano: histAusenciaParse.talentoHumano,
        estatus: histAusenciaParse.estatus
      };
      const histAusencia = await this.histAusenciaService.updateByTh(histAusenciaToChange, file);
      // SEND EMAIL AND NOTIFICATIONS
      await this.helperService.updateAusenciaByTh(histAusencia);
      return histAusencia;
    } catch (err) {
      throw new HttpException('Error al actualizar la Ausencia', HttpStatus.BAD_REQUEST);
    }
  }
}
