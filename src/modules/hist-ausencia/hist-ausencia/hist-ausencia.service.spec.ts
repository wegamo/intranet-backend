import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { CreateHistAusenciasDTO } from '../../../models/hist-ausencias/dto/create-hist-ausencias.dto';
import { UpdateHistAusenciasThDTO } from '../../../models/hist-ausencias/dto/update-hist-ausencias-th.dto';
import { SampleHistAusencias, SamplePersona, } from '../../../../test/ObjectSamples';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { HistAusencias } from '../../../models/hist-ausencias/hist-ausencias.entity';
import { HistAusenciaService } from './hist-ausencia.service';
import { Pagination } from 'nestjs-typeorm-paginate';
import { SolicitudEstatus } from '../../../enums/solicitud-estatus.enum';
import { Aws3CntmService } from '../../aws3-cntm/aws3-cntm/aws3-cntm.service';

describe('HistAusenciaService', () => {
  let service: HistAusenciaService;
  let RepositoryMock: jest.Mock;
  let repository: Repository<HistAusencias>;
  
  beforeEach(() => {
    RepositoryMock = jest.fn(() => ({
      find: jest.fn(),
      save: jest.fn(),
      update: jest.fn(),
      delete: jest.fn(),
      createQueryBuilder: jest.fn(() => ({
        take: jest.fn().mockReturnThis(),
      })),
    }));
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        HistAusenciaService,
        { provide: getRepositoryToken(HistAusencias), useValue: new RepositoryMock() },
        Aws3CntmService
      ],
    }).compile();
    repository = module.get(getRepositoryToken(HistAusencias));
    service = module.get<HistAusenciaService>(HistAusenciaService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
  /*
  describe('findAll()', () => {
    let expectedHistAusencias: HistAusencias[];
    let result: HistAusencias[];

    describe('case : success', () => {
      describe('', () => {
        beforeEach(async () => {
          expectedHistAusencias = [SampleHistAusencias];
          (repository.find as jest.Mock).mockResolvedValue(expectedHistAusencias);
          result = await service.findAll();
        });

        it('should invoke repository.find()', () => {
          expect(repository.find).toHaveBeenCalledTimes(1);
        });

        it('should return an array of HistAusencias', () => {
          expect(result).toStrictEqual(expectedHistAusencias);
        });
      });
    });

    describe('case : failure', () => {
      describe('when there are not HistAusencias', () => {
        beforeEach(async () => {
          expectedHistAusencias = [];
          (repository.find as jest.Mock).mockResolvedValue(expectedHistAusencias);
          result = await service.findAll();
        });

        it('should invoke repository.find()', () => {
          expect(repository.find).toHaveBeenCalledTimes(1);
        });

        it('should return an empty array', () => {
          expect(result).toStrictEqual(expectedHistAusencias);
        });
      });
    });
  });
  */

  describe('findByPerson(id)', () => {
    let expectedHistAusencia:HistAusencias[];
    let result: Pagination<HistAusencias>;

    describe('case : success', () => {
      describe('when an HistAusencia is found', () => {
        beforeEach(async () => {
          expectedHistAusencia = [SampleHistAusencias];
          (repository.find as jest.Mock).mockResolvedValue(expectedHistAusencia);
          result = await service.findAllByPersona(SampleHistAusencias.persona.id,{page:1,limit:10});
        });

        /*it('should invoke repository.findOne()', () => {
          expect(repository.find).toHaveBeenCalledTimes(1);
          expect(repository.find).toHaveBeenCalledWith({persona:{id:SampleHistAusencias.persona.id}});
        });*/

        it('should return an HistAusencia', () => {
          expect(result.items).toStrictEqual(expectedHistAusencia);
        });
      });
    });

    describe('case : failure', () => {
      describe('when an HistAusencia is not found', () => {
        beforeEach(async () => {
          expectedHistAusencia = undefined;
          (repository.find as jest.Mock).mockResolvedValue(expectedHistAusencia);
          result = await service.findAllByPersona(SampleHistAusencias.persona.id,{page:1,limit:10});
        });

        /*it('should invoke repository.findOne()', () => {
          expect(repository.find).toHaveBeenCalledTimes(1);
          expect(repository.find).toHaveBeenCalledWith({persona:{id:SampleHistAusencias.persona.id}});
        });*/

        it('should return undefined', () => {
          expect(result).toStrictEqual(expectedHistAusencia);
        });
      });
    });
  });
  /*
  describe('create(CreateHistAusenciaDTO)', () => {
    const { id, ...rest } = SampleHistAusencias;
    let body: CreateHistAusenciasDTO;

    describe('case : success', () => {
      let expectedHistAusencia: HistAusencias;
      let result: HistAusencias;

      describe('when an HistAusencia is created', () => {
        beforeEach(async () => {
          body = rest;
          expectedHistAusencia = SampleHistAusencias;
          (repository.save as jest.Mock).mockResolvedValue(expectedHistAusencia);
          result = await service.create(body);
        });

        it('should invoke repository.save()', () => {
          expect(repository.save).toHaveBeenCalledTimes(1);
          expect(repository.save).toHaveBeenCalledWith(body);
        });

        it('should create a HistAusencia', () => {
          expect(result).toStrictEqual(expectedHistAusencia);
        });
      });
    });

    describe('case : failure', () => {
      let expectedError: TypeError;
      let body: undefined;

      describe('when a HistAusencia is not created', () => {
        beforeEach(async () => {
          body = undefined;
          expectedError = new TypeError();
          jest.spyOn(service, 'create').mockRejectedValue(expectedError);
        });

        it('it should throw when the body is undefined', async () => {
          await expect(service.create(body)).rejects.toThrow(TypeError);
        });

        it('should not create an HistAusencia', () => {
          expect(repository.save).not.toHaveBeenCalled();
        });
      });
    });
  });
  */
  describe('update(id, UpdateHistAusenciaDTO)', () => {
    let update: HistAusencias;

    describe('case : success', () => {
      let updateResult: HistAusencias;
      let result: HistAusencias;

      describe('when a HistAusencia is updated', () => {
        beforeEach(async () => {
          update = {
            fechaRevision:new Date(),
            firmaTh:'SIGNOUTPUT',
            talentoHumano:SamplePersona,
            estatus:SolicitudEstatus.APROBADA,
            ...SampleHistAusencias
          };
          //updateResult = new UpdateResult();
          (repository.update as jest.Mock).mockResolvedValue(updateResult);
          result = await service.updateByTh(update);
        });

        it('should invoke repository.update()', () => {
          expect(repository.update).toHaveBeenCalledTimes(1);
          expect(repository.update).toHaveBeenCalledWith(1, update);
        });

        it('should update an HistAusencia', () => {
          expect(result).toStrictEqual(updateResult);
        });
      });
    });

    describe('case : failure', () => {
      let expectedError: TypeError;

      describe('when a HistAusencia is not updated', () => {
        beforeEach(async () => {
          expectedError = new TypeError();
          update = undefined;
          jest.spyOn(service, 'updateByTh').mockRejectedValue(expectedError);
        });

        it('should throw when the body is undefined', async () => {
          await expect(service.updateByTh(update)).rejects.toThrow(TypeError);
        });

        it('should not update an HistAusencia', () => {
          expect(repository.update).not.toHaveBeenCalled();
        });
      });
    });
  });
  /*
  describe('delete(id)', () => {
    let deleteResult: DeleteResult;
    let result: DeleteResult;

    describe('case : success', () => {
      describe('when an HistAusencia is found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 1;
          (repository.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await service.delete(1);
        });

        it('should invoke repository.delete()', () => {
          expect(repository.delete).toHaveBeenCalledTimes(1);
          expect(repository.delete).toHaveBeenCalledWith(1);
        });

        it('should delete an HistAusencia', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });

    describe('case : failure', () => {
      let expectedError: TypeError;

      describe('when a HistAusencia is not found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 0;
          (repository.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await service.delete(1);
        });

        it('should invoke repository.delete()', () => {
          expect(repository.delete).toHaveBeenCalledTimes(1);
          expect(repository.delete).toHaveBeenCalledWith(1);
        });

        it('should not delete an HistAusencia', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });
  });
  */
});

