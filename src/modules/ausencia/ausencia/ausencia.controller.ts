import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { Ausencia } from '../../../models/ausencia/ausencia.entity';
import { DeleteResult, UpdateResult } from 'typeorm';
import { AusenciaService } from './ausencia.service';
import { MotivoAusenciaService } from '../../motivo-ausencia/motivo-ausencia/motivo-ausencia.service';
import { promises } from 'fs';
import { TipoDescuentoService } from '../../tipo-descuento/tipo-descuento/tipo-descuento.service';
import { ApiBearerAuth, ApiUseTags } from '@nestjs/swagger';

@ApiBearerAuth()
@ApiUseTags('Ausencia')
@Controller('ausencia')
export class AusenciaController {
  constructor(
    private readonly ausenciaService: AusenciaService,
    private readonly motivoService: MotivoAusenciaService,
    private readonly tipoDescuentoService: TipoDescuentoService
  ) {}

  @Get('all')
  findAll(): Promise<Ausencia[]> {
    return this.ausenciaService.findAll();
  }

  @Get('motivos/solicitud/:id')
  findAllWithMotivosBySolicitud(@Param('id') id: number): Promise<Ausencia[]> {
    return this.ausenciaService.findWithMotivosBySolicitud(id);
  }

  @Get('tipos-descuento/by-motivo/:idMotivo/:idSolicitud')
  findAllWithTipoDescuentoByMotivo(
    @Param('idMotivo') idMotivo: number,
    @Param('idSolicitud') idSolicitud: number
  ): Promise<Ausencia[]> {
    return this.ausenciaService.findWithTipoDescuentoByMotivo(idMotivo, idSolicitud);
  }

  @Get('all/organized')
  async findOrganized(): Promise<any[]> {
    let solicitudes: any[] = (await this.ausenciaService.findWithSolicitud()).map(
      (ausencia) => ausencia.solicitud
    );

    let solicitudesMotivosPromesas = solicitudes.map(async (solicitud) => {
      let motivos = await this.motivoService.findBySolicitud(solicitud.id);
      return { ...solicitud, motivos };
    });

    solicitudes = await Promise.all(solicitudesMotivosPromesas);

    const solicitudesTipoDescuentoPromesas = solicitudes.map(async (solicitud) => {
      let motivosPromesas = solicitud.motivos.map(async (motivo) => {
        let tiposDescuentos = await this.tipoDescuentoService.findByMotivo(motivo.id, solicitud.id);
        return { ...motivo, tiposDescuentos };
      });
      let motivos = await Promise.all(motivosPromesas);
      return { ...solicitud, motivos };
    });

    return await Promise.all(solicitudesTipoDescuentoPromesas);
  }
}
