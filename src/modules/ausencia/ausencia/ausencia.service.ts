import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Ausencia } from '../../../models/ausencia/ausencia.entity';
import { Repository } from 'typeorm';

@Injectable()
export class AusenciaService {
  constructor(
    @InjectRepository(Ausencia) private readonly ausenciaRepository: Repository<Ausencia>
  ) {}

  findAll(): Promise<Ausencia[]> {
    return this.ausenciaRepository.find({
      relations: ['solicitud', 'motivo', 'tipoDescuento']
    });
  }

  findWithSolicitud(): Promise<Ausencia[]> {
    return this.ausenciaRepository
      .createQueryBuilder('ausencia')
      .distinctOn(['solicitud.id'])
      .innerJoinAndSelect('ausencia.solicitud', 'solicitud')
      .getMany();
  }

  findWithMotivosBySolicitud(idSolicitud: number): Promise<Ausencia[]> {
    return this.ausenciaRepository
      .createQueryBuilder('ausencia')
      .distinctOn(['motivo.id'])
      .innerJoinAndSelect('ausencia.motivo', 'motivo')
      .where('ausencia.solicitud = :idSolicitud', { idSolicitud })
      .getMany();
  }

  findBySolicitudMotivoAndTipoDescuento(
    idSolicitud: number,
    idMotivo: number,
    idTipoDescuento: number
  ): Promise<Ausencia> {
    return this.ausenciaRepository.findOne({
      where: { solicitud: idSolicitud, motivo: idMotivo, tipoDescuento: idTipoDescuento }
    });
  }

  findWithTipoDescuentoByMotivo(idMotivo: number, idSolicitud: number): Promise<Ausencia[]> {
    return this.ausenciaRepository
      .createQueryBuilder('ausencia')
      .distinctOn(['tipoDescuento.id'])
      .innerJoinAndSelect('ausencia.tipoDescuento', 'tipoDescuento')
      .where('ausencia.motivo = :idMotivo AND ausencia.solicitud = :idSolicitud', {
        idMotivo,
        idSolicitud
      })
      .getMany();
  }
}
