import { Test, TestingModule } from '@nestjs/testing';
import { MotivoAusenciaService } from '../../motivo-ausencia/motivo-ausencia/motivo-ausencia.service';
import { TipoDescuentoService } from '../../tipo-descuento/tipo-descuento/tipo-descuento.service';
import { AusenciaController } from './ausencia.controller';
import { AusenciaService } from './ausencia.service';

describe('Ausencia Controller', () => {
  let controller: AusenciaController;
  let AusenciaServiceMock: jest.Mock<Partial<AusenciaService>>;
  let MotivoAusenciaServiceMock: jest.Mock<Partial<MotivoAusenciaService>>;
  let TipoDescuentoServiceMock: jest.Mock<Partial<TipoDescuentoService>>;
  let ausenciaService: AusenciaService;

  beforeEach(() => {
    AusenciaServiceMock = jest.fn<Partial<AusenciaService>, AusenciaService[]>(() => ({

    }));
    MotivoAusenciaServiceMock = jest.fn<Partial<MotivoAusenciaService>, MotivoAusenciaService[]>(() => ({

    })); 
    TipoDescuentoServiceMock = jest.fn<Partial<TipoDescuentoService>, TipoDescuentoService[]>(() => ({

    })); 
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AusenciaController],
      providers: [
        { provide: AusenciaService, useValue: new AusenciaServiceMock() },
        { provide: MotivoAusenciaService, useValue: new MotivoAusenciaServiceMock() },
        { provide: TipoDescuentoService, useValue: new TipoDescuentoServiceMock() }]
    }).compile();

    ausenciaService = module.get<AusenciaService>(AusenciaService);
    controller = module.get<AusenciaController>(AusenciaController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
