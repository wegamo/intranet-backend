import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Ausencia } from '../../../models/ausencia/ausencia.entity';
import { Repository } from 'typeorm';
import { AusenciaService } from './ausencia.service';

describe('AusenciaService', () => {
  let service: AusenciaService;
  let RepositoryMock: jest.Mock;
  let repository: Repository<Ausencia>;

  beforeEach(() => {
    RepositoryMock = jest.fn(() => ({
      find: jest.fn(),
      findOne: jest.fn(),
      save: jest.fn(),
      update: jest.fn(),
      delete: jest.fn()
    }));
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AusenciaService,
        { provide: getRepositoryToken(Ausencia), useValue: new RepositoryMock() },
      ]
    }).compile();

    repository = module.get(getRepositoryToken(Ausencia));
    service = module.get<AusenciaService>(AusenciaService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
