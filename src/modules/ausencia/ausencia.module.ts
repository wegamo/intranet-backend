import { Module } from '@nestjs/common';
import { AusenciaService } from './ausencia/ausencia.service';
import { AusenciaController } from './ausencia/ausencia.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Ausencia } from '../../models/ausencia/ausencia.entity';
import { MotivoAusenciaModule } from '../motivo-ausencia/motivo-ausencia.module';
import { MotivoAusenciaService } from '../motivo-ausencia/motivo-ausencia/motivo-ausencia.service';
import { TipoDescuentoModule } from '../tipo-descuento/tipo-descuento.module';
import { TipoDescuentoService } from '../tipo-descuento/tipo-descuento/tipo-descuento.service';

@Module({
  imports: [TypeOrmModule.forFeature([Ausencia]), MotivoAusenciaModule, TipoDescuentoModule],
  providers: [AusenciaService, MotivoAusenciaService, TipoDescuentoService],
  controllers: [AusenciaController]
})
export class AusenciaModule {}
