import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Put,
  UploadedFile,
  UseInterceptors
} from '@nestjs/common';
import { CreateCursoDTO } from '../../../models/curso/dto/create-curso.dto';
import { DeleteResult } from 'typeorm';
import { Curso } from '../../../models/curso/curso.entity';
import { CursoService } from './curso.service';
import { UpdateCursoDTO } from '../../../models/curso/dto/update-curso.dto';
import { TipoGrado } from '../../../enums/tipo-grado.enum';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiBearerAuth, ApiUseTags } from '@nestjs/swagger';

@ApiUseTags('Curso')
@Controller('curso')
export class CursoController {
  constructor(private readonly cursoService: CursoService) {}

  @Get()
  findAll(): Promise<Curso[]> {
    return this.cursoService.findAll();
  }

  @Get('persona/:id')
  findByPerson(@Param('id') id: number): Promise<Curso[]> {
    return this.cursoService.findByPerson(id);
  }

  @Get('tipos-grado')
  getTiposGrado() {
    return TipoGrado;
  }

  @Post('create')
  @UseInterceptors(FileInterceptor('file'))
  async create(
    @Body() cursoData: { body: string },
    @UploadedFile() file: Express.Multer.File
  ): Promise<Curso> {
    try {
      let curso = JSON.parse(cursoData.body);
      let certificado = file;
      return await this.cursoService.create(curso, certificado);
    } catch (err) {
      throw new HttpException('Error al Crear el Curso', HttpStatus.BAD_REQUEST);
    }
  }

  @Put('update/:id')
  @UseInterceptors(FileInterceptor('file'))
  async update(
    @Param('id') id: number,
    @Body() cursoData: { body: string },
    @UploadedFile() file: Express.Multer.File
  ): Promise<Curso> {
    try {
      const body: UpdateCursoDTO = JSON.parse(cursoData.body);
      let curso = await this.cursoService.findOne(id);
      if (!curso) {
        throw new HttpException('El curso no existe', HttpStatus.BAD_REQUEST);
      }
      curso = { ...curso, ...body };
      return await this.cursoService.update(curso, file);
    } catch (err) {
      throw new HttpException('Error al actualizar el Curso', HttpStatus.BAD_REQUEST);
    }
  }

  @Delete(':id')
  async delete(@Param('id') id: number): Promise<DeleteResult> {
    try {
      let curso = await this.cursoService.findOne(id);
      if (!curso) {
        throw new HttpException('El curso no existe', HttpStatus.BAD_REQUEST);
      }
      return this.cursoService.delete(curso);
    } catch (err) {
      throw new HttpException('Error al eliminar el Curso', HttpStatus.BAD_REQUEST);
    }
  }
}
