import { Test, TestingModule } from '@nestjs/testing';
import { CreateCursoDTO } from '../../../models/curso/dto/create-curso.dto';
import { Aws3CntmService } from '../../aws3-cntm/aws3-cntm/aws3-cntm.service';
import { SampleCurso } from '../../../../test/ObjectSamples';
import { DeleteResult } from 'typeorm';
import { Curso } from '../../../models/curso/curso.entity';
import { CursoController } from './curso.controller';
import { CursoService } from './curso.service';

describe('Curso Controller Module Test', () => {
  let controller: CursoController;
  let cursoServiceMock: jest.Mock<Partial<CursoService>>;
  let aws3ServiceMock: jest.Mock<Partial<Aws3CntmService>>;
  let cursoService: CursoService;

  beforeEach(() => {
    cursoServiceMock = jest.fn<Partial<CursoService>, CursoService[]>(() => ({
      findAll: jest.fn(),
      findByPerson: jest.fn(),
      create: jest.fn(),
      delete: jest.fn(),
    }));
    aws3ServiceMock = jest.fn<Partial<Aws3CntmService>, Aws3CntmService[]>(() => ({

    }));
  });


  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CursoController],
      providers: [
        { provide: CursoService, useValue: new cursoServiceMock() },
        { provide: Aws3CntmService, useValue: new aws3ServiceMock() }
      ]
    }).compile();

    cursoService = module.get<CursoService>(CursoService);
    controller = module.get<CursoController>(CursoController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
  describe('findAll()', () => {
    let expectedCursos: Curso[];
    let result: Curso[];

    describe('case : success', () => {
      describe('when there are roles', () => {
        beforeEach(async () => {
          expectedCursos = [SampleCurso];
          (cursoService.findAll as jest.Mock).mockResolvedValue(expectedCursos);
          result = await controller.findAll();
        });
        it('should invoke cursoService.findAll()', () => {
          expect(cursoService.findAll).toHaveBeenCalledTimes(1);
        });
        it('should return an array of roles', () => {
          expect(result).toStrictEqual(expectedCursos);
        });
      });
    });

    describe('case : failure', () => {
      describe('when there are no roles', () => {
        beforeEach(async () => {
          expectedCursos = [];
          (cursoService.findAll as jest.Mock).mockResolvedValue(expectedCursos);
          result = await controller.findAll();
        });

        it('should invoke cursoService.findAll()', () => {
          expect(cursoService.findAll).toHaveBeenCalledTimes(1);
        });

        it('should return an array empty', () => {
          expect(result).toStrictEqual(expectedCursos);
        });
      });
    });
  });
  
  describe('findByPerson(id)', () => {
    let expectedCursos: Curso[];
    let result: Curso[];

    describe('case : success', () => {
      describe('when there are cursos of a person', () => {
        beforeEach(async () => {
          expectedCursos = [SampleCurso];
          (cursoService.findByPerson as jest.Mock).mockResolvedValue(expectedCursos);
          result = await controller.findByPerson(SampleCurso.persona.id);
        });
        it('should invoke cursoService.findByPerson(id)', () => {
          expect(cursoService.findByPerson).toHaveBeenCalledTimes(1);
        });
        it('should return an array of roles', () => {
          expect(result).toStrictEqual(expectedCursos);
        });
      });
    });

    describe('case : failure', () => {
      describe('when there are not cursos of a person', () => {
        beforeEach(async () => {
          expectedCursos = [];
          (cursoService.findByPerson as jest.Mock).mockResolvedValue(expectedCursos);
          result = await controller.findByPerson(SampleCurso.persona.id);
        });

        it('should invoke cursoService.findByPerson(id)', () => {
          expect(cursoService.findByPerson).toHaveBeenCalledTimes(1);
        });

        it('should return an array empty', () => {
          expect(result).toStrictEqual(expectedCursos);
        });
      });
    });
  });

  /*describe('create(CreateCargoDTO)', () => {
    const { id, ...rest } = SampleCurso;
    let body: CreateCursoDTO;

    describe('case : success', () => {
      let expectedCurso: Curso;
      let result: Curso;

      describe('when a role is created', () => {
        beforeEach(async () => {
          body = rest;
          expectedCurso = SampleCurso;
          (cursoService.create as jest.Mock).mockResolvedValue(expectedCurso);
          result = await controller.create(body);
        });
        it('should invoke cursoService.create()', () => {
          expect(cursoService.create).toHaveBeenCalledTimes(1);
          expect(cursoService.create).toHaveBeenCalledWith(body);
        });
        it('should create a role', () => {
          expect(result).toStrictEqual(expectedCurso);
        });
      });
    });

    describe('case : failure', () => {
      let expectedError: TypeError;
      describe('when a categoria-noticia is not created', () => {
        beforeEach(async () => {
          body = undefined;
          expectedError = new TypeError();
          jest.spyOn(controller, 'create').mockRejectedValue(expectedError);
        });
        it('should throw when the body is undefined', async () => {
          await expect(controller.create(body)).rejects.toThrow(TypeError);
        });
        it('should not create a role', () => {
          expect(cursoService.create).not.toHaveBeenCalled();
        });
      });
    });
  });*/
 
  describe('delete(id)', () => {
    let deleteResult: DeleteResult;
    let result: DeleteResult;

    describe('case : success', () => {
      describe('when an curso is found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 1;
          (cursoService.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await controller.delete(1);
        });
        it('should invoke cursoService.delete()', () => {
          expect(cursoService.delete).toHaveBeenCalledTimes(1);
          expect(cursoService.delete).toHaveBeenCalledWith(1);
        });
        it('should delete a curso', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });

    describe('case : failure', () => {
      let expectedError: TypeError;
      describe('when an role is not found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 0;
          (cursoService.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await controller.delete(1);
        });

        it('should invoke cursoService.delete()', () => {
          expect(cursoService.delete).toHaveBeenCalledTimes(1);
          expect(cursoService.delete).toHaveBeenCalledWith(1);
        });

        it('should not delete an role', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });
  });
});
