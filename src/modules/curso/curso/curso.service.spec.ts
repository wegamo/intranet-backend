import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Curso } from '../../../models/curso/curso.entity';
import { CreateCursoDTO } from '../../../models/curso/dto/create-curso.dto';
import { SampleCurso } from '../../../../test/ObjectSamples';
import { DeleteResult, Repository } from 'typeorm';
import { CursoService } from './curso.service';
import { Aws3CntmService } from '../../aws3-cntm/aws3-cntm/aws3-cntm.service';

describe('CursoService', () => {
  let service: CursoService;
  let RepositoryMock: jest.Mock;
  let repository: Repository<Curso>;
  
  beforeEach(() => {
    RepositoryMock = jest.fn(() => ({
      find: jest.fn(),
      save: jest.fn(),
      delete: jest.fn()
    }));
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CursoService,
        { provide: getRepositoryToken(Curso), useValue: new RepositoryMock() },
        Aws3CntmService,
      ],
    }).compile();

    repository = module.get(getRepositoryToken(Curso));
    service = module.get<CursoService>(CursoService);
  
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('findAll()', () => {
    let expectedCursos: Curso[];
    let result: Curso[];

    describe('case : success', () => {
      describe('', () => {
        beforeEach(async () => {
          expectedCursos = [SampleCurso];
          (repository.find as jest.Mock).mockResolvedValue(expectedCursos);
          result = await service.findAll();
        });

        it('should invoke repository.find()', () => {
          expect(repository.find).toHaveBeenCalledTimes(1);
        });

        it('should return an array of roles', () => {
          expect(result).toStrictEqual(expectedCursos);
        });
      });
    });

    describe('case : failure', () => {
      describe('when there are not roles', () => {
        beforeEach(async () => {
          expectedCursos = [];
          (repository.find as jest.Mock).mockResolvedValue(expectedCursos);
          result = await service.findAll();
        });

        it('should invoke repository.find()', () => {
          expect(repository.find).toHaveBeenCalledTimes(1);
        });

        it('should return an empty array', () => {
          expect(result).toStrictEqual(expectedCursos);
        });
      });
    });
  });

  describe('findByPerson()', () => {
    let expectedCursos: Curso[];
    let result: Curso[];

    describe('case : success', () => {
      describe('', () => {
        beforeEach(async () => {
          expectedCursos = [SampleCurso];
          (repository.find as jest.Mock).mockResolvedValue(expectedCursos);
          result = await service.findByPerson(SampleCurso.persona.id);
        });

        it('should invoke repository.find()', () => {
          expect(repository.find).toHaveBeenCalledTimes(1);
        });

        it('should return an array of roles', () => {
          expect(result).toStrictEqual(expectedCursos);
        });
      });
    });

    describe('case : failure', () => {
      describe('when there are not roles', () => {
        beforeEach(async () => {
          expectedCursos = [];
          (repository.find as jest.Mock).mockResolvedValue(expectedCursos);
          result = await service.findByPerson(SampleCurso.persona.id);
        });

        it('should invoke repository.find()', () => {
          expect(repository.find).toHaveBeenCalledTimes(1);
        });

        it('should return an empty array', () => {
          expect(result).toStrictEqual(expectedCursos);
        });
      });
    });
  });
  /*
  describe('create(CreateCursoDTO)', () => {
    const { id, ...rest } = SampleCurso;
    let body: CreateCursoDTO;

    describe('case : success', () => {
      let expectedCurso: Curso;
      let result: Curso;

      describe('when an Curso is created', () => {
        beforeEach(async () => {
          body = rest;
          expectedCurso = SampleCurso;
          (repository.save as jest.Mock).mockResolvedValue(expectedCurso);
          result = await service.create(body);
        });

        it('should invoke repository.save()', () => {
          expect(repository.save).toHaveBeenCalledTimes(1);
          expect(repository.save).toHaveBeenCalledWith(body);
        });

        it('should create a curso', () => {
          expect(result).toStrictEqual(expectedCurso);
        });
      });
    });

    describe('case : failure', () => {
      let expectedError: TypeError;
      let body: undefined;

      describe('when a curso is not created', () => {
        beforeEach(async () => {
          body = undefined;
          expectedError = new TypeError();
          jest.spyOn(service, 'create').mockRejectedValue(expectedError);
        });

        it('it should throw when the body is undefined', async () => {
          await expect(service.create(body)).rejects.toThrow(TypeError);
        });

        it('should not create an role', () => {
          expect(repository.save).not.toHaveBeenCalled();
        });
      });
    });
  });
  */
  describe('delete(id)', () => {
    let deleteResult: DeleteResult;
    let result: DeleteResult;

    describe('case : success', () => {
      describe('when an role is found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 1;
          (repository.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await service.delete(1);
        });

        it('should invoke repository.delete()', () => {
          expect(repository.delete).toHaveBeenCalledTimes(1);
          expect(repository.delete).toHaveBeenCalledWith(1);
        });

        it('should delete an role', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });

    describe('case : failure', () => {
      let expectedError: TypeError;

      describe('when a role is not found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 0;
          (repository.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await service.delete(1);
        });

        it('should invoke repository.delete()', () => {
          expect(repository.delete).toHaveBeenCalledTimes(1);
          expect(repository.delete).toHaveBeenCalledWith(1);
        });

        it('should not delete an role', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });
  }); 
});
