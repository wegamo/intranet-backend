import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Curso } from '../../../models/curso/curso.entity';
import { DeleteResult, Repository } from 'typeorm';
import configService from '../../../config/config/config.service';
import { Aws3CntmService } from '../../aws3-cntm/aws3-cntm/aws3-cntm.service';
import { CreateCursoDTO } from '../../../models/curso/dto/create-curso.dto';
import { UpdateCursoDTO } from '../../../models/curso/dto/update-curso.dto';

const AWS_S3_BUCKET_NAME = configService.get('AWS_S3_BUCKET_NAME');
@Injectable()
export class CursoService {
  constructor(
    @InjectRepository(Curso)
    private readonly cursoRepository: Repository<Curso>,
    private readonly s3Service: Aws3CntmService
  ) {}

  findOne(id: number): Promise<Curso> {
    return this.cursoRepository.findOne(id);
  }

  async findAll(): Promise<Curso[]> {
    return this.cursoRepository.find();
  }

  async findByPerson(personId: number): Promise<Curso[]> {
    return await this.cursoRepository.find({ persona: { id: personId } });
  }

  async create(cursoData: CreateCursoDTO, certificado: Express.Multer.File): Promise<Curso> {
    let curso = await this.cursoRepository.save(cursoData);
    let mediaExt = certificado.originalname.replace(/(^.*\.)/gi, '.');
    let urlImagen = `https://${AWS_S3_BUCKET_NAME}/complex/cursos/curso_${curso.id}${mediaExt}`;
    try {
      await this.s3Service.uploadCourses(curso.id, certificado, mediaExt);
      curso.certificado = urlImagen;
      return this.cursoRepository.save(curso);
    } catch (err) {
      throw new HttpException('Error al subir el archivo del Curso', HttpStatus.BAD_REQUEST);
    }
  }

  async update(curso: Curso, file: Express.Multer.File): Promise<Curso> {
    if (file) {
      let mediaExt = file.originalname.replace(/(^.*\.)/gi, '.');
      let urlImagen = `https://${AWS_S3_BUCKET_NAME}/complex/cursos/curso_${curso.id}${mediaExt}`;
      await this.s3Service.uploadCourses(curso.id, file, mediaExt);
      curso.certificado = urlImagen;
    }
    return this.cursoRepository.save(curso);
  }

  async delete(curso: Curso): Promise<DeleteResult> {
    if (curso.certificado) {
      const mediaExt = curso.certificado
        .split('https://galileo.andromedaventures.net/complex/cursos/')[1]
        .split('.')[1];
      await this.s3Service.deleteCourse(curso.id, mediaExt);
    }
    return this.cursoRepository.delete(curso.id);
  }
}
