import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Curso } from '../../models/curso/curso.entity';
import { Aws3CntmModule } from '../aws3-cntm/aws3-cntm.module';
import { Aws3CntmService } from '../aws3-cntm/aws3-cntm/aws3-cntm.service';
import { CursoController } from './curso/curso.controller';
import { CursoService } from './curso/curso.service';

@Module({
  imports: [TypeOrmModule.forFeature([Curso]), Aws3CntmModule],
  controllers: [CursoController],
  providers: [CursoService, Aws3CntmService]
})
export class CursoModule {}
