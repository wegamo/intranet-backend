import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';

import { CreateRolDTO } from '../../../models/rol/dto/create-rol.dto';
import { UpdateRolDTO } from '../../../models/rol/dto/update-rol.dto';
import { Rol } from '../../../models/rol/rol.entity';

@Injectable()
export class RolService {
  constructor(
    @InjectRepository(Rol)
    private readonly rolRepository: Repository<Rol>
  ) {}

  async findOne(id: number): Promise<Rol> {
    return await this.rolRepository.findOne(id);
  }

  async findAll(): Promise<Rol[]> {
    return await this.rolRepository.find();
  }

  async create(rol: CreateRolDTO): Promise<Rol> {
    return await this.rolRepository.save(rol);
  }

  async update(id: number, rol: UpdateRolDTO): Promise<UpdateResult> {
    return await this.rolRepository.update(id, rol);
  }

  async delete(id: number): Promise<DeleteResult> {
    return await this.rolRepository.delete(id);
  }
}
