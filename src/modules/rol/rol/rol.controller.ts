import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Put
} from '@nestjs/common';
import { DeleteResult, UpdateResult } from 'typeorm';

import { CreateRolDTO } from '../../../models/rol/dto/create-rol.dto';
import { UpdateRolDTO } from '../../../models/rol/dto/update-rol.dto';
import { Rol } from '../../../models/rol/rol.entity';

import { RolService } from './rol.service';

@Controller('rol')
export class RolController {
  constructor(private readonly rolService: RolService) {}

  @Get('all')
  findAll(): Promise<Rol[]> {
    return this.rolService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: number): Promise<Rol> {
    return this.rolService.findOne(id);
  }

  @Post()
  async create(@Body() RolData: CreateRolDTO): Promise<Rol> {
    return this.rolService.create(RolData);
  }

  @Put(':id')
  async update(@Param('id') id: number, @Body() RolData: UpdateRolDTO): Promise<UpdateResult> {
    return this.rolService.update(id, RolData);
  }

  @Delete(':id')
  async delete(@Param('id') id: number): Promise<DeleteResult> {
    try {
      return this.rolService.delete(id);
    } catch (err) {
      throw new HttpException('Error al eliminar el rol', HttpStatus.BAD_REQUEST);
    }
  }
}
