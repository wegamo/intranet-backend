import { Test, TestingModule } from '@nestjs/testing';
import { DeleteResult, UpdateResult } from 'typeorm';

import { CreateRolDTO } from '../../../models/rol/dto/create-rol.dto';
import { UpdateRolDTO } from '../../../models/rol/dto/update-rol.dto';
import { Rol } from '../../../models/rol/rol.entity';

import { RolController } from './rol.controller';
import { RolService } from './rol.service';

import { SampleRol } from './../../../../test/ObjectSamples';
//import { RolInt } from './../../dto_response/Rol_Payload';
import { GlobalException } from './../../exception/global-exception';

describe('Rol Controller', () => {
  let controller: RolController;
  let RolServiceMock: jest.Mock<Partial<RolService>>;
  let rolService: RolService;

  beforeEach(() => {
    RolServiceMock = jest.fn<Partial<RolService>, RolService[]>(() => ({
      findAll: jest.fn(),
      findOne: jest.fn(),
      create: jest.fn(),
      delete: jest.fn(),
      update: jest.fn()
    }));
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [RolController],
      providers: [{ provide: RolService, useValue: new RolServiceMock() }]
    }).compile();

    rolService = module.get<RolService>(RolService);
    controller = module.get<RolController>(RolController);
  });

  describe('findAll()', () => {
    let expectedRoles: Rol[];
    let result: Rol[];

    describe('case: success', () => {
      describe('when there are roles', () => {
        beforeEach(async () => {
          expectedRoles = [
            SampleRol
          ];
          (rolService.findAll as jest.Mock).mockResolvedValue(expectedRoles);
          result = await controller.findAll();
        });

        it('should invoke RolService.findAll()', () => {
          expect(rolService.findAll).toHaveBeenCalledTimes(1);
        });

        it('should return an array of roles', () => {
          expect(result).toStrictEqual(expectedRoles);
        });
      });
    });

    describe('case: failure', () => {
      describe('when there are not roles', () => {
        beforeEach(async () => {
          expectedRoles = [];
          (rolService.findAll as jest.Mock).mockResolvedValue(expectedRoles);
          result = await controller.findAll();
        });

        it('should invoke RolService.findAll()', () => {
          expect(rolService.findAll).toHaveBeenCalledTimes(1);
        });

        it('should return an array empty', () => {
          expect(result).toStrictEqual(expectedRoles);
        });
      });
    });
  });

  describe('findOne(id)', () => {
    let expectedRol: Rol;
    let result: Rol;

    describe('case: success', () => {
      describe('when a role is found', () => {
        beforeEach(async () => {
          expectedRol = SampleRol;
          (rolService.findOne as jest.Mock).mockResolvedValue(expectedRol);
          result = await controller.findOne(1);
        });

        it('should invoke RolService.findOne()', () => {
          expect(rolService.findOne).toHaveBeenCalledTimes(1);
          expect(rolService.findOne).toHaveBeenCalledWith(1);
        });

        it('should return a role', () => {
          expect(result).toStrictEqual(expectedRol);
        });
      });
    });

    describe('case: failure', () => {
      describe('when a role is not found', () => {
        beforeEach(async () => {
          expectedRol = undefined;
          (rolService.findOne as jest.Mock).mockResolvedValue(expectedRol);
          result = await controller.findOne(1);
        });

        it('should invoke RolService.findOne()', () => {
          expect(rolService.findOne).toHaveBeenCalledTimes(1);
          expect(rolService.findOne).toHaveBeenCalledWith(1);
        });

        it('should return undefined', () => {
          expect(result).toStrictEqual(expectedRol);
        });
      });
    });
  });

  describe('create(rolData)', () => {
    const {id,...rest} = SampleRol;
    let body: CreateRolDTO;

    describe('case: success', () => {
      let expectedRol: Rol;
      let result: Rol;

      describe('when a role is created', () => {
        beforeEach(async () => {
          body = rest;
          expectedRol = SampleRol;
          (rolService.create as jest.Mock).mockResolvedValue(expectedRol);
          result = await controller.create(body);
        });

        it('should invoke RolService.create()', () => {
          expect(rolService.create).toHaveBeenCalledTimes(1);
          expect(rolService.create).toHaveBeenCalledWith(body);
        });

        it('should create a role', () => {
          expect(result).toStrictEqual(expectedRol);
        });
      });
    });

    describe('case: failure', () => {
      let expectedError: TypeError;

      describe('when a role is not created', () => {
        beforeEach(async () => {
          body = undefined;
          expectedError = new TypeError();
          jest.spyOn(controller, 'create').mockRejectedValue(expectedError);
        });

        it('should throw when the body is undefined', async () => {
          await expect(controller.create(body)).rejects.toThrow(TypeError);
        });

        it('should not create a role', () => {
          expect(rolService.create).not.toHaveBeenCalled();
        });
      });
    });
  });

  describe('update(id, rolData)', () => {
    let update: UpdateRolDTO;

    describe('case: success', () => {
      let updateResult: UpdateResult | GlobalException;
      let result: UpdateResult | GlobalException;

      describe('when a role is updated', () => {
        beforeEach(async () => {
          update = {
            nombre: 'Test'
          };
          updateResult = new UpdateResult();
          (rolService.update as jest.Mock).mockResolvedValue(updateResult);
          result = await controller.update(1, update);
        });

        it('should invoke RolService.update()', () => {
          expect(rolService.update).toHaveBeenCalledTimes(1);
          expect(rolService.update).toHaveBeenCalledWith(1, update);
        });

        it('should update a role', () => {
          expect(result).toStrictEqual(updateResult);
        });
      });
    });

    describe('case: failure', () => {
      let expectedError: TypeError;

      describe('when a role is not updated', () => {
        beforeEach(async () => {
          expectedError = new TypeError();
          update = undefined;
          jest.spyOn(controller, 'update').mockRejectedValue(expectedError);
        });

        it('should throw when the body is undefined', async () => {
          await expect(controller.update(1, update)).rejects.toThrow(TypeError);
        });

        it('should not update a role', () => {
          expect(rolService.update).not.toHaveBeenCalled();
        });
      });
    });
  });

  describe('delete(id)', () => {
    let deleteResult: DeleteResult | GlobalException;
    let result: DeleteResult | GlobalException;

    describe('case: success', () => {
      describe('when a role is found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 1;
          (rolService.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await controller.delete(1);
        });

        it('should invoke RolService.delete()', () => {
          expect(rolService.delete).toHaveBeenCalledTimes(1);
          expect(rolService.delete).toHaveBeenCalledWith(1);
        });

        it('should delete a role', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });

    describe('case: failure', () => {
      describe('when a role is not found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 0;
          (rolService.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await controller.delete(1);
        });

        it('should invoke RolService.delete()', () => {
          expect(rolService.delete).toHaveBeenCalledTimes(1);
          expect(rolService.delete).toHaveBeenCalledWith(1);
        });

        it('should not delete a role', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });
  });
});
