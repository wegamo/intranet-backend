import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';

import { CreateRolDTO } from '../../../models/rol/dto/create-rol.dto';
import { UpdateRolDTO } from '../../../models/rol/dto/update-rol.dto';
import { Rol } from '../../../models/rol/rol.entity';

import { RolService } from './rol.service';

import { SampleRol } from './../../../../test/ObjectSamples';
//import { RolInt } from './../../dto_response/Rol_Payload';
import { GlobalException } from './../../exception/global-exception';

describe('Rol Service', () => {
  let service: RolService;
  let RepositoryMock: jest.Mock;
  let repository: Repository<Rol>;

  beforeEach(() => {
    RepositoryMock = jest.fn(() => ({
      find: jest.fn(),
      findOne: jest.fn(),
      save: jest.fn(),
      update: jest.fn(),
      delete: jest.fn()
    }));
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [RolService, { provide: getRepositoryToken(Rol), useValue: new RepositoryMock() }]
    }).compile();

    repository = module.get(getRepositoryToken(Rol));
    service = module.get<RolService>(RolService);
  });

  describe('findAll()', () => {
    let expectedRoles: Rol[];
    let result: Rol[];

    describe('case: success', () => {
      describe('when there are roles', () => {
        beforeEach(async () => {
          expectedRoles = [
            SampleRol
          ];
          (repository.find as jest.Mock).mockResolvedValue(expectedRoles);
          result = await service.findAll();
        });

        it('should invoke repository.find()', () => {
          expect(repository.find).toHaveBeenCalledTimes(1);
        });

        it('should return an array of roles', () => {
          expect(result).toStrictEqual(expectedRoles);
        });
      });
    });

    describe('case: failure', () => {
      describe('when there are not roles', () => {
        beforeEach(async () => {
          expectedRoles = [];
          (repository.find as jest.Mock).mockResolvedValue(expectedRoles);
          result = await service.findAll();
        });

        it('should invoke repository.find()', () => {
          expect(repository.find).toHaveBeenCalledTimes(1);
        });

        it('should return an array empty', () => {
          expect(result).toStrictEqual(expectedRoles);
        });
      });
    });
  });

  describe('findOne(id)', () => {
    let expectedRol: Rol;
    let result: Rol;

    describe('case: success', () => {
      describe('when a role is found', () => {
        beforeEach(async () => {
          expectedRol = SampleRol;
          (repository.findOne as jest.Mock).mockResolvedValue(expectedRol);
          result = await service.findOne(1);
        });

        it('should invoke repository.findOne()', () => {
          expect(repository.findOne).toHaveBeenCalledTimes(1);
          expect(repository.findOne).toHaveBeenCalledWith(1);
        });

        it('should return a role', () => {
          expect(result).toStrictEqual(expectedRol);
        });
      });
    });

    describe('case: failure', () => {
      describe('when a role is not found', () => {
        beforeEach(async () => {
          expectedRol = undefined;
          (repository.findOne as jest.Mock).mockResolvedValue(expectedRol);
          result = await service.findOne(1);
        });

        it('should invoke repository.findOne()', () => {
          expect(repository.findOne).toHaveBeenCalledTimes(1);
          expect(repository.findOne).toHaveBeenCalledWith(1);
        });

        it('should return undefined', () => {
          expect(result).toStrictEqual(expectedRol);
        });
      });
    });
  });

  describe('create(rol)', () => {
    const {id,...rest} = SampleRol;
    let body: CreateRolDTO;

    describe('case: success', () => {
      let expectedRol: Rol;
      let result: Rol;

      describe('when a role is created', () => {
        beforeEach(async () => {
          body = rest;
          expectedRol = SampleRol;
          (repository.save as jest.Mock).mockResolvedValue(expectedRol);
          result = await service.create(body);
        });

        it('should invoke repository.save()', () => {
          expect(repository.save).toHaveBeenCalledTimes(1);
          expect(repository.save).toHaveBeenCalledWith(body);
        });

        it('should create a role', () => {
          expect(result).toStrictEqual(expectedRol);
        });
      });
    });

    describe('case: failure', () => {
      let expectedError: TypeError;

      describe('when a role is not created', () => {
        beforeEach(async () => {
          body = undefined;
          expectedError = new TypeError();
          jest.spyOn(service, 'create').mockRejectedValue(expectedError);
        });

        it('should throw when the body is undefined', async () => {
          await expect(service.create(body)).rejects.toThrow(TypeError);
        });

        it('should not create a role', () => {
          expect(repository.save).not.toHaveBeenCalled();
        });
      });
    });
  });

  describe('update(id, rol)', () => {
    let update: UpdateRolDTO;

    describe('case: success', () => {
      let updateResult: UpdateResult | GlobalException;
      let result: UpdateResult | GlobalException;

      describe('when a role is updated', () => {
        beforeEach(async () => {
          update = {
            nombre: 'Test'
          };
          updateResult = new UpdateResult();
          (repository.update as jest.Mock).mockResolvedValue(updateResult);
          result = await service.update(1, update);
        });

        it('should invoke repository.update()', () => {
          expect(repository.update).toHaveBeenCalledTimes(1);
          expect(repository.update).toHaveBeenCalledWith(1, update);
        });

        it('should update a role', () => {
          expect(result).toStrictEqual(updateResult);
        });
      });
    });

    describe('case: failure', () => {
      let expectedError: TypeError;

      describe('when a role is not updated', () => {
        beforeEach(async () => {
          expectedError = new TypeError();
          update = undefined;
          jest.spyOn(service, 'update').mockRejectedValue(expectedError);
        });

        it('should throw when the body is undefined', async () => {
          await expect(service.update(1, update)).rejects.toThrow(TypeError);
        });

        it('should not update a role', () => {
          expect(repository.update).not.toHaveBeenCalled();
        });
      });
    });
  });

  describe('delete(id)', () => {
    let deleteResult: DeleteResult | GlobalException;
    let result: DeleteResult | GlobalException;

    describe('case: success', () => {
      describe('when a role is found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 1;
          (repository.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await service.delete(1);
        });

        it('should invoke repository.delete()', () => {
          expect(repository.delete).toHaveBeenCalledTimes(1);
          expect(repository.delete).toHaveBeenCalledWith(1);
        });

        it('should delete a role', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });

    describe('case: failure', () => {
      describe('when a role is not found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 0;
          (repository.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await service.delete(1);
        });

        it('should invoke repository.delete()', () => {
          expect(repository.delete).toHaveBeenCalledTimes(1);
          expect(repository.delete).toHaveBeenCalledWith(1);
        });

        it('should not delete a role', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });
  });
});
