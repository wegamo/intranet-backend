import { Module } from '@nestjs/common';
import { RolController } from './rol/rol.controller';
import { RolService } from './rol/rol.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Rol } from '../../models/rol/rol.entity';

//ESTE MODULO SOLO DEBE SER USADO POR LOS DESARROLLADORES DIRECTAMENTE
@Module({
  imports: [TypeOrmModule.forFeature([Rol])],
  controllers: [RolController],
  providers: [RolService]
})
export class RolModule {}
