import { Injectable } from '@nestjs/common';
import { Cron, Interval, Timeout, NestSchedule } from 'nest-schedule';
import { NoticiaTipo } from '../../../enums/noticia.tipo.enum';
import { AwsNoticias } from '../../aws3-cntm/aws3-cntm/aws.interface';
import { Aws3CntmService } from '../../aws3-cntm/aws3-cntm/aws3-cntm.service';
import { NoticiasService } from '../../noticia/noticia/noticias.service';
import { Noticia } from '../../../models/noticias/noticia.entity';
@Injectable()
export class CrontasksService extends NestSchedule {
  constructor(
    private readonly noticiasService: NoticiasService,
    private readonly s3Service: Aws3CntmService
  ) {
    super();
  }

  @Cron(
    {
      //hour: 0, //0-23
      //minute: 0, //0-59
      second: 15
      //date?: number,
      //month?: number,
      //year?: number,
      //dayOfWeek?: number,
    }
    /*{
        startTime: new Date(), 
        endTime: new Date(new Date().getTime() + 24 * 60 * 60 * 1000),
    }*/
  )
  async BucketUpdate() {
    console.log('Bucket auto update ', new Date());
    //LECTURA DE BSD
    let BasicNews: Noticia[] = [];
    let BannerNews: Noticia[] = [];
    BasicNews = await this.noticiasService.getNoticiasByTipo(NoticiaTipo.NORMAL);
    BannerNews = await this.noticiasService.getNoticiasByTipo(NoticiaTipo.BANNER);
    //ORGANIZAR CARGA (GENERAR .JSON DELEGADO PARA MANTENIMIENTO DE NOTICIAS)
    let AwsNews: AwsNoticias = {
      basicNews: BasicNews.map((Basic: Noticia, ind) => {
        const { posicion, titulo, descripcion, imagen, id } = Basic;
        return { posicion, titulo, descripcion, imagen, id };
      }),
      bannerNews: BannerNews.map((Banner: Noticia, ind) => {
        const { posicion, titulo, descripcion, imagen, id } = Banner;
        return { posicion, titulo, descripcion, imagen, id };
      })
    };
    console.log('NEWS:', BannerNews, AwsNews);
    //ACTUALIZAR ESTADO EN AWS (CARGA DE ARCHIVO DELEGADO)
    await this.s3Service.addBasicObject(
      JSON.stringify(AwsNews),
      'AwsNoticias.json',
      'complex/noticias/'
    );
  }

  /*@Interval(2000)
    intervalJob() {
        console.log('executing interval job');
        // if you want to cancel the job, you should return true;
        return false;
    }*/
}
