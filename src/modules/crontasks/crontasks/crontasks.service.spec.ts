import { Test, TestingModule } from '@nestjs/testing';
import { Aws3CntmService } from '../../aws3-cntm/aws3-cntm/aws3-cntm.service';
import { NoticiasService } from '../..//noticia/noticia/noticias.service';
import { CrontasksService } from './crontasks.service';
import { getRepositoryToken, TypeOrmModule } from '@nestjs/typeorm';
import { Noticia } from '../../../models/noticias/noticia.entity';
import { NoticiasModule } from '../../noticia/noticias.module';
import { Aws3CntmModule } from '../../aws3-cntm/aws3-cntm.module';
import { CrontasksModule } from '../crontasks.module';

describe('CrontasksService', () => {
  let service: CrontasksService;
  let RepositoryMock: jest.Mock;

  beforeEach(() => {
    RepositoryMock = jest.fn(() => ({
      find: jest.fn(),
      save: jest.fn(),
      delete: jest.fn()
    }));
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports:[],
      providers: [
        CrontasksService,
        NoticiasService,
        { provide: getRepositoryToken(Noticia), useValue: new RepositoryMock() },
        Aws3CntmService,
      ],
    }).compile();

    service = module.get<CrontasksService>(CrontasksService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
