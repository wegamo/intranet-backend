import { Module } from '@nestjs/common';
import { Aws3CntmModule } from '../aws3-cntm/aws3-cntm.module';
import { NoticiasModule } from '../noticia/noticias.module';
import { CrontasksService } from './crontasks/crontasks.service';

@Module({
  imports:[Aws3CntmModule,NoticiasModule],
  providers: [CrontasksService]
})
export class CrontasksModule {}
