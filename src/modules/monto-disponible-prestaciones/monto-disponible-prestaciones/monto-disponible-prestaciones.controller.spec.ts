import { Test, TestingModule } from '@nestjs/testing';
import { CreateMontoPrestacionesDTO } from '../../../models/monto-disponible-prestaciones/dto/create-monto-prestaciones.dto';
import { UpdateMontoPrestacionesDTO } from '../../../models/monto-disponible-prestaciones/dto/update-monto-prestaciones.dto';
import { MontoPrestaciones } from '../../../models/monto-disponible-prestaciones/monto-disponible-prestaciones.entity';
import { SampleMontoPrestaciones } from '../../../../test/ObjectSamples';
import { DeleteResult, UpdateResult } from 'typeorm';
import { MontoDisponiblePrestacionesController } from './monto-disponible-prestaciones.controller';
import { MontoDisponiblePrestacionesService } from './monto-disponible-prestaciones.service';
import { SolicitudEstatus } from '../../../enums/solicitud-estatus.enum';

describe('MontoDisponiblePrestaciones Controller', () => {
  let controller: MontoDisponiblePrestacionesController;
  let montoPrestacionesService: MontoDisponiblePrestacionesService;
  let MontoPrestacionesServiceMock: jest.Mock<Partial<MontoDisponiblePrestacionesService>>;


  beforeEach(() => {
    MontoPrestacionesServiceMock = jest.fn<Partial<MontoDisponiblePrestacionesService>, MontoDisponiblePrestacionesService[]>(() => ({
      findAll: jest.fn(),
      findByPerson: jest.fn(),
      findCurrentByPersona:jest.fn(),
      create: jest.fn(),
      delete: jest.fn(),
      update: jest.fn(),
      findOne:jest.fn(),
    }));
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MontoDisponiblePrestacionesController],
      providers: [{ provide: MontoDisponiblePrestacionesService, useValue: new MontoPrestacionesServiceMock() }]
    }).compile();
    montoPrestacionesService = module.get<MontoDisponiblePrestacionesService>(MontoDisponiblePrestacionesService);
    controller = module.get<MontoDisponiblePrestacionesController>(MontoDisponiblePrestacionesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('findAll()', () => {
    let expectedMontoPrestacionesss: MontoPrestaciones[];
    let result: MontoPrestaciones[];

    describe('case : success', () => {
      describe('when there are Monto Prestacioness', () => {
        beforeEach(async () => {
          expectedMontoPrestacionesss = [SampleMontoPrestaciones];
          (montoPrestacionesService.findAll as jest.Mock).mockResolvedValue(expectedMontoPrestacionesss);
          result = await controller.findAll();
        });
        it('should invoke montoPrestacionesService.findAll()', () => {
          expect(montoPrestacionesService.findAll).toHaveBeenCalledTimes(1);
        });
        it('should return an array of Monto Prestacioness', () => {
          expect(result).toStrictEqual(expectedMontoPrestacionesss);
        });
      });
    });

    describe('case : failure', () => {
      describe('when there are no Monto Prestacioness', () => {
        beforeEach(async () => {
          expectedMontoPrestacionesss = [];
          (montoPrestacionesService.findAll as jest.Mock).mockResolvedValue(expectedMontoPrestacionesss);
          result = await controller.findAll();
        });

        it('should invoke montoPrestacionesService.findAll()', () => {
          expect(montoPrestacionesService.findAll).toHaveBeenCalledTimes(1);
        });

        it('should return an array empty', () => {
          expect(result).toStrictEqual(expectedMontoPrestacionesss);
        });
      });
    });
  });

  
  describe('findByPerson(id)', () => {
    let expectedMontoPrestacioness: MontoPrestaciones;
    let result: MontoPrestaciones;

    describe('case : success', () => {
      describe('when an Monto Prestaciones is found', () => {
        beforeEach(async () => {
          expectedMontoPrestacioness = SampleMontoPrestaciones;
          (montoPrestacionesService.findCurrentByPersona as jest.Mock).mockResolvedValue(expectedMontoPrestacioness);
          result = await controller.findById(SampleMontoPrestaciones.persona.id);
        });
        it('should invoke montoPrestacionesService.findCurrentByPersona()', () => {
          expect(montoPrestacionesService.findCurrentByPersona).toHaveBeenCalledTimes(1);
          expect(montoPrestacionesService.findCurrentByPersona).toHaveBeenCalledWith(1);
        });
        it('should return an Monto Prestaciones', () => {
          expect(result).toStrictEqual(expectedMontoPrestacioness);
        });
      });
    });

    describe('case : failure', () => {
      describe('when an Monto Prestaciones is not found', () => {
        beforeEach(async () => {
          expectedMontoPrestacioness = undefined;
          (montoPrestacionesService.findCurrentByPersona as jest.Mock).mockResolvedValue(expectedMontoPrestacioness);
          result = await controller.findById(SampleMontoPrestaciones.persona.id);
        });
        it('should invoke montoPrestacionesService.findCurrentByPersona()', () => {
          expect(montoPrestacionesService.findCurrentByPersona).toHaveBeenCalledTimes(1);
          expect(montoPrestacionesService.findCurrentByPersona).toHaveBeenCalledWith(SampleMontoPrestaciones.persona.id);
        });
        it('should return undefined', () => {
          expect(result).toStrictEqual(expectedMontoPrestacioness);
        });
      });
    });
  });
  
  describe('create(CreateMontoPrestacionessDTO)', () => {
    const { id, ...rest } = SampleMontoPrestaciones;
    let body: CreateMontoPrestacionesDTO;

    describe('case : success', () => {
      let expectedMontoPrestacioness: MontoPrestaciones;
      let result: MontoPrestaciones;

      describe('when a Monto Prestaciones is created', () => {
        beforeEach(async () => {
          body = rest;
          expectedMontoPrestacioness = SampleMontoPrestaciones;
          (montoPrestacionesService.create as jest.Mock).mockResolvedValue(expectedMontoPrestacioness);
          result = await controller.create(body);
        });
        it('should invoke montoPrestacionesService.create()', () => {
          expect(montoPrestacionesService.create).toHaveBeenCalledTimes(1);
          expect(montoPrestacionesService.create).toHaveBeenCalledWith(body);
        });
        it('should create a Monto Prestaciones', () => {
          expect(result).toStrictEqual(expectedMontoPrestacioness);
        });
      });
    });

    describe('case : failure', () => {
      let expectedError: TypeError;
      describe('when a Monto Prestaciones is not created', () => {
        beforeEach(async () => {
          body = undefined;
          expectedError = new TypeError();
          jest.spyOn(controller, 'create').mockRejectedValue(expectedError);
        });
        it('should throw when the body is undefined', async () => {
          await expect(controller.create(body)).rejects.toThrow(TypeError);
        });
        it('should not create a Monto Prestaciones', () => {
          expect(montoPrestacionesService.create).not.toHaveBeenCalled();
        });
      });
    });
  });
 
  describe('update(id, UpdateMontoPrestacionessDTO)', () => {
    let update: UpdateMontoPrestacionesDTO;

    describe('case : success', () => {
      let updateResult: MontoPrestaciones;
      let result: MontoPrestaciones;

      describe('when an Monto Prestaciones is found', () => {
        beforeEach(async () => {
          update = {
            montoDisponible:'500',
            estatus:SolicitudEstatus.APROBADA,
            fechaInicio:new Date(),
            fechaFin:new Date(),
          };
          //updateResult = new UpdateResult();
          (montoPrestacionesService.update as jest.Mock).mockResolvedValue(updateResult);
          result = await controller.update(1, update);
        });
        it('should invoke montoPrestacionesService.update()', () => {
          expect(montoPrestacionesService.update).toHaveBeenCalledTimes(1);
          expect(montoPrestacionesService.update).toHaveBeenCalledWith(1, update);
        });
        it('should update an Monto Prestaciones', () => {
          expect(result).toStrictEqual(updateResult);
        });
      });
    });

    describe('case : failure', () => {
      let expectedError: TypeError;

      describe('when an Monto Prestaciones is not found', () => {
        beforeEach(async () => {
          expectedError = new TypeError();
          update = undefined;
          jest.spyOn(controller, 'update').mockRejectedValue(expectedError);
        });
        it('should invoke montoPrestacionesService.update()', async () => {
          await expect(controller.update(1, update)).rejects.toThrow(TypeError);
        });
        it('should not update an Monto Prestaciones', () => {
          expect(montoPrestacionesService.update).not.toHaveBeenCalled();
        });
      });
    });
  });
  /*
  describe('delete(id)', () => {
    let deleteResult: DeleteResult;
    let result: DeleteResult;

    describe('case : success', () => {
      describe('when an Monto Prestaciones is found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 1;
          (montoPrestacionesService.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await controller.delete(1);
        });
        it('should invoke montoPrestacionesService.delete()', () => {
          expect(montoPrestacionesService.delete).toHaveBeenCalledTimes(1);
          expect(montoPrestacionesService.delete).toHaveBeenCalledWith(1);
        });
        it('should delete an Monto Prestaciones', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });

    describe('case : failure', () => {
      let expectedError: TypeError;

      describe('when an Monto Prestaciones is not found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 0;
          (montoPrestacionesService.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await controller.delete(1);
        });

        it('should invoke montoPrestacionesService.delete()', () => {
          expect(montoPrestacionesService.delete).toHaveBeenCalledTimes(1);
          expect(montoPrestacionesService.delete).toHaveBeenCalledWith(1);
        });

        it('should not delete an Monto Prestaciones', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });
  });
  */
});
