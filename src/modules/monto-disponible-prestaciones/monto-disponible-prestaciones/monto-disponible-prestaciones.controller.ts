import {
  Body,
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
  Request
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { SolicitudEstatus } from '../../../enums/solicitud-estatus.enum';
import { Persona } from '../../../models/persona/persona.entity';
import { CreateMontoPrestacionesDTO } from '../../../models/monto-disponible-prestaciones/dto/create-monto-prestaciones.dto';
import { UpdateMontoPrestacionesDTO } from '../../../models/monto-disponible-prestaciones/dto/update-monto-prestaciones.dto';
import { MontoPrestaciones } from '../../../models/monto-disponible-prestaciones/monto-disponible-prestaciones.entity';
import { MontoDisponiblePrestacionesService } from './monto-disponible-prestaciones.service';
import { PersonaService } from '../../persona/persona/persona.service';
import { sendEmail } from '../../../helpers/sendEmail';
import { ConfigService } from '../../../config/config/config.service';
import { NotificacionesService } from '../../../modules/notificaciones/notificaciones/notificaciones.service';
import { TipoNotificacion } from '../../../enums/tipo-notificacion.enum';
import { ApiUseTags } from '@nestjs/swagger';
import { HelpersService } from '../../helpers/helpers/helpers/helpers.service';

@ApiUseTags('Monto disponible para prestaciones')
@Controller('monto-disponible-prestaciones')
export class MontoDisponiblePrestacionesController {
  constructor(
    private readonly montoPrestacionesService: MontoDisponiblePrestacionesService,
    private readonly personaService: PersonaService,
    private readonly _configService: ConfigService,
    private readonly notificacionesService: NotificacionesService,
    private readonly helpersService: HelpersService
  ) {}

  @UseGuards(AuthGuard('jwt'))
  @Get('all')
  async getAll(@Query('page') page = 1, @Query('limit') limit = 10) {
    limit = limit > 100 ? 100 : limit;
    const montoPrestaciones = await this.montoPrestacionesService.findAll({
      page,
      limit
    });
    return montoPrestaciones;
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('last/persona/:id')
  findLastByPersona(@Param('id') id: number): Promise<MontoPrestaciones[]> {
    return this.montoPrestacionesService.findLastAprobadoByPersona(id);
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('last-pendiente/persona/:id')
  findLastPendienteByPersona(@Param('id') id: number): Promise<MontoPrestaciones[]> {
    return this.montoPrestacionesService.findLastPendienteByPersona(id);
  }

  @UseGuards(AuthGuard('jwt'))
  @Post('create')
  async create(@Request() { user }: { user: Persona }): Promise<MontoPrestaciones> {
    try {
      const montoPrestaciones = new MontoPrestaciones();
      montoPrestaciones.persona = user;
      const monto = await this.montoPrestacionesService.create(montoPrestaciones);
      // Send email and notification according to environment
      await this.helpersService.createMontoDisp();
      return monto;
    } catch (err) {
      throw new HttpException(
        'Error al crear la solicitud de monto disponible de prestaciones',
        HttpStatus.BAD_REQUEST
      );
    }
  }

  @UseGuards(AuthGuard('jwt'))
  @Put('update/:id')
  async update(
    @Param('id') id: number,
    @Body() montoDisponibleData: UpdateMontoPrestacionesDTO,
    @Request() { user }: { user: Persona }
  ): Promise<MontoPrestaciones> {
    // Se verifica que la persona del request pertenece a talento humano

    const talentoHumano = await this.personaService.findOneHumanTalent(user.id);
    if (!talentoHumano) {
      throw new HttpException(
        'La persona que está tratando de modificar la solicitud no pertenece a talento humano',
        HttpStatus.BAD_REQUEST
      );
    }

    let montoDisponibleToChange = await this.montoPrestacionesService.findOneWithPersona(id);

    if (!montoDisponibleToChange) {
      throw new HttpException(
        'El historial de monto disponible de prestaciones no existe',
        HttpStatus.NOT_FOUND
      );
    }
    if (montoDisponibleToChange.estatus === SolicitudEstatus.APROBADA) {
      throw new HttpException(
        'La solicitud ya ha sido revisada, no puede editarse',
        HttpStatus.BAD_REQUEST
      );
    }
    if (montoDisponibleData.estatus !== SolicitudEstatus.APROBADA) {
      throw new HttpException('Estatus inválido', HttpStatus.BAD_REQUEST);
    }
    try {
      montoDisponibleToChange = { ...montoDisponibleToChange, ...montoDisponibleData };
      const montoDispoiblePrestaciones = await this.montoPrestacionesService.update(
        montoDisponibleToChange
      );

      // Send email and notification according to environment
      this.helpersService.updateMontoDisp(montoDisponibleToChange);

      return montoDispoiblePrestaciones;
    } catch (err) {
      throw new HttpException(
        'Error al actualizar el historial de monto disponible',
        HttpStatus.BAD_REQUEST
      );
    }
  }

  @Get(':id')
  findById(@Param('id') id: number): Promise<MontoPrestaciones> {
    return this.montoPrestacionesService.findOne(id);
  }
}
