import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MontoPrestaciones } from '../../../models/monto-disponible-prestaciones/monto-disponible-prestaciones.entity';
import { MoreThan, MoreThanOrEqual, Repository } from 'typeorm';
import { IPaginationOptions, paginate, Pagination } from 'nestjs-typeorm-paginate';
import { SolicitudEstatus } from '../../../enums/solicitud-estatus.enum';

@Injectable()
export class MontoDisponiblePrestacionesService {
  constructor(
    @InjectRepository(MontoPrestaciones)
    private readonly montoPrestacionesRepository: Repository<MontoPrestaciones>
  ) {}

  async findAll(options: IPaginationOptions): Promise<Pagination<MontoPrestaciones>> {
    const queryBuilder = this.montoPrestacionesRepository.createQueryBuilder('monto');
    queryBuilder
      .innerJoinAndSelect('monto.persona', 'persona')
      .innerJoinAndSelect('persona.empresa', 'empresa')
      .innerJoinAndSelect('persona.histCargos', 'histCargos')
      .innerJoinAndSelect('histCargos.cargo', 'cargo')
      .innerJoinAndSelect('cargo.area', 'area')
      .where('histCargos.fechaFin IS NULL')
      .orderBy('monto.fechaSolicitud', 'DESC')
      .addOrderBy('monto.estatus', 'DESC');

    return paginate<MontoPrestaciones>(queryBuilder, options);
  }

  findOne(id: number): Promise<MontoPrestaciones> {
    return this.montoPrestacionesRepository.findOne(id);
  }

  findOneWithPersona(id: number): Promise<MontoPrestaciones> {
    return this.montoPrestacionesRepository.findOne(id, {
      relations: ['persona']
    });
  }

  findCurrentByPersona(fecha: Date, personaId: number): Promise<MontoPrestaciones> {
    return this.montoPrestacionesRepository.findOne({
      where: { persona: personaId, fechaFin: MoreThanOrEqual(fecha) }
    });
  }

  findLastAprobadoByPersona(personaId: number): Promise<MontoPrestaciones[]> {
    return this.montoPrestacionesRepository.find({
      where: { persona: personaId, estatus: SolicitudEstatus.APROBADA },
      order: { fechaFin: 'DESC' },
      take: 1
    });
  }

  findLastPendienteByPersona(personaId: number): Promise<MontoPrestaciones[]> {
    return this.montoPrestacionesRepository.find({
      where: { persona: personaId, estatus: SolicitudEstatus.PENDIENTE },
      order: { fechaSolicitud: 'DESC' },
      take: 1
    });
  }

  async create(montoPrestaciones: Partial<MontoPrestaciones>): Promise<MontoPrestaciones> {
    return await this.montoPrestacionesRepository.save(montoPrestaciones);
  }

  update(montoDisponible: MontoPrestaciones): Promise<MontoPrestaciones> {
    return this.montoPrestacionesRepository.save(montoDisponible);
  }
}
