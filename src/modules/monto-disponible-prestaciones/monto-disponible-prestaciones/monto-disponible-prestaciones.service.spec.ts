import { Test, TestingModule } from '@nestjs/testing';
import { MontoPrestaciones } from '../../../models/monto-disponible-prestaciones/monto-disponible-prestaciones.entity';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { MontoDisponiblePrestacionesService } from './monto-disponible-prestaciones.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { SampleMontoPrestaciones } from '../../../../test/ObjectSamples';
import { CreateMontoPrestacionesDTO } from '../../../models/monto-disponible-prestaciones/dto/create-monto-prestaciones.dto';
import { UpdateMontoPrestacionesDTO } from '../../../models/monto-disponible-prestaciones/dto/update-monto-prestaciones.dto';
import { SolicitudEstatus } from '../../../enums/solicitud-estatus.enum';

describe('MontoDisponiblePrestacionesService', () => {
  let service: MontoDisponiblePrestacionesService;
  let RepositoryMock: jest.Mock;
  let repository: Repository<MontoPrestaciones>;

  beforeEach(() => {
    RepositoryMock = jest.fn(() => ({
      find: jest.fn(),
      save: jest.fn(),
      delete: jest.fn(),
      update:jest.fn(),
    }));
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        MontoDisponiblePrestacionesService,
        { provide: getRepositoryToken(MontoPrestaciones), useValue: new RepositoryMock() },
      ],
    }).compile();
    repository = module.get(getRepositoryToken(MontoPrestaciones));
    service = module.get<MontoDisponiblePrestacionesService>(MontoDisponiblePrestacionesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('findAll()', () => {
    let expectedMontoPrestaciones: MontoPrestaciones[];
    let result: MontoPrestaciones[];

    describe('case : success', () => {
      describe('', () => {
        beforeEach(async () => {
          expectedMontoPrestaciones = [SampleMontoPrestaciones];
          (repository.find as jest.Mock).mockResolvedValue(expectedMontoPrestaciones);
          result = await service.findAll();
        });

        it('should invoke repository.find()', () => {
          expect(repository.find).toHaveBeenCalledTimes(1);
        });

        it('should return an array of MontoPrestaciones', () => {
          expect(result).toStrictEqual(expectedMontoPrestaciones);
        });
      });
    });

    describe('case : failure', () => {
      describe('when there are not MontoPrestaciones', () => {
        beforeEach(async () => {
          expectedMontoPrestaciones = [];
          (repository.find as jest.Mock).mockResolvedValue(expectedMontoPrestaciones);
          result = await service.findAll();
        });

        it('should invoke repository.find()', () => {
          expect(repository.find).toHaveBeenCalledTimes(1);
        });

        it('should return an empty array', () => {
          expect(result).toStrictEqual(expectedMontoPrestaciones);
        });
      });
    });
  });

  describe('findAll()', () => {
    let expectedMontoPrestaciones: MontoPrestaciones[];
    let result: MontoPrestaciones[];

    describe('case : success', () => {
      describe('when there are MontoPrestaciones', () => {
        beforeEach(async () => {
          expectedMontoPrestaciones = [SampleMontoPrestaciones];
          (repository.find as jest.Mock).mockResolvedValue(expectedMontoPrestaciones);
          result = await service.findAll();
        });

        it('should invoke repository.find()', () => {
          expect(repository.find).toHaveBeenCalledTimes(1);
        });

        it('should return an array of MontoPrestaciones', () => {
          expect(result).toStrictEqual(expectedMontoPrestaciones);
        });
      });
    });

    describe('case : failure', () => {
      describe('when there are not MontoPrestaciones', () => {
        beforeEach(async () => {
          expectedMontoPrestaciones = [];
          (repository.find as jest.Mock).mockResolvedValue(expectedMontoPrestaciones);
          result = await service.findAll();
        });

        it('should invoke repository.find()', () => {
          expect(repository.find).toHaveBeenCalledTimes(1);
        });

        it('should return an empty array', () => {
          expect(result).toStrictEqual(expectedMontoPrestaciones);
        });
      });
    });
  });

  describe('findByID(id)', () => {
    let expectedMontoPrestaciones: MontoPrestaciones[];
    let result: MontoPrestaciones[];

    describe('case : success', () => {
      describe('when an MontoPrestaciones is found', () => {
        beforeEach(async () => {
          expectedMontoPrestaciones = [SampleMontoPrestaciones];
          (repository.find as jest.Mock).mockResolvedValue(expectedMontoPrestaciones);
          result = await service.findCurrentByPersona(SampleMontoPrestaciones.persona.id);
        });

        it('should invoke repository.findOne()', () => {
          expect(repository.find).toHaveBeenCalledTimes(1);
          expect(repository.find).toHaveBeenCalledWith(
            {where:{persona:{id:SampleMontoPrestaciones.persona.id}},
            order: { fechaFin: 'DESC' },
            take: 1
          });
        });

        it('should return an MontoPrestaciones', () => {
          expect(result).toStrictEqual(expectedMontoPrestaciones);
        });
      });
    });

    describe('case : failure', () => {
      describe('when an MontoPrestaciones is not found', () => {
        beforeEach(async () => {
          expectedMontoPrestaciones = undefined;
          (repository.find as jest.Mock).mockResolvedValue(expectedMontoPrestaciones);
          result = await service.findCurrentByPersona(SampleMontoPrestaciones.persona.id);
        });

        it('should invoke repository.findOne()', () => {
          expect(repository.find).toHaveBeenCalledTimes(1);
          expect(repository.find).toHaveBeenCalledWith(
            {where:{persona:{id:SampleMontoPrestaciones.persona.id}},
            order: { fechaFin: 'DESC' },
            take: 1
          });
        });

        it('should return undefined', () => {
          expect(result).toStrictEqual(expectedMontoPrestaciones);
        });
      });
    });
  });

  describe('create(CreateMontoPrestacionesDTO)', () => {
    const { id, ...rest } = SampleMontoPrestaciones;
    let body: CreateMontoPrestacionesDTO;

    describe('case : success', () => {
      let expectedMontoPrestaciones: MontoPrestaciones;
      let result: MontoPrestaciones;

      describe('when an MontoPrestaciones is created', () => {
        beforeEach(async () => {
          body = rest;
          expectedMontoPrestaciones = SampleMontoPrestaciones;
          (repository.save as jest.Mock).mockResolvedValue(expectedMontoPrestaciones);
          result = await service.create(body);
        });

        it('should invoke repository.save()', () => {
          expect(repository.save).toHaveBeenCalledTimes(1);
          expect(repository.save).toHaveBeenCalledWith(body);
        });

        it('should create a MontoPrestaciones', () => {
          expect(result).toStrictEqual(expectedMontoPrestaciones);
        });
      });
    });

    describe('case : failure', () => {
      let expectedError: TypeError;
      let body: undefined;

      describe('when a MontoPrestaciones is not created', () => {
        beforeEach(async () => {
          body = undefined;
          expectedError = new TypeError();
          jest.spyOn(service, 'create').mockRejectedValue(expectedError);
        });

        it('it should throw when the body is undefined', async () => {
          await expect(service.create(body)).rejects.toThrow(TypeError);
        });

        it('should not create an MontoPrestaciones', () => {
          expect(repository.save).not.toHaveBeenCalled();
        });
      });
    });
  });

  describe('update(id, UpdateMontoPrestacionesDTO)', () => {
    let update: MontoPrestaciones;

    describe('case : success', () => {
      let updateResult: MontoPrestaciones;
      let result: MontoPrestaciones;

      describe('when a MontoPrestaciones is updated', () => {
        beforeEach(async () => {
          update = {
            montoDisponible:'500',
            estatus:SolicitudEstatus.APROBADA,
            fechaInicio:new Date(),
            fechaFin:new Date(),
            ...SampleMontoPrestaciones
          };
          //updateResult = new UpdateResult();
          (repository.save as jest.Mock).mockResolvedValue(updateResult);
          result = await service.update(update);
        });

        it('should invoke repository.save()', () => {
          expect(repository.save).toHaveBeenCalledTimes(1);
          expect(repository.save).toHaveBeenCalledWith(update);
        });

        it('should update an MontoPrestaciones', () => {
          expect(result).toStrictEqual(updateResult);
        });
      });
    });

    describe('case : failure', () => {
      let expectedError: TypeError;

      describe('when a MontoPrestaciones is not updated', () => {
        beforeEach(async () => {
          expectedError = new TypeError();
          update = undefined;
          jest.spyOn(service, 'update').mockRejectedValue(expectedError);
        });

        it('should throw when the body is undefined', async () => {
          await expect(service.update(update)).rejects.toThrow(TypeError);
        });

        it('should not update an MontoPrestaciones', () => {
          expect(repository.save).not.toHaveBeenCalled();
        });
      });
    });
  });
  /*
  describe('delete(id)', () => {
    let deleteResult: DeleteResult;
    let result: DeleteResult;

    describe('case : success', () => {
      describe('when an MontoPrestaciones is found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 1;
          (repository.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await service.delete(1);
        });

        it('should invoke repository.delete()', () => {
          expect(repository.delete).toHaveBeenCalledTimes(1);
          expect(repository.delete).toHaveBeenCalledWith(1);
        });

        it('should delete an MontoPrestaciones', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });

    describe('case : failure', () => {
      let expectedError: TypeError;

      describe('when a MontoPrestaciones is not found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 0;
          (repository.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await service.delete(1);
        });

        it('should invoke repository.delete()', () => {
          expect(repository.delete).toHaveBeenCalledTimes(1);
          expect(repository.delete).toHaveBeenCalledWith(1);
        });

        it('should not delete an MontoPrestaciones', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });
  });
  */
});
