import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '../../config/config.module';
import { MontoPrestaciones } from '../../models/monto-disponible-prestaciones/monto-disponible-prestaciones.entity';
import { HelpersModule } from '../helpers/helpers.module';
import { HelpersService } from '../helpers/helpers/helpers/helpers.service';
import { NotificacionesModule } from '../notificaciones/notificaciones.module';
import { PersonaModule } from '../persona/persona.module';
import { PersonaService } from '../persona/persona/persona.service';
import { MontoDisponiblePrestacionesController } from './monto-disponible-prestaciones/monto-disponible-prestaciones.controller';
import { MontoDisponiblePrestacionesService } from './monto-disponible-prestaciones/monto-disponible-prestaciones.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([MontoPrestaciones]),
    forwardRef(() => PersonaModule),
    ConfigModule,
    NotificacionesModule,
    HelpersModule
  ],
  controllers: [MontoDisponiblePrestacionesController],
  providers: [MontoDisponiblePrestacionesService, PersonaService, HelpersService]
})
export class MontoDisponiblePrestacionesModule {}
