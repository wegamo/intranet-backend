import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Tarea } from '../../../models/tarea/tarea.entity';
import { CreateTareaDTO } from '../../../models/tarea/dto/create-tarea.dto';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { Persona } from '../../../models/persona/persona.entity';

@Injectable()
export class TareasService {
  constructor(
    @InjectRepository(Tarea)
    private readonly tareaRepository: Repository<Tarea>
  ) {}

  async findOne(id: number): Promise<Tarea> {
    return await this.tareaRepository.findOne(id);
  }

  async findAll(): Promise<Tarea[]> {
    return await this.tareaRepository.find();
  }

  async findAllByUser(persona: Persona): Promise<Tarea[]> {
    return await this.tareaRepository.find({
      where: {
        persona
      }
    });
  }

  async create(newTarea: CreateTareaDTO, persona: Persona): Promise<Tarea> {
    return await this.tareaRepository.save({ persona, texto: newTarea.texto });
  }

  async delete(id: number): Promise<DeleteResult> {
    return await this.tareaRepository.delete(id);
  }

  async deleteBatch(ids: number[]): Promise<DeleteResult> {
    return await this.tareaRepository.delete(ids);
  }
}
