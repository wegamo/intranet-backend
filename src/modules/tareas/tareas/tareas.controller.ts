import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Request,
  UseGuards
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { DeleteResult } from 'typeorm';
import { Tarea } from '../../../models/tarea/tarea.entity';
import { CreateTareaDTO } from '../../../models/tarea/dto/create-tarea.dto';
import { Persona } from '../../../models/persona/persona.entity';
import { TareasService } from './tareas.service';

@Controller('tareas')
export class TareasController {
  constructor(private readonly tareasService: TareasService) {}

  @Get('all')
  @UseGuards(AuthGuard('jwt'))
  findAll(@Request() { user }: { user: Persona }): Promise<Tarea[]> {
    return this.tareasService.findAllByUser(user);
  }

  @Post('new')
  @UseGuards(AuthGuard('jwt'))
  async create(
    @Request() { user }: { user: Persona },
    @Body() tareaData: CreateTareaDTO
  ): Promise<Tarea> {
    return this.tareasService.create(tareaData, user);
  }

  @Post('remove/batch')
  @UseGuards(AuthGuard('jwt'))
  async deleteBatch(
    @Request() { user }: { user: Persona },
    @Body() body: { tareas: number[] }
  ): Promise<DeleteResult> {
    return this.tareasService.deleteBatch(body.tareas);
  }

  @Delete(':id')
  async delete(@Param('id') id: number): Promise<DeleteResult> {
    try {
      return this.tareasService.delete(id);
    } catch (err) {
      throw new HttpException('Error al eliminar la tarea', HttpStatus.BAD_REQUEST);
    }
  }
}
