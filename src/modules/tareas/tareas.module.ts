import { Module } from '@nestjs/common';
import { TareasController } from './tareas/tareas.controller';
import { TareasService } from './tareas/tareas.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Tarea } from '../../models/tarea/tarea.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Tarea])],
  controllers: [TareasController],
  providers: [TareasService]
})
export class TareasModule {}
