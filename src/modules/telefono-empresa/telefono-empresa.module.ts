import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TelefonoEmpresa } from '../../models/telefono-empresa/telefono-empresa.entity';
import { TelefonoEmpresaService } from './telefono-empresa/telefono-empresa.service';

@Module({
  imports:[TypeOrmModule.forFeature([TelefonoEmpresa])],
  providers:[TelefonoEmpresaService],
  exports:[TelefonoEmpresaService],
})
export class TelefonoEmpresaModule {}
