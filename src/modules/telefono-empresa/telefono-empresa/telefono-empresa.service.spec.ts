import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { TelefonoEmpresa } from '../../../models/telefono-empresa/telefono-empresa.entity';
import { DeleteResult, Repository } from 'typeorm';
import { TelefonoEmpresaService } from './telefono-empresa.service';
import { SampleTelefonoEmpresa } from '../../../../test/ObjectSamples';
import { CreateTelefonoEmpresaDTO } from '../../../models/telefono-empresa/dto/create-telefono-empresa.dto';

describe('TelefonoEmpresaService', () => {
  let service: TelefonoEmpresaService;
  let RepositoryMock: jest.Mock;
  let repository: Repository<TelefonoEmpresa>;

  beforeEach(() => {
    RepositoryMock = jest.fn(() => ({
      find: jest.fn(),
      save: jest.fn(),
      delete: jest.fn()
    }));
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TelefonoEmpresaService,
        { provide: getRepositoryToken(TelefonoEmpresa), useValue: new RepositoryMock() },
      ],
    }).compile();

    repository = module.get(getRepositoryToken(TelefonoEmpresa));
    service = module.get<TelefonoEmpresaService>(TelefonoEmpresaService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('create(CreateTelefonoEmpresaDTO)', () => {
    const { id, ...rest } = SampleTelefonoEmpresa;
    let body: CreateTelefonoEmpresaDTO;

    describe('case : success', () => {
      let expectedTelefonoEmpresa: TelefonoEmpresa;
      let result: TelefonoEmpresa;

      describe('when an role is created', () => {
        beforeEach(async () => {
          body = rest;
          expectedTelefonoEmpresa = SampleTelefonoEmpresa;
          (repository.save as jest.Mock).mockResolvedValue(expectedTelefonoEmpresa);
          result = await service.create(body);
        });

        it('should invoke repository.save()', () => {
          expect(repository.save).toHaveBeenCalledTimes(1);
          expect(repository.save).toHaveBeenCalledWith(body);
        });

        it('should create a role', () => {
          expect(result).toStrictEqual(expectedTelefonoEmpresa);
        });
      });
    });

    describe('case : failure', () => {
      let expectedError: TypeError;
      let body: undefined;

      describe('when a role is not created', () => {
        beforeEach(async () => {
          body = undefined;
          expectedError = new TypeError();
          jest.spyOn(service, 'create').mockRejectedValue(expectedError);
        });

        it('it should throw when the body is undefined', async () => {
          await expect(service.create(body)).rejects.toThrow(TypeError);
        });

        it('should not create an role', () => {
          expect(repository.save).not.toHaveBeenCalled();
        });
      });
    });
  });

  describe('delete(id)', () => {
    let deleteResult: DeleteResult;
    let result: DeleteResult;

    describe('case : success', () => {
      describe('when an role is found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 1;
          (repository.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await service.delete(1);
        });

        it('should invoke repository.delete()', () => {
          expect(repository.delete).toHaveBeenCalledTimes(1);
          expect(repository.delete).toHaveBeenCalledWith(1);
        });

        it('should delete an role', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });

    describe('case : failure', () => {
      let expectedError: TypeError;

      describe('when a telefono-Empresa is not found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 0;
          (repository.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await service.delete(1);
        });

        it('should invoke repository.delete()', () => {
          expect(repository.delete).toHaveBeenCalledTimes(1);
          expect(repository.delete).toHaveBeenCalledWith(1);
        });

        it('should not delete an role', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });
  }); 

});
