import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TelefonoEmpresa } from '../../../models/telefono-empresa/telefono-empresa.entity';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { CreateTelefonoEmpresaDTO } from '../../../models/telefono-empresa/dto/create-telefono-empresa.dto';

@Injectable()
export class TelefonoEmpresaService {
    constructor(
        @InjectRepository(TelefonoEmpresa) private readonly telefonoEmpresaRepository: Repository<TelefonoEmpresa>,
    ){}

    async create(telefonoEmpresa: CreateTelefonoEmpresaDTO): Promise<TelefonoEmpresa> {
        return await this.telefonoEmpresaRepository.save(telefonoEmpresa);
    }

    /*async update(id:number,telefonoEmpresa: UpdatetelefonoEmpresaDTO): Promise<UpdateResult> {
        return await this.telefonoEmpresaRepository.update(id,telefonoEmpresa);
    }*/

    async delete(id: number): Promise<DeleteResult> {
        try {
            return await this.telefonoEmpresaRepository.delete(id);
        }
        catch (Err) {
            throw new HttpException(
                {
                  status: HttpStatus.INTERNAL_SERVER_ERROR,
                  error: 'Ha ocurrido un error al eliminar registro telefonico (empresas)'
                },
                HttpStatus.INTERNAL_SERVER_ERROR
              );
        }
    }
}
