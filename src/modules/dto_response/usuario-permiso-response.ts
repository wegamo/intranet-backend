import { ApiModelProperty } from "@nestjs/swagger";

export class UsuarioPermisoResp {
    [prop:string]: Permiso;
}

export class Permiso {
    @ApiModelProperty()
    tipoPermiso:string;
    @ApiModelProperty()
    objId:number;
}