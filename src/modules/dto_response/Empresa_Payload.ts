//import { EstatusInt } from './Estatus_Payload';
import { TelefonoInt } from './Telefono_Payload';
/*import { NormaInt } from './Norma_Payload';
import { ValorInt } from './Valor_Payload';*/


export interface EmpresaInt {
    
  id: number;
  nombre: string;
  mision?: string | null;
  vision?: string | null;
  correo: string;

  //estatus: String;

  normas?: Array<String[2]>[] | null;
  valores?: Array<String[2]>[] | null;
  telefonos?: TelefonoInt[] | null;
}