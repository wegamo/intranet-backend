import { EstatusInt } from './Estatus_Payload';

export interface EventoInt {
  id: number;
  nombre: string;
  descripcion: string;
  ubicacion?: string | null;
  fechaInicio: Date;
  fechaFin: Date | null;
  tipo: string;

  estatus: string;
}
