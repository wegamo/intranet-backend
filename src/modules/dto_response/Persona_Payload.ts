import { TelefonoInt } from './Telefono_Payload';

export interface PersonaInt {
  id: number;
  cedula: string;
  primerNombre: string;
  segundoNombre?: string | null;
  primerApellido: string;
  segundoApellido?: string | null;

  email: string;
  fechaNacimiento: Date;
  fechaIngreso: Date;
  direccion: string;

  estatus: String;
  telefonos?: TelefonoInt[] | null;
  //empresa: String;
}
