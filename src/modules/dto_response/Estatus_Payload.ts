export interface EstatusInt {
  id: number;
  nombre: string;
  tipo: string;
}