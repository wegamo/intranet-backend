import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import {
  createQueryBuilder,
  DeleteResult,
  getConnection,
  getRepository,
  IsNull,
  Repository,
  UpdateResult,
  In
} from 'typeorm';
import { CreatePersonaDTO } from '../../../models/persona/dto/create-persona.dto';
import { UpdatePersonaDTO } from '../../../models/persona/dto/update-persona.dto';
import { Persona } from '../../../models/persona/persona.entity';
import { GlobalException } from '../../exception/global-exception';
import { IPaginationOptions, paginate, Pagination } from 'nestjs-typeorm-paginate';
import { PersonaEstatus } from '../../../enums/persona-estatus.enum';

@Injectable()
export class PersonaService {
  constructor(@InjectRepository(Persona) private readonly personaRepository: Repository<Persona>) {}

  findByEmail(email: string): Promise<Persona> {
    return this.personaRepository.findOne({ where: { email } });
  }

  findByCedula(cedula: string): Promise<Persona> {
    return this.personaRepository.findOne({ where: { cedula } });
  }

  findByEmailAndPassword(email: string, password: string): Promise<Persona> {
    return this.personaRepository
      .createQueryBuilder('persona')
      .select([
        'persona.cedula',
        'persona.direccion',
        'persona.email',
        'persona.estatus',
        'persona.fechaIngreso',
        'persona.fechaNacimiento',
        'persona.genero',
        'persona.id',
        'persona.imgPerfil',
        'persona.primerApellido',
        'persona.primerNombre',
        'persona.segundoApellido',
        'persona.segundoNombre'
      ])
      .innerJoinAndSelect('persona.rol', 'rol')
      .innerJoinAndSelect('persona.histCargos', 'histCargos')
      .innerJoinAndSelect('histCargos.cargo', 'cargo')
      .where(
        'persona.email = :email AND persona.password = :password AND histCargos.fechaFin IS NULL',
        { email, password }
      )
      .getOne();
    /*
    return this.personaRepository.findOne({
      select: [
        'cedula',
        'direccion',
        'email',
        'estatus',
        'fechaIngreso',
        'fechaNacimiento',
        'genero',
        'id',
        'imgPerfil',
        'primerApellido',
        'primerNombre',
        'segundoApellido',
        'segundoNombre',
        'password'
      ],
      relations: ['rol', 'histCargos', 'histCargos.cargo'],
      where: { email, histCargos: { fechaFin: IsNull() } }
    });
    */
  }

  findCurrentPersonaByCargo(idCargo: number): Promise<Persona> {
    return this.personaRepository
      .createQueryBuilder('persona')
      .innerJoin('persona.histCargos', 'histCargos')
      .innerJoin('histCargos.cargo', 'cargo')
      .where('cargo.id = :id AND histCargos.fechaFin IS NULL', { id: idCargo })
      .getOne();
  }

  async findSupervisor(id: number): Promise<Persona> {
    const persona = await getRepository(Persona)
      .createQueryBuilder('persona')
      .innerJoinAndSelect('persona.histCargos', 'histCargos')
      .innerJoinAndSelect('histCargos.cargo', 'cargo')
      .innerJoinAndSelect('cargo.parentCargo', 'parentCargo')
      .innerJoinAndSelect('parentCargo.histCargos', 'histCargosParent')
      .innerJoinAndSelect('histCargosParent.persona', 'supervisor')
      .where(
        'persona.id = :id AND histCargos.fechaFin is NULL AND histCargosParent.fechaFin is NULL',
        { id }
      )
      .getOne();
    const supervisor = persona.histCargos[0].cargo.parentCargo.histCargos[0].persona;
    console.log(supervisor);
    return supervisor;
  }

  findPersonaByEmail(email: string): Promise<Persona> {
    return this.personaRepository.findOne({ email }, { relations: ['rol'] });
  }

  findPersonaByEmailAndCedula(email: string, cedula: string): Promise<Persona> {
    return this.personaRepository.findOne({ email, cedula }, { relations: ['rol'] });
  }

  findOne(id: number): Promise<Persona> {
    return this.personaRepository.findOne(id);
  }

  findOneHumanTalent(id: number): Promise<Persona> {
    return getRepository(Persona)
      .createQueryBuilder('persona')
      .innerJoinAndSelect('persona.histCargos', 'histCargos')
      .innerJoinAndSelect('histCargos.cargo', 'cargo')
      .innerJoinAndSelect('cargo.area', 'area')
      .where('persona.id = :id AND area.nombre = :nombre', { id, nombre: 'Human Talent' })
      .getOne();
  }

  findAllHumanTalent(): Promise<Persona[]> {
    return getRepository(Persona)
      .createQueryBuilder('persona')
      .innerJoinAndSelect('persona.histCargos', 'histCargos')
      .innerJoinAndSelect('histCargos.cargo', 'cargo')
      .innerJoinAndSelect('cargo.area', 'area')
      .where('area.nombre = :nombre', { nombre: 'Human Talent' })
      .getMany();
  }

  async findOneCI(ci: string): Promise<Persona> {
    let where: string = '';

    if (ci) {
      where = `ci = '${ci}'`;
    }

    const persona: Persona = await this.personaRepository
      .createQueryBuilder()
      .where(where)
      .getOne();

    return persona;
  }

  async findOneCorreo(correo: string): Promise<Persona> {
    let where: string = '';

    if (correo) {
      where = `correo = '${correo}'`;
    }

    const persona: Persona = await this.personaRepository
      .createQueryBuilder()
      .where(where)
      .getOne();

    return persona;
  }

  findOneWithPersonalDetails(id: number): Promise<Persona> {
    return getRepository(Persona)
      .createQueryBuilder('persona')
      .innerJoinAndSelect('persona.empresa', 'empresa')
      .leftJoinAndSelect('persona.telefonos', 'telefonos')
      .innerJoinAndSelect('persona.histCargos', 'histCargos')
      .innerJoinAndSelect('histCargos.cargo', 'cargo')
      .innerJoinAndSelect('cargo.area', 'area')
      .where('persona.id = :id', { id })
      .getOne();
  }

  findOneWithDocuments(id: number): Promise<Persona> {
    return getRepository(Persona)
      .createQueryBuilder('persona')
      .leftJoinAndSelect('persona.telefonos', 'telefonos')
      .innerJoinAndSelect('persona.histCargos', 'histCargos')
      .innerJoinAndSelect('histCargos.cargo', 'cargo')
      .leftJoinAndSelect('persona.documentosPersonales', 'documentosPersonales')
      .leftJoinAndSelect('documentosPersonales.tipoDocumentoPersonal', 'tipoDocumentoPersonal')
      .where('persona.id = :id', { id })
      .getOne();
  }

  findOneWithCourses(id: number): Promise<Persona> {
    return getRepository(Persona)
      .createQueryBuilder('persona')
      .leftJoinAndSelect('persona.telefonos', 'telefonos')
      .innerJoinAndSelect('persona.histCargos', 'histCargos')
      .innerJoinAndSelect('histCargos.cargo', 'cargo')
      .leftJoinAndSelect('persona.cursos', 'cursos')
      .where('persona.id = :id', { id })
      .getOne();
  }

  async findAll(options: IPaginationOptions): Promise<Pagination<Persona>> {
    const queryBuilder = this.personaRepository.createQueryBuilder('persona');
    queryBuilder
      .select([
        'persona.id',
        'persona.primerNombre',
        'persona.primerApellido',
        'persona.imgPerfil',
        'persona.email',
        'persona.estatus'
      ])
      .innerJoinAndSelect('persona.empresa', 'empresa')
      .innerJoinAndSelect('persona.rol', 'rol')
      .innerJoinAndSelect('persona.histCargos', 'histCargos')
      .innerJoinAndSelect('histCargos.cargo', 'cargo')
      .innerJoinAndSelect('cargo.area', 'area')
      .where('histCargos.fechaFin IS NULL')
      .orderBy('persona.estatus', 'ASC');

    return paginate<Persona>(queryBuilder, options);
  }

  findAllWithoutPagination(): Promise<Persona[]> {
    return this.personaRepository.find({ where: { estatus: PersonaEstatus.ACTIVO } });
  }

  async create(persona: CreatePersonaDTO) {
    return this.personaRepository.save(persona);
  }

  async update(persona: Persona): Promise<Persona> {
    return this.personaRepository.save(persona);
  }

  async delete(id: number): Promise<DeleteResult | GlobalException> {
    try {
      let DelRes: DeleteResult = await this.personaRepository.delete(id);
      return DelRes;
    } catch (Err) {
      console.log(Err.message + '\n' + Err.query + '\n');
      if (Err.message.match('viola la llave foránea')) {
        throw new GlobalException(
          'Informacion vinculada, se debe eliminar la informacion dependiente primero',
          1
        );
      }
    }

    return await this.personaRepository.delete(id);
  }

  async deleteOnCascade(personId: number): Promise<DeleteResult> {
    return await this.personaRepository.delete(personId);
  }

  async getMultipleId(idToLook: number[]): Promise<Persona[]> {
    let personaResp: Persona[] = await this.personaRepository.find({ where: { id: In(idToLook) } });
    return personaResp;
  }
}
