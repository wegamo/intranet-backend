import { Test, TestingModule } from '@nestjs/testing';
import { DeleteResult, UpdateResult } from 'typeorm';

import { CreatePersonaDTO } from '../../../models/persona/dto/create-persona.dto';
import { UpdatePersonaDTO } from '../../../models/persona/dto/update-persona.dto';
import { Persona } from '../../../models/persona/persona.entity';

import { PersonaController } from './persona.controller';
import { PersonaService } from './persona.service';

import { SamplePersona } from '../../../../test/ObjectSamples';
import { PersonaInt } from '../../dto_response/Persona_Payload';
import { GlobalException } from '../../exception/global-exception';
import { Pagination } from 'nestjs-typeorm-paginate';
import { DocumentoPersonalService } from '../../documento-personal/documento-personal/documento-personal.service';
import { TipoDocumentoPersonalService } from '../../tipo-documento-personal/tipo-documento-personal/tipoDocumentoPersonal.service';

describe('Persona Controller', () => {
  let controller: PersonaController;
  let PersonaServiceMock: jest.Mock<Partial<PersonaService>>;
  let personaService: PersonaService;
  let DocumentoPersonalServiceMock: jest.Mock<Partial<DocumentoPersonalService>>;
  let TipoDocumentoPersonalServiceMock: jest.Mock<Partial<TipoDocumentoPersonalService>>

  beforeEach(() => {
    PersonaServiceMock = jest.fn<Partial<PersonaService>, PersonaService[]>(() => ({
      findAll: jest.fn(),
      findOne: jest.fn(),
      findOneCI: jest.fn(),
      findOneCorreo: jest.fn(),
      create: jest.fn(),
      delete: jest.fn(),
      update: jest.fn()
    }));
    DocumentoPersonalServiceMock = jest.fn<Partial<DocumentoPersonalService>, DocumentoPersonalService[]>(() => ({
      createAllRequired:jest.fn(),
    }));
    TipoDocumentoPersonalServiceMock = jest.fn<Partial<TipoDocumentoPersonalService>, TipoDocumentoPersonalService[]>(() => ({
      findAllRequired:jest.fn(),
    }));
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PersonaController],
      providers: [
        { provide: PersonaService, useValue: new PersonaServiceMock() },
        { provide: DocumentoPersonalService, useValue: new DocumentoPersonalServiceMock() },
        { provide: TipoDocumentoPersonalService, useValue: new TipoDocumentoPersonalServiceMock() },
      ]
    }).compile();

    personaService = module.get<PersonaService>(PersonaService);
    controller = module.get<PersonaController>(PersonaController);
  });

  describe('findAll()', () => {
    let expectedPersonas: Pagination<Persona>;
    let result: Pagination<Persona>;

    describe('case: success', () => {
      describe('when there are persons', () => {
        beforeEach(async () => {
          //expectedPersonas = [SamplePersona];
          (personaService.findAll as jest.Mock).mockResolvedValue(expectedPersonas);
          result = await controller.getAll();
        });

        it('should invoke PersonaService.getAll()', () => {
          expect(personaService.findAll).toHaveBeenCalledTimes(1);
        });

        it('should return an array of persons', () => {
          expect(result.items).toStrictEqual(expectedPersonas);
        });
      });
    });

    describe('case: failure', () => {
      describe('when there are not persons', () => {
        beforeEach(async () => {
          //expectedPersonas = [];
          (personaService.findAll as jest.Mock).mockResolvedValue(expectedPersonas);
          result = await controller.getAll();
        });

        it('should invoke PersonaService.getAll()', () => {
          expect(personaService.findAll).toHaveBeenCalledTimes(1);
        });

        it('should return an array empty', () => {
          expect(result.items).toStrictEqual(expectedPersonas);
        });
      });
    });
  });

  describe('findOne(id)', () => {
    let expectedPersona: Persona | PersonaInt;
    let result: Persona | PersonaInt;

    describe('case: success', () => {
      describe('when a person is found', () => {
        beforeEach(async () => {
          expectedPersona = SamplePersona;
          (personaService.findOne as jest.Mock).mockResolvedValue(expectedPersona);
          result = await controller.findOne(1);
        });

        it('should invoke PersonaService.findOne()', () => {
          expect(personaService.findOne).toHaveBeenCalledTimes(1);
          expect(personaService.findOne).toHaveBeenCalledWith(1);
        });

        it('should return a person', () => {
          expect(result).toStrictEqual(expectedPersona);
        });
      });
    });

    describe('case: failure', () => {
      describe('when a person is not found', () => {
        beforeEach(async () => {
          expectedPersona = undefined;
          (personaService.findOne as jest.Mock).mockResolvedValue(expectedPersona);
          result = await controller.findOne(1);
        });

        it('should invoke PersonaService.findOne()', () => {
          expect(personaService.findOne).toHaveBeenCalledTimes(1);
          expect(personaService.findOne).toHaveBeenCalledWith(1);
        });

        it('should return undefined', () => {
          expect(result).toStrictEqual(expectedPersona);
        });
      });
    });
  });

  describe('findOneCI()', () => {
    let expectedPersona: Persona;
    let result: Persona;
    let ci: string;

    describe('case : success', () => {
      describe('when there are persona', () => {
        beforeEach(async () => {
          expectedPersona = SamplePersona;
          ci = SamplePersona.cedula;
          (personaService.findOneCI as jest.Mock).mockResolvedValue(expectedPersona);
          result = await controller.findOneCI(ci);
        });
        it('should invoke personaService.findOneCI()', () => {
          expect(personaService.findOneCI).toHaveBeenCalledTimes(1);
          expect(personaService.findOneCI).toHaveBeenCalledWith(ci);
        });
        it('should return a person', () => {
          expect(result).toStrictEqual(expectedPersona);
        });
      });
    });

    describe('case : failure', () => {
      describe('when there are no persona', () => {
        beforeEach(async () => {
          expectedPersona = undefined;
          ci = SamplePersona.cedula;
          (personaService.findOneCI as jest.Mock).mockResolvedValue(expectedPersona);
          result = await controller.findOneCI(ci);
        });

        it('should invoke personaService.findOneCI()', () => {
          expect(personaService.findOneCI).toHaveBeenCalledTimes(1);
          expect(personaService.findOneCI).toHaveBeenCalledWith(ci);
        });

        it('should return undefined', () => {
          expect(result).toStrictEqual(expectedPersona);
        });
      });
    });
  });

  describe('findOneCorreo()', () => {
    let expectedPersona: Persona;
    let result: Persona;
    let correo: string;

    describe('case : success', () => {
      describe('when there are persona', () => {
        beforeEach(async () => {
          expectedPersona = SamplePersona;
          correo = SamplePersona.email;
          (personaService.findOneCorreo as jest.Mock).mockResolvedValue(expectedPersona);
          result = await controller.findOneCorreo(correo);
        });
        it('should invoke personaService.findOneCorreo()', () => {
          expect(personaService.findOneCorreo).toHaveBeenCalledTimes(1);
          expect(personaService.findOneCorreo).toHaveBeenCalledWith(correo);
        });
        it('should return a person', () => {
          expect(result).toStrictEqual(expectedPersona);
        });
      });
    });

    describe('case : failure', () => {
      describe('when there are no persona', () => {
        beforeEach(async () => {
          expectedPersona = undefined;
          correo = SamplePersona.email;
          (personaService.findOneCorreo as jest.Mock).mockResolvedValue(expectedPersona);
          result = await controller.findOneCorreo(correo);
        });

        it('should invoke personaService.findOneCorreo()', () => {
          expect(personaService.findOneCorreo).toHaveBeenCalledTimes(1);
          expect(personaService.findOneCorreo).toHaveBeenCalledWith(correo);
        });

        it('should return undefined', () => {
          expect(result).toStrictEqual(expectedPersona);
        });
      });
    });
  });

  describe('create(personaData)', () => {
    const { id, ...rest } = SamplePersona;
    let body: CreatePersonaDTO;

    describe('case: success', () => {
      let expectedPersona: Persona;
      let result: Persona;

      describe('when a person is created', () => {
        beforeEach(async () => {
          body = rest;
          expectedPersona = SamplePersona;

          (personaService.create as jest.Mock).mockResolvedValue(expectedPersona);
          result = await controller.create(body);
        });

        it('should invoke PersonaService.create()', () => {
          expect(personaService.create).toHaveBeenCalledTimes(1);
          expect(personaService.create).toHaveBeenCalledWith(body);
        });

        it('should create a person', () => {
          expect(result).toStrictEqual(expectedPersona);
        });
      });
    });

    describe('case: failure', () => {
      let expectedError: TypeError;

      describe('when a person is not created', () => {
        beforeEach(async () => {
          body = undefined;
          expectedError = new TypeError();
          jest.spyOn(controller, 'create').mockRejectedValue(expectedError);
        });

        it('should throw when the body is undefined', async () => {
          await expect(controller.create(body)).rejects.toThrow(TypeError);
        });

        it('should not create a person', () => {
          expect(personaService.create).not.toHaveBeenCalled();
        });
      });
    });
  });

  describe('update(id, normaData)', () => {
    let update: UpdatePersonaDTO;

    describe('case: success', () => {
      let updateResult: Persona;
      let result: Persona;

      describe('when a person is updated', () => {
        beforeEach(async () => {
          update = {
            cedula: 'Test',
            primerNombre: 'Test',
            segundoNombre: 'Test',
            primerApellido: 'Test',
            segundoApellido: 'Test',
            email: 'Test',
            fechaNacimiento: new Date('2018-01-01'),
            fechaIngreso: new Date('2019-01-01')
          };
          //updateResult = new UpdateResult();
          (personaService.update as jest.Mock).mockResolvedValue(updateResult);
          result = await controller.update(1, update);
        });

        it('should invoke PersonaService.update()', () => {
          expect(personaService.update).toHaveBeenCalledTimes(1);
          expect(personaService.update).toHaveBeenCalledWith(1, update);
        });

        it('should update a person', () => {
          expect(result).toStrictEqual(updateResult);
        });
      });
    });

    describe('case: failure', () => {
      let expectedError: TypeError;

      describe('when a person is not updated', () => {
        beforeEach(async () => {
          expectedError = new TypeError();
          update = undefined;
          jest.spyOn(controller, 'update').mockRejectedValue(expectedError);
        });

        it('should throw when the body is undefined', async () => {
          await expect(controller.update(1, update)).rejects.toThrow(TypeError);
        });

        it('should not update a person', () => {
          expect(personaService.update).not.toHaveBeenCalled();
        });
      });
    });
  });

  describe('delete(id)', () => {
    let deleteResult: DeleteResult | GlobalException;
    let result: DeleteResult | GlobalException;

    describe('case: success', () => {
      describe('when a person is found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 1;
          (personaService.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await controller.delete(1);
        });

        it('should invoke PersonaService.delete()', () => {
          expect(personaService.delete).toHaveBeenCalledTimes(1);
          expect(personaService.delete).toHaveBeenCalledWith(1);
        });

        it('should delete a person', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });

    describe('case: failure', () => {
      describe('when a person is not found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 0;
          (personaService.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await controller.delete(1);
        });

        it('should invoke PersonaService.delete()', () => {
          expect(personaService.delete).toHaveBeenCalledTimes(1);
          expect(personaService.delete).toHaveBeenCalledWith(1);
        });

        it('should not delete a person', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });
  });
});
