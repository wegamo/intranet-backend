import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Put,
  Query,
  Req,
  UseGuards
} from '@nestjs/common';
import { DeleteResult, UpdateResult } from 'typeorm';
import { sha1 } from 'object-hash';
import { CreatePersonaDTO } from '../../../models/persona/dto/create-persona.dto';
import { UpdatePersonaDTO } from '../../../models/persona/dto/update-persona.dto';
import { Persona } from '../../../models/persona/persona.entity';

import { PersonaService } from './persona.service';
import { DocumentoPersonalService } from '../../documento-personal/documento-personal/documento-personal.service';
import { GlobalException } from '../../exception/global-exception';

import { PersonaInt } from '../../dto_response/Persona_Payload';
import { AuthGuard } from '@nestjs/passport';
import { TipoDocumentoPersonalService } from '../../tipo-documento-personal/tipo-documento-personal/tipoDocumentoPersonal.service';
import { Roles } from '../../../decorators/roles.decorator';
import { RolesGuard } from '../../../guards/roles.guard';
import { HistDiasDispVacacionesService } from '../../hist-dias-disp-vacaciones/hist-dias-disp-vacaciones/hist-dias-disp-vacaciones.service';
import { HistPrestacionesService } from '../../hist-prestaciones/hist-prestaciones/hist-prestaciones.service';
import { HistAusenciaService } from '../../hist-ausencia/hist-ausencia/hist-ausencia.service';
import { ApiBearerAuth, ApiUseTags } from '@nestjs/swagger';

@ApiBearerAuth()
@ApiUseTags('Personas')
@Controller('persona')
export class PersonaController {
  constructor(
    private readonly personaService: PersonaService,
    private readonly documentoPersonalService: DocumentoPersonalService,
    private readonly tipoDocumentoPersonalService: TipoDocumentoPersonalService,
    private readonly histDiasDispService: HistDiasDispVacacionesService,
    private readonly histPrestacionesService: HistPrestacionesService,
    private readonly histAusenciaService: HistAusenciaService
  ) {}

  @Roles('ADMIN_TH', 'SUPER_ADMIN')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Get()
  async getAll(@Query('page') page = 1, @Query('limit') limit = 10) {
    limit = limit > 100 ? 100 : limit;
    const personas = await this.personaService.findAll({
      page,
      limit
    });
    return {
      personas
    };
  }

  @UseGuards(AuthGuard('jwt'))
  @Get(':id/personal-details')
  getOneWithPersonalDetails(@Param('id') id: number) {
    return this.personaService.findOneWithPersonalDetails(id);
  }

  @UseGuards(AuthGuard('jwt'))
  @Get(':id/documents')
  async getOneWithDocuments(@Param('id') id: number) {
    const user = await this.personaService.findOneWithDocuments(id);
    user.histCargos = user.histCargos.filter((cargo) => cargo.fechaFin === null);
    return user;
  }

  @UseGuards(AuthGuard('jwt'))
  @Get(':id/courses')
  async getOneWithCurses(@Param('id') id: number) {
    const user = await this.personaService.findOneWithCourses(id);
    user.histCargos = user.histCargos.filter((cargo) => cargo.fechaFin === null);
    return user;
  }

  @UseGuards(AuthGuard('jwt'))
  @Get(':id/statistics')
  async findOne(@Param('id') id: number): Promise<any> {
    const persona = await this.personaService.findOne(id);
    const diasVacaciones = await this.histDiasDispService.findLastest(id);
    const prestacionesPendientes = await this.histPrestacionesService.findAllPendienteByPersona(id);
    const ausenciasDelMes = await this.histAusenciaService.countByCurrentMonth(persona.id);
    return {
      vacacionesDisponibles: diasVacaciones.diasDisponibles || 0,
      prestacionesPendientes: prestacionesPendientes || 0,
      ausenciasDelMes: ausenciasDelMes || 0
    };
  }

  @Get('buscar/cedula/:ci')
  findOneCI(@Param('ci') ci: string): Promise<Persona> {
    return this.personaService.findOneCI(ci);
  }

  @Get('buscar/correo/:correo')
  findOneCorreo(@Param('correo') correo: string): Promise<Persona> {
    return this.personaService.findOneCorreo(correo);
  }

  @Get(':id/supervisor')
  getSupervisor(@Param('id') id: number): Promise<Persona> {
    return this.personaService.findSupervisor(id);
  }

  @Post('create')
  async create(@Body() personaData: CreatePersonaDTO): Promise<Persona> {
    const password = sha1(personaData.primerNombre.toLocaleLowerCase());
    const persona = {
      ...personaData,
      password
    };
    const personaFoundEmail = await this.personaService.findPersonaByEmail(personaData.email);
    const personaFoundCedula = await this.personaService.findByCedula(personaData.cedula);
    if (personaFoundEmail || personaFoundCedula) {
      throw new HttpException(
        'Este Correo o Cedula ya se encuentra registrado en el sistema',
        HttpStatus.FORBIDDEN
      );
    }
    try {
      const personaCreated = await this.personaService.create(persona);
      const documentosRequeridos = await this.tipoDocumentoPersonalService.findAllRequired();
      await this.documentoPersonalService.createAllRequired(
        documentosRequeridos,
        personaCreated.id
      );
      await this.histDiasDispService.create({
        diasDisponibles: 0,
        fechaSolicitud: new Date(),
        persona: personaCreated
      });
      delete personaCreated.password;
      return personaCreated;
    } catch (err) {
      throw new HttpException('Error al Crear el Usuario', HttpStatus.BAD_REQUEST);
    }
  }

  @Put('update/:id')
  async update(@Param('id') id: number, @Body() PersonaData: UpdatePersonaDTO) {
    try {
      let userToChange = await this.personaService.findOne(id);

      if (!userToChange) {
        throw new HttpException('El usuario no existe', HttpStatus.FOUND);
      }
      userToChange = { ...userToChange, ...PersonaData };
      const user = await this.personaService.update(userToChange);

      return { user };
    } catch (err) {
      throw new HttpException('Error al actualizar la Persona', HttpStatus.BAD_REQUEST);
    }
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('statistics')
  getStatistics(@Param('id') id: number): Promise<Persona | PersonaInt> {
    return this.personaService.findOne(id);
  }

  @Delete(':id')
  async delete(@Param('id') id: number): Promise<DeleteResult | GlobalException> {
    return this.personaService.delete(id);
  }

  @Delete('cascade/:personId')
  async deleteOnCascade(@Param('personId') personId: number): Promise<DeleteResult> {
    return this.personaService.deleteOnCascade(personId);
  }
}
