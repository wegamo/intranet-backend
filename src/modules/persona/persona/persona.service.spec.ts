import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';

import { CreatePersonaDTO } from '../../../models/persona/dto/create-persona.dto';
import { UpdatePersonaDTO } from '../../../models/persona/dto/update-persona.dto';
import { Persona } from '../../../models/persona/persona.entity';

import { PersonaService } from './persona.service';

import { SamplePersona } from '../../../../test/ObjectSamples';
import { PersonaInt } from '../../dto_response/Persona_Payload';
import { GlobalException } from '../../exception/global-exception';
import { TelefonoPersonaService } from '../../telefono-persona/telefono-persona/telefono-persona.service';
import { TelefonoPersona } from '../../../models/telefono-persona/telefono-persona.entity';

describe('Persona Service', () => {
  
  let service: PersonaService;
  let RepositoryMock: jest.Mock;
  let repository: Repository<Persona>;
  let getOneSpy: jest.Mock;

  beforeEach(() => {
    const whereSpy = jest.fn().mockReturnThis();
    getOneSpy = jest.fn().mockReturnThis();

    RepositoryMock = jest.fn(() => ({
      find: jest.fn(),
      findOne: jest.fn(),
      createQueryBuilder: jest.fn(() => ({
        where: whereSpy,
        getOne: getOneSpy,
        take: jest.fn(),
      })),
      save: jest.fn(),
      update: jest.fn(),
      delete: jest.fn()
    }));
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        PersonaService,
        { provide: getRepositoryToken(Persona), useValue: new RepositoryMock() },
        TelefonoPersonaService,
        { provide: getRepositoryToken(TelefonoPersona), useValue: new RepositoryMock() }
      ]
    }).compile();

    repository = module.get(getRepositoryToken(Persona));
    service = module.get<PersonaService>(PersonaService);
  });
  describe('create(persona)', () => {
    const { id, ...rest } = SamplePersona;
    let body: CreatePersonaDTO;

    describe('case: success', () => {
      let expectedPersona: Persona;
      let result: Persona;

      describe('when a person is created', () => {
        beforeEach(async () => {
          body = rest;
          expectedPersona = SamplePersona;
          (repository.save as jest.Mock).mockResolvedValue(expectedPersona);
          result = await service.create(body);
        });

        it('should invoke repository.save()', () => {
          expect(repository.save).toHaveBeenCalledTimes(1);
          expect(repository.save).toHaveBeenCalledWith(body);
        });

        it('should create a person', () => {
          expect(result).toStrictEqual(expectedPersona);
        });
      });
    });

    describe('case: failure', () => {
      let expectedError: TypeError;

      describe('when a person is not created', () => {
        beforeEach(async () => {
          body = undefined;
          expectedError = new TypeError();
          jest.spyOn(service, 'create').mockRejectedValue(expectedError);
        });

        it('should throw when the body is undefined', async () => {
          await expect(service.create(body)).rejects.toThrow(TypeError);
        });

        it('should not create a person', () => {
          expect(repository.save).not.toHaveBeenCalled();
        });
      });
    });
  });
  /*
  describe('findAll()', () => {
    const { estatus, ...rest } = SamplePersona;
    let expectedPersonas: PersonaInt[];
    let result: PersonaInt[];

    describe('case: success', () => {
      describe('when there are persons', () => {
        beforeEach(async () => {
          expectedPersonas = [{ estatus: estatus, ...rest }];
          (repository.find as jest.Mock).mockResolvedValue(expectedPersonas);
          result = await service.findAll();
        });

        it('should invoke repository.find()', () => {
          expect(repository.find).toHaveBeenCalledTimes(1);
        });

        it('should return an array of persons', () => {
          expect(result).toStrictEqual(expectedPersonas);
        });
      });
    });

    describe('case: failure', () => {
      describe('when there are not persons', () => {
        beforeEach(async () => {
          expectedPersonas = [];
          (repository.find as jest.Mock).mockResolvedValue(expectedPersonas);
          result = await service.findAll();
        });

        it('should invoke repository.find()', () => {
          expect(repository.find).toHaveBeenCalledTimes(1);
        });

        it('should return an array empty', () => {
          expect(result).toStrictEqual(expectedPersonas);
        });
      });
    });
  });
  */
  describe('findOne(id)', () => {
    const { estatus, ...rest } = SamplePersona;
    let expectedPersona: Persona;
    let result: Persona;

    describe('case: success', () => {
      describe('when a person is found', () => {
        beforeEach(async () => {
          expectedPersona = { estatus:estatus, ...rest };
          (repository.findOne as jest.Mock).mockResolvedValue(expectedPersona);
          result = await service.findOne(1);
        });

        it('should invoke repository.findOne()', () => {
          expect(repository.findOne).toHaveBeenCalledTimes(1);
          expect(repository.findOne).toHaveBeenCalledWith(1);
        });

        it('should return a person', () => {
          expect(result).toStrictEqual(expectedPersona);
        });
      });
    });

    describe('case: failure', () => {
      describe('when a person is not found', () => {
        beforeEach(async () => {
          expectedPersona = undefined;
          (repository.findOne as jest.Mock).mockResolvedValue(expectedPersona);
          result = await service.findOne(1);
        });

        it('should invoke repository.findOne(id)', () => {
          expect(repository.findOne).toHaveBeenCalledTimes(1);
          expect(repository.findOne).toHaveBeenCalledWith(1);
        });

        it('should return undefined', () => {
          expect(result).toStrictEqual(expectedPersona);
        });
      });
    });
  });

  describe('findOnecedula()', () => {
    let expectedPersona: Persona;
    let result: Persona;
    let cedula: string;
 
    describe('case : success', () => {
      describe('when there are persona', () => {
        beforeEach(async () => {
          expectedPersona = SamplePersona;
          cedula = SamplePersona.cedula;
          getOneSpy.mockResolvedValue(expectedPersona);
          result = await service.findOneCI(cedula);
        });

        it('should invoke repository.createQueryBuilder()', () => {
          expect(repository.createQueryBuilder).toHaveBeenCalledTimes(1);
        });

        it('should return a person', () => {
          expect(result).toStrictEqual(expectedPersona);
        });
      });
    });

    describe('case : failure', () => {
      describe('when there are not persona', () => {
        beforeEach(async () => {
          expectedPersona = undefined;
          cedula = SamplePersona.cedula;
          getOneSpy.mockResolvedValue(expectedPersona);
          result = await service.findOneCI(cedula);
        });

        it('should invoke repository.createQueryBuilder()', () => {
          expect(repository.createQueryBuilder).toHaveBeenCalledTimes(1);
        });

        it('should return undefined', () => {
          expect(result).toStrictEqual(expectedPersona);
        });
      });
    });
  });

  describe('findOneCorreo()', () => {
    let expectedPersona: Persona;
    let result: Persona;
    let correo: string;

    describe('case : success', () => {
      describe('when there are persona', () => {
        beforeEach(async () => {
          expectedPersona = SamplePersona;
          correo = SamplePersona.email;
          getOneSpy.mockResolvedValue(expectedPersona);
          result = await service.findOneCorreo(correo);
        });

        it('should invoke repository.createQueryBuilder()', () => {
          expect(repository.createQueryBuilder).toHaveBeenCalledTimes(1);
        });

        it('should return a person', () => {
          expect(result).toStrictEqual(expectedPersona);
        });
      });
    });

    describe('case : failure', () => {
      describe('when there are not persona', () => {
        beforeEach(async () => {
          expectedPersona = undefined;
          correo = SamplePersona.email;
          getOneSpy.mockResolvedValue(expectedPersona);
          result = await service.findOneCorreo(correo);
        });

        it('should invoke repository.createQueryBuilder()', () => {
          expect(repository.createQueryBuilder).toHaveBeenCalledTimes(1);
        });

        it('should return undefined', () => {
          expect(result).toStrictEqual(expectedPersona);
        });
      });
    });
  });

  describe('update(id, persona)', () => {
    let update:Persona;

    describe('case: success', () => {
      let updateResult: Persona;
      let result: Persona;

      describe('when a person is updated', () => {
        beforeEach(async () => {
          update = {
            cedula: 'Test',
            primerNombre: 'Test',
            segundoNombre: 'Test',
            primerApellido: 'Test',
            segundoApellido: 'Test',
            email: 'Test',
            fechaNacimiento: new Date('2019-01-01'),
            fechaIngreso: new Date('2019-01-01'),
            direccion: 'Test',
            ...SamplePersona
          };
          //updateResult = new UpdateResult();
          (repository.save as jest.Mock).mockResolvedValue(updateResult);
          result = await service.update(update);
        });

        it('should invoke repository.update()', () => {
          expect(repository.save).toHaveBeenCalledTimes(1);
          expect(repository.save).toHaveBeenCalledWith(update);
        });

        it('should update a person', () => {
          expect(result).toStrictEqual(updateResult);
        });
      });
    });

    describe('case: failure', () => {
      let expectedError: TypeError;

      describe('when a person is not updated', () => {
        beforeEach(async () => {
          expectedError = new TypeError();
          update = undefined;
          jest.spyOn(service, 'update').mockRejectedValue(expectedError);
        });

        it('should throw when the body is undefined', async () => {
          await expect(service.update(update)).rejects.toThrow(TypeError);
        });

        it('should not update a person', () => {
          expect(repository.save).not.toHaveBeenCalled();
        });
      });
    });
  });

  describe('delete(id)', () => {
    let deleteResult: DeleteResult | GlobalException;
    let result: DeleteResult | GlobalException;

    describe('case: success', () => {
      describe('when a person is found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 1;
          (repository.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await service.delete(1);
        });

        it('should invoke repository.delete()', () => {
          expect(repository.delete).toHaveBeenCalledTimes(1);
          expect(repository.delete).toHaveBeenCalledWith(1);
        });

        it('should delete a person', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });

    describe('case: failure', () => {
      describe('when a person is not found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 0;
          (repository.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await service.delete(1);
        });

        it('should invoke repository.delete()', () => {
          expect(repository.delete).toHaveBeenCalledTimes(1);
          expect(repository.delete).toHaveBeenCalledWith(1);
        });

        it('should not delete a person', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });
  });

});
