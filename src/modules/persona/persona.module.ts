import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Persona } from '../../models/persona/persona.entity';
import { PersonaController } from './persona/persona.controller';
import { PersonaService } from './persona/persona.service';
import { TelefonoPersonaModule } from '../telefono-persona/telefono-persona.module';
import { TipoDocumentoPersonalModule } from '../tipo-documento-personal/tipoDocumentoPersonal.module';
import { DocumentoPersonalModule } from '../documento-personal/documento-personal.module';
import { TipoDocumentoPersonalService } from '../tipo-documento-personal/tipo-documento-personal/tipoDocumentoPersonal.service';
import { DocumentoPersonalService } from '../documento-personal/documento-personal/documento-personal.service';
import { HistDiasDispVacacionesModule } from '../hist-dias-disp-vacaciones/hist-dias-disp-vacaciones.module';
import { HistPrestacionesModule } from '../hist-prestaciones/hist-prestaciones.module';
import { HistAusenciaModule } from '../hist-ausencia/hist-ausencia.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Persona]),
    TelefonoPersonaModule,
    TipoDocumentoPersonalModule,
    forwardRef(() => HistDiasDispVacacionesModule),
    forwardRef(() => HistAusenciaModule),
    forwardRef(() => HistPrestacionesModule),
    forwardRef(() => DocumentoPersonalModule)
  ],
  controllers: [PersonaController],
  providers: [PersonaService, TipoDocumentoPersonalService, DocumentoPersonalService],
  exports: [PersonaService]
})
export class PersonaModule {}
