import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TipoDocumentoPersonal } from '../../models/tipo-documento-personal/tipoDocumentoPersonal.entity';
import { TipoDocumentoPersonalController } from './tipo-documento-personal/tipoDocumentoPersonal.controller';
import { TipoDocumentoPersonalService } from './tipo-documento-personal/tipoDocumentoPersonal.service';

@Module({
  imports: [TypeOrmModule.forFeature([TipoDocumentoPersonal])],
  providers: [TipoDocumentoPersonalService],
  controllers: [TipoDocumentoPersonalController],
  exports: [TipoDocumentoPersonalService]
})
export class TipoDocumentoPersonalModule {}
