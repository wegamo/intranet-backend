import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';

import { TipoDocumentoPersonal } from '../../../models/tipo-documento-personal/tipoDocumentoPersonal.entity';
import { CreateTipoDocumentoDTO } from '../../../models/tipo-documento-personal/dto/create-tipo-documento-personal.dto';
import { UpdateDocumentoDTO } from '../../../models/tipo-documento-personal/dto/update-tipo-documento-personal.dto';

@Injectable()
export class TipoDocumentoPersonalService {
  constructor(
    @InjectRepository(TipoDocumentoPersonal)
    private readonly documentoRepository: Repository<TipoDocumentoPersonal>
  ) {}

  async findAll(): Promise<TipoDocumentoPersonal[]> {
    return await this.documentoRepository.find();
  }

  findAllRequired(): Promise<TipoDocumentoPersonal[]> {
    return this.documentoRepository.find({ where: { requerido: '1' } });
  }

  findAllNotRequired(): Promise<TipoDocumentoPersonal[]> {
    return this.documentoRepository.find({ where: { requerido: '0' } });
  }

  async findOne(id: number): Promise<TipoDocumentoPersonal> {
    return await this.documentoRepository.findOne(id);
  }

  async create(documento: CreateTipoDocumentoDTO): Promise<TipoDocumentoPersonal> {
    return await this.documentoRepository.save(documento);
  }

  async update(id: number, documento: UpdateDocumentoDTO): Promise<UpdateResult> {
    try {
    } catch (Err) {}

    return await this.documentoRepository.update(id, documento);
  }

  async delete(id: number): Promise<DeleteResult> {
    try {
    } catch (Err) {}

    return await this.documentoRepository.delete(id);
  }
}
