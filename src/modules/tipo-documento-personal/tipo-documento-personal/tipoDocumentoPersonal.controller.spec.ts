import { Test, TestingModule } from '@nestjs/testing';
import { DeleteResult, UpdateResult } from 'typeorm';

import { TipoDocumentoPersonal } from '../../../models/tipo-documento-personal/tipoDocumentoPersonal.entity';
import { CreateTipoDocumentoDTO } from '../../../models/tipo-documento-personal/dto/create-tipo-documento-personal.dto';
import { UpdateDocumentoDTO } from '../../../models/tipo-documento-personal/dto/update-tipo-documento-personal.dto';

import { TipoDocumentoPersonalController } from './tipoDocumentoPersonal.controller';
import { TipoDocumentoPersonalService } from './tipoDocumentoPersonal.service';

import { SampleTipoDocumentoPersonal } from '../../../../test/ObjectSamples';
import { GlobalException } from '../../exception/global-exception';

describe('Documento Controller', () => {
  let controller: TipoDocumentoPersonalController;
  let DocumentoServiceMock: jest.Mock<Partial<TipoDocumentoPersonalService>>;
  let documentoService: TipoDocumentoPersonalService;

  beforeEach(() => {
    DocumentoServiceMock = jest.fn<
      Partial<TipoDocumentoPersonalService>,
      TipoDocumentoPersonalService[]
    >(() => ({
      findAll: jest.fn(),
      findOne: jest.fn(),
      create: jest.fn(),
      delete: jest.fn(),
      update: jest.fn()
    }));
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TipoDocumentoPersonalController],
      providers: [{ provide: TipoDocumentoPersonalService, useValue: new DocumentoServiceMock() }]
    }).compile();

    documentoService = module.get<TipoDocumentoPersonalService>(TipoDocumentoPersonalService);
    controller = module.get<TipoDocumentoPersonalController>(TipoDocumentoPersonalController);
  });

  describe('findAll()', () => {
    let expectedDocumentos: TipoDocumentoPersonal[];
    let result: TipoDocumentoPersonal[];

    describe('case: success', () => {
      describe('when there are documents', () => {
        beforeEach(async () => {
          expectedDocumentos = [SampleTipoDocumentoPersonal];
          (documentoService.findAll as jest.Mock).mockResolvedValue(expectedDocumentos);
          result = await controller.findAll();
        });

        it('should invoke DocumentoService.findAll()', () => {
          expect(documentoService.findAll).toHaveBeenCalledTimes(1);
        });

        it('should return an array of documents', () => {
          expect(result).toStrictEqual(expectedDocumentos);
        });
      });
    });

    describe('case: failure', () => {
      describe('when there are not documents', () => {
        beforeEach(async () => {
          expectedDocumentos = [];
          (documentoService.findAll as jest.Mock).mockResolvedValue(expectedDocumentos);
          result = await controller.findAll();
        });

        it('should invoke DocumentoService.findAll()', () => {
          expect(documentoService.findAll).toHaveBeenCalledTimes(1);
        });

        it('should return an array empty', () => {
          expect(result).toStrictEqual(expectedDocumentos);
        });
      });
    });
  });

  describe('findOne(id)', () => {
    let expectedDocumento: TipoDocumentoPersonal;
    let result: TipoDocumentoPersonal;

    describe('case: success', () => {
      describe('when a document is found', () => {
        beforeEach(async () => {
          expectedDocumento = SampleTipoDocumentoPersonal;
          (documentoService.findOne as jest.Mock).mockResolvedValue(expectedDocumento);
          result = await controller.findOne(1);
        });

        it('should invoke DocumentoService.findOne()', () => {
          expect(documentoService.findOne).toHaveBeenCalledTimes(1);
          expect(documentoService.findOne).toHaveBeenCalledWith(1);
        });

        it('should return a document', () => {
          expect(result).toStrictEqual(expectedDocumento);
        });
      });
    });

    describe('case: failure', () => {
      describe('when a document is not found', () => {
        beforeEach(async () => {
          expectedDocumento = undefined;
          (documentoService.findOne as jest.Mock).mockResolvedValue(expectedDocumento);
          result = await controller.findOne(1);
        });

        it('should invoke DocumentoService.findOne()', () => {
          expect(documentoService.findOne).toHaveBeenCalledTimes(1);
          expect(documentoService.findOne).toHaveBeenCalledWith(1);
        });

        it('should return undefined', () => {
          expect(result).toStrictEqual(expectedDocumento);
        });
      });
    });
  });

  describe('create(documentoData)', () => {
    const { id, ...rest } = SampleTipoDocumentoPersonal;
    let body: CreateTipoDocumentoDTO;

    describe('case: success', () => {
      let expectedDocumento: TipoDocumentoPersonal;
      let result: TipoDocumentoPersonal;

      describe('when a document is created', () => {
        beforeEach(async () => {
          body = rest;
          expectedDocumento = SampleTipoDocumentoPersonal;

          (documentoService.create as jest.Mock).mockResolvedValue(expectedDocumento);
          result = await controller.create(body);
        });

        it('should invoke DocumentoService.create()', () => {
          expect(documentoService.create).toHaveBeenCalledTimes(1);
          expect(documentoService.create).toHaveBeenCalledWith(body);
        });

        it('should create a document', () => {
          expect(result).toStrictEqual(expectedDocumento);
        });
      });
    });

    describe('case: failure', () => {
      let expectedError: TypeError;

      describe('when a document is not created', () => {
        beforeEach(async () => {
          body = undefined;
          expectedError = new TypeError();
          jest.spyOn(controller, 'create').mockRejectedValue(expectedError);
        });

        it('should throw when the body is undefined', async () => {
          await expect(controller.create(body)).rejects.toThrow(TypeError);
        });

        it('should not create a document', () => {
          expect(documentoService.create).not.toHaveBeenCalled();
        });
      });
    });
  });

  describe('update(id, documentoData)', () => {
    let update: UpdateDocumentoDTO;

    describe('case: success', () => {
      let updateResult: UpdateResult | GlobalException;
      let result: UpdateResult | GlobalException;

      describe('when a document is updated', () => {
        beforeEach(async () => {
          update = {
            nombre: 'Test',
            archivo: 'Test'
          };
          updateResult = new UpdateResult();
          (documentoService.update as jest.Mock).mockResolvedValue(updateResult);
          result = await controller.update(1, update);
        });

        it('should invoke DocumentoService.update()', () => {
          expect(documentoService.update).toHaveBeenCalledTimes(1);
          expect(documentoService.update).toHaveBeenCalledWith(1, update);
        });

        it('should update a document', () => {
          expect(result).toStrictEqual(updateResult);
        });
      });
    });

    describe('case: failure', () => {
      let expectedError: TypeError;

      describe('when a document is not updated', () => {
        beforeEach(async () => {
          expectedError = new TypeError();
          update = undefined;
          jest.spyOn(controller, 'update').mockRejectedValue(expectedError);
        });

        it('should throw when the body is undefined', async () => {
          await expect(controller.update(1, update)).rejects.toThrow(TypeError);
        });

        it('should not update a document', () => {
          expect(documentoService.update).not.toHaveBeenCalled();
        });
      });
    });
  });

  describe('delete(id)', () => {
    let deleteResult: DeleteResult | GlobalException;
    let result: DeleteResult | GlobalException;

    describe('case: success', () => {
      describe('when a document is found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 1;
          (documentoService.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await controller.delete(1);
        });

        it('should invoke DocumentoService.delete()', () => {
          expect(documentoService.delete).toHaveBeenCalledTimes(1);
          expect(documentoService.delete).toHaveBeenCalledWith(1);
        });

        it('should delete a document', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });

    describe('case: failure', () => {
      describe('when a document is not found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 0;
          (documentoService.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await controller.delete(1);
        });

        it('should invoke DocumentoService.delete()', () => {
          expect(documentoService.delete).toHaveBeenCalledTimes(1);
          expect(documentoService.delete).toHaveBeenCalledWith(1);
        });

        it('should not delete a document', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });
  });
});
