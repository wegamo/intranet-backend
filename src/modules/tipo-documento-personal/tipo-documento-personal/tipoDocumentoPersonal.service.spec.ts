import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { TipoDocumentoPersonal } from '../../../models/tipo-documento-personal/tipoDocumentoPersonal.entity';
import { CreateTipoDocumentoDTO } from '../../../models/tipo-documento-personal/dto/create-tipo-documento-personal.dto';
import { UpdateDocumentoDTO } from '../../../models/tipo-documento-personal/dto/update-tipo-documento-personal.dto';
import { TipoDocumentoPersonalService } from './tipoDocumentoPersonal.service';
import { SampleTipoDocumentoPersonal } from '../../../../test/ObjectSamples';
import { GlobalException } from '../../exception/global-exception';

describe('DocumentoService', () => {
  let service: TipoDocumentoPersonalService;
  let RepositoryMock: jest.Mock;
  let repository: Repository<TipoDocumentoPersonal>;

  beforeEach(() => {
    RepositoryMock = jest.fn(() => ({
      find: jest.fn(),
      findOne: jest.fn(),
      save: jest.fn(),
      update: jest.fn(),
      delete: jest.fn()
    }));
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TipoDocumentoPersonalService,
        { provide: getRepositoryToken(TipoDocumentoPersonal), useValue: new RepositoryMock() }
      ]
    }).compile();

    repository = module.get(getRepositoryToken(TipoDocumentoPersonal));
    service = module.get<TipoDocumentoPersonalService>(TipoDocumentoPersonalService);
  });

  describe('findAll()', () => {
    let expectedDocumentos: TipoDocumentoPersonal[];
    let result: TipoDocumentoPersonal[];

    describe('case: success', () => {
      describe('when there are documents', () => {
        beforeEach(async () => {
          expectedDocumentos = [SampleTipoDocumentoPersonal];
          (repository.find as jest.Mock).mockResolvedValue(expectedDocumentos);
          result = await service.findAll();
        });

        it('should invoke repository.find()', () => {
          expect(repository.find).toHaveBeenCalledTimes(1);
        });

        it('should return an array of documents', () => {
          expect(result).toStrictEqual(expectedDocumentos);
        });
      });
    });

    describe('case: failure', () => {
      describe('when there are not documents', () => {
        beforeEach(async () => {
          expectedDocumentos = [];
          (repository.find as jest.Mock).mockResolvedValue(expectedDocumentos);
          result = await service.findAll();
        });

        it('should invoke repository.find()', () => {
          expect(repository.find).toHaveBeenCalledTimes(1);
        });

        it('should return an array empty', () => {
          expect(result).toStrictEqual(expectedDocumentos);
        });
      });
    });
  });

  describe('findOne(id)', () => {
    let expectedDocumento: TipoDocumentoPersonal;
    let result: TipoDocumentoPersonal;

    describe('case: success', () => {
      describe('when a document is found', () => {
        beforeEach(async () => {
          expectedDocumento = SampleTipoDocumentoPersonal;
          (repository.findOne as jest.Mock).mockResolvedValue(expectedDocumento);
          result = await service.findOne(1);
        });

        it('should invoke repository.findOne()', () => {
          expect(repository.findOne).toHaveBeenCalledTimes(1);
          expect(repository.findOne).toHaveBeenCalledWith(1);
        });

        it('should return a document', () => {
          expect(result).toStrictEqual(expectedDocumento);
        });
      });
    });

    describe('case: failure', () => {
      describe('when a document is not found', () => {
        beforeEach(async () => {
          expectedDocumento = undefined;
          (repository.findOne as jest.Mock).mockResolvedValue(expectedDocumento);
          result = await service.findOne(1);
        });

        it('should invoke repository.findOne()', () => {
          expect(repository.findOne).toHaveBeenCalledTimes(1);
          expect(repository.findOne).toHaveBeenCalledWith(1);
        });

        it('should return undefined', () => {
          expect(result).toStrictEqual(expectedDocumento);
        });
      });
    });
  });

  describe('create(documento)', () => {
    const { id, ...rest } = SampleTipoDocumentoPersonal;
    let body: CreateTipoDocumentoDTO;

    describe('case: success', () => {
      let expectedDocumento: TipoDocumentoPersonal;
      let result: TipoDocumentoPersonal;

      describe('when a document is created', () => {
        beforeEach(async () => {
          body = rest;
          expectedDocumento = SampleTipoDocumentoPersonal;
          (repository.save as jest.Mock).mockResolvedValue(expectedDocumento);
          result = await service.create(body);
        });

        it('should invoke repository.save()', () => {
          expect(repository.save).toHaveBeenCalledTimes(1);
          expect(repository.save).toHaveBeenCalledWith(body);
        });

        it('should create a document', () => {
          expect(result).toStrictEqual(expectedDocumento);
        });
      });
    });

    describe('case: failure', () => {
      let expectedError: TypeError;

      describe('when a document is not created', () => {
        beforeEach(async () => {
          body = undefined;
          expectedError = new TypeError();
          jest.spyOn(service, 'create').mockRejectedValue(expectedError);
        });

        it('should throw when the body is undefined', async () => {
          await expect(service.create(body)).rejects.toThrow(TypeError);
        });

        it('should not create a document', () => {
          expect(repository.save).not.toHaveBeenCalled();
        });
      });
    });
  });

  describe('update(id, documento)', () => {
    let update: UpdateDocumentoDTO;

    describe('case: success', () => {
      let updateResult: UpdateResult | GlobalException;
      let result: UpdateResult | GlobalException;

      describe('when a document is updated', () => {
        beforeEach(async () => {
          update = {
            nombre: 'Test'
          };
          updateResult = new UpdateResult();
          (repository.update as jest.Mock).mockResolvedValue(updateResult);
          result = await service.update(1, update);
        });

        it('should invoke repository.update()', () => {
          expect(repository.update).toHaveBeenCalledTimes(1);
          expect(repository.update).toHaveBeenCalledWith(1, update);
        });

        it('should update a document', () => {
          expect(result).toStrictEqual(updateResult);
        });
      });
    });

    describe('case: failure', () => {
      let expectedError: TypeError;

      describe('when a document is not updated', () => {
        beforeEach(async () => {
          expectedError = new TypeError();
          update = undefined;
          jest.spyOn(service, 'update').mockRejectedValue(expectedError);
        });

        it('should throw when the body is undefined', async () => {
          await expect(service.update(1, update)).rejects.toThrow(TypeError);
        });

        it('should not update a document', () => {
          expect(repository.update).not.toHaveBeenCalled();
        });
      });
    });
  });

  describe('delete(id)', () => {
    let deleteResult: DeleteResult | GlobalException;
    let result: DeleteResult | GlobalException;

    describe('case: success', () => {
      describe('when a document is found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 1;
          (repository.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await service.delete(1);
        });

        it('should invoke repository.delete()', () => {
          expect(repository.delete).toHaveBeenCalledTimes(1);
          expect(repository.delete).toHaveBeenCalledWith(1);
        });

        it('should delete a document', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });

    describe('case: failure', () => {
      describe('when a document is not found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 0;
          (repository.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await service.delete(1);
        });

        it('should invoke repository.delete()', () => {
          expect(repository.delete).toHaveBeenCalledTimes(1);
          expect(repository.delete).toHaveBeenCalledWith(1);
        });

        it('should not delete a document', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });
  });
});
