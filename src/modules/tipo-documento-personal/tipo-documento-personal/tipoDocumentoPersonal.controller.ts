import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { DeleteResult, UpdateResult } from 'typeorm';

import { TipoDocumentoPersonal } from '../../../models/tipo-documento-personal/tipoDocumentoPersonal.entity';
import { CreateTipoDocumentoDTO } from '../../../models/tipo-documento-personal/dto/create-tipo-documento-personal.dto';
import { UpdateDocumentoDTO } from '../../../models/tipo-documento-personal/dto/update-tipo-documento-personal.dto';

import { TipoDocumentoPersonalService } from './tipoDocumentoPersonal.service';
import { CreateDocumentoPersonalDTO } from '../../../models/documento-personal/dto/create-documento-personal.dto';
import { ApiUseTags } from '@nestjs/swagger';

@ApiUseTags('Tipo de documento personal')
@Controller('tipo-doc-personal')
export class TipoDocumentoPersonalController {
  constructor(private readonly documentoService: TipoDocumentoPersonalService) {}

  @Get('all')
  findAll(): Promise<TipoDocumentoPersonal[]> {
    return this.documentoService.findAll();
  }

  @Get('all/ordered-by-required')
  async findAllOrderedByRequired(): Promise<any> {
    const tipoDocumentoRequired = await this.documentoService.findAllRequired();
    const tipoDocumentoNotRequired = await this.documentoService.findAllNotRequired();
    const tiposDocumentos = {
      requeridos: tipoDocumentoRequired,
      noRequeridos: tipoDocumentoNotRequired
    };
    return tiposDocumentos;
  }

  @Get(':id')
  findOne(@Param('id') id: number): Promise<TipoDocumentoPersonal> {
    return this.documentoService.findOne(id);
  }

  @Post()
  async create(@Body() documentoData: CreateTipoDocumentoDTO): Promise<TipoDocumentoPersonal> {
    return this.documentoService.create(documentoData);
  }

  @Put(':id')
  async update(
    @Param('id') id: number,
    @Body() documentoData: UpdateDocumentoDTO
  ): Promise<UpdateResult> {
    return this.documentoService.update(id, documentoData);
  }

  @Delete(':id')
  async delete(@Param('id') id: number): Promise<DeleteResult> {
    return this.documentoService.delete(id);
  }
}
