import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-local';

import { Persona } from '../../models/persona/persona.entity';

import { AuthService } from './auth/auth.service';
import { sha1 } from 'object-hash';
@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly authService: AuthService) {
    super();
  }

  async validate(
    email: string,
    password: string
  ): Promise<UnauthorizedException | Partial<Persona>> {
    const encryptedPass = sha1(password);
    return await this.authService.validateUser(email, encryptedPass);
  }
}
