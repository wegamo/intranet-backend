import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { ConfigService } from '../../config/config/config.service';
import { AuthService } from './auth/auth.service';

@Injectable()
export class RecoverStrategy extends PassportStrategy(Strategy, 'recover') {
  constructor(private readonly configService: ConfigService, private authService: AuthService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: configService.get('SECRET')
    });
  }

  async validate({ email }: any) {
    console.log(email);

    const user = await this.authService.validateUserTokenByEmail(email);
    if (!user) {
      throw new UnauthorizedException();
    }
    return user;
  }
}
