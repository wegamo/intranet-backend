import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { ConfigModule } from './../../config/config.module';
import configService, { ConfigService } from './../../config/config/config.service';

import { PersonaModule } from '../persona/persona.module';

import { AuthController } from './auth/auth.controller';
import { AuthService } from './auth/auth.service';
import { LocalStrategy } from './local.strategy';
import { PersonaService } from '../persona/persona/persona.service';
import { JwtStrategy } from './jwt.strategy';
import { RecoverStrategy } from './recover.strategy';

@Module({
  imports: [
    PersonaModule,
    PassportModule,
    ConfigModule,
    JwtModule.register({
      secret: configService.get('SECRET'),
      signOptions: { expiresIn: '1h' }
    })
  ],
  providers: [
    AuthService,
    JwtStrategy,
    LocalStrategy,
    RecoverStrategy,
    PersonaService,
    {
      provide: ConfigService,
      useValue: configService
    }
  ],
  exports: [AuthService],
  controllers: [AuthController]
})
export class AuthModule {}
