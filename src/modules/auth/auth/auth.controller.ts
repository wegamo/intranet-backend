import { Body, Controller, HttpStatus, Post, Req, Res, UseGuards, Request } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Response } from 'express';
import { ConfigService } from '../../../config/config/config.service';
import { sendEmail } from '../../../helpers/sendEmail';
import { Persona } from '../../../models/persona/persona.entity';
import { sha1 } from 'object-hash';

import { AuthService } from './auth.service';
import { PersonaService } from '../../persona/persona/persona.service';
import { ApiUseTags } from '@nestjs/swagger';

@ApiUseTags('Auth')
@Controller('auth')
export class AuthController {
  private readonly _tokenName: string;

  constructor(
    private readonly _authService: AuthService,
    private readonly _configService: ConfigService,
    private readonly personaService: PersonaService
  ) {
    this._tokenName = this._configService.get('TOKEN_NAME');
  }

  @UseGuards(AuthGuard('local'))
  @Post('login')
  async login(@Req() req: Request & { user: any }, @Res() res: Response) {
    const result = await this._authService.generateToken(req.user);

    res.status(HttpStatus.OK).send({
      user: result.user,
      token: result.token
    });
  }

  @UseGuards(AuthGuard('jwt'))
  @Post('logout')
  async logout(@Res() res: Response) {
    res
      .status(HttpStatus.NO_CONTENT)
      .clearCookie(this._tokenName)
      .send();
  }

  @Post('recover-password')
  async recoverPassword(@Body() body: { email: string; isClient: boolean }, @Res() res: Response) {
    if (body.email) {
      const foundUser = await this.personaService.findByEmail(body.email);
      if (foundUser) {
        const result = await this._authService.generateTokenByEmail(body.email);
        sendEmail(
          body.email,
          'Andromeda: Recupera tu Contraseña',
          '',
          `Cambia tu contraseña en el siguiente link: <a href="${
            body.isClient
              ? this._configService.get('FRONTEND_CLIENT_URL')
              : this._configService.get('FRONTEND_URL')
          }/reset-password?token=${result}">enlace</a>"`
        );
      }
      res.status(HttpStatus.OK).send({ message: 'Recover password email sent' });
    }
  }

  @Post('reset-password')
  @UseGuards(AuthGuard('recover'))
  async resetPassword(
    @Request() { user }: { user: Persona },
    @Body() body: { newPassword: string },
    @Res() res: Response
  ) {
    const passwordEncripted = sha1(body.newPassword);
    user.password = passwordEncripted;
    const response = await this.personaService.update(user);
    delete response.password;
    res.status(HttpStatus.OK).send(response);
  }
}
