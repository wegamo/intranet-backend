import { HttpStatus } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { createRequest, createResponse } from 'node-mocks-http';
import { Persona } from '../../../models/persona/persona.entity';

import { ConfigService } from '../../../config/config/config.service';
import { AuthController } from '../../../modules/auth/auth/auth.controller';
import { AuthService } from '../../../modules/auth/auth/auth.service';

import { SamplePersona } from './../../../../test/ObjectSamples';

jest.mock('fs', () => ({
  readFileSync: jest.fn()
}));
jest.mock('dotenv', () => ({
  parse: jest.fn()
}));

describe('Auth Controller', () => {
  const tokenKey = 'TOKEN_NAME';
  const tokenName = 'token-name';
  let controller: AuthController;
  let AuthServiceMock: jest.Mock<Partial<AuthService>>;
  let ConfigServiceMock: jest.Mock<Partial<ConfigService>>;
  let authService: AuthService;
  let configService: ConfigService;
  
  beforeEach(() => {
    AuthServiceMock = jest.fn<Partial<AuthService>, AuthService[]>(() => ({
      //login: jest.fn()
      generateToken:jest.fn(),
    }));
    ConfigServiceMock = jest.fn<Partial<ConfigService>, ConfigService[]>(() => ({
      get: jest.fn().mockReturnValue(tokenName)
    }));
  });

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      controllers: [AuthController],
      providers: [
        { provide: AuthService, useValue: new AuthServiceMock() },
        { provide: ConfigService, useValue: new ConfigServiceMock() }
      ]
    }).compile();

    authService = module.get<AuthService>(AuthService);
    configService = module.get<ConfigService>(ConfigService);
    controller = module.get<AuthController>(AuthController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
  
  describe('login(req, res)', () => {
    let req: import('express').Request & { user: Persona & App.Auth.Request };
    let res: import('express').Response;
    let user: Persona;
    let loginResponse: App.Auth.LoginResponse;
    let accessToken: string;

    beforeEach(() => {
      req = createRequest();
      res = createResponse();
      user = SamplePersona;
      req.user = { ...user, userId: user.id, username: user.primerNombre };
      accessToken = 'abc123';
      loginResponse = { access_token: accessToken, user };
      (authService.generateToken as jest.Mock).mockResolvedValue(loginResponse);
      jest.spyOn(res, 'cookie');
      jest.spyOn(res, 'status');
      jest.spyOn(res, 'send');
    });

    beforeEach(async () => {
      await controller.login(req, res);
    });

    it('should invoke ConfigService.get(tokenKey)', () => {
      expect(configService.get).toHaveBeenCalledTimes(1);
      expect(configService.get).toHaveBeenCalledWith(tokenKey);
    });

    it('should invoke AuthService.login(user)', () => {
      expect(authService.generateToken).toHaveBeenCalledTimes(1);
      expect(authService.generateToken).toHaveBeenCalledWith(req.user);
    });

    it('should create a new cookie', () => {
      const { NODE_ENV }: NodeJS.ProcessEnv = process.env;
      const cookieOptions: import('express').CookieOptions = {
        path: '/',
        httpOnly: false,
        signed: true,
        secure: NODE_ENV === 'prod',
        domain: 'localhost',
        maxAge: 7200000
      };

      expect(res.status).toHaveBeenCalledTimes(1);
      //expect(res.cookie).toHaveBeenCalledWith(tokenName, accessToken, cookieOptions);
    });

    it('should return HTTP status: 200 OK', () => {
      expect(res.status).toHaveBeenCalledTimes(1);
      expect(res.status).toHaveBeenCalledWith(HttpStatus.OK);
    });

    it('should send the authenticated user', () => {
      const body: { user: Persona } = { user:SamplePersona };

      expect(res.send).toHaveBeenCalledTimes(1);
      expect(res.send).toHaveBeenCalledWith(body);
    });
  });
  
  describe('logout()', () => {
    let res: import('express').Response;

    beforeEach(() => {
      res = createResponse();
      jest.spyOn(res, 'status');
      jest.spyOn(res, 'clearCookie');
      jest.spyOn(res, 'send');
    });

    beforeEach(async () => {
      await controller.logout(res);
    });

    it('should return HTTP status: 204 NO CONTENT', () => {
      expect(res.status).toHaveBeenCalledTimes(1);
      expect(res.status).toHaveBeenCalledWith(HttpStatus.NO_CONTENT);
    });

    it('should clear the authentication cookie', () => {
      expect(res.clearCookie).toHaveBeenCalledTimes(1);
      expect(res.clearCookie).toHaveBeenCalledWith(tokenName);
    });

    it('should send the response', () => {
      expect(res.send).toHaveBeenCalledTimes(1);
      expect(res.send).toHaveBeenCalledWith();
    });
  });
});
