import { HttpException, HttpStatus, Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

import { Persona } from '../../../models/persona/persona.entity';
import { PersonaService } from '../../persona/persona/persona.service';

import { PersonaEstatus } from '../../../enums/persona-estatus.enum';
@Injectable()
export class AuthService {
  constructor(
    private readonly personaService: PersonaService,
    private readonly jwtService: JwtService
  ) {}

  async userExists(email: string): Promise<boolean> {
    return !!this.personaService.findByEmail(email);
  }

  async validateUser(email: string, passwordReceived: string): Promise<Persona> {
    const user = await this.personaService.findByEmailAndPassword(email, passwordReceived);
    if (user) {
      if (user.estatus !== PersonaEstatus.ACTIVO) {
        throw new HttpException(
          'La cuenta de este usuario se ha desactivado',
          HttpStatus.FORBIDDEN
        );
      }
      return user;
    } else {
      throw new HttpException('Clave o correo inválidas', HttpStatus.NOT_FOUND);
    }
  }

  async validateUserToken(email: string, cedula: string): Promise<Partial<Persona>> {
    const user = await this.personaService.findPersonaByEmailAndCedula(email, cedula);

    if (!user) {
      throw new UnauthorizedException('Usuario no existe');
    }
    return user;
  }

  async validateUserTokenByEmail(email: string): Promise<Partial<Persona>> {
    const user = await this.personaService.findPersonaByEmail(email);

    if (!user) {
      throw new UnauthorizedException('Usuario no existe');
    }
    return user;
  }

  async generateToken(user: Persona): Promise<any> {
    const payload: any = { email: user.email, cedula: user.cedula };

    return {
      token: this.jwtService.sign(payload),
      user
    };
  }

  async generateTokenByEmail(email: string) {
    const payload: any = { email };
    return this.jwtService.sign(payload);
  }
}
