import { UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Test, TestingModule } from '@nestjs/testing';

import { Persona } from '../../../models/Persona/Persona.entity';
import { PersonaService } from '../../persona/persona/persona.service';

import { AuthService } from './auth.service';

import { SamplePersona } from './../../../../test/ObjectSamples';

describe('AuthService', () => {
  
  let service: AuthService;
  let PersonaServiceMock: jest.Mock<Partial<PersonaService>>;
  let JwtServiceMock: jest.Mock<Partial<JwtService>>;
  let personaService: PersonaService;
  let jwtService: JwtService;

  beforeEach(() => {
    PersonaServiceMock = jest.fn<Partial<PersonaService>, PersonaService[]>(() => ({
      findAll: jest.fn(),
      findOneCI: jest.fn(),
      create: jest.fn(),
      delete: jest.fn(),
      update: jest.fn(),
      findPersonaByEmail: jest.fn(),
    }));
    JwtServiceMock = jest.fn<Partial<JwtService>, JwtService[]>(() => ({
      sign: jest.fn()
    }));
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthService,
        { provide: PersonaService, useValue: new PersonaServiceMock() },
        { provide: JwtService, useValue: new JwtServiceMock() }
      ]
    }).compile();

    personaService = module.get<PersonaService>(PersonaService);
    jwtService = module.get<JwtService>(JwtService);
    service = module.get<AuthService>(AuthService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('validateUser(cedula, password)', () => {
    let cedula: string;
    let password: string;
    let user: Persona;
    let result: Partial<Persona> | null;

    describe('case: success', () => {
      describe('when the user status is active', () => {
        beforeEach(async () => {
          cedula = SamplePersona.cedula;
          password = SamplePersona.password;
          user = SamplePersona;
          (personaService.findOneCI as jest.Mock).mockResolvedValue(user);
          result = await service.validateUser(cedula, password);
        });

        it('should invoke PersonaService.findOneCI(cedula, password)', () => {
          expect(personaService.findOneCI).toHaveBeenCalledTimes(1);
          expect(personaService.findOneCI).toHaveBeenCalledWith(cedula);
        });

        it('should return the user found (without the password)', () => {
          const { password: excludedProp, ...expectedUser }: Persona = user;

          expect(result).toStrictEqual(expectedUser);
        });
      });
    });

    describe('case: failure', () => {
      describe('when the user does not exist', () => {
        beforeEach(async () => {
          cedula = SamplePersona.cedula;
          password = SamplePersona.password;
          user = undefined;
          (personaService.findOneCI as jest.Mock).mockResolvedValue(user);
        });

        it('should throw when user is undefined', async () => {
          await expect(service.validateUser(cedula, password)).rejects.toThrow(
            UnauthorizedException
          );
        });
      });

      describe('when the password is incorrect', () => {
        beforeEach(async () => {
          cedula = SamplePersona.cedula;
          password = 'Test 2';
          user = SamplePersona;
          (personaService.findOneCI as jest.Mock).mockResolvedValue(user);
        });

        it('should throw when the password is incorrect', async () => {
          await expect(service.validateUser(cedula, password)).rejects.toThrow(
            UnauthorizedException
          );
        });
      });
    });
  });

  describe('generateToken(user) / New Login', () => {
    let user: Partial<Persona>;
    let accessToken: string;
    let result: App.Auth.LoginResponse;

    beforeEach(async () => {
      accessToken = 'abc123';
      user = { cedula: SamplePersona.cedula, email: SamplePersona.email };
      (jwtService.sign as jest.Mock).mockReturnValue(accessToken);
      result = await service.generateToken(user as Persona);
    });

    it('should invoke JwtService.sign(payload)', () => {
      const payload: App.JWT.JWTPayload = { cedula: SamplePersona.cedula, email: SamplePersona.email };

      expect(jwtService.sign).toHaveBeenCalledTimes(1);
      expect(jwtService.sign).toHaveBeenCalledWith(payload);
    });

    it('should assign a token to the user', () => {
      const expectedResult: App.Auth.LoginResponse = { access_token: accessToken, user };

      expect(result).toStrictEqual(expectedResult);
    });
  });
});
