import { Module } from '@nestjs/common';
import { MotivoAusenciaService } from './motivo-ausencia/motivo-ausencia.service';
import { MotivoAusenciaController } from './motivo-ausencia/motivo-ausencia.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MotivoAusencia } from '../../models/motivo-ausencia/motivoAusencia.entity';

@Module({
  imports: [TypeOrmModule.forFeature([MotivoAusencia])],
  providers: [MotivoAusenciaService],
  controllers: [MotivoAusenciaController],
  exports: [MotivoAusenciaService]
})
export class MotivoAusenciaModule {}
