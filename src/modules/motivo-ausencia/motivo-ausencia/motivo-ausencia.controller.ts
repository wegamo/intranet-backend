import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { ApiUseTags } from '@nestjs/swagger';
import { MotivoAusencia } from '../../../models/motivo-ausencia/motivoAusencia.entity';
import { MotivoAusenciaService } from './motivo-ausencia.service';

@ApiUseTags('Motivo de ausencia')
@Controller('motivo-ausencia')
export class MotivoAusenciaController {
  constructor(private readonly motivoAusenciaService: MotivoAusenciaService) {}

  @Get('all')
  findAll(): Promise<MotivoAusencia[]> {
    return this.motivoAusenciaService.findAll();
  }
}
