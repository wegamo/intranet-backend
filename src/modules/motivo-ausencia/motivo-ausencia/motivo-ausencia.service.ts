import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { MotivoAusencia } from '../../../models/motivo-ausencia/motivoAusencia.entity';

@Injectable()
export class MotivoAusenciaService {
  constructor(
    @InjectRepository(MotivoAusencia)
    private readonly motivoAusenciaRepository: Repository<MotivoAusencia>
  ) {}

  findAll(): Promise<MotivoAusencia[]> {
    return this.motivoAusenciaRepository.find();
  }

  findBySolicitud(idSolicitud: number): Promise<MotivoAusencia[]> {
    return this.motivoAusenciaRepository
      .createQueryBuilder('motivo')
      .innerJoin('motivo.ausencias', 'ausencia')
      .where('ausencia.solicitud = :idSolicitud', { idSolicitud })
      .getMany();
  }
}
