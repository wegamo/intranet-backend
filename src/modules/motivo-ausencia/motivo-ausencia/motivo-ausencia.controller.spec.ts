import { Test, TestingModule } from '@nestjs/testing';
import { MotivoAusenciaController } from './motivo-ausencia.controller';
import { MotivoAusenciaService } from './motivo-ausencia.service';

describe('MotivoAusencia Controller', () => {
  let controller: MotivoAusenciaController;
  let MotivoAusenciaServiceMock: jest.Mock<Partial<MotivoAusenciaService>>;
  let motivoAusenciaService: MotivoAusenciaService;

  beforeEach(() => {
    MotivoAusenciaServiceMock = jest.fn<Partial<MotivoAusenciaService>, MotivoAusenciaService[]>(() => ({

    }));
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MotivoAusenciaController],
      providers: [{ provide: MotivoAusenciaService, useValue: new MotivoAusenciaServiceMock() }]
    }).compile();

    motivoAusenciaService = module.get<MotivoAusenciaService>(MotivoAusenciaService);
    controller = module.get<MotivoAusenciaController>(MotivoAusenciaController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
