import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { MotivoAusencia } from '../../../models/motivo-ausencia/motivoAusencia.entity';
import { Repository } from 'typeorm';
import { MotivoAusenciaService } from './motivo-ausencia.service';

describe('MotivoAusenciaService', () => {
  let service: MotivoAusenciaService;
  let RepositoryMock: jest.Mock;
  let repository: Repository<MotivoAusencia>;

  beforeEach(() => {
    RepositoryMock = jest.fn(() => ({
      find: jest.fn(),
      findOne: jest.fn(),
      save: jest.fn(),
      update: jest.fn(),
      delete: jest.fn()
    }));
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        MotivoAusenciaService,
        { provide: getRepositoryToken(MotivoAusencia), useValue: new RepositoryMock() },
      ]
    }).compile();

    repository = module.get(getRepositoryToken(MotivoAusencia));
    service = module.get<MotivoAusenciaService>(MotivoAusenciaService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
