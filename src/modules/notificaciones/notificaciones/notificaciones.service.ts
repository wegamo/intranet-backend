import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Notificacion } from '../../../models/notificacion/notificacion.entity';
import { DeleteResult, Repository } from 'typeorm';
import { CreateNotificacionDTO } from '../../../models/notificacion/dto/create-notificacion.dto';
import { TipoNotificacion } from '../../../enums/tipo-notificacion.enum';

@Injectable()
export class NotificacionesService {
  constructor(
    @InjectRepository(Notificacion)
    private readonly notificacionesRepository: Repository<Notificacion>
  ) {}

  findAll(): Promise<Notificacion[]> {
    return this.notificacionesRepository.find({
      order: {
        fechaPublicacion: 'DESC'
      }
    });
  }

  findAllByPersona(personaId: number, tipo: TipoNotificacion): Promise<Notificacion[]> {
    return this.notificacionesRepository.find({
      where: {
        persona: personaId,
        tipo
      },
      order: {
        fechaPublicacion: 'DESC'
      }
    });
  }

  create(notificacion: CreateNotificacionDTO): Promise<Notificacion> {
    return this.notificacionesRepository.save(notificacion);
  }

  delete(notificacionId: number): Promise<DeleteResult> {
    return this.notificacionesRepository.delete(notificacionId);
  }
}
