import { Controller, Delete, Get, Param, Request, UseFilters, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { DeleteResult } from 'typeorm';
import { NotificacionesService } from './notificaciones.service';
import { GlobalExceptionFilter } from '../../exception/global-exception.filter';
import { Notificacion } from '../../../models/notificacion/notificacion.entity';
import { Persona } from '../../../models/persona/persona.entity';
import { TipoNotificacion } from '../../../enums/tipo-notificacion.enum';
import { ApiBearerAuth, ApiUseTags } from '@nestjs/swagger';

@ApiBearerAuth()
@ApiUseTags('Notificaciones')
@Controller('notificaciones')
export class NotificacionesController {
  constructor(private readonly notificacionesService: NotificacionesService) {}

  @UseGuards(AuthGuard('jwt'))
  @Get('aplicacion/:aplicacionId')
  getAllByPersona(
    @Request() { user }: { user: Persona },
    @Param('aplicacionId') aplicacionId: TipoNotificacion
  ): Promise<Notificacion[]> {
    return this.notificacionesService.findAllByPersona(user.id, aplicacionId);
  }

  @UseGuards(AuthGuard('jwt'))
  @Delete('/:notificacionId')
  deleteNotificacion(@Param('notificacionId') notificacionId: number): Promise<DeleteResult> {
    return this.notificacionesService.delete(notificacionId);
  }
}
