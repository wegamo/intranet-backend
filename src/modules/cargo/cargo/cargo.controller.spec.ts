import { Test, TestingModule } from '@nestjs/testing';
import { CargoController } from './cargo.controller';
import { CargoService } from './cargo.service';
import { Cargo } from '../../../models/cargo/cargo.entity';
import { CreateCargoDTO } from '../../../models/cargo/dto/create-cargo.dto';
import { UpdateCargoDTO } from '../../../models/cargo/dto/update-cargo.dto';
import { UpdateResult, DeleteResult } from 'typeorm';

import { SampleCargo } from './../../../../test/ObjectSamples';
import { GlobalException } from './../../exception/global-exception';

describe('Cargo Controller', () => {
  let controller: CargoController;
  let cargoService: CargoService;
  let CargoServiceMock: jest.Mock<Partial<CargoService>>;

  beforeEach(() => {
    CargoServiceMock = jest.fn<Partial<CargoService>, CargoService[]>(() => ({
      findAll: jest.fn(),
      findOne: jest.fn(),
      findAllId: jest.fn(),
      create: jest.fn(),
      delete: jest.fn(),
      update: jest.fn()
    }));
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CargoController],
      providers: [{ provide: CargoService, useValue: new CargoServiceMock() }]
    }).compile();

    cargoService = module.get<CargoService>(CargoService);
    controller = module.get<CargoController>(CargoController);
  });

  //Test findAll()
  describe('findAll()', () => {
    let expectedCargos: Cargo[];
    let result: Cargo[];

    describe('case : success', () => {
      describe('when there are roles', () => {
        beforeEach(async () => {
          expectedCargos = [SampleCargo];
          (cargoService.findAll as jest.Mock).mockResolvedValue(expectedCargos);
          result = await controller.findAll();
        });
        it('should invoke cargoService.findAll()', () => {
          expect(cargoService.findAll).toHaveBeenCalledTimes(1);
        });
        it('should return an array of roles', () => {
          expect(result).toStrictEqual(expectedCargos);
        });
      });
    });

    describe('case : failure', () => {
      describe('when there are no roles', () => {
        beforeEach(async () => {
          expectedCargos = [];
          (cargoService.findAll as jest.Mock).mockResolvedValue(expectedCargos);
          result = await controller.findAll();
        });

        it('should invoke cargoService.findAll()', () => {
          expect(cargoService.findAll).toHaveBeenCalledTimes(1);
        });

        it('should return an array empty', () => {
          expect(result).toStrictEqual(expectedCargos);
        });
      });
    });
  });

  //Test findAllId()
  describe('findAllId()', () => {
    let expectedCargos: Cargo[];
    let result: Cargo[];

    describe('case : success', () => {
      describe('when there are cargos', () => {
        beforeEach(async () => {
          expectedCargos = [SampleCargo];
          (cargoService.findAllId as jest.Mock).mockResolvedValue(expectedCargos);
          result = await controller.findAllId();
        });
        it('should invoke cargoService.findAllId()', () => {
          expect(cargoService.findAllId).toHaveBeenCalledTimes(1);
        });
        it('should return an array of cargos', () => {
          expect(result).toStrictEqual(expectedCargos);
        });
      });
    });

    describe('case : failure', () => {
      describe('when there are no cargos', () => {
        beforeEach(async () => {
          expectedCargos = [];
          (cargoService.findAllId as jest.Mock).mockResolvedValue(expectedCargos);
          result = await controller.findAllId();
        });

        it('should invoke cargoService.findAllId()', () => {
          expect(cargoService.findAllId).toHaveBeenCalledTimes(1);
        });

        it('should return an array empty', () => {
          expect(result).toStrictEqual(expectedCargos);
        });
      });
    });
  });

  //Test findOne()
  describe('findOne(id)', () => {
    let expectedCargo: Cargo;
    let result: Cargo;

    describe('case : success', () => {
      describe('when an role is found', () => {
        beforeEach(async () => {
          expectedCargo = SampleCargo;
          (cargoService.findOne as jest.Mock).mockResolvedValue(expectedCargo);
          result = await controller.findOne(1);
        });
        it('should invoke cargoService.findOne()', () => {
          expect(cargoService.findOne).toHaveBeenCalledTimes(1);
          expect(cargoService.findOne).toHaveBeenCalledWith(1);
        });
        it('should return an role', () => {
          expect(result).toStrictEqual(expectedCargo);
        });
      });
    });

    describe('case : failure', () => {
      describe('when an role is not found', () => {
        beforeEach(async () => {
          expectedCargo = undefined;
          (cargoService.findOne as jest.Mock).mockResolvedValue(expectedCargo);
          result = await controller.findOne(1);
        });
        it('should invoke cargoService.findOne()', () => {
          expect(cargoService.findOne).toHaveBeenCalledTimes(1);
          expect(cargoService.findOne).toHaveBeenCalledWith(1);
        });
        it('should return undefined', () => {
          expect(result).toStrictEqual(expectedCargo);
        });
      });
    });
  });
  //Test create()
  describe('create(CreateCargoDTO)', () => {
    const { id, ...rest } = SampleCargo;
    let body: CreateCargoDTO;

    describe('case : success', () => {
      let expectedCargo: Cargo;
      let result: Cargo;

      describe('when a role is created', () => {
        beforeEach(async () => {
          body = rest;
          expectedCargo = SampleCargo;
          (cargoService.create as jest.Mock).mockResolvedValue(expectedCargo);
          result = await controller.create(body);
        });
        it('should invoke cargoService.create()', () => {
          expect(cargoService.create).toHaveBeenCalledTimes(1);
          expect(cargoService.create).toHaveBeenCalledWith(body);
        });
        it('should create a role', () => {
          expect(result).toStrictEqual(expectedCargo);
        });
      });
    });

    describe('case : failure', () => {
      let expectedError: TypeError;
      describe('when a role is not created', () => {
        beforeEach(async () => {
          body = undefined;
          expectedError = new TypeError();
          jest.spyOn(controller, 'create').mockRejectedValue(expectedError);
        });
        it('should throw when the body is undefined', async () => {
          await expect(controller.create(body)).rejects.toThrow(TypeError);
        });
        it('should not create a role', () => {
          expect(cargoService.create).not.toHaveBeenCalled();
        });
      });
    });
  });
  //Test update()
  describe('update(id, UpdateCargoDTO)', () => {
    let update: UpdateCargoDTO;

    describe('case : success', () => {
      let updateResult: UpdateResult | GlobalException;
      let result: UpdateResult | GlobalException;

      describe('when an role is found', () => {
        beforeEach(async () => {
          update = {
            nombre: 'testmod',
            descripcion: 'testdesc'
          };
          updateResult = new UpdateResult();
          (cargoService.update as jest.Mock).mockResolvedValue(updateResult);
          result = await controller.update(1, update);
        });
        it('should invoke CargoService.update()', () => {
          expect(cargoService.update).toHaveBeenCalledTimes(1);
          expect(cargoService.update).toHaveBeenCalledWith(1, update);
        });
        it('should update an role', () => {
          expect(result).toStrictEqual(updateResult);
        });
      });
    });

    describe('case : failure', () => {
      let expectedError: TypeError;

      describe('when an role is not found', () => {
        beforeEach(async () => {
          expectedError = new TypeError();
          update = undefined;
          jest.spyOn(controller, 'update').mockRejectedValue(expectedError);
        });
        it('should invoke CargoService.update()', async () => {
          await expect(controller.update(1, update)).rejects.toThrow(TypeError);
        });
        it('should not update an role', () => {
          expect(cargoService.update).not.toHaveBeenCalled();
        });
      });
    });
  });
  //Test delete()
  describe('delete(id)', () => {
    let deleteResult: DeleteResult | GlobalException;
    let result: DeleteResult | GlobalException;

    describe('case : success', () => {
      describe('when an role is found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 1;
          (cargoService.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await controller.delete(1);
        });
        it('should invoke CargoService.delete()', () => {
          expect(cargoService.delete).toHaveBeenCalledTimes(1);
          expect(cargoService.delete).toHaveBeenCalledWith(1);
        });
        it('should delete an role', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });

    describe('case : failure', () => {
      let expectedError: TypeError;

      describe('when an role is not found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 0;
          (cargoService.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await controller.delete(1);
        });

        it('should invoke CargoService.delete()', () => {
          expect(cargoService.delete).toHaveBeenCalledTimes(1);
          expect(cargoService.delete).toHaveBeenCalledWith(1);
        });

        it('should not delete an role', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });
  });
});
