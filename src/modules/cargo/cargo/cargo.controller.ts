import { Body, Controller, Delete, Get, Param, Post, Put, UseFilters } from '@nestjs/common';
import { DeleteResult, UpdateResult } from 'typeorm';

import { Cargo } from '../../../models/cargo/cargo.entity';
import { CreateCargoDTO } from '../../../models/cargo/dto/create-cargo.dto';
import { UpdateCargoDTO } from '../../../models/cargo/dto/update-cargo.dto';

import { CargoService } from './cargo.service';
import { GlobalException } from './../../exception/global-exception';
import { GlobalExceptionFilter } from './../../exception/global-exception.filter';
import { ApiBearerAuth, ApiUseTags } from '@nestjs/swagger';
@ApiBearerAuth()
@ApiUseTags('Cargo')
@Controller('cargo')
export class CargoController {
  constructor(private readonly cargoService: CargoService) {}

  @Get()
  findAll(): Promise<Cargo[]> {
    return this.cargoService.findAll();
  }

  @Get('id')
  findAllId(): Promise<Cargo[]> {
    return this.cargoService.findAllId();
  }

  @Get(':id')
  findOne(@Param('id') id: number): Promise<Cargo> {
    return this.cargoService.findOne(id);
  }

  @Get('persona/:id')
  findCurrentPersonaByCargo(@Param('id') id: number): Promise<Cargo> {
    return this.cargoService.findCurrentPersonaByCargoId(id);
  }

  @Post()
  create(@Body() cargoData: CreateCargoDTO): Promise<Cargo> {
    return this.cargoService.create(cargoData);
  }

  @Put(':id')
  update(
    @Param('id') id: number,
    @Body() cargoData: UpdateCargoDTO
  ): Promise<UpdateResult | GlobalException> {
    return this.cargoService.update(id, cargoData);
  }

  @Delete(':id')
  delete(@Param('id') id: number): Promise<DeleteResult | GlobalException> {
    return this.cargoService.delete(id);
  }
}
