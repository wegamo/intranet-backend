import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, IsNull, Repository, UpdateResult } from 'typeorm';
import { isNull } from 'util';

import { Cargo } from '../../../models/cargo/cargo.entity';
import { CreateCargoDTO } from '../../../models/cargo/dto/create-cargo.dto';
import { UpdateCargoDTO } from '../../../models/cargo/dto/update-cargo.dto';
import { GlobalException } from './../../exception/global-exception';

@Injectable()
export class CargoService {
  constructor(@InjectRepository(Cargo) private readonly cargoRepository: Repository<Cargo>) {}

  async findAll(): Promise<Cargo[]> {
    return await this.cargoRepository.find();
  }

  async findAllId(): Promise<Cargo[]> {
    const cargos: Cargo[] = await this.cargoRepository
      .createQueryBuilder()
      .select('id, nombre')
      .getRawMany();

    return cargos;
  }

  async findOne(id: number): Promise<Cargo> {
    return await this.cargoRepository.findOne(id);
  }
  // ARREGLAR
  findCurrentPersonaByCargoId(id: number): Promise<Cargo> {
    /*
    return this.cargoRepository.findOne({
      relations: ['histCargos', 'histCargos.persona'],
      where: { id, histCargos: { fechaFin: IsNull() } }
    });
*/
    return this.cargoRepository
      .createQueryBuilder('cargo')
      .innerJoinAndSelect('cargo.histCargos', 'histCargos')
      .innerJoinAndSelect('histCargos.persona', 'persona')
      .where('cargo.id = :id AND histCargos.fechaFin IS NULL', { id })
      .getOne();
  }

  async create(cargo: CreateCargoDTO): Promise<Cargo> {
    return await this.cargoRepository.save(cargo);
  }

  async update(id: number, cargo: UpdateCargoDTO): Promise<UpdateResult | GlobalException> {
    try {
    } catch (Err) {}
    return await this.cargoRepository.update(id, cargo);
  }

  async delete(id: number): Promise<DeleteResult | GlobalException> {
    try {
      let DelRes: DeleteResult = await this.cargoRepository.delete(id);
      return DelRes;
    } catch (Err) {
      console.log(Err.message + '\n' + Err.query + '\n');
      if (Err.message.match('viola la llave foránea')) {
        throw new GlobalException(
          'Informacion vinculada, se debe eliminar la informacion dependiente primero',
          1
        );
      }
    }
    return await this.cargoRepository.delete(id);
  }
}
