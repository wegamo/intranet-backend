import { Test, TestingModule } from '@nestjs/testing';
import { CargoService } from './cargo.service';
import { Repository, UpdateResult, DeleteResult } from 'typeorm';
import { Cargo } from '../../../models/cargo/cargo.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { CreateCargoDTO } from '../../../models/cargo/dto/create-cargo.dto';
import { UpdateCargoDTO } from '../../../models/cargo/dto/update-cargo.dto';

import { SampleCargo } from './../../../../test/ObjectSamples';
import { CargoInt } from './../../dto_response/Cargo_Payload';
import { GlobalException } from './../../exception/global-exception';

describe('CargoService', () => {
  let service: CargoService;
  let RepositoryMock: jest.Mock;
  let repository: Repository<Cargo>;
  let getRawManySpy: jest.Mock;

  beforeEach(() => {
    const selectSpy = jest.fn().mockReturnThis();
    getRawManySpy = jest.fn().mockReturnThis();

    RepositoryMock = jest.fn(() => ({
      find: jest.fn(),
      findOne: jest.fn(),
      createQueryBuilder: jest.fn(() => ({
        select: selectSpy,
        getRawMany: getRawManySpy
      })),
      save: jest.fn(),
      update: jest.fn(),
      delete: jest.fn()
    }));
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CargoService,
        { provide: getRepositoryToken(Cargo), useValue: new RepositoryMock() }
      ]
    }).compile();

    repository = module.get(getRepositoryToken(Cargo));
    service = module.get<CargoService>(CargoService);
  });

  describe('findAll()', () => {
    let expectedCargos: Cargo[];
    let result: Cargo[];

    describe('case : success', () => {
      describe('', () => {
        beforeEach(async () => {
          expectedCargos = [SampleCargo];
          (repository.find as jest.Mock).mockResolvedValue(expectedCargos);
          result = await service.findAll();
        });

        it('should invoke repository.find()', () => {
          expect(repository.find).toHaveBeenCalledTimes(1);
        });

        it('should return an array of roles', () => {
          expect(result).toStrictEqual(expectedCargos);
        });
      });
    });

    describe('case : failure', () => {
      describe('when there are not roles', () => {
        beforeEach(async () => {
          expectedCargos = [];
          (repository.find as jest.Mock).mockResolvedValue(expectedCargos);
          result = await service.findAll();
        });

        it('should invoke repository.find()', () => {
          expect(repository.find).toHaveBeenCalledTimes(1);
        });

        it('should return an empty array', () => {
          expect(result).toStrictEqual(expectedCargos);
        });
      });
    });
  });

  describe('findAllId()', () => {
    let expectedCargos: Cargo[];
    let result: Cargo[];

    describe('case : success', () => {
      describe('when there are cargos', () => {
        beforeEach(async () => {
          expectedCargos = [SampleCargo];
          getRawManySpy.mockResolvedValue(expectedCargos);
          result = await service.findAllId();
        });

        it('should invoke repository.createQueryBuilder()', () => {
          expect(repository.createQueryBuilder).toHaveBeenCalledTimes(1);
        });

        it('should return an array of cargos', () => {
          expect(result).toStrictEqual(expectedCargos);
        });
      });
    });

    describe('case : failure', () => {
      describe('when there are not cargos', () => {
        beforeEach(async () => {
          expectedCargos = [];
          getRawManySpy.mockResolvedValue(expectedCargos);
          result = await service.findAllId();
        });

        it('should invoke repository.createQueryBuilder()', () => {
          expect(repository.createQueryBuilder).toHaveBeenCalledTimes(1);
        });

        it('should return an empty array', () => {
          expect(result).toStrictEqual(expectedCargos);
        });
      });
    });
  });

  describe('findOne(id)', () => {
    let expectedCargo: Cargo;
    let result: Cargo;

    describe('case : success', () => {
      describe('when an role is found', () => {
        beforeEach(async () => {
          expectedCargo = SampleCargo;
          (repository.findOne as jest.Mock).mockResolvedValue(expectedCargo);
          result = await service.findOne(1);
        });

        it('should invoke repository.findOne()', () => {
          expect(repository.findOne).toHaveBeenCalledTimes(1);
          expect(repository.findOne).toHaveBeenCalledWith(1);
        });

        it('should return an Cargo', () => {
          expect(result).toStrictEqual(expectedCargo);
        });
      });
    });

    describe('case : failure', () => {
      describe('when an role is not found', () => {
        beforeEach(async () => {
          expectedCargo = undefined;
          (repository.findOne as jest.Mock).mockResolvedValue(expectedCargo);
          result = await service.findOne(1);
        });

        it('should invoke repository.findOne()', () => {
          expect(repository.findOne).toHaveBeenCalledTimes(1);
          expect(repository.findOne).toHaveBeenCalledWith(1);
        });

        it('should return undefined', () => {
          expect(result).toStrictEqual(expectedCargo);
        });
      });
    });
  });

  describe('create(CreateCargoDTO)', () => {
    const { id, ...rest } = SampleCargo;
    let body: CreateCargoDTO;

    describe('case : success', () => {
      let expectedCargo: Cargo;
      let result: Cargo;

      describe('when an role is created', () => {
        beforeEach(async () => {
          body = rest;
          expectedCargo = SampleCargo;
          (repository.save as jest.Mock).mockResolvedValue(expectedCargo);
          result = await service.create(body);
        });

        it('should invoke repository.save()', () => {
          expect(repository.save).toHaveBeenCalledTimes(1);
          expect(repository.save).toHaveBeenCalledWith(body);
        });

        it('should create a role', () => {
          expect(result).toStrictEqual(expectedCargo);
        });
      });
    });

    describe('case : failure', () => {
      let expectedError: TypeError;
      let body: undefined;

      describe('when a role is not created', () => {
        beforeEach(async () => {
          body = undefined;
          expectedError = new TypeError();
          jest.spyOn(service, 'create').mockRejectedValue(expectedError);
        });

        it('it should throw when the body is undefined', async () => {
          await expect(service.create(body)).rejects.toThrow(TypeError);
        });

        it('should not create an role', () => {
          expect(repository.save).not.toHaveBeenCalled();
        });
      });
    });
  });

  describe('update(id, UpdateCargoDTO)', () => {
    let update: UpdateCargoDTO;

    describe('case : success', () => {
      let updateResult: UpdateResult | GlobalException;
      let result: UpdateResult | GlobalException;

      describe('when a role is updated', () => {
        beforeEach(async () => {
          update = {
            nombre: 'testmod',
            descripcion: 'testdesc'
          };
          updateResult = new UpdateResult();
          (repository.update as jest.Mock).mockResolvedValue(updateResult);
          result = await service.update(1, update);
        });

        it('should invoke repository.update()', () => {
          expect(repository.update).toHaveBeenCalledTimes(1);
          expect(repository.update).toHaveBeenCalledWith(1, update);
        });

        it('should update an role', () => {
          expect(result).toStrictEqual(updateResult);
        });
      });
    });

    describe('case : failure', () => {
      let expectedError: TypeError;

      describe('when a role is not updated', () => {
        beforeEach(async () => {
          expectedError = new TypeError();
          update = undefined;
          jest.spyOn(service, 'update').mockRejectedValue(expectedError);
        });

        it('should throw when the body is undefined', async () => {
          await expect(service.update(1, update)).rejects.toThrow(TypeError);
        });

        it('should not update an role', () => {
          expect(repository.update).not.toHaveBeenCalled();
        });
      });
    });
  });

  describe('delete(id)', () => {
    let deleteResult: DeleteResult | GlobalException;
    let result: DeleteResult | GlobalException;

    describe('case : success', () => {
      describe('when an role is found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 1;
          (repository.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await service.delete(1);
        });

        it('should invoke repository.delete()', () => {
          expect(repository.delete).toHaveBeenCalledTimes(1);
          expect(repository.delete).toHaveBeenCalledWith(1);
        });

        it('should delete an role', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });

    describe('case : failure', () => {
      let expectedError: TypeError;

      describe('when a role is not found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 0;
          (repository.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await service.delete(1);
        });

        it('should invoke repository.delete()', () => {
          expect(repository.delete).toHaveBeenCalledTimes(1);
          expect(repository.delete).toHaveBeenCalledWith(1);
        });

        it('should not delete an role', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });
  });
});
