import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { Cargo } from './../../models/cargo/cargo.entity';
import { CargoController } from './cargo/cargo.controller';
import { CargoService } from './cargo/cargo.service';

@Module({
  imports: [TypeOrmModule.forFeature([Cargo])],
  controllers: [CargoController],
  providers: [CargoService]
})
export class CargoModule {}
