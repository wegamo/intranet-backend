import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { Solicitud } from '../../../models/solicitud/solicitud.entity';
import { SolicitudesService } from './solicitudes.service';
import { SampleSolicitud } from '../../../../test/ObjectSamples';
import { CreateSolicitudDTO } from '../../../models/solicitud/dto/create-solicitud.dto';
import { UpdateSolicitudDTO } from '../../../models/solicitud/dto/update-solicitud.dto';

describe('SolicitudesService', () => {
  let service: SolicitudesService;
  let repository: Repository<Solicitud>;
  let SolicitudRepositoryMock:jest.Mock;

  beforeEach(() => {
    SolicitudRepositoryMock = jest.fn(() => ({
      find: jest.fn(),
      save: jest.fn(),
      update: jest.fn(),
      delete: jest.fn()
    }));
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        SolicitudesService,
        { provide: getRepositoryToken(Solicitud), useValue: new SolicitudRepositoryMock() },
      ],
    }).compile();

    repository = module.get(getRepositoryToken(Solicitud));
    service = module.get<SolicitudesService>(SolicitudesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('findAll()', () => {
    let expectedSolicitud: Solicitud[];
    let result: Solicitud[];

    describe('case : success', () => {
      describe('', () => {
        beforeEach(async () => {
          expectedSolicitud = [SampleSolicitud];
          (repository.find as jest.Mock).mockResolvedValue(expectedSolicitud);
          result = await service.findAll();
        });

        it('should invoke repository.find()', () => {
          expect(repository.find).toHaveBeenCalledTimes(1);
        });

        it('should return an array of Solicitud', () => {
          expect(result).toStrictEqual(expectedSolicitud);
        });
      });
    });

    describe('case : failure', () => {
      describe('when there are not Solicitud', () => {
        beforeEach(async () => {
          expectedSolicitud = [];
          (repository.find as jest.Mock).mockResolvedValue(expectedSolicitud);
          result = await service.findAll();
        });

        it('should invoke repository.find()', () => {
          expect(repository.find).toHaveBeenCalledTimes(1);
        });

        it('should return an empty array', () => {
          expect(result).toStrictEqual(expectedSolicitud);
        });
      });
    });
  });

  describe('findAll()', () => {
    let expectedSolicitud: Solicitud[];
    let result: Solicitud[];

    describe('case : success', () => {
      describe('when there are Solicitud', () => {
        beforeEach(async () => {
          expectedSolicitud = [SampleSolicitud];
          (repository.find as jest.Mock).mockResolvedValue(expectedSolicitud);
          result = await service.findAll();
        });

        it('should invoke repository.find()', () => {
          expect(repository.find).toHaveBeenCalledTimes(1);
        });

        it('should return an array of Solicitud', () => {
          expect(result).toStrictEqual(expectedSolicitud);
        });
      });
    });

    describe('case : failure', () => {
      describe('when there are not Solicitud', () => {
        beforeEach(async () => {
          expectedSolicitud = [];
          (repository.find as jest.Mock).mockResolvedValue(expectedSolicitud);
          result = await service.findAll();
        });

        it('should invoke repository.find()', () => {
          expect(repository.find).toHaveBeenCalledTimes(1);
        });

        it('should return an empty array', () => {
          expect(result).toStrictEqual(expectedSolicitud);
        });
      });
    });
  });


  describe('create(CreateSolicitudDTO)', () => {
    const { id, ...rest } = SampleSolicitud;
    let body: CreateSolicitudDTO;

    describe('case : success', () => {
      let expectedSolicitud: Solicitud;
      let result: Solicitud;

      describe('when an Solicitud is created', () => {
        beforeEach(async () => {
          body = rest;
          expectedSolicitud = SampleSolicitud;
          (repository.save as jest.Mock).mockResolvedValue(expectedSolicitud);
          result = await service.create(body);
        });

        it('should invoke repository.save()', () => {
          expect(repository.save).toHaveBeenCalledTimes(1);
          expect(repository.save).toHaveBeenCalledWith(body);
        });

        it('should create a Solicitud', () => {
          expect(result).toStrictEqual(expectedSolicitud);
        });
      });
    });

    describe('case : failure', () => {
      let expectedError: TypeError;
      let body: undefined;

      describe('when a Solicitud is not created', () => {
        beforeEach(async () => {
          body = undefined;
          expectedError = new TypeError();
          jest.spyOn(service, 'create').mockRejectedValue(expectedError);
        });

        it('it should throw when the body is undefined', async () => {
          await expect(service.create(body)).rejects.toThrow(TypeError);
        });

        it('should not create an Solicitud', () => {
          expect(repository.save).not.toHaveBeenCalled();
        });
      });
    });
  });

  describe('update(id, UpdateSolicitudDTO)', () => {
    let update: UpdateSolicitudDTO;

    describe('case : success', () => {
      let updateResult: UpdateResult;
      let result: UpdateResult;

      describe('when a Solicitud is updated', () => {
        beforeEach(async () => {
          update = {
            fechaRevision:new Date(),
          };
          updateResult = new UpdateResult();
          (repository.update as jest.Mock).mockResolvedValue(updateResult);
          result = await service.update(1, update);
        });

        it('should invoke repository.update()', () => {
          expect(repository.update).toHaveBeenCalledTimes(1);
          expect(repository.update).toHaveBeenCalledWith(1, update);
        });

        it('should update an Solicitud', () => {
          expect(result).toStrictEqual(updateResult);
        });
      });
    });

    describe('case : failure', () => {
      let expectedError: TypeError;

      describe('when a Solicitud is not updated', () => {
        beforeEach(async () => {
          expectedError = new TypeError();
          update = undefined;
          jest.spyOn(service, 'update').mockRejectedValue(expectedError);
        });

        it('should throw when the body is undefined', async () => {
          await expect(service.update(1, update)).rejects.toThrow(TypeError);
        });

        it('should not update an Solicitud', () => {
          expect(repository.update).not.toHaveBeenCalled();
        });
      });
    });
  });

  describe('delete(id)', () => {
    let deleteResult: DeleteResult;
    let result: DeleteResult;

    describe('case : success', () => {
      describe('when an Solicitud is found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 1;
          (repository.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await service.delete(1);
        });

        it('should invoke repository.delete()', () => {
          expect(repository.delete).toHaveBeenCalledTimes(1);
          expect(repository.delete).toHaveBeenCalledWith(1);
        });

        it('should delete an Solicitud', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });

    describe('case : failure', () => {
      let expectedError: TypeError;

      describe('when a Solicitud is not found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 0;
          (repository.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await service.delete(1);
        });

        it('should invoke repository.delete()', () => {
          expect(repository.delete).toHaveBeenCalledTimes(1);
          expect(repository.delete).toHaveBeenCalledWith(1);
        });

        it('should not delete an Solicitud', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });
  });

});
