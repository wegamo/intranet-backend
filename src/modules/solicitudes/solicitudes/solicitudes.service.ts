import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Solicitud } from '../../../models/solicitud/solicitud.entity';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { CreateSolicitudDTO } from '../../../models/solicitud/dto/create-solicitud.dto';
import { UpdateSolicitudDTO } from '../../../models/solicitud/dto/update-solicitud.dto';

@Injectable()
export class SolicitudesService {
  constructor(
    @InjectRepository(Solicitud) private readonly solicitudRepository: Repository<Solicitud>
  ) {}

  async findAll(): Promise<Solicitud[]> {
    return await this.solicitudRepository.find();
  }

  async create(Solicitud: CreateSolicitudDTO): Promise<Solicitud> {
    return await this.solicitudRepository.save(Solicitud);
  }

  async update(id: number, data: UpdateSolicitudDTO): Promise<UpdateResult> {
    try {
      return await this.solicitudRepository.update(id, data);
    } catch (Err) {
      throw new HttpException(
        {
          status: HttpStatus.INTERNAL_SERVER_ERROR,
          error: 'Ha ocurrido un error al modificar registro solicitud'
        },
        HttpStatus.INTERNAL_SERVER_ERROR
      );
    }
  }

  async delete(id: number): Promise<DeleteResult> {
    try {
      return await this.solicitudRepository.delete(id);
    } catch (Err) {
      throw new HttpException(
        {
          status: HttpStatus.INTERNAL_SERVER_ERROR,
          error: 'Ha ocurrido un error al eliminar registro solicitud'
        },
        HttpStatus.INTERNAL_SERVER_ERROR
      );
    }
  }
}
