import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { CreateSolicitudDTO } from '../../../models/solicitud/dto/create-solicitud.dto';
import { UpdateSolicitudDTO } from '../../../models/solicitud/dto/update-solicitud.dto';
import { Solicitud } from '../../../models/solicitud/solicitud.entity';
import { DeleteResult, UpdateResult } from 'typeorm';
import { SolicitudesService } from './solicitudes.service';
import { ApiUseTags } from '@nestjs/swagger';

@ApiUseTags('Solicitudes')
@Controller('solicitudes')
export class SolicitudesController {
    constructor(private readonly solicitudService: SolicitudesService) {}

  @Get()
  findAll(): Promise<Solicitud[]> {
    return this.solicitudService.findAll();
  }

  @Post()
  create(@Body() SolicitudData: CreateSolicitudDTO): Promise<Solicitud> {
    return this.solicitudService.create(SolicitudData);
  }

  @Put(':id')
  update(@Param('id') id: number, @Body() SolicitudData: UpdateSolicitudDTO): Promise<UpdateResult> {
    return this.solicitudService.update(id, SolicitudData);
  }
  
  @Delete(':id')
  delete(@Param('id') id: number): Promise<DeleteResult> {
    return this.solicitudService.delete(id);
  }

}
