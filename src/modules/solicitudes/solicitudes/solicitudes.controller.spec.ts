import { Test, TestingModule } from '@nestjs/testing';
import { CreateSolicitudDTO } from '../../../models/solicitud/dto/create-solicitud.dto';
import { UpdateSolicitudDTO } from '../../../models/solicitud/dto/update-solicitud.dto';
import { Solicitud } from '../../../models/solicitud/solicitud.entity';
import { SampleSolicitud } from '../../../../test/ObjectSamples';
import { DeleteResult, UpdateResult } from 'typeorm';
import { SolicitudesController } from './solicitudes.controller';
import { SolicitudesService } from './solicitudes.service';

describe('Solicitudes Controller', () => {
  let controller: SolicitudesController;
  let solicitudService: SolicitudesService;
  let solicitudServiceMock: jest.Mock<Partial<SolicitudesService>>;

  beforeEach(() => {
    solicitudServiceMock = jest.fn<Partial<SolicitudesService>, SolicitudesService[]>(() => ({
      findAll:jest.fn(),
      create:jest.fn(),
      update:jest.fn(),
      delete:jest.fn(), 
    }));
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SolicitudesController],
      providers: [{ provide: SolicitudesService, useValue: new solicitudServiceMock() }]
    }).compile();
    solicitudService = module.get<SolicitudesService>(SolicitudesService);
    controller = module.get<SolicitudesController>(SolicitudesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('findAll()', () => {
    let expectedSolicitudss: Solicitud[];
    let result: Solicitud[];

    describe('case : success', () => {
      describe('when there are Solicituds', () => {
        beforeEach(async () => {
          expectedSolicitudss = [SampleSolicitud];
          (solicitudService.findAll as jest.Mock).mockResolvedValue(expectedSolicitudss);
          result = await controller.findAll();
        });
        it('should invoke solicitudService.findAll()', () => {
          expect(solicitudService.findAll).toHaveBeenCalledTimes(1);
        });
        it('should return an array of Solicituds', () => {
          expect(result).toStrictEqual(expectedSolicitudss);
        });
      });
    });

    describe('case : failure', () => {
      describe('when there are no Solicituds', () => {
        beforeEach(async () => {
          expectedSolicitudss = [];
          (solicitudService.findAll as jest.Mock).mockResolvedValue(expectedSolicitudss);
          result = await controller.findAll();
        });

        it('should invoke solicitudService.findAll()', () => {
          expect(solicitudService.findAll).toHaveBeenCalledTimes(1);
        });

        it('should return an array empty', () => {
          expect(result).toStrictEqual(expectedSolicitudss);
        });
      });
    });
  });

  describe('create(CreateSolicitudsDTO)', () => {
    const { id, ...rest } = SampleSolicitud;
    let body: CreateSolicitudDTO;

    describe('case : success', () => {
      let expectedSolicituds: Solicitud;
      let result: Solicitud;

      describe('when a Solicitud is created', () => {
        beforeEach(async () => {
          body = rest;
          expectedSolicituds = SampleSolicitud;
          (solicitudService.create as jest.Mock).mockResolvedValue(expectedSolicituds);
          result = await controller.create(body);
        });
        it('should invoke solicitudService.create()', () => {
          expect(solicitudService.create).toHaveBeenCalledTimes(1);
          expect(solicitudService.create).toHaveBeenCalledWith(body);
        });
        it('should create a Solicitud', () => {
          expect(result).toStrictEqual(expectedSolicituds);
        });
      });
    });

    describe('case : failure', () => {
      let expectedError: TypeError;
      describe('when a Solicitud is not created', () => {
        beforeEach(async () => {
          body = undefined;
          expectedError = new TypeError();
          jest.spyOn(controller, 'create').mockRejectedValue(expectedError);
        });
        it('should throw when the body is undefined', async () => {
          await expect(controller.create(body)).rejects.toThrow(TypeError);
        });
        it('should not create a Solicitud', () => {
          expect(solicitudService.create).not.toHaveBeenCalled();
        });
      });
    });
  });

  describe('update(id, UpdateSolicitudDTO)', () => {
    let update: UpdateSolicitudDTO;

    describe('case : success', () => {
      let updateResult: UpdateResult;
      let result: UpdateResult;

      describe('when an Solicitud is found', () => {
        beforeEach(async () => {
          update = {
            fechaRevision:new Date(),
          };
          updateResult = new UpdateResult();
          (solicitudService.update as jest.Mock).mockResolvedValue(updateResult);
          result = await controller.update(1, update);
        });
        it('should invoke solicitudService.update()', () => {
          expect(solicitudService.update).toHaveBeenCalledTimes(1);
          expect(solicitudService.update).toHaveBeenCalledWith(1, update);
        });
        it('should update an Solicitud', () => {
          expect(result).toStrictEqual(updateResult);
        });
      });
    });

    describe('case : failure', () => {
      let expectedError: TypeError;

      describe('when an Solicitud is not found', () => {
        beforeEach(async () => {
          expectedError = new TypeError();
          update = undefined;
          jest.spyOn(controller, 'update').mockRejectedValue(expectedError);
        });
        it('should invoke solicitudService.update()', async () => {
          await expect(controller.update(1, update)).rejects.toThrow(TypeError);
        });
        it('should not update an Solicitud', () => {
          expect(solicitudService.update).not.toHaveBeenCalled();
        });
      });
    });
  });
  //Test delete()
  describe('delete(id)', () => {
    let deleteResult: DeleteResult;
    let result: DeleteResult;

    describe('case : success', () => {
      describe('when an Solicitud is found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 1;
          (solicitudService.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await controller.delete(1);
        });
        it('should invoke solicitudService.delete()', () => {
          expect(solicitudService.delete).toHaveBeenCalledTimes(1);
          expect(solicitudService.delete).toHaveBeenCalledWith(1);
        });
        it('should delete an Solicitud', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });

    describe('case : failure', () => {
      let expectedError: TypeError;

      describe('when an Solicitud is not found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 0;
          (solicitudService.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await controller.delete(1);
        });

        it('should invoke solicitudService.delete()', () => {
          expect(solicitudService.delete).toHaveBeenCalledTimes(1);
          expect(solicitudService.delete).toHaveBeenCalledWith(1);
        });

        it('should not delete an Solicitud', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });
  });

});
