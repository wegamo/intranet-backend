import { Module } from '@nestjs/common';
import { SolicitudesService } from './solicitudes/solicitudes.service';
import { SolicitudesController } from './solicitudes/solicitudes.controller';
import { HistPrestacionesModule } from '../hist-prestaciones/hist-prestaciones.module';
import { HistVacacionesModule } from '../hist-vacaciones/hist-vacaciones.module';
import { HistAusenciaModule } from '../hist-ausencia/hist-ausencia.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Solicitud } from '../../models/solicitud/solicitud.entity';

@Module({
  imports:[
    TypeOrmModule.forFeature([Solicitud]),
    HistAusenciaModule,
    HistPrestacionesModule,
    HistVacacionesModule
  ],
  providers: [SolicitudesService],
  controllers: [SolicitudesController], 
})
export class SolicitudesModule {}
