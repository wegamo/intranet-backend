import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { sendEmail } from '../../../../helpers/sendEmail';
import { ConfigService } from '../../../../config/config/config.service';
import { SolicitudEstatus } from '../../../../enums/solicitud-estatus.enum';
import { PersonaService } from '../../../persona/persona/persona.service';
import { NotificacionesService } from '../../../notificaciones/notificaciones/notificaciones.service';
import { TipoNotificacion } from '../../../../enums/tipo-notificacion.enum';

@Injectable()
export class HelpersService {
  constructor(
    private readonly personaService: PersonaService,
    private readonly notificacionesService: NotificacionesService,
    private readonly _configService: ConfigService
  ) {}

  createAusencia(histAusencia) {
    try {
      if (this._configService.get('NODE_ENV') === 'development') {
        sendEmail(
          'pouteda@conectium.com', // deberia parametrizar esta variable
          'Solicitud de Permiso',
          '',
          `Un miembro de tu equipo ha solicitado un permiso. Para gestionarlo ingrese a este <a href="${this._configService.get(
            'FRONTEND_URL'
          )}/permisos/${histAusencia.id}">enlace</a>`
        );
      } else {
        sendEmail(
          histAusencia.supervisor.email,
          'Solicitud de Permiso',
          '',
          `Un miembro de tu equipo ha solicitado un permiso. Para gestionarlo ingrese a este <a href="${this._configService.get(
            'FRONTEND_URL'
          )}/permisos/${histAusencia.id}">enlace</a>`
        );
      }
      return { message: 'Email sent' };
    } catch (error) {
      console.log(JSON.stringify(error));
      throw 'Error al enviar el email';
    }
  }

  async updateAusenciaBySup(histAusencia) {
    try {
      if (this._configService.get('NODE_ENV') === 'development') {
        //DESARROLLO
        if (histAusencia.estatus === SolicitudEstatus.APROBADA_SUPERVISOR) {
          const THPersona = await this.personaService.findByEmail('ipulido@conectium.com');
          sendEmail(
            'pouteda@conectium.com',
            'Solicitud de Permiso Aprobado',
            '',
            `Un supervisor ha aprobado una solicitud de permiso. Para gestionarla ingrese al este <a href="${this._configService.get(
              'FRONTEND_URL'
            )}/permisos/${histAusencia.id}">enlace</a>`
          );
          this.notificacionesService.create({
            accion: `/permisos/${histAusencia.id}`,
            mensaje: 'Permiso aprobado por un supervisor!',
            persona: THPersona,
            tipo: TipoNotificacion.ADMIN
          });
        } else {
          sendEmail(
            'pouteda@conectium.com',
            'Solicitud de Permiso Rechazado',
            '',
            `Su solicitud de permiso ha sido rechazada. Para verla ingrese a este <a href="${this._configService.get(
              'FRONTEND_CLIENT_URL'
            )}/solicitudes?tab=ausencias">enlace</a>`
          );
          this.notificacionesService.create({
            accion: `/solicitudes?tab=ausencias`,
            mensaje: 'Solicitud de permiso rechazada!',
            persona: histAusencia.persona,
            tipo: TipoNotificacion.CLIENTE
          });
        }
      } else {
        // PRODUCCION
        if (histAusencia.estatus === SolicitudEstatus.APROBADA_SUPERVISOR) {
          // Send email to talento humano department
          const arrayTalentoHumano = await this.personaService.findAllHumanTalent();
          arrayTalentoHumano.forEach((thPersona) => {
            sendEmail(
              thPersona.email,
              'Solicitud de Permiso Aprobado',
              '',
              `Un supervisor ha aprobado una solicitud de permiso. Para gestionarla ingrese al este <a href="${this._configService.get(
                'FRONTEND_URL'
              )}/permisos/${histAusencia.id}">enlace</a>`
            );
            this.notificacionesService.create({
              accion: `/permisos/${histAusencia.id}`,
              mensaje: 'Permiso aprobado por un supervisor!',
              persona: thPersona,
              tipo: TipoNotificacion.ADMIN
            });
          });
        } else {
          sendEmail(
            histAusencia.persona.email,
            'Solicitud de Permiso Rechazado',
            '',
            `Su solicitud de permiso ha sido rechazada. Para verla ingrese a este <a href="${this._configService.get(
              'FRONTEND_CLIENT_URL'
            )}/solicitudes?tab=ausencias">enlace</a>`
          );
          this.notificacionesService.create({
            accion: `/solicitudes?tab=ausencias`,
            mensaje: 'Solicitud de permiso rechazada!',
            persona: histAusencia.persona,
            tipo: TipoNotificacion.CLIENTE
          });
        }
      }
      return { message: 'Email sent' };
    } catch (error) {
      console.log(JSON.stringify(error));
      throw 'Error al enviar el email';
    }
  }

  updateAusenciaByTh(histAusencia) {
    try {
      if (this._configService.get('NODE_ENV') === 'development') {
        // DESARROLLO
        if (histAusencia.estatus === SolicitudEstatus.APROBADA) {
          sendEmail(
            'pouteda@conectium.com',
            'Solicitud de ausencia aprobada',
            '',
            `Su solicitud de ausencia ha sido aprobada. Para verla ingrese a este <a href="${this._configService.get(
              'FRONTEND_CLIENT_URL'
            )}/solicitudes?tab=ausencias">enlace</a>`
          );
          this.notificacionesService.create({
            accion: `/solicitudes?tab=ausencias`,
            mensaje: 'Solicitud de permiso aprobado!',
            persona: histAusencia.persona,
            tipo: TipoNotificacion.CLIENTE
          });
        } else {
          sendEmail(
            'pouteda@conectium.com',
            'Solicitud de ausencia rechazada',
            '',
            `Su solicitud de ausencia ha sido rechazada. Para verla ingrese a este <a href="${this._configService.get(
              'FRONTEND_CLIENT_URL'
            )}/solicitudes?tab=ausencias">enlace</a>`
          );
          this.notificacionesService.create({
            accion: `/solicitudes?tab=ausencias`,
            mensaje: 'Solicitud de permiso rechazada!',
            persona: histAusencia.persona,
            tipo: TipoNotificacion.CLIENTE
          });
        }
      } else {
        // PRODUCCION
        if (histAusencia.estatus === SolicitudEstatus.APROBADA) {
          sendEmail(
            histAusencia.persona.email,
            'Solicitud de ausencia aprobada',
            '',
            `Su solicitud de ausencia ha sido aprobada. Para verla ingrese a este <a href="${this._configService.get(
              'FRONTEND_CLIENT_URL'
            )}/solicitudes?tab=ausencias">enlace</a>`
          );
          this.notificacionesService.create({
            accion: `/solicitudes?tab=ausencias`,
            mensaje: 'Solicitud de permiso aprobado!',
            persona: histAusencia.persona,
            tipo: TipoNotificacion.CLIENTE
          });
        } else {
          sendEmail(
            histAusencia.persona.email,
            'Solicitud de ausencia rechazada',
            '',
            `Su solicitud de ausencia ha sido rechazada. Para verla ingrese a este <a href="${this._configService.get(
              'FRONTEND_CLIENT_URL'
            )}/solicitudes?tab=ausencias">enlace</a>`
          );
          this.notificacionesService.create({
            accion: `/solicitudes?tab=ausencias`,
            mensaje: 'Solicitud de permiso rechazada!',
            persona: histAusencia.persona,
            tipo: TipoNotificacion.CLIENTE
          });
        }
      }
      return { message: 'Email sent' };
    } catch (error) {
      console.log(JSON.stringify(error));
      throw 'Error al enviar el email';
    }
  }

  createVacaciones(histVacaciones) {
    try {
      if (this._configService.get('NODE_ENV') === 'development') {
        // DESARROLLO
        sendEmail(
          'pouteda@conectium.com',
          'Solicitud de Vacaciones',
          '',
          `Un miembro de tu equipo ha solicitado vacaciones. Para revisar la solicitud ingrese a este <a href="${this._configService.get(
            'FRONTEND_URL'
          )}/vacaciones/${histVacaciones.id}">enlace</a>`
        );
      } else {
        // PRODUCCION
        sendEmail(
          histVacaciones.supervisor.email,
          'Solicitud de Vacaciones',
          '',
          `Un miembro de tu equipo ha solicitado vacaciones. Para revisar la solicitud ingrese a este <a href="${this._configService.get(
            'FRONTEND_URL'
          )}/vacaciones/${histVacaciones.id}">enlace</a>`
        );
      }
      return { message: 'Email sent' };
    } catch (error) {
      console.log(JSON.stringify(error));
      throw 'Error al enviar el email';
    }
  }

  async updateVacacionesBySup(histVacaciones) {
    try {
      if (this._configService.get('NODE_ENV') === 'development') {
        // DESARROLLO
        if (histVacaciones.estatus === SolicitudEstatus.APROBADA_SUPERVISOR) {
          const THPersona = await this.personaService.findByEmail('ipulido@conectium.com');
          sendEmail(
            'pouteda@conectium.com',
            'Solicitud de Vacaciones Aprobada',
            '',
            `Un supervisor ha aprobado una solicitud de vacaciones. Para gestionarla ingrese a este <a href="${this._configService.get(
              'FRONTEND_URL'
            )}/vacaciones/${histVacaciones.id}">enlace</a>`
          );
          this.notificacionesService.create({
            accion: `/vacaciones/${histVacaciones.id}`,
            mensaje: 'Vacaciones aprobadas por un supervisor!',
            persona: THPersona,
            tipo: TipoNotificacion.ADMIN
          });
        } else {
          sendEmail(
            'pouteda@conectium.com',
            'Solicitud de Vacaciones Rechazada',
            '',
            `Su solicitud de vacaciones ha sido rechazada. Para verla ingrese a este <a href="${this._configService.get(
              'FRONTEND_CLIENT_URL'
            )}/solicitudes?tab=vacaciones">enlace</a>`
          );
          this.notificacionesService.create({
            accion: `/solicitudes?tab=vacaciones`,
            mensaje: 'Solicitud de vacaciones rechazada',
            persona: histVacaciones.persona,
            tipo: TipoNotificacion.CLIENTE
          });
        }
      } else {
        // PRODUCCION
        if (histVacaciones.estatus === SolicitudEstatus.APROBADA_SUPERVISOR) {
          // Send email to talento humano department
          const arrayTalentoHumano = await this.personaService.findAllHumanTalent();
          arrayTalentoHumano.forEach((thPersona) => {
            sendEmail(
              thPersona.email,
              'Solicitud de Vacaciones Aprobada',
              '',
              `Un supervisor ha aprobado una solicitud de vacaciones. Para gestionarla ingrese a este <a href="${this._configService.get(
                'FRONTEND_URL'
              )}/vacaciones/${histVacaciones.id}">enlace</a>`
            );
            this.notificacionesService.create({
              accion: `/vacaciones/${histVacaciones.id}`,
              mensaje: 'Vacaciones aprobadas por un supervisor!',
              persona: thPersona,
              tipo: TipoNotificacion.ADMIN
            });
          });
        } else {
          sendEmail(
            histVacaciones.persona.email,
            'Solicitud de Vacaciones Rechazada',
            '',
            `Su solicitud de vacaciones ha sido rechazada. Para verla ingrese a este <a href="${this._configService.get(
              'FRONTEND_CLIENT_URL'
            )}/solicitudes?tab=vacaciones">enlace</a>`
          );
          this.notificacionesService.create({
            accion: `/solicitudes?tab=vacaciones`,
            mensaje: 'Solicitud de vacaciones rechazada',
            persona: histVacaciones.persona,
            tipo: TipoNotificacion.CLIENTE
          });
        }
      }
      return { message: 'Email sent' };
    } catch (error) {
      console.log(JSON.stringify(error));
      throw 'Error al enviar el email';
    }
  }

  updateVacacionesByTh(histVacaciones) {
    try {
      if (this._configService.get('NODE_ENV') === 'development') {
        // DESARROLLO
        if (histVacaciones.estatus === SolicitudEstatus.APROBADA) {
          sendEmail(
            'pouteda@conectium.com',
            'Solicitud de Vacaciones Aprobada',
            '',
            `Su solicitud de vacaciones ha sido aprobada. Para verla ingrese a este <a href="${this._configService.get(
              'FRONTEND_CLIENT_URL'
            )}/solicitudes?tab=vacaciones">enlace</a>`
          );
          this.notificacionesService.create({
            accion: `/solicitudes?tab=vacaciones`,
            mensaje: 'Solicitud de vacaciones aprobada!',
            persona: histVacaciones.persona,
            tipo: TipoNotificacion.CLIENTE
          });
        } else {
          sendEmail(
            'pouteda@conectium.com',
            'Solicitud de Vacaciones Rechazada',
            '',
            `Su solicitud de vacaciones ha sido rechazada. Para verla ingrese a este <a href="${this._configService.get(
              'FRONTEND_CLIENT_URL'
            )}/solicitudes?tab=vacaciones">enlace</a>`
          );
          this.notificacionesService.create({
            accion: `/solicitudes?tab=vacaciones`,
            mensaje: 'Solicitud de vacaciones rechazada!',
            persona: histVacaciones.persona,
            tipo: TipoNotificacion.CLIENTE
          });
        }
      } else {
        // PRODUCCION
        if (histVacaciones.estatus === SolicitudEstatus.APROBADA) {
          sendEmail(
            histVacaciones.persona.email,
            'Solicitud de Vacaciones Aprobada',
            '',
            `Su solicitud de vacaciones ha sido aprobada. Para verla ingrese a este <a href="${this._configService.get(
              'FRONTEND_CLIENT_URL'
            )}/solicitudes?tab=vacaciones">enlace</a>`
          );
          this.notificacionesService.create({
            accion: `/solicitudes?tab=vacaciones`,
            mensaje: 'Solicitud de vacaciones aprobada!',
            persona: histVacaciones.persona,
            tipo: TipoNotificacion.CLIENTE
          });
        } else {
          sendEmail(
            histVacaciones.persona.email,
            'Solicitud de Vacaciones Rechazada',
            '',
            `Su solicitud de vacaciones ha sido rechazada. Para verla ingrese a este <a href="${this._configService.get(
              'FRONTEND_CLIENT_URL'
            )}/solicitudes?tab=vacaciones">enlace</a>`
          );
          this.notificacionesService.create({
            accion: `/solicitudes?tab=vacaciones`,
            mensaje: 'Solicitud de vacaciones rechazada!',
            persona: histVacaciones.persona,
            tipo: TipoNotificacion.CLIENTE
          });
        }
      }
      return { message: 'Email sent' };
    } catch (error) {
      console.log(JSON.stringify(error));
      throw 'Error al enviar el email';
    }
  }

  async createPrestaciones(histPrestaciones) {
    try {
      if (this._configService.get('NODE_ENV') === 'development') {
        // DESARROLLO
        const THPersona = await this.personaService.findByEmail('ipulido@conectium.com');
        sendEmail(
          'pouteda@conectium.com',
          'Nueva solicitud de prestacion',
          '',
          `Se ha creado una nueva solicitud de prestacion. Para verla ingrese a este <a href="${this._configService.get(
            'FRONTEND_URL'
          )}/prestaciones/${histPrestaciones.id}">enlace</a>`
        );
        this.notificacionesService.create({
          accion: `/prestaciones/${histPrestaciones.id}`,
          mensaje: 'Nueva solicitud de prestacion!',
          persona: THPersona,
          tipo: TipoNotificacion.ADMIN
        });
      } else {
        // PRODUCCION
        // Send Email to Talento Humano Department
        const arrayTalentoHumano = await this.personaService.findAllHumanTalent();
        arrayTalentoHumano.forEach((thPersona) => {
          sendEmail(
            thPersona.email,
            'Nueva solicitud de prestacion',
            '',
            `Se ha creado una nueva solicitud de prestacion. Para verla ingrese a este <a href="${this._configService.get(
              'FRONTEND_URL'
            )}/prestaciones/${histPrestaciones.id}">enlace</a>`
          );
          this.notificacionesService.create({
            accion: `/prestaciones/${histPrestaciones.id}`,
            mensaje: 'Nueva solicitud de prestacion!',
            persona: thPersona,
            tipo: TipoNotificacion.ADMIN
          });
        });
      }
      return { message: 'Email sent' };
    } catch (error) {
      console.log(JSON.stringify(error));
      throw 'Error al enviar el email';
    }
  }

  updatePrestacionesRechazada(histPrestaciones) {
    try {
      if (this._configService.get('NODE_ENV') === 'development') {
        // DESARROLLO
        sendEmail(
          'pouteda@conectium.com',
          'Solicitud de prestación rechazada',
          '',
          `Su solicitud de prestación ha sido rechazada. Para verla ingrese a este <a href="${this._configService.get(
            'FRONTEND_CLIENT_URL'
          )}/solicitudes?tab=prestaciones">enlace</a>`
        );
      } else {
        // PRODUCCION
        sendEmail(
          histPrestaciones.persona.email,
          'Solicitud de prestación rechazada',
          '',
          `Su solicitud de prestación ha sido rechazada. Para verla ingrese a este <a href="${this._configService.get(
            'FRONTEND_CLIENT_URL'
          )}/solicitudes?tab=prestaciones">enlace</a>`
        );
      }
      this.notificacionesService.create({
        accion: `/solicitudes?tab=prestaciones`,
        mensaje: 'Solicitud de prestación rechazada!',
        persona: histPrestaciones.persona,
        tipo: TipoNotificacion.CLIENTE
      });
      return { message: 'Email sent' };
    } catch (error) {
      console.log(JSON.stringify(error));
      throw 'Error al enviar el email';
    }
  }

  updatePrestacionesAprobada(histPrestaciones) {
    try {
      if (this._configService.get('NODE_ENV') === 'development') {
        // DESARROLLO
        sendEmail(
          'pouteda@conectium.com',
          'Solicitud de prestación aprobada',
          '',
          `Su solicitud de prestación ha sido aprobada. Para verla ingrese a este <a href="${this._configService.get(
            'FRONTEND_CLIENT_URL'
          )}/solicitudes?tab=prestaciones">enlace</a>`
        );
      } else {
        // PRODUCCION
        sendEmail(
          histPrestaciones.persona.email,
          'Solicitud de prestación aprobada',
          '',
          `Su solicitud de prestación ha sido aprobada. Para verla ingrese a este <a href="${this._configService.get(
            'FRONTEND_CLIENT_URL'
          )}/solicitudes?tab=prestaciones">enlace</a>`
        );
      }
      this.notificacionesService.create({
        accion: `/solicitudes?tab=prestaciones`,
        mensaje: 'Solicitud de prestación aprobada!',
        persona: histPrestaciones.persona,
        tipo: TipoNotificacion.CLIENTE
      });
      return { message: 'Email sent' };
    } catch (error) {
      console.log(JSON.stringify(error));
      throw 'Error al enviar el email';
    }
  }

  async createMontoDisp() {
    try {
      if (this._configService.get('NODE_ENV') === 'development') {
        // DESARROLLO
        const THPersona = await this.personaService.findByEmail('ipulido@conectium.com');
        sendEmail(
          'pouteda@conectium.com',
          'Nueva solicitud de monto disponible',
          '',
          `Un empleado ha solicitado su monto disponible. Para verla ingrese a este <a href="${this._configService.get(
            'FRONTEND_URL'
          )}/montos-disponibles">enlace</a>`
        );
        this.notificacionesService.create({
          accion: `/montos-disponibles`,
          mensaje: 'Nueva solicitud de monto disponible',
          persona: THPersona,
          tipo: TipoNotificacion.ADMIN
        });
      } else {
        // PRODUCCION
        // Send email to talento humano department
        const arrayTalentoHumano = await this.personaService.findAllHumanTalent();
        arrayTalentoHumano.forEach((thPersona) => {
          sendEmail(
            thPersona.email,
            'Nueva solicitud de monto disponible de prestaciones',
            '',
            `Un empleado ha solicitado su monto disponible. Para verla ingrese a este <a href="${this._configService.get(
              'FRONTEND_URL'
            )}/montos-disponibles">enlace</a>`
          );
          this.notificacionesService.create({
            accion: `/montos-disponibles`,
            mensaje: 'Nueva solicitud de monto disponible',
            persona: thPersona,
            tipo: TipoNotificacion.ADMIN
          });
        });
      }
      return { message: 'Email sent' };
    } catch (error) {
      console.log(JSON.stringify(error));
      throw 'Error al enviar el email';
    }
  }

  updateMontoDisp(montoDisponible) {
    try {
      if (this._configService.get('NODE_ENV') === 'development') {
        // DESARROLLO
        sendEmail(
          'pouteda@conectium.com',
          'Informacion de monto disponible lista',
          '',
          `Su monto disponible está listo. Para verlo ingrese a este <a href="${this._configService.get(
            'FRONTEND_CLIENT_URL'
          )}/solicitudes">enlace</a>`
        );
      } else {
        // PRODUCCION
        sendEmail(
          montoDisponible.persona.email,
          'Informacion de monto disponible lista',
          '',
          `Su monto disponible está listo. Para verlo ingrese a este <a href="${this._configService.get(
            'FRONTEND_CLIENT_URL'
          )}/solicitudes">enlace</a>`
        );
      }
      this.notificacionesService.create({
        accion: `/solicitudes`,
        mensaje: 'Monto solicitado ahora disponible!',
        persona: montoDisponible.persona,
        tipo: TipoNotificacion.CLIENTE
      });
      return { message: 'Email sent' };
    } catch (error) {
      console.log(JSON.stringify(error));
      throw 'Error al enviar el email';
    }
  }
}
