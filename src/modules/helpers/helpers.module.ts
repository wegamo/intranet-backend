import { forwardRef, Module } from '@nestjs/common';
import { PersonaModule } from '../persona/persona.module';
import { PersonaService } from '../persona/persona/persona.service';
import { HelpersService } from './helpers/helpers/helpers.service';

@Module({
  imports: [forwardRef(() => PersonaModule)],
  providers: [HelpersService, PersonaService],
  exports: [HelpersService]
})
export class HelpersModule {}
