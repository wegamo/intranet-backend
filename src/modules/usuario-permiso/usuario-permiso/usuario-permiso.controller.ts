import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Post, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { UsuarioPermisoResp, Permiso } from '../../dto_response/usuario-permiso-response';
import { TipoPermiso } from '../../../enums/tipo-permiso.enum';
import { Persona } from '../../../models/persona/persona.entity';
import { Publicacion } from '../../../models/publicacion/publicacion.entity';
import { UsuarioPermiso } from '../../../models/usuario-permiso/usuario-permiso.entity';
import { PersonaService } from '../../persona/persona/persona.service';
import { PublicacionService } from '../../publicacion/publicacion/publicacion.service';
import { CreateUsuarioPermisoDTO } from '../../../models/usuario-permiso/dto/create-usuario-permiso.dto';
import { UsuarioPermisoService } from './usuario-permiso.service';
import { DeleteResult } from 'typeorm';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiUseTags } from '@nestjs/swagger';
import { InstancePermisoDTO } from '../../../models/usuario-permiso/dto/instance-permiso.dto';
import { UsuarioPermisoResponseDTO } from '../../../models/usuario-permiso/dto/usuario-permiso-response.dto';


export type PersonaShare = {
  perId:number;
  tipoPermiso:string;
  nombreCompleto:string;
  primerNombre:string;
  primerApellido:string;
}

@ApiBearerAuth()
@ApiUseTags('Permisos de usuario')
@Controller('usuario-permiso')
export class UsuarioPermisoController {
  constructor(
    private readonly usuarioPermisoService: UsuarioPermisoService,
    private readonly personaService: PersonaService,
    private readonly publicacionService: PublicacionService) { }
  
  @ApiOperation({ title:'Crear permiso de usuario', description:'Creación de permiso de usuario' })
  @ApiResponse({
    status: 200,
    description:'Permiso generado',
    type:UsuarioPermisoResponseDTO,
  })
  @UseGuards(AuthGuard('jwt'))
  @Post('create')
  async create(
    @Body() permisoData: InstancePermisoDTO,
  ): Promise<UsuarioPermiso> {
    try {
      let user: Persona = await this.personaService.findOne(permisoData.userId);
      let file: Publicacion = await this.publicacionService.findByID(permisoData.fileId);
      let resCreate: UsuarioPermiso;
      resCreate = await this.usuarioPermisoService.create({ publicacion:file,persona:user,tipoPermiso:permisoData.tipoPermiso });
      return resCreate;
    } catch (err) {
      throw new HttpException(`Error al Crear el permiso ${err.message}`, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
  /*
  @ApiOperation({ title:'Crear permiso de usuario', description:'Creación de permiso de usuario' })
  @ApiResponse({
    status: 200,
    description:'Permiso generado',
    type:UsuarioPermisoResponseDTO,
  })
  @UseGuards(AuthGuard('jwt'))
  @Delete('ofUser/:pId')
  async deletePermisoOfPub(@Param('pId') pId:number): Promise<DeleteResult> {
    try{
        return await this.usuarioPermisoService.deleteByFile(pId);
    }
    catch(err){
        throw new HttpException(`Error al eliminar el permisos asociados ${err.message}`, HttpStatus.BAD_REQUEST);
    }
  }*/
  
  @ApiOperation({ title:'Obtener permisos de usuario', description:'Obtención de permisos de usuario por ID' })
  @ApiResponse({
    status: 200,
    description:'Permiso registrados del usuario',
    type:Permiso,
    isArray:true,
  })
  @UseGuards(AuthGuard('jwt'))
  @Get('byUser/:id')
  async getPermisos(@Param('id') id: number): Promise<UsuarioPermisoResp> {
    try{
        return await this.usuarioPermisoService.getByUser(id);
    }
    catch(err){
        throw new HttpException(`Error al obtener permisos de usuario ${err.message}`, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('byFile/:pId')
  async getPermisosByFile(@Param('pId') pId: number): Promise<UsuarioPermiso[]> {
    try{
        return await this.usuarioPermisoService.getByFile(pId);
    }
    catch(err){
        throw new HttpException(`Error al obtener permisos de usuario ${err.message}`, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
  
  @ApiOperation({ title:'Asignar permisos a usuarios', description:'Asignación de permisos sobre archivo objetivo a otros usuarios' })
  @ApiResponse({
    status: 200,
    description:'Permisos registrados y actualizados',
    type:UsuarioPermisoResponseDTO,
    isArray:true,
  })
  @UseGuards(AuthGuard('jwt'))
  @Post('share/:pId')
  async setPermisos(
    @Body() data: { 
      newPermisos:{[prop:string]:{id:number,tipoPermiso:TipoPermiso, perId:number}}
    },
    @Param('pId') pId: number
    ): Promise<any> {
    try{
    const { newPermisos } = data;
    //PUBLICACION OBJETIVO
    let pubTarget : Publicacion = await this.publicacionService.findByID(pId); 
     
    //PERSONAS INVOLUCRADAS
    let toPersonas: {
      [prop:string]:Persona
    }  = {}

    let _permisos:{[prop:string]:{id:number,tipoPermiso:TipoPermiso, perId:number}} = newPermisos;
    //BUSCAR PERMISOS YA EXISTENTES SEGUN IDS DE USUARIOS RECIBIDOS
    let toUpdate : UsuarioPermiso[] = [];
    let foundPermissions : UsuarioPermiso[] = await this.usuarioPermisoService.getMultipleId(
      Object.entries(_permisos).map(([key,value],ind) => value.perId),pId
    );
    foundPermissions.forEach((perm,ind) => {
      toUpdate.push(
        {
          ...perm,
          tipoPermiso:newPermisos[`${perm.persona.id}`].tipoPermiso
        }
      );
      //IDENTIFICAR PERMISOS A ACTUALIZAR Y NO EXISTENTES
      _permisos[`${perm.persona.id}`].id = perm.id;
    });
    //PERMISOS A GENERAR
    let permsNoId:number[] = [];
    //DTOS PARA CREACION
    let newPerms: CreateUsuarioPermisoDTO[] = [];

    //SEPARCION PERMISOS
    Object.entries(_permisos).forEach(([key,value],ind) => {
       if(value.id === 0) {
        permsNoId.push(value.perId);
      }
    });

    let toCreatePersona : Persona[] = await this.personaService.getMultipleId(
      permsNoId.map((perm,ind) => perm)
    );

    toCreatePersona.forEach((persona,ind) => {
      newPerms.push({ 
        publicacion: pubTarget,
        tipoPermiso:_permisos[`${persona.id}`].tipoPermiso, 
        persona:persona,
      });
    });
      //PERMISOS ACTUALIZADOS
      let createResp : UsuarioPermiso[] = await this.usuarioPermisoService.createMultipleQB(newPerms);
      let updResp : UsuarioPermiso[] = await this.usuarioPermisoService.createMultiple(toUpdate); 
      //console.log('Permisos Creados : ',createResp);
      //console.log('Permisos Actualizados : ',updResp);
      //ESTA RESPONSE DEBE CAMBIAR
      return [...updResp,...createResp];
    }
    catch(err){
      throw new HttpException(`Error al actualizar permisos de usuario ${err.message}`, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
