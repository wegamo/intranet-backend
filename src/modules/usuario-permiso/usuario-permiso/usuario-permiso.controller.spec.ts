import { Test, TestingModule } from '@nestjs/testing';
import { UsuarioPermisoController } from './usuario-permiso.controller';
import { UsuarioPermisoService } from './usuario-permiso.service';

describe('UsuarioPermiso Controller', () => {
  let controller: UsuarioPermisoController;
  let UsuarioPermisoServiceMock: jest.Mock<Partial<UsuarioPermisoService>>;
  let usuarioPermisoService: UsuarioPermisoService;

  beforeEach(() => {
    UsuarioPermisoServiceMock = jest.fn<Partial<UsuarioPermisoService>, UsuarioPermisoService[]>(() => ({

    }));
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsuarioPermisoController],
      providers: [{ provide: UsuarioPermisoService, useValue: new UsuarioPermisoServiceMock() }]
    }).compile();

    usuarioPermisoService = module.get<UsuarioPermisoService>(UsuarioPermisoService);
    controller = module.get<UsuarioPermisoController>(UsuarioPermisoController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
