import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UsuarioPermiso } from '../../../models/usuario-permiso/usuario-permiso.entity';
import { DeleteResult, Repository, UpdateResult, In, InsertResult } from 'typeorm';
import { CreateUsuarioPermisoDTO } from '../../../models/usuario-permiso/dto/create-usuario-permiso.dto';
import { TipoPermiso } from '../../../enums/tipo-permiso.enum';
import { UsuarioPermisoResp } from '../../dto_response/usuario-permiso-response';

@Injectable()
export class UsuarioPermisoService {
    constructor(
        @InjectRepository(UsuarioPermiso) private readonly usuarioPermisoRepository: Repository<UsuarioPermiso>,
      ) {}

    async create(permisoData:CreateUsuarioPermisoDTO):Promise<UsuarioPermiso> {
      //INSTANCIAR BSD
      return await this.usuarioPermisoRepository.save(permisoData);
    }

    async createMultipleQB(
      nPermisos:CreateUsuarioPermisoDTO[],
    ):Promise<UsuarioPermiso[]> {
      let createdPermisos : UsuarioPermiso[] = [];
      for(const perm of nPermisos){
        createdPermisos.push(await this.usuarioPermisoRepository.save(perm));
      }
      return createdPermisos;
    }

    async createMultiple(
      updPermisos:UsuarioPermiso[],
    ):Promise<UsuarioPermiso[]> {
      return await this.usuarioPermisoRepository.save(updPermisos);
    }

    async getByUser(userId:number): Promise<UsuarioPermisoResp> {
      let permissions: UsuarioPermisoResp = {};
      let permisoResp: UsuarioPermiso[] = await this.usuarioPermisoRepository.find({where:{ persona:{ id:userId } },
        relations:['publicacion']
      });
      permisoResp.forEach((perm,id) => {
        permissions[perm.publicacion.ubicacion] = {
          tipoPermiso:perm.tipoPermiso,
          objId:perm.publicacion.id
        }
      });
      return permissions;
    }

    async getByFile(pId:number): Promise<UsuarioPermiso[]> {
      let permisoResp: UsuarioPermiso[] = await this.usuarioPermisoRepository.find({where:{ publicacion:{ id:pId } },
        relations:['publicacion','persona']
      });
      return permisoResp;
    }


    async getMultipleId(idToLook:number[],pId:number): Promise<UsuarioPermiso[]> {
      let permisoResp: UsuarioPermiso[] = await this.usuarioPermisoRepository.find({where:{ publicacion:{id:pId}, persona:{ id:In(idToLook)}, },
        relations:['publicacion','persona'],
      });
      return permisoResp;
    }

    async deleteByUser(userId:number): Promise<DeleteResult> {
      //Eliminar permisos de usuario
      return await this.usuarioPermisoRepository.update({persona:{id:userId}},{tipoPermiso:TipoPermiso.NONE});
    }
    async deletePermiso(permisoId:number): Promise<UsuarioPermiso> {
      //Eliminar permisos de usuario (CAMBIO A NONE)
      let _permiso : UsuarioPermiso = await this.usuarioPermisoRepository.findOne({where:{id:permisoId}})
      return await this.usuarioPermisoRepository.save({..._permiso,tipoPermiso:TipoPermiso.NONE});
    }
    
    async deleteByFile(fileId:number): Promise<DeleteResult> {
      //Eliminar permisos para un objeto (CAMBIO A NONE)
      return await this.usuarioPermisoRepository.delete({publicacion:{id:fileId}});
    }
} 
