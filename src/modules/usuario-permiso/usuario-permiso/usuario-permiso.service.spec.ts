import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { UsuarioPermiso } from '../../../models/usuario-permiso/usuario-permiso.entity';
import { Repository } from 'typeorm';
import { UsuarioPermisoService } from './usuario-permiso.service';

describe('UsuarioPermisoService', () => {
  let service: UsuarioPermisoService;
  let RepositoryMock: jest.Mock;
  let repository: Repository<UsuarioPermiso>;

  beforeEach(() => {
    RepositoryMock = jest.fn(() => ({
      find: jest.fn(),
      findOne: jest.fn(),
      save: jest.fn(),
      update: jest.fn(),
      delete: jest.fn()
    }));
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UsuarioPermisoService,
        { provide: getRepositoryToken(UsuarioPermiso), useValue: new RepositoryMock() },
      ]
    }).compile();

    repository = module.get(getRepositoryToken(UsuarioPermiso));
    service = module.get<UsuarioPermisoService>(UsuarioPermisoService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
