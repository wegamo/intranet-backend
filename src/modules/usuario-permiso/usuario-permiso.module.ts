import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsuarioPermiso } from '../../models/usuario-permiso/usuario-permiso.entity';
import { PersonaModule } from '../persona/persona.module';
import { PersonaService } from '../persona/persona/persona.service';
import { PublicacionModule } from '../publicacion/publicacion.module';
import { UsuarioPermisoController } from './usuario-permiso/usuario-permiso.controller';
import { UsuarioPermisoService } from './usuario-permiso/usuario-permiso.service';

@Module({
  imports:[
    TypeOrmModule.forFeature([UsuarioPermiso]),
    PersonaModule,
    forwardRef(() => PublicacionModule,)  
  ],
  controllers: [UsuarioPermisoController],
  providers: [UsuarioPermisoService, PersonaService],
  exports:[UsuarioPermisoService]
})
export class UsuarioPermisoModule {}
