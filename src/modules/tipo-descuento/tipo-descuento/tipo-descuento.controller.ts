import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { ApiUseTags } from '@nestjs/swagger';
import { TipoDescuento } from '../../../models/tipo-descuento/tipoDescuento.entity';
import { TipoDescuentoService } from './tipo-descuento.service';

@ApiUseTags('Motivo de ausencia')
@Controller('motivo-ausencia')
export class TipoDescuentoController {
  constructor(private readonly tipoDescuentoService: TipoDescuentoService) {}

  @Get('all')
  findAll(): Promise<TipoDescuento[]> {
    return this.tipoDescuentoService.findAll();
  }
}
