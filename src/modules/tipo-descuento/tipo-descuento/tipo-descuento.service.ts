import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TipoDescuento } from '../../../models/tipo-descuento/tipoDescuento.entity';
import { Repository } from 'typeorm';

@Injectable()
export class TipoDescuentoService {
  constructor(
    @InjectRepository(TipoDescuento)
    private readonly tipoDescuentoRepository: Repository<TipoDescuento>
  ) {}

  findAll(): Promise<TipoDescuento[]> {
    return this.tipoDescuentoRepository.find();
  }

  findByMotivo(idMotivo: number, idSolicitud: number): Promise<TipoDescuento[]> {
    return this.tipoDescuentoRepository
      .createQueryBuilder('tipoDescuento')
      .innerJoin('tipoDescuento.ausencias', 'ausencia')
      .where('ausencia.motivo = :idMotivo AND ausencia.solicitud = :idSolicitud', {
        idMotivo,
        idSolicitud
      })
      .getMany();
  }
}
