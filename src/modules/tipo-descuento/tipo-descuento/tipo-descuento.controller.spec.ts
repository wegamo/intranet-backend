import { Test, TestingModule } from '@nestjs/testing';
import { TipoDescuentoController } from './tipo-descuento.controller';
import { TipoDescuentoService } from './tipo-descuento.service';

describe('TipoDescuento Controller', () => {
  let controller: TipoDescuentoController;
  let TipoDescuentoServiceMock: jest.Mock<Partial<TipoDescuentoService>>;
  let tipoDescuentoService: TipoDescuentoService;

  beforeEach(() => {
    TipoDescuentoServiceMock = jest.fn<Partial<TipoDescuentoService>, TipoDescuentoService[]>(() => ({

    }));
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TipoDescuentoController],
      providers: [{ provide: TipoDescuentoService, useValue: new TipoDescuentoServiceMock() }]
    }).compile();

    tipoDescuentoService = module.get<TipoDescuentoService>(TipoDescuentoService);
    controller = module.get<TipoDescuentoController>(TipoDescuentoController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
