import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { TipoDescuento } from '../../../models/tipo-descuento/tipoDescuento.entity';
import { Repository } from 'typeorm';
import { TipoDescuentoService } from './tipo-descuento.service';

describe('TipoDescuentoService', () => {
  let service: TipoDescuentoService;
  let RepositoryMock: jest.Mock;
  let repository: Repository<TipoDescuento>;

  beforeEach(() => {
    RepositoryMock = jest.fn(() => ({
      find: jest.fn(),
      findOne: jest.fn(),
      save: jest.fn(),
      update: jest.fn(),
      delete: jest.fn()
    }));
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TipoDescuentoService,
        { provide: getRepositoryToken(TipoDescuento), useValue: new RepositoryMock() },
      ]
    }).compile();

    repository = module.get(getRepositoryToken(TipoDescuento));
    service = module.get<TipoDescuentoService>(TipoDescuentoService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
