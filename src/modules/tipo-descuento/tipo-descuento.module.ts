import { Module } from '@nestjs/common';
import { TipoDescuentoService } from './tipo-descuento/tipo-descuento.service';
import { TipoDescuentoController } from './tipo-descuento/tipo-descuento.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TipoDescuento } from '../../models/tipo-descuento/tipoDescuento.entity';

@Module({
  imports: [TypeOrmModule.forFeature([TipoDescuento])],
  providers: [TipoDescuentoService],
  controllers: [TipoDescuentoController],
  exports: [TipoDescuentoService]
})
export class TipoDescuentoModule {}
