import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Put,
  Query,
  Request,
  UploadedFile,
  UseGuards,
  UseInterceptors
} from '@nestjs/common';
import { Pagination } from 'nestjs-typeorm-paginate';
import { UpdateHistVacacionesSupervisorDTO } from '../../../models/hist-vacaciones/dto/update-hist-vacaciones-supervisor.dto';
import { HistVacaciones } from '../../../models/hist-vacaciones/hist-vacaciones.entity';
import { HistVacacionesService } from './hist-vacaciones.service';
import { UpdateHistVacacionesThDTO } from '../../../models/hist-vacaciones/dto/update-hist-vacaciones-th.dto';
import { CargoService } from '../../cargo/cargo/cargo.service';
import { PersonaService } from '../../persona/persona/persona.service';
import { Persona } from '../../../models/persona/persona.entity';
import { FileInterceptor } from '@nestjs/platform-express';
import { AuthGuard } from '@nestjs/passport';
import { SolicitudEstatus } from '../../../enums/solicitud-estatus.enum';
import { RolesGuard } from '../../../guards/roles.guard';
import { Roles } from '../../../decorators/roles.decorator';
import { HistDiasDispVacacionesService } from '../../hist-dias-disp-vacaciones/hist-dias-disp-vacaciones/hist-dias-disp-vacaciones.service';
import { sendEmail } from '../../../helpers/sendEmail';
import { ConfigService } from '../../../config/config/config.service';
import { NotificacionesService } from '../../../modules/notificaciones/notificaciones/notificaciones.service';
import { TipoNotificacion } from '../../../enums/tipo-notificacion.enum';
import { ApiUseTags } from '@nestjs/swagger';
import { HelpersService } from '../../helpers/helpers/helpers/helpers.service';
@ApiUseTags('Histórico de vacaciones')
@Controller('hist-vacaciones')
export class HistVacacionesController {
  constructor(
    private readonly histVacacionesService: HistVacacionesService,
    private readonly personaService: PersonaService,
    private readonly _configService: ConfigService,
    private readonly histDiasDispService: HistDiasDispVacacionesService,
    private readonly notificacionesService: NotificacionesService,
    private readonly helpersService: HelpersService
  ) {}

  @Roles('ADMIN_TH', 'SUPER_ADMIN', 'ADMIN_SUPERVISOR')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Get('all')
  async getAll(
    @Request() { user }: { user: Persona },
    @Query('page') page = 1,
    @Query('limit') limit = 10
  ) {
    limit = limit > 100 ? 100 : limit;
    let histVacaciones;
    if (user.rol.nombre === 'ADMIN_SUPERVISOR') {
      histVacaciones = await this.histVacacionesService.findAllSupervisor(user.id, {
        page,
        limit
      });
    } else {
      histVacaciones = await this.histVacacionesService.findAllTH({
        page,
        limit
      });
    }

    return histVacaciones;
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('dashboard')
  async getDashboard(): Promise<any> {
    const histVacaciones = await this.histVacacionesService.findAllCurrentMonth();
    const histVacacionesPendientes = await this.histVacacionesService.findAllPendientes();
    let estadisticas = [
      { decision: 'Aprobadas', area: 0 },
      { decision: 'Rechazadas', area: 0 },
      { decision: 'Pendientes', area: 0 }
    ];

    histVacaciones.map((histVacacion) => {
      if (histVacacion.estatus === SolicitudEstatus.APROBADA) {
        estadisticas[0].area++;
      } else {
        if (histVacacion.estatus === SolicitudEstatus.RECHAZADA) {
          estadisticas[1].area++;
        } else {
          if (
            histVacacion.estatus === SolicitudEstatus.PENDIENTE ||
            histVacacion.estatus === SolicitudEstatus.APROBADA_SUPERVISOR
          ) {
            estadisticas[2].area++;
          }
        }
      }
    });

    return { estadisticas, pendientes: histVacacionesPendientes };
  }

  @UseGuards(AuthGuard('jwt'))
  @Get(':id')
  async findById(@Param('id') id: number): Promise<any> {
    let histVacaciones: any = await this.histVacacionesService.findOneWithDetails(id);
    const cargoSupervisor = histVacaciones.persona.histCargos[0].cargo.parentCargo;
    let supervisor = await this.personaService.findCurrentPersonaByCargo(cargoSupervisor.id);
    histVacaciones.supervisor = supervisor;
    return histVacaciones;
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('persona/:id')
  async findByPersona(
    @Param('id') id: number,
    @Query('page') page = 1,
    @Query('limit') limit = 10
  ): Promise<Pagination<HistVacaciones>> {
    limit = limit > 100 ? 100 : limit;
    const histVacaciones = await this.histVacacionesService.findAllByPersona(id, {
      page,
      limit
    });
    return histVacaciones;
  }

  @UseGuards(AuthGuard('jwt'))
  @Post('create')
  @UseInterceptors(FileInterceptor('file'))
  async create(
    @Body() histVacacionesData: { body: string },
    @UploadedFile() file: Express.Multer.File
  ): Promise<HistVacaciones> {
    try {
      let histVacaciones = JSON.parse(histVacacionesData.body);
      histVacaciones.supervisor = await this.personaService.findSupervisor(
        histVacaciones.persona.id
      );
      let firmaSolicitante = file;

      const vacacion = await this.histVacacionesService.create(histVacaciones, firmaSolicitante);
      vacacion.supervisor = histVacaciones.supervisor;
      // Send email to manager
      await this.helpersService.createVacaciones(vacacion);
      await this.notificacionesService.create({
        accion: `/vacaciones/${vacacion.id}`,
        mensaje: 'Nueva solicitud de vacaciones!',
        persona: histVacaciones.supervisor,
        tipo: TipoNotificacion.ADMIN
      });
      return vacacion;
    } catch (err) {
      console.log(err);
      throw new HttpException('Error al Crear el histórico de Vacaciones', HttpStatus.BAD_REQUEST);
    }
  }

  @UseGuards(AuthGuard('jwt'))
  @Put('update/supervisor/:id')
  @UseInterceptors(FileInterceptor('file'))
  async updateBySupervisor(
    @Param('id') id: number,
    @Body() histVacacionesData: { body: string },
    @UploadedFile() file: Express.Multer.File,
    @Request() { user }: { user: Persona }
  ): Promise<HistVacaciones> {
    let histVacacionToChange = await this.histVacacionesService.findOne(id);
    if (!histVacacionToChange) {
      throw new HttpException('El historial de vacación no existe', HttpStatus.NOT_FOUND);
    }
    if (histVacacionToChange.supervisor.id !== user.id) {
      throw new HttpException(
        'El usuario no es supervisor del empleado perteneciente a la solicitud de Vacaciones',
        HttpStatus.BAD_REQUEST
      );
    }
    const histVacacionesParse: UpdateHistVacacionesSupervisorDTO = JSON.parse(
      histVacacionesData.body
    );

    if (histVacacionToChange.estatus !== SolicitudEstatus.PENDIENTE) {
      throw new HttpException(
        'La solicitud ya ha sido revisada, no puede editarse',
        HttpStatus.BAD_REQUEST
      );
    }
    if (
      histVacacionesParse.estatus === SolicitudEstatus.APROBADA ||
      histVacacionesParse.estatus === SolicitudEstatus.PENDIENTE
    ) {
      throw new HttpException(`Estatus Inválido`, HttpStatus.BAD_REQUEST);
    }
    if (
      histVacacionesParse.motivoRechazo &&
      histVacacionesParse.estatus === SolicitudEstatus.APROBADA_SUPERVISOR
    ) {
      throw new HttpException(`Solicitud Inválida`, HttpStatus.BAD_REQUEST);
    }
    if (
      histVacacionesParse.estatus === SolicitudEstatus.RECHAZADA &&
      !histVacacionesParse.motivoRechazo
    ) {
      throw new HttpException(`Solicitud Inválida`, HttpStatus.BAD_REQUEST);
    }
    try {
      histVacacionToChange = {
        ...histVacacionToChange,
        fechaRevision: histVacacionesParse.fechaRevision,
        motivoRechazo: histVacacionesParse.motivoRechazo,
        estatus: histVacacionesParse.estatus
      };

      const histVacaciones = await this.histVacacionesService.updateBySupervisor(
        histVacacionToChange,
        file
      );
      // Send emails according to environment
      await this.helpersService.updateVacacionesBySup(histVacaciones);
      return histVacaciones;
    } catch (err) {
      throw new HttpException('Error al actualizar la Vacación', HttpStatus.BAD_REQUEST);
    }
  }

  @UseGuards(AuthGuard('jwt'))
  @Put('update/talento-humano/:id')
  @UseInterceptors(FileInterceptor('file'))
  async updateByTh(
    @Param('id') id: number,
    @Body() histVacacionesData: { body: string },
    @UploadedFile() file: Express.Multer.File,
    @Request() { user }: { user: Persona }
  ): Promise<HistVacaciones> {
    let histVacacionToChange = await this.histVacacionesService.findOne(id);

    if (!histVacacionToChange) {
      throw new HttpException('El historial de vacación no existe', HttpStatus.NOT_FOUND);
    }
    // Se verifica que la persona del request pertenece a talento humano

    const talentoHumano = await this.personaService.findOneHumanTalent(user.id);

    if (!talentoHumano) {
      throw new HttpException(
        'La persona que está tratando de modificar la solicitud no pertenece a talento humano',
        HttpStatus.BAD_REQUEST
      );
    }
    const histVacacionesParse: UpdateHistVacacionesThDTO = JSON.parse(histVacacionesData.body);

    if (
      histVacacionToChange.estatus === SolicitudEstatus.APROBADA ||
      histVacacionToChange.estatus === SolicitudEstatus.RECHAZADA
    ) {
      throw new HttpException(
        'La solicitud ya ha sido revisada, no puede editarse',
        HttpStatus.BAD_REQUEST
      );
    }
    if (histVacacionToChange.estatus === SolicitudEstatus.PENDIENTE) {
      throw new HttpException(
        'No puedes editar la solicitud hasta que el supervisor lo autorice',
        HttpStatus.BAD_REQUEST
      );
    }
    if (
      histVacacionesParse.estatus === SolicitudEstatus.APROBADA_SUPERVISOR ||
      histVacacionesParse.estatus === SolicitudEstatus.PENDIENTE
    ) {
      throw new HttpException(`Estatus Inválido`, HttpStatus.BAD_REQUEST);
    }
    if (
      histVacacionesParse.motivoRechazo &&
      histVacacionesParse.estatus === SolicitudEstatus.APROBADA
    ) {
      throw new HttpException(`Solicitud Inválida`, HttpStatus.BAD_REQUEST);
    }
    if (
      histVacacionesParse.estatus === SolicitudEstatus.RECHAZADA &&
      !histVacacionesParse.motivoRechazo
    ) {
      throw new HttpException(`Solicitud Inválida`, HttpStatus.BAD_REQUEST);
    }
    try {
      histVacacionToChange = {
        ...histVacacionToChange,
        fechaRevision: histVacacionesParse.fechaRevision,
        fechaPago: histVacacionesParse.fechaPago,
        fechaExamenPreVacacional: histVacacionesParse.fechaExamenPreVacacional,
        motivoRechazo: histVacacionesParse.motivoRechazo,
        periodoPagado: histVacacionesParse.periodoPagado,
        periodoDisfrute: histVacacionesParse.periodoDisfrute,
        diasPendientePago: histVacacionesParse.diasPendientePago,
        diasPendienteDisfrute: histVacacionesParse.diasPendienteDisfrute,
        estatus: histVacacionesParse.estatus,
        talentoHumano: user
      };
      const histVacaciones = await this.histVacacionesService.updateByTh(
        histVacacionToChange,
        file
      );

      if (histVacacionToChange.estatus === SolicitudEstatus.APROBADA) {
        const histDiasDisponibles = await this.histDiasDispService.findLastest(
          histVacacionToChange.persona.id
        );
        const diasDisponiblesNuevos =
          histDiasDisponibles.diasDisponibles - histVacacionToChange.cantidadDias;
        await this.histDiasDispService.create({
          fechaSolicitud: histVacacionToChange.fechaSolicitud,
          diasDisponibles: diasDisponiblesNuevos,
          persona: histVacacionToChange.persona
        });
      }

      // Send email and notifications according to environment
      await this.helpersService.updateAusenciaByTh(histVacaciones);
      return histVacaciones;
    } catch (err) {
      throw new HttpException('Error al actualizar la Vacación', HttpStatus.BAD_REQUEST);
    }
  }
}
