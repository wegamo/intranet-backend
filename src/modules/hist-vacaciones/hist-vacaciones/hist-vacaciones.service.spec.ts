import { Test, TestingModule } from '@nestjs/testing';
import { HistVacaciones } from '../../../models/hist-vacaciones/hist-vacaciones.entity';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { HistVacacionesService } from './hist-vacaciones.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { UpdateHistVacacionesSupervisorDTO } from '../../../models/hist-vacaciones/dto/update-hist-vacaciones-supervisor.dto';
import { SampleHistVacaciones, SamplePersona } from '../../../../test/ObjectSamples';
import { CreateHistVacacionesDTO } from '../../../models/hist-vacaciones/dto/create-hist-vacaciones.dto';
import { SolicitudEstatus } from '../../../enums/solicitud-estatus.enum';
import { Aws3CntmService } from '../../aws3-cntm/aws3-cntm/aws3-cntm.service';

describe('HistVacacionesService', () => {
  let service: HistVacacionesService;
  let RepositoryMock: jest.Mock;
  let repository: Repository<HistVacaciones>;
  const getMany = jest.fn();
  const where = jest.fn(() => ({ getMany }));
  const select = jest.fn(() => ({ where }));
  beforeEach(() => {
    RepositoryMock = jest.fn(() => ({
      find: jest.fn(),
      save: jest.fn(),
      update: jest.fn(),
      delete: jest.fn(),
      createQueryBuilder: jest.fn(() => ({
        take: jest.fn().mockReturnThis(),
        getMany:getMany,
        where:where,
        select:select,
      })),
    }));
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        HistVacacionesService,
        { provide: getRepositoryToken(HistVacaciones), useValue: new RepositoryMock() },
        Aws3CntmService
      ]
    }).compile();
    repository = module.get(getRepositoryToken(HistVacaciones));
    service = module.get<HistVacacionesService>(HistVacacionesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
  /*
  describe('getAll()', () => {
    let expectedHistVacaciones: HistVacaciones[];
    let result: HistVacaciones[];

    describe('case : success', () => {
      describe('', () => {
        beforeEach(async () => {
          expectedHistVacaciones = [SampleHistVacaciones];
          (repository.find as jest.Mock).mockResolvedValue(expectedHistVacaciones);
          result = await service.findAllByPersona(SampleHistVacaciones.persona.id,{page:1,limit:10});
        });

        it('should invoke repository.find()', () => {
          expect(repository.find).toHaveBeenCalledTimes(1);
        });

        it('should return an array of HistVacaciones', () => {
          expect(result).toStrictEqual(expectedHistVacaciones);
        });
      });
    });

    describe('case : failure', () => {
      describe('when there are not HistVacaciones', () => {
        beforeEach(async () => {
          expectedHistVacaciones = [];
          (repository.find as jest.Mock).mockResolvedValue(expectedHistVacaciones);
          result = await service.findAll();
        });

        it('should invoke repository.find()', () => {
          expect(repository.find).toHaveBeenCalledTimes(1);
        });

        it('should return an empty array', () => {
          expect(result).toStrictEqual(expectedHistVacaciones);
        });
      });
    });
  });
  
 */
/*
  describe('findByPerson(id)', () => {
    let expectedHistVacaciones: HistVacaciones[];
    let result: HistVacaciones[];

    describe('case : success', () => {
      describe('when an HistVacaciones is found', () => {
        beforeEach(async () => {
          expectedHistVacaciones = [SampleHistVacaciones];
          (repository.find as jest.Mock).mockResolvedValue(expectedHistVacaciones);
          result = await service.findAllByPersona(SampleHistVacaciones.persona.id,{page:1,limit:10});
        });

        it('should invoke repository.findOne()', () => {
          expect(repository.find).toHaveBeenCalledTimes(1);
          expect(repository.find).toHaveBeenCalledWith({
            persona: { id: SampleHistVacaciones.persona.id }
          });
        });

        it('should return an HistVacaciones', () => {
          expect(result).toStrictEqual(expectedHistVacaciones);
        });
      });
    });

    describe('case : failure', () => {
      describe('when an HistVacaciones is not found', () => {
        beforeEach(async () => {
          expectedHistVacaciones = undefined;
          (repository.find as jest.Mock).mockResolvedValue(expectedHistVacaciones);
          result = await service.findAllByPersona(SampleHistVacaciones.persona.id);
        });

        it('should invoke repository.findOne()', () => {
          expect(repository.find).toHaveBeenCalledTimes(1);
          expect(repository.find).toHaveBeenCalledWith({
            persona: { id: SampleHistVacaciones.persona.id }
          });
        });

        it('should return undefined', () => {
          expect(result).toStrictEqual(expectedHistVacaciones);
        });
      });
    });
  });
  */
 /*
  describe('create(CreateHistVacacionesDTO)', () => {
    const { id, ...rest } = SampleHistVacaciones;
    let body: CreateHistVacacionesDTO;

    describe('case : success', () => {
      let expectedHistVacaciones: HistVacaciones;
      let result: HistVacaciones;

      describe('when an HistVacaciones is created', () => {
        beforeEach(async () => {
          body = rest;
          expectedHistVacaciones = SampleHistVacaciones;
          (repository.save as jest.Mock).mockResolvedValue(expectedHistVacaciones);
          result = await service.create(body);
        });

        it('should invoke repository.save()', () => {
          expect(repository.save).toHaveBeenCalledTimes(1);
          expect(repository.save).toHaveBeenCalledWith(body);
        });

        it('should create a HistVacaciones', () => {
          expect(result).toStrictEqual(expectedHistVacaciones);
        });
      });
    });

    describe('case : failure', () => {
      let expectedError: TypeError;
      let body: undefined;

      describe('when a HistVacaciones is not created', () => {
        beforeEach(async () => {
          body = undefined;
          expectedError = new TypeError();
          jest.spyOn(service, 'create').mockRejectedValue(expectedError);
        });

        it('it should throw when the body is undefined', async () => {
          await expect(service.create(body)).rejects.toThrow(TypeError);
        });

        it('should not create an HistVacaciones', () => {
          expect(repository.save).not.toHaveBeenCalled();
        });
      });
    });
  });
  */
  describe('update(id, UpdateHistVacacionesSupervisorDTO)', () => {
    let update: HistVacaciones;

    describe('case : success', () => {
      let updateResult: HistVacaciones;
      let result: HistVacaciones;

      describe('when a HistVacaciones is updated', () => {
        beforeEach(async () => {
          update = {
            fechaRevision: new Date(),
            estatus:SolicitudEstatus.APROBADA,
            supervisor:SamplePersona,
            firmaSupervisor:'signoutput',
            ...SampleHistVacaciones,
          };
          //updateResult = new UpdateResult();
          (repository.save as jest.Mock).mockResolvedValue(updateResult);
          result = await service.update(update);
        });

        it('should invoke repository.update()', () => {
          expect(repository.save).toHaveBeenCalledTimes(1);
          expect(repository.save).toHaveBeenCalledWith(update);
        });

        it('should update an HistVacaciones', () => {
          expect(result).toStrictEqual(updateResult);
        });
      });
    });

    describe('case : failure', () => {
      let expectedError: TypeError;

      describe('when a HistVacaciones is not updated', () => {
        beforeEach(async () => {
          expectedError = new TypeError();
          update = undefined;
          jest.spyOn(service, 'update').mockRejectedValue(expectedError);
        });

        it('should throw when the body is undefined', async () => {
          await expect(service.update(update)).rejects.toThrow(TypeError);
        });

        it('should not update an HistVacaciones', () => {
          expect(repository.save).not.toHaveBeenCalled();
        });
      });
    });
  });
  /*
  describe('delete(id)', () => {
    let deleteResult: DeleteResult;
    let result: DeleteResult;

    describe('case : success', () => {
      describe('when an HistVacaciones is found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 1;
          (repository.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await service.delete(1);
        });

        it('should invoke repository.delete()', () => {
          expect(repository.delete).toHaveBeenCalledTimes(1);
          expect(repository.delete).toHaveBeenCalledWith(1);
        });

        it('should delete an HistVacaciones', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });

    describe('case : failure', () => {
      let expectedError: TypeError;

      describe('when a HistVacaciones is not found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 0;
          (repository.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await service.delete(1);
        });

        it('should invoke repository.delete()', () => {
          expect(repository.delete).toHaveBeenCalledTimes(1);
          expect(repository.delete).toHaveBeenCalledWith(1);
        });

        it('should not delete an HistVacaciones', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });
  });
  */
});
