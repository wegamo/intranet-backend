import { Test, TestingModule } from '@nestjs/testing';
import { CreateHistVacacionesDTO } from '../../../models/hist-vacaciones/dto/create-hist-vacaciones.dto';
import { UpdateHistVacacionesSupervisorDTO } from '../../../models/hist-vacaciones/dto/update-hist-vacaciones-supervisor.dto';
import { HistVacaciones } from '../../../models/hist-vacaciones/hist-vacaciones.entity';
import { SampleHistVacaciones, SamplePersona } from '../../../../test/ObjectSamples';
import { DeleteResult, UpdateResult } from 'typeorm';
import { HistVacacionesController } from './hist-vacaciones.controller';
import { HistVacacionesService } from './hist-vacaciones.service';
import { SolicitudEstatus } from '../../../enums/solicitud-estatus.enum';
import { CargoService } from '../../cargo/cargo/cargo.service';
import { PersonaService } from '../../persona/persona/persona.service';

describe('HistVacaciones Controller', () => {
  let controller: HistVacacionesController;
  let histVacacionesService: HistVacacionesService;
  let HistVacacionesServiceMock: jest.Mock<Partial<HistVacacionesService>>;
  let CargoServiceMock: jest.Mock<Partial<CargoService>>;
  let PersonaServiceMock: jest.Mock<Partial<PersonaService>>;

  beforeEach(() => {
    HistVacacionesServiceMock = jest.fn<Partial<HistVacacionesService>, HistVacacionesService[]>(
      () => ({
        findAll: jest.fn(),
        findByPerson: jest.fn(),
        create: jest.fn(),
        delete: jest.fn(),
        update: jest.fn(),
        findAllByPersona:jest.fn(),
      })
    );
    CargoServiceMock = jest.fn<Partial<CargoService>, CargoService[]>(
      () => ({
       
      })
    );
    PersonaServiceMock= jest.fn<Partial<PersonaService>, PersonaService[]>(
      () => ({
        
      })
    );
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [HistVacacionesController],
      providers: [
        { provide: HistVacacionesService, useValue: new HistVacacionesServiceMock() },
        { provide: CargoService, useValue: new  CargoServiceMock() },
        { provide: PersonaService, useValue: new PersonaServiceMock() }]
    }).compile();
    histVacacionesService = module.get<HistVacacionesService>(HistVacacionesService);
    controller = module.get<HistVacacionesController>(HistVacacionesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
  /*
  describe('getAll()', () => {
    let expectedHistVacacionesss: HistVacaciones[];
    let result: HistVacaciones[];

    describe('case : success', () => {
      describe('when there are HistVacacioness', () => {
        beforeEach(async () => {
          expectedHistVacacionesss = [SampleHistVacaciones];
          (histVacacionesService.findAll as jest.Mock).mockResolvedValue(expectedHistVacacionesss);
          result = await controller.getAll();
        });
        it('should invoke HistVacacionesService.findAll()', () => {
          expect(histVacacionesService.findAll).toHaveBeenCalledTimes(1);
        });
        it('should return an array of HistVacacioness', () => {
          expect(result).toStrictEqual(expectedHistVacacionesss);
        });
      });
    });

    describe('case : failure', () => {
      describe('when there are no HistVacacioness', () => {
        beforeEach(async () => {
          expectedHistVacacionesss = [];
          (histVacacionesService.findAll as jest.Mock).mockResolvedValue(expectedHistVacacionesss);
          result = await controller.getAll();
        });

        it('should invoke HistVacacionesService.findAll()', () => {
          expect(histVacacionesService.findAll).toHaveBeenCalledTimes(1);
        });

        it('should return an array empty', () => {
          expect(result).toStrictEqual(expectedHistVacacionesss);
        });
      });
    });
  });
  */
  
  describe('findByPerson(id)', () => {
    let expectedHistVacacioness: HistVacaciones[];
    let result: HistVacaciones[];

    describe('case : success', () => {
      describe('when an HistVacaciones is found', () => {
        beforeEach(async () => {
          expectedHistVacacioness = [SampleHistVacaciones];
          (histVacacionesService.findAllByPersona as jest.Mock).mockResolvedValue(
            expectedHistVacacioness
          );
          result = await controller.findById(SampleHistVacaciones.persona.id);
        });
        it('should invoke HistVacacionesService.findAllByPersona()', () => {
          expect(histVacacionesService.findAllByPersona).toHaveBeenCalledTimes(1);
          expect(histVacacionesService.findAllByPersona).toHaveBeenCalledWith(1);
        });
        it('should return an HistVacaciones', () => {
          expect(result).toStrictEqual(expectedHistVacacioness);
        });
      });
    });

    describe('case : failure', () => {
      describe('when an HistVacaciones is not found', () => {
        beforeEach(async () => {
          expectedHistVacacioness = undefined;
          (histVacacionesService.findAllByPersona as jest.Mock).mockResolvedValue(
            expectedHistVacacioness
          );
          result = await controller.findById(SampleHistVacaciones.persona.id);
        });
        it('should invoke HistVacacionesService.findAllByPersona()', () => {
          expect(histVacacionesService.findAllByPersona).toHaveBeenCalledTimes(1);
          expect(histVacacionesService.findAllByPersona).toHaveBeenCalledWith(
            SampleHistVacaciones.persona.id
          );
        });
        it('should return undefined', () => {
          expect(result).toStrictEqual(expectedHistVacacioness);
        });
      });
    });
  });
  /*
  describe('create(CreateHistVacacionessDTO)', () => {
    const { id, ...rest } = SampleHistVacaciones;
    let body: CreateHistVacacionesDTO;

    describe('case : success', () => {
      let expectedHistVacacioness: HistVacaciones;
      let result: HistVacaciones;

      describe('when a HistVacaciones is created', () => {
        beforeEach(async () => {
          body = rest;
          expectedHistVacacioness = SampleHistVacaciones;
          (histVacacionesService.create as jest.Mock).mockResolvedValue(expectedHistVacacioness);
          result = await controller.create(body);
        });
        it('should invoke HistVacacionesService.create()', () => {
          expect(histVacacionesService.create).toHaveBeenCalledTimes(1);
          expect(histVacacionesService.create).toHaveBeenCalledWith(body);
        });
        it('should create a HistVacaciones', () => {
          expect(result).toStrictEqual(expectedHistVacacioness);
        });
      });
    });

    describe('case : failure', () => {
      let expectedError: TypeError;
      describe('when a HistVacaciones is not created', () => {
        beforeEach(async () => {
          body = undefined;
          expectedError = new TypeError();
          jest.spyOn(controller, 'create').mockRejectedValue(expectedError);
        });
        it('should throw when the body is undefined', async () => {
          await expect(controller.create(body)).rejects.toThrow(TypeError);
        });
        it('should not create a HistVacaciones', () => {
          expect(histVacacionesService.create).not.toHaveBeenCalled();
        });
      });
    });
  });
  */
 
  describe('update(id, UpdateHistVacacionesSupervisorDTO)', () => {
    let update: UpdateHistVacacionesSupervisorDTO;

    describe('case : success', () => {
      let updateResult: HistVacaciones;
      let result: HistVacaciones;

      describe('when an HistVacaciones is found', () => {
        beforeEach(async () => {
          update = {
            fechaRevision: new Date(),
            estatus:SolicitudEstatus.APROBADA,
            supervisor:SamplePersona,
            firmaSupervisor:'signoutput',
          };
          //updateResult = new UpdateResult();
          (histVacacionesService.update as jest.Mock).mockResolvedValue(updateResult);
          result = await controller.updateBySupervisor(1, update);
        });
        it('should invoke HistVacacionesService.update()', () => {
          expect(histVacacionesService.update).toHaveBeenCalledTimes(1);
          expect(histVacacionesService.update).toHaveBeenCalledWith(1, update);
        });
        it('should update an HistVacaciones', () => {
          expect(result).toStrictEqual(updateResult);
        });
      });
    });

    describe('case : failure', () => {
      let expectedError: TypeError;

      describe('when an HistVacaciones is not found', () => {
        beforeEach(async () => {
          expectedError = new TypeError();
          update = undefined;
          jest.spyOn(controller, 'updateBySupervisor').mockRejectedValue(expectedError);
        });
        it('should invoke HistVacacionesService.update()', async () => {
          await expect(controller.updateBySupervisor(1, update)).rejects.toThrow(TypeError);
        });
        it('should not update an HistVacaciones', () => {
          expect(histVacacionesService.update).not.toHaveBeenCalled();
        });
      });
    });
  });
  /*
  describe('delete(id)', () => {
    let deleteResult: DeleteResult;
    let result: DeleteResult;

    describe('case : success', () => {
      describe('when an HistVacaciones is found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 1;
          (histVacacionesService.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await controller.delete(1);
        });
        it('should invoke HistVacacionesService.delete()', () => {
          expect(histVacacionesService.delete).toHaveBeenCalledTimes(1);
          expect(histVacacionesService.delete).toHaveBeenCalledWith(1);
        });
        it('should delete an HistVacaciones', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });

    describe('case : failure', () => {
      let expectedError: TypeError;

      describe('when an HistVacaciones is not found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 0;
          (histVacacionesService.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await controller.delete(1);
        });

        it('should invoke HistVacacionesService.delete()', () => {
          expect(histVacacionesService.delete).toHaveBeenCalledTimes(1);
          expect(histVacacionesService.delete).toHaveBeenCalledWith(1);
        });

        it('should not delete an HistVacaciones', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });
  });*/
});
