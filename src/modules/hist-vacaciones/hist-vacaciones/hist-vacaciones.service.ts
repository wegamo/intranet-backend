import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { IsNull, Repository } from 'typeorm';
import { IPaginationOptions, paginate, paginateRaw, Pagination } from 'nestjs-typeorm-paginate';
import { HistVacaciones } from '../../../models/hist-vacaciones/hist-vacaciones.entity';
import { CreateHistVacacionesDTO } from '../../../models/hist-vacaciones/dto/create-hist-vacaciones.dto';
import configService from '../../../config/config/config.service';
import { Aws3CntmService } from '../../aws3-cntm/aws3-cntm/aws3-cntm.service';
import { SolicitudEstatus } from '../../../enums/solicitud-estatus.enum';
import { PersonaService } from '../../../modules/persona/persona/persona.service';
import { HistDiasDispVacacionesService } from '../../../modules/hist-dias-disp-vacaciones/hist-dias-disp-vacaciones/hist-dias-disp-vacaciones.service';
import { Cron } from 'nest-schedule';

const AWS_S3_BUCKET_NAME = configService.get('AWS_S3_BUCKET_NAME');
@Injectable()
export class HistVacacionesService {
  constructor(
    @InjectRepository(HistVacaciones)
    private readonly histVacacionesRepository: Repository<HistVacaciones>,
    private readonly s3Service: Aws3CntmService,
    private readonly personaService: PersonaService,
    private readonly diasDispService: HistDiasDispVacacionesService
  ) {}

  findAllTH(options: IPaginationOptions): Promise<Pagination<HistVacaciones>> {
    const queryBuilder = this.histVacacionesRepository.createQueryBuilder('histVacaciones');
    queryBuilder
      .select([
        'histVacaciones.id',
        'histVacaciones.fechaSolicitud',
        'histVacaciones.cantidadDias',
        'histVacaciones.firmaSupervisor',
        'histVacaciones.firmaTh',
        'histVacaciones.estatus'
      ])
      .innerJoin('histVacaciones.persona', 'persona')
      .addSelect(['persona.primerNombre', 'persona.primerApellido', 'persona.imgPerfil'])
      .orderBy('histVacaciones.fechaSolicitud', 'DESC');

    return paginateRaw<HistVacaciones>(queryBuilder, options);
  }

  findAllSupervisor(
    idSupervisor: number,
    options: IPaginationOptions
  ): Promise<Pagination<HistVacaciones>> {
    const queryBuilder = this.histVacacionesRepository.createQueryBuilder('histVacaciones');
    queryBuilder
      .select([
        'histVacaciones.id',
        'histVacaciones.fechaSolicitud',
        'histVacaciones.cantidadDias',
        'histVacaciones.firmaSupervisor',
        'histVacaciones.firmaTh',
        'histVacaciones.estatus'
      ])
      .innerJoin('histVacaciones.persona', 'persona')
      .addSelect(['persona.primerNombre', 'persona.primerApellido', 'persona.imgPerfil'])
      .where('histVacaciones.supervisor.id = :id', { id: idSupervisor })
      .orderBy('histVacaciones.fechaSolicitud', 'DESC');

    return paginateRaw<HistVacaciones>(queryBuilder, options);
  }

  findOne(id: number): Promise<HistVacaciones> {
    return this.histVacacionesRepository.findOne(id, {
      relations: ['supervisor', 'persona']
    });
  }

  findOneWithDetails(id: number): Promise<HistVacaciones> {
    /*
    return this.histVacacionesRepository.find({
      join: {
        alias: 'histVacaciones',
        innerJoinAndSelect: {
          persona: 'histVacaciones.persona',
          empresa: 'persona.empresa',
          histCargos: 'persona.histCargos',
          cargo: 'histCargos.cargo',
          parentCargo: 'cargo.parentCargo',
          area: 'cargo.area'
        }
      },
      where: (qb) => {
        qb.where({
          // Filter Role fields
          id
        }).andWhere('histCargos.fechaFin IS NULL'); // Filter related field
      }
    });
    */
    return this.histVacacionesRepository
      .createQueryBuilder('histVacaciones')
      .innerJoinAndSelect('histVacaciones.persona', 'persona')
      .innerJoinAndSelect('persona.empresa', 'empresa')
      .innerJoinAndSelect('persona.histCargos', 'histCargos')
      .innerJoinAndSelect('histCargos.cargo', 'cargo')
      .innerJoinAndSelect('cargo.parentCargo', 'parentCargo')
      .innerJoinAndSelect('cargo.area', 'area')
      .where('histCargos.fechaFin IS NULL AND histVacaciones.id = :id', { id })
      .getOne();
  }

  findAllByPersona(
    personaId: number,
    options: IPaginationOptions
  ): Promise<Pagination<HistVacaciones>> {
    return paginate<HistVacaciones>(this.histVacacionesRepository, options, {
      where: { persona: personaId },
      order: { fechaSolicitud: 'DESC' }
    });
  }

  async create(
    histVacacionesData: CreateHistVacacionesDTO,
    firmaSolicitante: Express.Multer.File
  ): Promise<HistVacaciones> {
    let histVacaciones = await this.histVacacionesRepository.save(histVacacionesData);
    let mediaExt = firmaSolicitante.originalname.replace(/(^.*\.)/gi, '.');
    let urlImagen = `https://${AWS_S3_BUCKET_NAME}/complex/hist-vacaciones/firma-solicitante/vacaciones_firma_empleado_${
      histVacaciones.id
    }${mediaExt}`;
    await this.s3Service.uploadFirmaSolicitanteVacaciones(
      histVacaciones.id,
      firmaSolicitante,
      mediaExt
    );
    histVacaciones.firmaSolicitante = urlImagen;
    return this.histVacacionesRepository.save(histVacaciones);
  }

  async updateBySupervisor(
    histVacaciones: HistVacaciones,
    firmaSupervisor: Express.Multer.File
  ): Promise<HistVacaciones> {
    let mediaExt = firmaSupervisor.originalname.replace(/(^.*\.)/gi, '.');
    let urlImagen = `https://${AWS_S3_BUCKET_NAME}/complex/hist-vacaciones/firma-supervisor/vacaciones_firma_supervisor_${
      histVacaciones.id
    }${mediaExt}`;
    await this.s3Service.uploadFirmaSupervisorVacaciones(
      histVacaciones.id,
      firmaSupervisor,
      mediaExt
    );
    histVacaciones.firmaSupervisor = urlImagen;
    return this.histVacacionesRepository.save(histVacaciones);
  }

  async updateByTh(
    histVacaciones: HistVacaciones,
    firmaTh: Express.Multer.File
  ): Promise<HistVacaciones> {
    let mediaExt = firmaTh.originalname.replace(/(^.*\.)/gi, '.');
    let urlImagen = `https://${AWS_S3_BUCKET_NAME}/complex/hist-vacaciones/firma-th/vacaciones_firma_th_${
      histVacaciones.id
    }${mediaExt}`;
    await this.s3Service.uploadFirmaThVacaciones(histVacaciones.id, firmaTh, mediaExt);
    histVacaciones.firmaTh = urlImagen;
    return this.histVacacionesRepository.save(histVacaciones);
  }

  findAllCurrentMonth(): Promise<HistVacaciones[]> {
    return this.histVacacionesRepository
      .createQueryBuilder('histVacaciones')
      .where('EXTRACT(MONTH FROM histVacaciones.fechaSolicitud) = EXTRACT(MONTH FROM NOW())')
      .getMany();
  }

  findAllPendientes(): Promise<number> {
    return this.histVacacionesRepository.count({
      where: [
              {estatus: SolicitudEstatus.PENDIENTE},
              {estatus: SolicitudEstatus.APROBADA_SUPERVISOR}],
    });
  }

  @Cron('0 30 23 * * 1-5')
  async handleCron() {
    const minVacaciones = 14;

    const personas = await this.personaService.findAllWithoutPagination();
    const fechaActual = new Date();

    for (let index = 0; index < personas.length; index++) {
      const persona = personas[index];
      const lastHistDiasDisp = await this.diasDispService.findLastest(persona.id);
      if (lastHistDiasDisp.fechaSolicitud.getFullYear() !== fechaActual.getFullYear()) {
        const diferenciaTiempo = fechaActual.getFullYear() - persona.fechaIngreso.getFullYear();
        let diasNuevos = diferenciaTiempo + minVacaciones;
        if (diferenciaTiempo >= 17) {
          diasNuevos = 30;
        }
        const histDiasDisp = {
          fechaSolicitud: fechaActual,
          diasDisponibles: diasNuevos,
          persona
        };
        await this.diasDispService.create(histDiasDisp);
      }
    }
  }
}
