import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { HistVacaciones } from '../../models/hist-vacaciones/hist-vacaciones.entity';
import { HistVacacionesService } from './hist-vacaciones/hist-vacaciones.service';
import { HistVacacionesController } from './hist-vacaciones/hist-vacaciones.controller';
import { CargoService } from '../cargo/cargo/cargo.service';
import { CargoModule } from '../cargo/cargo.module';
import { PersonaModule } from '../persona/persona.module';
import { PersonaService } from '../persona/persona/persona.service';
import { Aws3CntmModule } from '../aws3-cntm/aws3-cntm.module';
import { Aws3CntmService } from '../aws3-cntm/aws3-cntm/aws3-cntm.service';
import { HistDiasDispVacacionesService } from '../hist-dias-disp-vacaciones/hist-dias-disp-vacaciones/hist-dias-disp-vacaciones.service';
import { HistDiasDispVacacionesModule } from '../hist-dias-disp-vacaciones/hist-dias-disp-vacaciones.module';
import { ConfigModule } from '../../config/config.module';
import { NotificacionesModule } from '../notificaciones/notificaciones.module';
import { HelpersModule } from '../helpers/helpers.module';
import { HelpersService } from '../helpers/helpers/helpers/helpers.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([HistVacaciones]),
    CargoModule,
    forwardRef(() => PersonaModule),
    ConfigModule,
    NotificacionesModule,
    forwardRef(() => HistDiasDispVacacionesModule),
    Aws3CntmModule,
    HelpersModule
  ],
  providers: [
    HistVacacionesService,
    CargoService,
    PersonaService,
    Aws3CntmService,
    HistDiasDispVacacionesService,
    HelpersService
  ],
  exports: [HistVacacionesService],
  controllers: [HistVacacionesController]
})
export class HistVacacionesModule {}
