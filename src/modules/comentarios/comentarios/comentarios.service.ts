import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Comentario } from '../../../models/comentario/comentario.entity';
import { Repository } from 'typeorm';
import { CreateComentarioDTO } from '../../../models/comentario/dto/create-comentario.dto';

@Injectable()
export class ComentariosService {
  constructor(
    @InjectRepository(Comentario) private readonly comentariosRepository: Repository<Comentario>
  ) {}

  findAll(): Promise<Comentario[]> {
    return this.comentariosRepository.find({
      order: {
        fechaPublicacion: 'DESC'
      }
    });
  }

  async findByNoticia(noticiaId: number): Promise<Comentario[]> {
    return this.comentariosRepository.find({
      relations: ['persona'],
      where: {
        noticia: noticiaId
      }
    });
  }

  create(comentario: CreateComentarioDTO): Promise<Comentario> {
    return this.comentariosRepository.save(comentario);
  }
}
