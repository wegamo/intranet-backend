import { Body, Controller, Get, Param, Post, UseFilters, Request, UseGuards } from '@nestjs/common';

import { ComentariosService } from './comentarios.service';
import { GlobalExceptionFilter } from './../../exception/global-exception.filter';
import { Comentario } from '../../../models/comentario/comentario.entity';
import { AuthGuard } from '@nestjs/passport';
import { Persona } from '../../../models/persona/persona.entity';
import { CreateComentarioDTO } from '../../../models/comentario/dto/create-comentario.dto';
import { ApiBearerAuth, ApiUseTags } from '@nestjs/swagger';

@ApiBearerAuth()
@ApiUseTags('Comentario')
@Controller('comentarios')
export class ComentariosController {
  constructor(private readonly comentariosService: ComentariosService) {}

  @UseGuards(AuthGuard('jwt'))
  @Get('noticia/:noticiaId')
  findByNoticia(@Param('noticiaId') noticiaId: number): Promise<Comentario[]> {
    return this.comentariosService.findByNoticia(noticiaId);
  }

  @UseGuards(AuthGuard('jwt'))
  @Post('create')
  create(
    @Request() { user }: { user: Persona },
    @Body() comentario: CreateComentarioDTO
  ): Promise<Comentario> {
    return this.comentariosService.create({ ...comentario, persona: user });
  }
}
