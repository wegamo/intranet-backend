import { Test, TestingModule } from '@nestjs/testing';
import { CreateHistPrestacionesDTO } from '../../../models/hist-prestaciones/dto/create-hist-prestaciones.dto';
import { UpdateHistPrestacionesDTO } from '../../../models/hist-prestaciones/dto/update-hist-prestaciones.dto';
import { HistPrestaciones } from '../../../models/hist-prestaciones/hist-prestaciones.entity';
import { SampleHistPrestaciones, SamplePersona } from '../../../../test/ObjectSamples';
import { DeleteResult, UpdateResult } from 'typeorm';
import { HistPrestacionesController } from './hist-prestaciones.controller';
import { HistPrestacionesService } from './hist-prestaciones.service';
import { Pagination } from 'nestjs-typeorm-paginate';
import { SolicitudEstatus } from '../../../enums/solicitud-estatus.enum';
import { PersonaService } from '../../persona/persona/persona.service';

describe('HistPrestaciones Controller', () => {
  let controller: HistPrestacionesController;
  let histPrestacionesService: HistPrestacionesService;
  let HistPrestacionesServiceMock: jest.Mock<Partial<HistPrestacionesService>>;
  let PersonaServiceMock: jest.Mock<Partial<PersonaService>>;


  beforeEach(() => {
    HistPrestacionesServiceMock = jest.fn<Partial<HistPrestacionesService>, HistPrestacionesService[]>(() => ({
      findAll: jest.fn(),
      findByPerson: jest.fn(),
      create: jest.fn(),
      delete: jest.fn(),
      update: jest.fn()
    }));
    PersonaServiceMock = jest.fn<Partial<PersonaService>, PersonaService[]>(() => ({
    
    }));
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [HistPrestacionesController],
      providers: [
        { provide: HistPrestacionesService, useValue: new HistPrestacionesServiceMock() },
        { provide: PersonaService, useValue: new PersonaServiceMock() }
      ]
    }).compile();
    histPrestacionesService = module.get<HistPrestacionesService>(HistPrestacionesService);
    controller = module.get<HistPrestacionesController>(HistPrestacionesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  
  describe('findAll()', () => {
    let expectedHistPrestacionesss: HistPrestaciones[];
    let result: Pagination<HistPrestaciones>;

    describe('case : success', () => {
      describe('when there are roles', () => {
        beforeEach(async () => {
          expectedHistPrestacionesss = [SampleHistPrestaciones];
          (histPrestacionesService.findAll as jest.Mock).mockResolvedValue(expectedHistPrestacionesss);
          result = await controller.getAll();
        });
        it('should invoke HistPrestacionesService.getAll()', () => {
          expect(histPrestacionesService.findAll).toHaveBeenCalledTimes(1);
        });
        it('should return an array of roles', () => {
          expect(result.items).toStrictEqual(expectedHistPrestacionesss);
        });
      });
    });

    describe('case : failure', () => {
      describe('when there are no roles', () => {
        beforeEach(async () => {
          expectedHistPrestacionesss = [];
          (histPrestacionesService.findAll as jest.Mock).mockResolvedValue(expectedHistPrestacionesss);
          result = await controller.getAll();
        });

        it('should invoke HistPrestacionesService.getAll()', () => {
          expect(histPrestacionesService.findAll).toHaveBeenCalledTimes(1);
        });

        it('should return an array empty', () => {
          expect(result).toStrictEqual(expectedHistPrestacionesss);
        });
      });
    });
  });

  //Test findByPerson()
  describe('findByPerson(id)', () => {
    let expectedHistPrestacioness: HistPrestaciones[];
    let result: HistPrestaciones[];

    describe('case : success', () => {
      describe('when an role is found', () => {
        beforeEach(async () => {
          expectedHistPrestacioness = [SampleHistPrestaciones];
          (histPrestacionesService.findAllByPersona as jest.Mock).mockResolvedValue(expectedHistPrestacioness);
          result = await controller.findById(SampleHistPrestaciones.persona.id);
        });
        it('should invoke HistPrestacionesService.findAllByPersona()', () => {
          expect(histPrestacionesService.findAllByPersona).toHaveBeenCalledTimes(1);
          expect(histPrestacionesService.findAllByPersona).toHaveBeenCalledWith(1);
        });
        it('should return an role', () => {
          expect(result).toStrictEqual(expectedHistPrestacioness);
        });
      });
    });

    describe('case : failure', () => {
      describe('when an role is not found', () => {
        beforeEach(async () => {
          expectedHistPrestacioness = undefined;
          (histPrestacionesService.findAllByPersona as jest.Mock).mockResolvedValue(expectedHistPrestacioness);
          result = await controller.findById(SampleHistPrestaciones.persona.id);
        });
        it('should invoke HistPrestacionesService.findAllByPersona()', () => {
          expect(histPrestacionesService.findAllByPersona).toHaveBeenCalledTimes(1);
          expect(histPrestacionesService.findAllByPersona).toHaveBeenCalledWith(SampleHistPrestaciones.persona.id);
        });
        it('should return undefined', () => {
          expect(result).toStrictEqual(expectedHistPrestacioness);
        });
      });
    });
  });
  /*
  describe('create(CreateHistPrestacionessDTO)', () => {
    const { id, ...rest } = SampleHistPrestaciones;
    let body: CreateHistPrestacionesDTO;

    describe('case : success', () => {
      let expectedHistPrestacioness: HistPrestaciones;
      let result: HistPrestaciones;

      describe('when a role is created', () => {
        beforeEach(async () => {
          body = rest;
          expectedHistPrestacioness = SampleHistPrestaciones;
          (histPrestacionesService.create as jest.Mock).mockResolvedValue(expectedHistPrestacioness);
          result = await controller.create(body);
        });
        it('should invoke HistPrestacionesService.create()', () => {
          expect(histPrestacionesService.create).toHaveBeenCalledTimes(1);
          expect(histPrestacionesService.create).toHaveBeenCalledWith(body);
        });
        it('should create a role', () => {
          expect(result).toStrictEqual(expectedHistPrestacioness);
        });
      });
    });

    describe('case : failure', () => {
      let expectedError: TypeError;
      describe('when a role is not created', () => {
        beforeEach(async () => {
          body = undefined;
          expectedError = new TypeError();
          jest.spyOn(controller, 'create').mockRejectedValue(expectedError);
        });
        it('should throw when the body is undefined', async () => {
          await expect(controller.create(body)).rejects.toThrow(TypeError);
        });
        it('should not create a role', () => {
          expect(histPrestacionesService.create).not.toHaveBeenCalled();
        });
      });
    });
  });
  */
  //Test update()
  describe('update(id, UpdateHistPrestacionessDTO)', () => {
    let update: UpdateHistPrestacionesDTO;

    describe('case : success', () => {
      let updateResult: HistPrestaciones;
      let result: HistPrestaciones;

      describe('when an role is found', () => {
        beforeEach(async () => {
          update = {
            fechaRevision:new Date(),
            talentoHumano:SamplePersona,
            firmaTh:'SIGOUTPUT',
            estatus:SolicitudEstatus.APROBADA,
            montoAprobado:'117'
          };
          //updateResult = new UpdateResult();
          (histPrestacionesService.update as jest.Mock).mockResolvedValue(updateResult);
          result = await controller.update(1, update);
        });
        it('should invoke HistPrestacionesService.update()', () => {
          expect(histPrestacionesService.update).toHaveBeenCalledTimes(1);
          expect(histPrestacionesService.update).toHaveBeenCalledWith(1, update);
        });
        it('should update an role', () => {
          expect(result).toStrictEqual(updateResult);
        });
      });
    });

    describe('case : failure', () => {
      let expectedError: TypeError;

      describe('when an role is not found', () => {
        beforeEach(async () => {
          expectedError = new TypeError();
          update = undefined;
          jest.spyOn(controller, 'update').mockRejectedValue(expectedError);
        });
        it('should invoke HistPrestacionesService.update()', async () => {
          await expect(controller.update(1, update)).rejects.toThrow(TypeError);
        });
        it('should not update an role', () => {
          expect(histPrestacionesService.update).not.toHaveBeenCalled();
        });
      });
    });
  });
  //Test delete()
  describe('delete(id)', () => {
    let deleteResult: DeleteResult;
    let result: DeleteResult;

    describe('case : success', () => {
      describe('when an role is found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 1;
          (histPrestacionesService.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await controller.delete(1);
        });
        it('should invoke HistPrestacionesService.delete()', () => {
          expect(histPrestacionesService.delete).toHaveBeenCalledTimes(1);
          expect(histPrestacionesService.delete).toHaveBeenCalledWith(1);
        });
        it('should delete an role', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });

    describe('case : failure', () => {
      let expectedError: TypeError;

      describe('when an role is not found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 0;
          (histPrestacionesService.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await controller.delete(1);
        });

        it('should invoke HistPrestacionesService.delete()', () => {
          expect(histPrestacionesService.delete).toHaveBeenCalledTimes(1);
          expect(histPrestacionesService.delete).toHaveBeenCalledWith(1);
        });

        it('should not delete an role', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });
  });
});
