import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Put,
  Query,
  Request,
  UploadedFile,
  UploadedFiles,
  UseGuards,
  UseInterceptors
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Pagination } from 'nestjs-typeorm-paginate';
import { FileFieldsInterceptor, FileInterceptor } from '@nestjs/platform-express';
import { CreateHistPrestacionesDTO } from '../../../models/hist-prestaciones/dto/create-hist-prestaciones.dto';
import { UpdateHistPrestacionesDTO } from '../../../models/hist-prestaciones/dto/update-hist-prestaciones.dto';
import { HistPrestaciones } from '../../../models/hist-prestaciones/hist-prestaciones.entity';
import { HistPrestacionesService } from './hist-prestaciones.service';
import { PersonaService } from '../../persona/persona/persona.service';
import { Persona } from '../../../models/persona/persona.entity';
import { ConfigService } from '../../../config/config/config.service';
import { SolicitudEstatus } from '../../../enums/solicitud-estatus.enum';
import { MontoDisponiblePrestacionesService } from '../../monto-disponible-prestaciones/monto-disponible-prestaciones/monto-disponible-prestaciones.service';
import { sendEmail } from '../../../helpers/sendEmail';
import { NotificacionesService } from '../../../modules/notificaciones/notificaciones/notificaciones.service';
import { TipoNotificacion } from '../../../enums/tipo-notificacion.enum';
import { ApiUseTags } from '@nestjs/swagger';
import { HelpersService } from '../../helpers/helpers/helpers/helpers.service';
@ApiUseTags('Histórico de prestaciones')
@Controller('hist-prestaciones')
export class HistPrestacionesController {
  constructor(
    private readonly histPrestacionesService: HistPrestacionesService,
    private readonly personaService: PersonaService,
    private readonly _configService: ConfigService,
    private readonly montoDisponibleService: MontoDisponiblePrestacionesService,
    private readonly notificacionesService: NotificacionesService,
    private readonly helpersService: HelpersService
  ) {}

  @UseGuards(AuthGuard('jwt'))
  @Get('all')
  async getAll(
    @Query('page') page = 1,
    @Query('limit') limit = 10
  ): Promise<Pagination<HistPrestaciones>> {
    limit = limit > 100 ? 100 : limit;
    const histPrestaciones = await this.histPrestacionesService.findAll({
      page,
      limit
    });
    return histPrestaciones;
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('dashboard')
  async getDashboard(): Promise<any> {
    const histPrestaciones = await this.histPrestacionesService.findAllCurrentYear();
    const histPrestacionesPendientes = await this.histPrestacionesService.findAllPendientes();
    const estadisticas = [
      { month: 1, rejected: 0, approved: 0 },
      { month: 2, rejected: 0, approved: 0 },
      { month: 3, rejected: 0, approved: 0 },
      { month: 4, rejected: 0, approved: 0 },
      { month: 5, rejected: 0, approved: 0 },
      { month: 6, rejected: 0, approved: 0 },
      { month: 7, rejected: 0, approved: 0 },
      { month: 8, rejected: 0, approved: 0 },
      { month: 9, rejected: 0, approved: 0 },
      { month: 10, rejected: 0, approved: 0 },
      { month: 11, rejected: 0, approved: 0 },
      { month: 12, rejected: 0, approved: 0 }
    ];

    histPrestaciones.map((histPrestacion) => {
      const mes = histPrestacion.fechaSolicitud.getMonth();
      if (histPrestacion.estatus === SolicitudEstatus.APROBADA) {
        estadisticas[mes] = { ...estadisticas[mes], approved: estadisticas[mes].approved + 1 };
      } else {
        if (histPrestacion.estatus === SolicitudEstatus.RECHAZADA) {
          estadisticas[mes] = { ...estadisticas[mes], rejected: estadisticas[mes].rejected + 1 };
        }
      }
    });

    return { estadisticas, pendientes: histPrestacionesPendientes };
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('persona/:id')
  async findByPersona(
    @Param('id') id: number,
    @Query('page') page = 1,
    @Query('limit') limit = 10
  ): Promise<Pagination<HistPrestaciones>> {
    limit = limit > 100 ? 100 : limit;
    const histPrestaciones = await this.histPrestacionesService.findAllByPersona(id, {
      page,
      limit
    });
    return histPrestaciones;
  }

  @UseGuards(AuthGuard('jwt'))
  @Get(':id')
  async findById(@Param('id') id: number): Promise<any> {
    let histPrestaciones: any = await this.histPrestacionesService.findOneWithDetails(id);
    const cargoSupervisor = histPrestaciones.persona.histCargos[0].cargo.parentCargo;
    let supervisor = await this.personaService.findCurrentPersonaByCargo(cargoSupervisor.id);
    histPrestaciones.supervisor = supervisor;
    return histPrestaciones;
  }

  @UseGuards(AuthGuard('jwt'))
  @Post('create')
  @UseInterceptors(
    FileFieldsInterceptor([
      { name: 'firmaSolicitante', maxCount: 1 },
      { name: 'presupuesto', maxCount: 1 }
    ])
  )
  async create(
    @Body() histPrestacionesData: { body: string },
    @UploadedFiles() files,
    @Request() { user }: { user: Persona }
  ): Promise<HistPrestaciones> {
    try {
      const histPrestaciones: CreateHistPrestacionesDTO = JSON.parse(histPrestacionesData.body);
      histPrestaciones.persona = user;
      const montoDisponibleEntity = await this.montoDisponibleService.findCurrentByPersona(
        new Date(),
        user.id
      );
      if (!montoDisponibleEntity) {
        throw new HttpException(
          'No existen Montos Disponibles actualizados',
          HttpStatus.BAD_REQUEST
        );
      }
      const acumulado75 = parseInt(montoDisponibleEntity.montoDisponible) * 0.75;
      histPrestaciones.acumulado75 = acumulado75.toString();
      const histPrestacionesCreada = await this.histPrestacionesService.create(
        histPrestaciones,
        files.firmaSolicitante[0],
        files.presupuesto[0]
      );
      // Send Email to Talento Humano Department according to environment
      await this.helpersService.createPrestaciones(histPrestacionesCreada);

      return histPrestacionesCreada;
    } catch (err) {
      throw new HttpException(
        'Error al Crear el histórico de Prestaciones',
        HttpStatus.BAD_REQUEST
      );
    }
  }

  @UseGuards(AuthGuard('jwt'))
  @Put('update/:id')
  @UseInterceptors(FileInterceptor('file'))
  async update(
    @Param('id') id: number,
    @Body() histPrestacionesData: { body: string },
    @UploadedFile() file: Express.Multer.File,
    @Request() { user }: { user: Persona }
  ): Promise<HistPrestaciones> {
    let histPrestacionToChange = await this.histPrestacionesService.findOne(id);

    if (!histPrestacionToChange) {
      throw new HttpException('El historial de prestación no existe', HttpStatus.NOT_FOUND);
    }
    if (
      histPrestacionToChange.estatus === SolicitudEstatus.APROBADA ||
      histPrestacionToChange.estatus === SolicitudEstatus.RECHAZADA
    ) {
      throw new HttpException(
        'La solicitud ya ha sido revisada, no puede editarse',
        HttpStatus.BAD_REQUEST
      );
    }
    const histPrestacionesParse: UpdateHistPrestacionesDTO = JSON.parse(histPrestacionesData.body);
    if (histPrestacionesParse.estatus === SolicitudEstatus.PENDIENTE) {
      throw new HttpException(`Estatus Inválido`, HttpStatus.BAD_REQUEST);
    }
    if (
      histPrestacionesParse.motivoRechazo &&
      histPrestacionesParse.estatus === SolicitudEstatus.APROBADA
    ) {
      throw new HttpException(`Solicitud Inválida`, HttpStatus.BAD_REQUEST);
    }
    if (
      histPrestacionesParse.estatus === SolicitudEstatus.RECHAZADA &&
      !histPrestacionesParse.motivoRechazo
    ) {
      throw new HttpException(`Solicitud Inválida`, HttpStatus.BAD_REQUEST);
    }
    // Se verifica que la persona del request pertenece a talento humano
    const talentoHumano = await this.personaService.findOneHumanTalent(user.id);
    if (!talentoHumano) {
      throw new HttpException(
        'La persona que está tratando de modificar la solicitud no pertenece a talento humano',
        HttpStatus.BAD_REQUEST
      );
    }
    try {
      histPrestacionToChange = {
        ...histPrestacionToChange,
        fechaRevision: histPrestacionesParse.fechaRevision,
        fechaPago: histPrestacionesParse.fechaPago,
        motivoRechazo: histPrestacionesParse.motivoRechazo,
        talentoHumano: user,
        estatus: histPrestacionesParse.estatus
      };
      const histPrestacion = await this.histPrestacionesService.update(
        histPrestacionToChange,
        file
      );
      if (histPrestacion.estatus === SolicitudEstatus.RECHAZADA) {
        // Send email and notification according to environment
        this.helpersService.updatePrestacionesRechazada(histPrestacion);
      } else {
        const montoDisponible = await this.montoDisponibleService.findCurrentByPersona(
          new Date(),
          histPrestacionToChange.persona.id
        );
        const nuevoMontoDisponible =
          parseFloat(montoDisponible.montoDisponible) -
          parseFloat(histPrestacionToChange.montoSolicitado);
        const nuevoMontoDisponibleObjeto = {
          persona: histPrestacionToChange.persona,
          estatus: SolicitudEstatus.APROBADA,
          montoDisponible: nuevoMontoDisponible.toString(),
          fechaInicio: montoDisponible.fechaInicio,
          fechaFin: montoDisponible.fechaFin,
          fechaSolicitud: new Date()
        };
        await this.montoDisponibleService.create(nuevoMontoDisponibleObjeto);
        await this.montoDisponibleService.update({
          ...montoDisponible,
          fechaFin: histPrestacionToChange.fechaSolicitud
        });
        // Send email and notification according to environment
        this.helpersService.updatePrestacionesAprobada(histPrestacionToChange);
      }
      return histPrestacion;
    } catch (err) {
      throw new HttpException(
        'Error al actualizar el historial de prestación',
        HttpStatus.BAD_REQUEST
      );
    }
  }
}
