import { Test, TestingModule } from '@nestjs/testing';
import { HistPrestaciones } from '../../../models/hist-prestaciones/hist-prestaciones.entity';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { HistPrestacionesService } from './hist-prestaciones.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { UpdateHistPrestacionesDTO } from '../../../models/hist-prestaciones/dto/update-hist-prestaciones.dto';
import { CreateHistPrestacionesDTO } from '../../../models/hist-prestaciones/dto/create-hist-prestaciones.dto';
import { SampleHistPrestaciones, SamplePersona } from '../../../../test/ObjectSamples';
import { Pagination } from 'nestjs-typeorm-paginate';
import { SolicitudEstatus } from '../../../enums/solicitud-estatus.enum';
import { Aws3CntmService } from '../../aws3-cntm/aws3-cntm/aws3-cntm.service';

describe('HistPrestacionesService', () => {
  let service: HistPrestacionesService;
  let RepositoryMock: jest.Mock;
  let repository: Repository<HistPrestaciones>;
  
  beforeEach(() => {
    RepositoryMock = jest.fn(() => ({
      find: jest.fn(),
      save: jest.fn(),
      delete: jest.fn(),
      update:jest.fn(),
      createQueryBuilder: jest.fn(() => ({
        take: jest.fn().mockReturnThis(),
      })),
    }));
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        HistPrestacionesService,
        { provide: getRepositoryToken(HistPrestaciones), useValue: new RepositoryMock() },
        Aws3CntmService,
      ],
    }).compile();
    repository = module.get(getRepositoryToken(HistPrestaciones));
    service = module.get<HistPrestacionesService>(HistPrestacionesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('findAll({page:1,limit:10})', () => {
    let expectedHistPrestaciones: HistPrestaciones[];
    let result: Pagination<HistPrestaciones>;

    describe('case : success', () => {
      describe('', () => {
        beforeEach(async () => {
          expectedHistPrestaciones = [SampleHistPrestaciones];
          (repository.find as jest.Mock).mockResolvedValue(expectedHistPrestaciones);
          result = await service.findAll({page:1,limit:10});
        });

        /*it('should invoke repository.find()', () => {
          expect(repository.find).toHaveBeenCalledTimes(1);
        });*/

        it('should return an array of HistPrestaciones', () => {
          expect(result.items).toStrictEqual(expectedHistPrestaciones);
        });
      });
    });

    describe('case : failure', () => {
      describe('when there are not HistPrestaciones', () => {
        beforeEach(async () => {
          expectedHistPrestaciones = [];
          (repository.find as jest.Mock).mockResolvedValue(expectedHistPrestaciones);
          result = await service.findAll({page:1,limit:10});
        });

        /*it('should invoke repository.find()', () => {
          expect(repository.find).toHaveBeenCalledTimes(1);
        });*/

        it('should return an empty array', () => {
          expect(result).toStrictEqual(expectedHistPrestaciones);
        });
      });
    });
  });


  describe('findByID(id)', () => {
    let expectedHistPrestaciones: HistPrestaciones[];
    let result: Pagination<HistPrestaciones>;

    describe('case : success', () => {
      describe('when an HistPrestaciones is found', () => {
        beforeEach(async () => {
          expectedHistPrestaciones = [SampleHistPrestaciones];
          (repository.find as jest.Mock).mockResolvedValue(expectedHistPrestaciones);
          result = await service.findAllByPersona(SampleHistPrestaciones.persona.id,{page:1,limit:10});
        });

        it('should invoke repository.findOne()', () => {
          expect(repository.find).toHaveBeenCalledTimes(1);
          expect(repository.find).toHaveBeenCalledWith({persona:{id:SampleHistPrestaciones.persona.id}});
        });

        it('should return an HistPrestaciones', () => {
          expect(result).toStrictEqual(expectedHistPrestaciones);
        });
      });
    });

    describe('case : failure', () => {
      describe('when an HistPrestaciones is not found', () => {
        beforeEach(async () => {
          expectedHistPrestaciones = undefined;
          (repository.find as jest.Mock).mockResolvedValue(expectedHistPrestaciones);
          result = await service.findAllByPersona(SampleHistPrestaciones.persona.id,{page:1,limit:10});
        });

        it('should invoke repository.findOne()', () => {
          expect(repository.find).toHaveBeenCalledTimes(1);
          expect(repository.find).toHaveBeenCalledWith({persona:{id:SampleHistPrestaciones.persona.id}});
        });

        it('should return undefined', () => {
          expect(result).toStrictEqual(expectedHistPrestaciones);
        });
      });
    });
  });
  /*
  describe('create(CreateHistPrestacionesDTO)', () => {
    const { id, ...rest } = SampleHistPrestaciones;
    let body: CreateHistPrestacionesDTO;

    describe('case : success', () => {
      let expectedHistPrestaciones: HistPrestaciones;
      let result: HistPrestaciones;

      describe('when an HistPrestaciones is created', () => {
        beforeEach(async () => {
          body = rest;
          expectedHistPrestaciones = SampleHistPrestaciones;
          (repository.save as jest.Mock).mockResolvedValue(expectedHistPrestaciones);
          result = await service.create(body);
        });

        it('should invoke repository.save()', () => {
          expect(repository.save).toHaveBeenCalledTimes(1);
          expect(repository.save).toHaveBeenCalledWith(body);
        });

        it('should create a HistPrestaciones', () => {
          expect(result).toStrictEqual(expectedHistPrestaciones);
        });
      });
    });

    describe('case : failure', () => {
      let expectedError: TypeError;
      let body: undefined;

      describe('when a HistPrestaciones is not created', () => {
        beforeEach(async () => {
          body = undefined;
          expectedError = new TypeError();
          jest.spyOn(service, 'create').mockRejectedValue(expectedError);
        });

        it('it should throw when the body is undefined', async () => {
          await expect(service.create(body)).rejects.toThrow(TypeError);
        });

        it('should not create an HistPrestaciones', () => {
          expect(repository.save).not.toHaveBeenCalled();
        });
      });
    });
  });
  */
  describe('update(id, UpdateHistPrestacionesDTO)', () => {
    let update: HistPrestaciones;

    describe('case : success', () => {
      let updateResult: HistPrestaciones;
      let result: HistPrestaciones;

      describe('when a HistPrestaciones is updated', () => {
        beforeEach(async () => {
          update = {
            fechaRevision:new Date(),
            talentoHumano:SamplePersona,
            firmaTh:'SIGOUTPUT',
            estatus:SolicitudEstatus.APROBADA,
            montoAprobado:'117',
            ...SampleHistPrestaciones
          };
          //updateResult = new UpdateResult();
          (repository.save as jest.Mock).mockResolvedValue(updateResult);
          result = await service.update(update);
        });

        it('should invoke repository.update()', () => {
          expect(repository.save).toHaveBeenCalledTimes(1);
          expect(repository.save).toHaveBeenCalledWith(1, update);
        });

        it('should update an HistPrestaciones', () => {
          expect(result).toStrictEqual(updateResult);
        });
      });
    });

    describe('case : failure', () => {
      let expectedError: TypeError;

      describe('when a HistPrestaciones is not updated', () => {
        beforeEach(async () => {
          expectedError = new TypeError();
          update = undefined;
          jest.spyOn(service, 'update').mockRejectedValue(expectedError);
        });

        it('should throw when the body is undefined', async () => {
          await expect(service.update(update)).rejects.toThrow(TypeError);
        });

        it('should not update an HistPrestaciones', () => {
          expect(repository.save).not.toHaveBeenCalled();
        });
      });
    });
  });

  describe('delete(id)', () => {
    let deleteResult: DeleteResult;
    let result: DeleteResult;

    describe('case : success', () => {
      describe('when an HistPrestaciones is found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 1;
          (repository.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await service.delete(1);
        });

        it('should invoke repository.delete()', () => {
          expect(repository.delete).toHaveBeenCalledTimes(1);
          expect(repository.delete).toHaveBeenCalledWith(1);
        });

        it('should delete an HistPrestaciones', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });

    describe('case : failure', () => {
      let expectedError: TypeError;

      describe('when a HistPrestaciones is not found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 0;
          (repository.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await service.delete(1);
        });

        it('should invoke repository.delete()', () => {
          expect(repository.delete).toHaveBeenCalledTimes(1);
          expect(repository.delete).toHaveBeenCalledWith(1);
        });

        it('should not delete an HistPrestaciones', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });
  });

});
