import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { HistPrestaciones } from '../../../models/hist-prestaciones/hist-prestaciones.entity';
import { DeleteResult, getRepository, IsNull, Repository, UpdateResult } from 'typeorm';
import { CreateHistPrestacionesDTO } from '../../../models/hist-prestaciones/dto/create-hist-prestaciones.dto';
import { IPaginationOptions, paginate, paginateRaw, Pagination } from 'nestjs-typeorm-paginate';
import { Aws3CntmService } from '../../aws3-cntm/aws3-cntm/aws3-cntm.service';
import configService from '../../../config/config/config.service';
import { SolicitudEstatus } from '../../../enums/solicitud-estatus.enum';

const AWS_S3_BUCKET_NAME = configService.get('AWS_S3_BUCKET_NAME');

@Injectable()
export class HistPrestacionesService {
  constructor(
    @InjectRepository(HistPrestaciones)
    private readonly histPrestacionesRepository: Repository<HistPrestaciones>,
    private readonly s3Service: Aws3CntmService
  ) {}

  findAll(options: IPaginationOptions): Promise<Pagination<HistPrestaciones>> {
    const queryBuilder = this.histPrestacionesRepository.createQueryBuilder('histPrestaciones');
    queryBuilder
      .select([
        'histPrestaciones.id',
        'histPrestaciones.presupuesto',
        'histPrestaciones.fechaSolicitud',
        'histPrestaciones.montoSolicitado',
        'histPrestaciones.estatus'
      ])
      .innerJoin('histPrestaciones.persona', 'persona')
      .addSelect(['persona.primerNombre', 'persona.primerApellido', 'persona.imgPerfil'])
      .leftJoinAndSelect('persona.montoPresDisponible', 'montoDisponible')
      .where('montoDisponible.fechaFin > histPrestaciones.fechaSolicitud')
      .orderBy('histPrestaciones.fechaSolicitud', 'DESC');

    return paginateRaw<HistPrestaciones>(queryBuilder, options);
  }

  findMontoDisponibleByFechaAndPersona(fecha: Date, idPersona): Promise<HistPrestaciones> {
    return this.histPrestacionesRepository
      .createQueryBuilder('histPrestaciones')
      .innerJoinAndSelect('histPrestaciones.persona', 'persona')
      .innerJoinAndSelect('persona.montoPresDisponible', 'montoDisponible')
      .where('montoDisponible.fechaFin >= :fecha AND persona.id = :idPersona', { fecha, idPersona })
      .getOne();
  }

  findOnePRUEBA(id: number): Promise<HistPrestaciones> {
    return getRepository(HistPrestaciones)
      .createQueryBuilder('histPrestaciones')
      .innerJoinAndSelect('histPrestaciones.persona', 'persona')
      .innerJoinAndSelect('persona.histCargos', 'histCargos')
      .innerJoinAndSelect('histCargos.cargo', 'cargo')
      .innerJoinAndSelect('cargo.area', 'area')
      .innerJoinAndSelect('cargo.parentCargo', 'parentCargo')
      .innerJoinAndSelect('persona.empresa', 'empresa')
      .where('histPrestaciones.id = :id', { id })
      .andWhere('histCargos.fechaFin = :fechaFin', { fechaFin: null })
      .getOne();
  }

  findOneWithDetails(id: number): Promise<HistPrestaciones> {
    return this.histPrestacionesRepository
      .createQueryBuilder('histPrestaciones')
      .innerJoinAndSelect('histPrestaciones.persona', 'persona')
      .innerJoinAndSelect('persona.empresa', 'empresa')
      .innerJoinAndSelect('persona.histCargos', 'histCargos')
      .innerJoinAndSelect('histCargos.cargo', 'cargo')
      .innerJoinAndSelect('cargo.parentCargo', 'parentCargo')
      .innerJoinAndSelect('cargo.area', 'area')
      .where('histCargos.fechaFin IS NULL AND histPrestaciones.id = :id', { id })
      .getOne();
  }

  findOne(id: number): Promise<HistPrestaciones> {
    return this.histPrestacionesRepository.findOne(id, {
      relations: ['persona']
    });
  }

  async findAllByPersona(
    personaId: number,
    options: IPaginationOptions
  ): Promise<Pagination<HistPrestaciones>> {
    return paginate<HistPrestaciones>(this.histPrestacionesRepository, options, {
      order: { fechaSolicitud: 'DESC' },
      where: { persona: personaId }
    });
  }

  findAllPendienteByPersona(id: number): Promise<number> {
    return this.histPrestacionesRepository.count({
      where: {
        persona: id,
        estatus: SolicitudEstatus.PENDIENTE
      }
    });
  }
  async create(
    histPrestacionesData: CreateHistPrestacionesDTO,
    firmaSolicitante: Express.Multer.File,
    presupuesto: Express.Multer.File
  ): Promise<HistPrestaciones> {
    let histPrestaciones = await this.histPrestacionesRepository.save(histPrestacionesData);
    // Guardando firma en AWS
    let mediaExtFirma = firmaSolicitante.originalname.replace(/(^.*\.)/gi, '.');
    let urlFirma = `https://${AWS_S3_BUCKET_NAME}/complex/hist-prestaciones/firma-solicitante/prestaciones_firma_empleado_${
      histPrestaciones.id
    }${mediaExtFirma}`;
    await this.s3Service.uploadFirmaSolicitantePrestaciones(
      histPrestaciones.id,
      firmaSolicitante,
      mediaExtFirma
    );
    // Guardando presupuesto en AWS
    let mediaExtPresupuesto = presupuesto.originalname.replace(/(^.*\.)/gi, '.');
    let urlPresupuesto = `https://${AWS_S3_BUCKET_NAME}/complex/hist-prestaciones/presupuesto/prestaciones_presupuesto_${
      histPrestaciones.id
    }${mediaExtPresupuesto}`;
    await this.s3Service.uploadPresupuestoPrestaciones(
      histPrestaciones.id,
      presupuesto,
      mediaExtPresupuesto
    );
    // Actualizando en la base de datos
    histPrestaciones.firmaSolicitante = urlFirma;
    histPrestaciones.presupuesto = urlPresupuesto;
    return this.histPrestacionesRepository.save(histPrestaciones);
  }

  async update(
    histPrestaciones: HistPrestaciones,
    firmaTh: Express.Multer.File
  ): Promise<HistPrestaciones> {
    let mediaExt = firmaTh.originalname.replace(/(^.*\.)/gi, '.');
    let urlImagen = `https://${AWS_S3_BUCKET_NAME}/complex/hist-prestaciones/firma-th/prestaciones_firma_th_${
      histPrestaciones.id
    }${mediaExt}`;
    await this.s3Service.uploadFirmaThPrestaciones(histPrestaciones.id, firmaTh, mediaExt);
    histPrestaciones.firmaTh = urlImagen;
    return this.histPrestacionesRepository.save(histPrestaciones);
  }

  findAllCurrentYear(): Promise<HistPrestaciones[]> {
    return this.histPrestacionesRepository
      .createQueryBuilder('histPrestaciones')
      .where('EXTRACT(YEAR FROM histPrestaciones.fechaSolicitud) = EXTRACT(YEAR FROM NOW())')
      .getMany();
  }

  findAllPendientes(): Promise<number> {
    return this.histPrestacionesRepository.count({
      where: {estatus: SolicitudEstatus.PENDIENTE}
    });
  }
}
