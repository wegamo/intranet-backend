import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '../../config/config.module';
import { HistPrestaciones } from '../../models/hist-prestaciones/hist-prestaciones.entity';
import { Aws3CntmModule } from '../aws3-cntm/aws3-cntm.module';
import { Aws3CntmService } from '../aws3-cntm/aws3-cntm/aws3-cntm.service';
import { HelpersModule } from '../helpers/helpers.module';
import { HelpersService } from '../helpers/helpers/helpers/helpers.service';
import { MontoDisponiblePrestacionesModule } from '../monto-disponible-prestaciones/monto-disponible-prestaciones.module';
import { MontoDisponiblePrestacionesService } from '../monto-disponible-prestaciones/monto-disponible-prestaciones/monto-disponible-prestaciones.service';
import { NotificacionesModule } from '../notificaciones/notificaciones.module';
import { PersonaModule } from '../persona/persona.module';
import { PersonaService } from '../persona/persona/persona.service';
import { HistPrestacionesController } from './hist-prestaciones/hist-prestaciones.controller';
import { HistPrestacionesService } from './hist-prestaciones/hist-prestaciones.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([HistPrestaciones]),
    forwardRef(() => PersonaModule),
    Aws3CntmModule,
    ConfigModule,
    NotificacionesModule,
    MontoDisponiblePrestacionesModule,
    HelpersModule
  ],
  controllers: [HistPrestacionesController],
  providers: [
    HistPrestacionesService,
    PersonaService,
    Aws3CntmService,
    MontoDisponiblePrestacionesService,
    HelpersService
  ],
  exports: [HistPrestacionesService]
})
export class HistPrestacionesModule {}
