import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { Norma } from '../../models/norma/norma.entity';

import { NormaController } from './norma/norma.controller';
import { NormaService } from './norma/norma.service';

@Module({
  imports: [TypeOrmModule.forFeature([Norma])],
  providers: [NormaService],
  controllers: [NormaController]
})
export class NormaModule {}
