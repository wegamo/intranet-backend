import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';

import { CreateNormaDTO } from '../../../models/norma/dto/create-norma.dto';
import { UpdateNormaDTO } from '../../../models/norma/dto/update-norma.dto';
import { Norma } from '../../../models/norma/norma.entity';

@Injectable()
export class NormaService {
  constructor(
    @InjectRepository(Norma)
    private readonly normaRepository: Repository<Norma>
  ) {}

  async findAll(): Promise<Norma[]> {
    return await this.normaRepository.find();
  }

  async findOne(id: number): Promise<Norma> {
    return await this.normaRepository.findOne(id);
  }

  async create(norma: CreateNormaDTO): Promise<Norma> {
    return await this.normaRepository.save(norma);
  }

  async update(id: number, norma: UpdateNormaDTO): Promise<UpdateResult> {
    return await this.normaRepository.update(id, norma);
  }

  async delete(id: number): Promise<DeleteResult> {
    return await this.normaRepository.delete(id);
  }
}
