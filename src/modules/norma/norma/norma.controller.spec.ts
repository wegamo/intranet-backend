import { Test, TestingModule } from '@nestjs/testing';
import { DeleteResult, UpdateResult } from 'typeorm';

import { CreateNormaDTO } from '../../../models/norma/dto/create-norma.dto';
import { UpdateNormaDTO } from '../../../models/norma/dto/update-norma.dto';
import { Norma } from '../../../models/norma/norma.entity';

import { NormaTipo } from '../../../models/norma/const/norma-tipo.enum';

import { NormaController } from './norma.controller';
import { NormaService } from './norma.service';

import { GlobalException } from './../../exception/global-exception';
import { NormaInt } from './../../dto_response/Norma_Payload';
import { SampleNorma } from './../../../../test/ObjectSamples';

describe('Norma Controller', () => {
  let controller: NormaController;
  let NormaServiceMock: jest.Mock<Partial<NormaService>>;
  let normaService: NormaService;

  beforeEach(() => {
    NormaServiceMock = jest.fn<Partial<NormaService>, NormaService[]>(() => ({
      findAll: jest.fn(),
      findOne: jest.fn(),
      create: jest.fn(),
      delete: jest.fn(),
      update: jest.fn()
    }));
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [NormaController],
      providers: [{ provide: NormaService, useValue: new NormaServiceMock() }]
    }).compile();

    normaService = module.get<NormaService>(NormaService);
    controller = module.get<NormaController>(NormaController);
  });

  describe('findAll()', () => {
    let expectedNormas: Norma[];
    let result: Norma[];

    describe('case: success', () => {
      describe('when there are rules', () => {
        beforeEach(async () => {
          expectedNormas = [
            SampleNorma
          ];
          (normaService.findAll as jest.Mock).mockResolvedValue(expectedNormas);
          result = await controller.findAll();
        });

        it('should invoke NormaService.findAll()', () => {
          expect(normaService.findAll).toHaveBeenCalledTimes(1);
        });

        it('should return an array of rules', () => {
          expect(result).toStrictEqual(expectedNormas);
        });
      });
    });

    describe('case: failure', () => {
      describe('when there are not rules', () => {
        beforeEach(async () => {
          expectedNormas = [];
          (normaService.findAll as jest.Mock).mockResolvedValue(expectedNormas);
          result = await controller.findAll();
        });

        it('should invoke NormaService.findAll()', () => {
          expect(normaService.findAll).toHaveBeenCalledTimes(1);
        });

        it('should return an array empty', () => {
          expect(result).toStrictEqual(expectedNormas);
        });
      });
    });
  });

  describe('findOne(id)', () => {
    let expectedNorma: Norma;
    let result: Norma;

    describe('case: success', () => {
      describe('when a rule is found', () => {
        beforeEach(async () => {
          expectedNorma = SampleNorma;
          (normaService.findOne as jest.Mock).mockResolvedValue(expectedNorma);
          result = await controller.findOne(1);
        });

        it('should invoke NormaService.findOne()', () => {
          expect(normaService.findOne).toHaveBeenCalledTimes(1);
          expect(normaService.findOne).toHaveBeenCalledWith(1);
        });

        it('should return a rule', () => {
          expect(result).toStrictEqual(expectedNorma);
        });
      });
    });

    describe('case: failure', () => {
      describe('when a rule is not found', () => {
        beforeEach(async () => {
          expectedNorma = undefined;
          (normaService.findOne as jest.Mock).mockResolvedValue(expectedNorma);
          result = await controller.findOne(1);
        });

        it('should invoke NormaService.findOne()', () => {
          expect(normaService.findOne).toHaveBeenCalledTimes(1);
          expect(normaService.findOne).toHaveBeenCalledWith(1);
        });

        it('should return undefined', () => {
          expect(result).toStrictEqual(expectedNorma);
        });
      });
    });
  });

  describe('create(normaData)', () => {
    const {id,...rest} = SampleNorma;
    let body: CreateNormaDTO;

    describe('case: success', () => {
      let expectedNorma: Norma;
      let result: Norma;

      describe('when a rule is created', () => {
        beforeEach(async () => {
          body = rest;
          expectedNorma = SampleNorma;
          (normaService.create as jest.Mock).mockResolvedValue(expectedNorma);
          result = await controller.create(body);
        });

        it('should invoke NormaService.create()', () => {
          expect(normaService.create).toHaveBeenCalledTimes(1);
          expect(normaService.create).toHaveBeenCalledWith(body);
        });

        it('should create a rule', () => {
          expect(result).toStrictEqual(expectedNorma);
        });
      });
    });

    describe('case: failure', () => {
      let expectedError: TypeError;

      describe('when a rule is not created', () => {
        beforeEach(async () => {
          body = undefined;
          expectedError = new TypeError();
          jest.spyOn(controller, 'create').mockRejectedValue(expectedError);
        });

        it('should throw when the body is undefined', async () => {
          await expect(controller.create(body)).rejects.toThrow(TypeError);
        });

        it('should not create a rule', () => {
          expect(normaService.create).not.toHaveBeenCalled();
        });
      });
    });
  });

  describe('update(id, normaData)', () => {
    let update: UpdateNormaDTO;

    describe('case: success', () => {
      let updateResult: UpdateResult | GlobalException;
      let result: UpdateResult | GlobalException;

      describe('when a rule is updated', () => {
        beforeEach(async () => {
          update = {
            nombre: 'Test',
            descripcion: 'Test'
          };
          updateResult = new UpdateResult();
          (normaService.update as jest.Mock).mockResolvedValue(updateResult);
          result = await controller.update(1, update);
        });

        it('should invoke NormaService.update()', () => {
          expect(normaService.update).toHaveBeenCalledTimes(1);
          expect(normaService.update).toHaveBeenCalledWith(1, update);
        });

        it('should update a rule', () => {
          expect(result).toStrictEqual(updateResult);
        });
      });
    });

    describe('case: failure', () => {
      let expectedError: TypeError;

      describe('when a rule is not updated', () => {
        beforeEach(async () => {
          expectedError = new TypeError();
          update = undefined;
          jest.spyOn(controller, 'update').mockRejectedValue(expectedError);
        });

        it('should throw when the body is undefined', async () => {
          await expect(controller.update(1, update)).rejects.toThrow(TypeError);
        });

        it('should not update a rule', () => {
          expect(normaService.update).not.toHaveBeenCalled();
        });
      });
    });
  });

  describe('delete(id)', () => {
    let deleteResult: DeleteResult | GlobalException;
    let result: DeleteResult | GlobalException;

    describe('case: success', () => {
      describe('when a rule is found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 1;
          (normaService.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await controller.delete(1);
        });

        it('should invoke NormaService.delete()', () => {
          expect(normaService.delete).toHaveBeenCalledTimes(1);
          expect(normaService.delete).toHaveBeenCalledWith(1);
        });

        it('should delete a rule', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });

    describe('case: failure', () => {
      describe('when a rule is not found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 0;
          (normaService.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await controller.delete(1);
        });

        it('should invoke NormaService.delete()', () => {
          expect(normaService.delete).toHaveBeenCalledTimes(1);
          expect(normaService.delete).toHaveBeenCalledWith(1);
        });

        it('should not delete a rule', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });
  });
});
