import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';

import { NormaTipo } from '../../../models/norma/const/norma-tipo.enum';
import { CreateNormaDTO } from '../../../models/norma/dto/create-norma.dto';
import { UpdateNormaDTO } from '../../../models/norma/dto/update-norma.dto';
import { Norma } from '../../../models/norma/norma.entity';

import { NormaService } from './norma.service';

import { GlobalException } from './../../exception/global-exception';
import { NormaInt } from './../../dto_response/Norma_Payload';
import { SampleNorma } from './../../../../test/ObjectSamples';

describe('NormaService', () => {
  let service: NormaService;
  let RepositoryMock: jest.Mock;
  let repository: Repository<Norma>;

  beforeEach(() => {
    RepositoryMock = jest.fn(() => ({
      find: jest.fn(),
      findOne: jest.fn(),
      save: jest.fn(),
      update: jest.fn(),
      delete: jest.fn()
    }));
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        NormaService,
        { provide: getRepositoryToken(Norma), useValue: new RepositoryMock() }
      ]
    }).compile();

    repository = module.get(getRepositoryToken(Norma));
    service = module.get<NormaService>(NormaService);
  });

  describe('findAll()', () => {
    let expectedNormas: Norma[];
    let result: Norma[];

    describe('case: success', () => {
      describe('when there are rules', () => {
        beforeEach(async () => {
          expectedNormas = [
            SampleNorma
          ];
          (repository.find as jest.Mock).mockResolvedValue(expectedNormas);
          result = await service.findAll();
        });

        it('should invoke repository.find()', () => {
          expect(repository.find).toHaveBeenCalledTimes(1);
        });

        it('should return an array of rules', () => {
          expect(result).toStrictEqual(expectedNormas);
        });
      });
    });

    describe('case: failure', () => {
      describe('when there are not rules', () => {
        beforeEach(async () => {
          expectedNormas = [];
          (repository.find as jest.Mock).mockResolvedValue(expectedNormas);
          result = await service.findAll();
        });

        it('should invoke repository.find()', () => {
          expect(repository.find).toHaveBeenCalledTimes(1);
        });

        it('should return an array empty', () => {
          expect(result).toStrictEqual(expectedNormas);
        });
      });
    });
  });

  describe('findOne(id)', () => {
    let expectedNorma: Norma;
    let result: Norma;

    describe('case: success', () => {
      describe('when a rule is found', () => {
        beforeEach(async () => {
          expectedNorma = SampleNorma;
          (repository.findOne as jest.Mock).mockResolvedValue(expectedNorma);
          result = await service.findOne(1);
        });

        it('should invoke repository.findOne()', () => {
          expect(repository.findOne).toHaveBeenCalledTimes(1);
          expect(repository.findOne).toHaveBeenCalledWith(1);
        });

        it('should return a rule', () => {
          expect(result).toStrictEqual(expectedNorma);
        });
      });
    });

    describe('case: failure', () => {
      describe('when a rule is not found', () => {
        beforeEach(async () => {
          expectedNorma = undefined;
          (repository.findOne as jest.Mock).mockResolvedValue(expectedNorma);
          result = await service.findOne(1);
        });

        it('should invoke repository.findOne()', () => {
          expect(repository.findOne).toHaveBeenCalledTimes(1);
          expect(repository.findOne).toHaveBeenCalledWith(1);
        });

        it('should return undefined', () => {
          expect(result).toStrictEqual(expectedNorma);
        });
      });
    });
  });

  describe('create(norma)', () => {
    let body: CreateNormaDTO;

    describe('case: success', () => {
      const {id,...rest} = SampleNorma;
      let expectedNorma: Norma;
      let result: Norma;

      describe('when a rule is created', () => {
        beforeEach(async () => {
          body = rest;
          expectedNorma = SampleNorma;
          (repository.save as jest.Mock).mockResolvedValue(expectedNorma);
          result = await service.create(body);
        });

        it('should invoke repository.save()', () => {
          expect(repository.save).toHaveBeenCalledTimes(1);
          expect(repository.save).toHaveBeenCalledWith(body);
        });

        it('should create a rule', () => {
          expect(result).toStrictEqual(expectedNorma);
        });
      });
    });

    describe('case: failure', () => {
      let expectedError: TypeError;

      describe('when a rule is not created', () => {
        beforeEach(async () => {
          body = undefined;
          expectedError = new TypeError();
          jest.spyOn(service, 'create').mockRejectedValue(expectedError);
        });

        it('should throw when the body is undefined', async () => {
          await expect(service.create(body)).rejects.toThrow(TypeError);
        });

        it('should not create a rule', () => {
          expect(repository.save).not.toHaveBeenCalled();
        });
      });
    });
  });

  describe('update(id, norma)', () => {
    let update: UpdateNormaDTO;

    describe('case: success', () => {
      let updateResult: UpdateResult | GlobalException;
      let result: UpdateResult | GlobalException;

      describe('when a rule is updated', () => {
        beforeEach(async () => {
          update = {
            nombre: 'Test',
            descripcion: 'Test'
          };
          updateResult = new UpdateResult();
          (repository.update as jest.Mock).mockResolvedValue(updateResult);
          result = await service.update(1, update);
        });

        it('should invoke repository.update()', () => {
          expect(repository.update).toHaveBeenCalledTimes(1);
          expect(repository.update).toHaveBeenCalledWith(1, update);
        });

        it('should update a rule', () => {
          expect(result).toStrictEqual(updateResult);
        });
      });
    });

    describe('case: failure', () => {
      let expectedError: TypeError;

      describe('when a rule is not updated', () => {
        beforeEach(async () => {
          expectedError = new TypeError();
          update = undefined;
          jest.spyOn(service, 'update').mockRejectedValue(expectedError);
        });

        it('should throw when the body is undefined', async () => {
          await expect(service.update(1, update)).rejects.toThrow(TypeError);
        });

        it('should not update a rule', () => {
          expect(repository.update).not.toHaveBeenCalled();
        });
      });
    });
  });

  describe('delete(id)', () => {
    let deleteResult: DeleteResult | GlobalException;
    let result: DeleteResult | GlobalException;

    describe('case: success', () => {
      describe('when a rule is found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 1;
          (repository.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await service.delete(1);
        });

        it('should invoke repository.delete()', () => {
          expect(repository.delete).toHaveBeenCalledTimes(1);
          expect(repository.delete).toHaveBeenCalledWith(1);
        });

        it('should delete a rule', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });

    describe('case: failure', () => {
      describe('when a rule is not found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 0;
          (repository.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await service.delete(1);
        });

        it('should invoke repository.delete()', () => {
          expect(repository.delete).toHaveBeenCalledTimes(1);
          expect(repository.delete).toHaveBeenCalledWith(1);
        });

        it('should not delete a rule', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });
  });
});
