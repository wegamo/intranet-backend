import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { DeleteResult, UpdateResult } from 'typeorm';

import { CreateNormaDTO } from '../../../models/norma/dto/create-norma.dto';
import { UpdateNormaDTO } from '../../../models/norma/dto/update-norma.dto';
import { Norma } from '../../../models/norma/norma.entity';

import { NormaService } from './norma.service';

@Controller('norma')
export class NormaController {
  constructor(private readonly normaService: NormaService) {}

  @Get()
  findAll(): Promise<Norma[]> {
    return this.normaService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: number): Promise<Norma> {
    return this.normaService.findOne(id);
  }

  @Post()
  async create(@Body() normaData: CreateNormaDTO): Promise<Norma> {
    return this.normaService.create(normaData);
  }

  @Put(':id')
  async update(@Param('id') id: number, @Body() normaData: UpdateNormaDTO): Promise<UpdateResult> {
    return this.normaService.update(id, normaData);
  }

  @Delete(':id')
  async delete(@Param('id') id: number): Promise<DeleteResult> {
    return this.normaService.delete(id);
  }
}
