import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateTelefonoPersonaDTO } from '../../../models/telefono-persona/dto/create-telefono-persona.dto';
import { TelefonoPersona } from '../../../models/telefono-persona/telefono-persona.entity';
import { DeleteResult, Repository } from 'typeorm';

@Injectable()
export class TelefonoPersonaService {
    constructor(
        @InjectRepository(TelefonoPersona) private readonly telefonoPersonaRepository: Repository<TelefonoPersona>,
    ){}

    async create(telefonoPersona: CreateTelefonoPersonaDTO): Promise<TelefonoPersona> {
        return await this.telefonoPersonaRepository.save(telefonoPersona);
    }

    /*async update(id:number,telefonoPersona: UpdatetelefonoPersonaDTO): Promise<UpdateResult> {
        return await this.telefonoPersonaRepository.update(id,telefonoPersona);
    }*/

    async delete(id: number): Promise<DeleteResult> {
        try {
            return await this.telefonoPersonaRepository.delete(id);
        }
        catch (Err) {
            throw new HttpException(
                {
                  status: HttpStatus.INTERNAL_SERVER_ERROR,
                  error: 'Ha ocurrido un error al eliminar registro telefonico (personas)'
                },
                HttpStatus.INTERNAL_SERVER_ERROR
              );
        }
    }
}
