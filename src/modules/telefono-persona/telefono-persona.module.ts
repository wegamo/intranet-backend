import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TelefonoPersona } from '../../models/telefono-persona/telefono-persona.entity';
import { TelefonoPersonaService } from './telefono-persona/telefono-persona.service';

@Module({
  imports:[TypeOrmModule.forFeature([TelefonoPersona])],
  providers:[TelefonoPersonaService],
  exports:[TelefonoPersonaService]
})
export class TelefonoPersonaModule {}
