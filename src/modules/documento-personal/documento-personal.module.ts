import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DocumentoPersonal } from '../../models/documento-personal/documento-personal.entity';
import { Aws3CntmModule } from '../aws3-cntm/aws3-cntm.module';
import { Aws3CntmService } from '../aws3-cntm/aws3-cntm/aws3-cntm.service';
import { PersonaModule } from '../persona/persona.module';
import { PersonaService } from '../persona/persona/persona.service';
import { TipoDocumentoPersonalModule } from '../tipo-documento-personal/tipoDocumentoPersonal.module';
import { TipoDocumentoPersonalService } from '../tipo-documento-personal/tipo-documento-personal/tipoDocumentoPersonal.service';
import { DocumentoPersonalController } from './documento-personal/documento-personal.controller';
import { DocumentoPersonalService } from './documento-personal/documento-personal.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([DocumentoPersonal]),
    TipoDocumentoPersonalModule,
    Aws3CntmModule,
    forwardRef(() => PersonaModule)
  ],
  controllers: [DocumentoPersonalController],
  providers: [
    DocumentoPersonalService,
    TipoDocumentoPersonalService,
    PersonaService,
    Aws3CntmService
  ]
})
export class DocumentoPersonalModule {}
