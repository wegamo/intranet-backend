import { Test, TestingModule } from '@nestjs/testing';
import { DocumentoPersonal } from '../../../models/documento-personal/documento-personal.entity';
import { Repository } from 'typeorm';
import { DocumentoPersonalService } from './documento-personal.service';
import { getRepositoryToken } from '@nestjs/typeorm';

describe('DocumentoPersonalService', () => {
  let service: DocumentoPersonalService;
  let RepositoryMock: jest.Mock;
  let repository: Repository<DocumentoPersonal>;

  beforeEach(() => {
    RepositoryMock = jest.fn(() => ({
      find: jest.fn(),
      findOne: jest.fn(),
      save: jest.fn(),
      update: jest.fn(),
      delete: jest.fn()
    }));
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        DocumentoPersonalService,
        { provide: getRepositoryToken(DocumentoPersonal), useValue: new RepositoryMock()  }
      ],
    }).compile();

    service = module.get<DocumentoPersonalService>(DocumentoPersonalService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
