import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DocumentoPersonal } from '../../../models/documento-personal/documento-personal.entity';
import { DeleteResult, Repository } from 'typeorm';
import { CreateDocumentoPersonalDTO } from '../../../models/documento-personal/dto/create-documento-personal.dto';
import { TipoDocumentoPersonal } from '../../../models/tipo-documento-personal/tipoDocumentoPersonal.entity';
import configService from '../../../config/config/config.service';
import { Aws3CntmService } from '../../aws3-cntm/aws3-cntm/aws3-cntm.service';
import { DocumentoPersonalEstatus } from '../../../enums/documento-personal-estatus.enum';

const AWS_S3_BUCKET_NAME = configService.get('AWS_S3_BUCKET_NAME');
@Injectable()
export class DocumentoPersonalService {
  constructor(
    @InjectRepository(DocumentoPersonal)
    private readonly documentoPersonalRepository: Repository<DocumentoPersonal>,
    private readonly s3Service: Aws3CntmService
  ) {}

  findAll(): Promise<DocumentoPersonal[]> {
    return this.documentoPersonalRepository.find();
  }

  findOne(id: number): Promise<DocumentoPersonal> {
    return this.documentoPersonalRepository.findOne(id, { relations: ['tipoDocumentoPersonal'] });
  }

  async create(
    documentoPersonalData: CreateDocumentoPersonalDTO,
    archivo: Express.Multer.File
  ): Promise<DocumentoPersonal> {
    let documentoPersonal = await this.documentoPersonalRepository.save({
      ...documentoPersonalData,
      estatus: DocumentoPersonalEstatus.LISTO
    });
    let mediaExt = archivo.originalname.replace(/(^.*\.)/gi, '.');
    let urlArchivo = `https://${AWS_S3_BUCKET_NAME}/complex/documentos-personales/documento_${
      documentoPersonal.id
    }${mediaExt}`;
    await this.s3Service.uploadPersonalDocuments(documentoPersonal.id, archivo, mediaExt);
    documentoPersonal.archivo = urlArchivo;
    return this.documentoPersonalRepository.save(documentoPersonal);
  }

  createMany(documentosPersonales: CreateDocumentoPersonalDTO[]) {
    return this.documentoPersonalRepository.save(documentosPersonales);
  }

  createAllRequired(
    tiposDocumentosRequeridos: TipoDocumentoPersonal[],
    idPersona: number
  ): Promise<DocumentoPersonal[]> {
    let documentos = tiposDocumentosRequeridos.map((tipoDocumentoPersonal) => {
      return {
        persona: { id: idPersona },
        tipoDocumentoPersonal
      };
    });

    return this.documentoPersonalRepository.save(documentos);
  }

  async update(
    documentoPersonal: DocumentoPersonal,
    file: Express.Multer.File
  ): Promise<DocumentoPersonal> {
    if (file) {
      let mediaExt = file.originalname.replace(/(^.*\.)/gi, '.');
      let urlImagen = `https://${AWS_S3_BUCKET_NAME}/complex/documentos-personales/documento_${
        documentoPersonal.id
      }${mediaExt}`;
      await this.s3Service.uploadPersonalDocuments(documentoPersonal.id, file, mediaExt);
      documentoPersonal.archivo = urlImagen;
    }
    return this.documentoPersonalRepository.save(documentoPersonal);
  }

  async delete(documento: DocumentoPersonal): Promise<DeleteResult> {
    if (documento.archivo) {
      const mediaExt = documento.archivo
        .split('https://galileo.andromedaventures.net/complex/documentos-personales/')[1]
        .split('.')[1];
      await this.s3Service.deletePersonalDocument(documento.id, mediaExt);
    }
    return this.documentoPersonalRepository.delete(documento.id);
  }
}
