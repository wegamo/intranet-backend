import { Test, TestingModule } from '@nestjs/testing';
import { PersonaService } from '../../persona/persona/persona.service';
import { TipoDocumentoPersonalService } from '../../tipo-documento-personal/tipo-documento-personal/tipoDocumentoPersonal.service';
import { DocumentoPersonalController } from './documento-personal.controller';
import { DocumentoPersonalService } from './documento-personal.service';

describe('DocumentoPersonal Controller', () => {
  let controller: DocumentoPersonalController;
  let documentoPersonalService:DocumentoPersonalService;
  let DocumentoPersonalServiceMock:jest.Mock<Partial<DocumentoPersonalService>>;
  let TipoDocumentoPersonalServiceMock: jest.Mock<Partial<TipoDocumentoPersonalService>>;
  let PersonaServiceMock: jest.Mock<Partial<PersonaService>>;

 
  beforeEach(() => {
    DocumentoPersonalServiceMock = jest.fn<Partial<DocumentoPersonalService>, DocumentoPersonalService[]>(() => ({
    }));
    TipoDocumentoPersonalServiceMock = jest.fn<Partial<TipoDocumentoPersonalService>, TipoDocumentoPersonalService[]>(() => ({
    }));
    PersonaServiceMock = jest.fn<Partial<PersonaService>, PersonaService[]>(() => ({
    }));
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DocumentoPersonalController],
      providers: [
        { provide: DocumentoPersonalService, useValue: new DocumentoPersonalServiceMock() },
        { provide: TipoDocumentoPersonalService, useValue: new TipoDocumentoPersonalServiceMock() },
        { provide: PersonaService, useValue: new PersonaServiceMock() }]
    }).compile();
    documentoPersonalService = module.get<DocumentoPersonalService>(DocumentoPersonalService);
    controller = module.get<DocumentoPersonalController>(DocumentoPersonalController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
