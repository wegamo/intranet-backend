import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Put,
  UploadedFile,
  UseInterceptors
} from '@nestjs/common';
import { UpdateDocumentoPersonalDTO } from '../../../models/documento-personal/dto/update-documento-personal.dto';
import { DocumentoPersonal } from '../../../models/documento-personal/documento-personal.entity';
import { CreateDocumentoPersonalDTO } from '../../../models/documento-personal/dto/create-documento-personal.dto';
import { DocumentoPersonalService } from './documento-personal.service';
import { TipoDocumentoPersonalService } from '../../tipo-documento-personal/tipo-documento-personal/tipoDocumentoPersonal.service';
import { PersonaService } from '../../persona/persona/persona.service';
import { FileInterceptor } from '@nestjs/platform-express';
import { DeleteResult } from 'typeorm';
import { ApiUseTags } from '@nestjs/swagger';

@ApiUseTags('Documento personal')
@Controller('documento-personal')
export class DocumentoPersonalController {
  constructor(
    private readonly documentoPersonalService: DocumentoPersonalService,
    private readonly tipoDocumentoPersonalService: TipoDocumentoPersonalService,
    private readonly personaService: PersonaService
  ) {}

  @Get('all')
  getAll(): Promise<DocumentoPersonal[]> {
    return this.documentoPersonalService.findAll();
  }

  @Post('create')
  @UseInterceptors(FileInterceptor('file'))
  async create(
    @Body() documentoPersonal: { body: string },
    @UploadedFile() file: Express.Multer.File
  ): Promise<DocumentoPersonal> {
    try {
      let documentoPersonalData = JSON.parse(documentoPersonal.body);
      // Si el tipo de documento ya existe en la BD
      if (documentoPersonalData.tipoDocumentoPersonal) {
        const documentoCreado = await this.documentoPersonalService.create(
          documentoPersonalData,
          file
        );
        return documentoCreado;
        // Si es un nuevo tipo de documento
      } else {
        const tipoDocumentoPersonal = {
          nombre: documentoPersonalData.nombre,
          requerido: documentoPersonalData.requerido
        };
        const tipoDocumentoPersonalCreated = await this.tipoDocumentoPersonalService.create(
          tipoDocumentoPersonal
        );
        if (documentoPersonalData.requerido === '1') {
          const personas = await this.personaService.findAllWithoutPagination();
          const documentos = personas.map((persona) => {
            return {
              persona,
              tipoDocumentoPersonal: tipoDocumentoPersonalCreated
            };
          });
          const documentosCreados = await this.documentoPersonalService.createMany(documentos);
          const documento = documentosCreados.find(
            (documento) => documento.persona.id === documentoPersonalData.persona.id
          );
          return documento;
        } else {
          const documento = {
            ...documentoPersonalData,
            tipoDocumentoPersonal: tipoDocumentoPersonalCreated
          };
          const documentoCreado = this.documentoPersonalService.create(documento, file);
          return documentoCreado;
        }
      }
    } catch (err) {
      throw new HttpException('Error al Crear el Documento Personal', HttpStatus.BAD_REQUEST);
    }
  }

  @Put('update/:id')
  @UseInterceptors(FileInterceptor('file'))
  async update(
    @Param('id') id: number,
    @Body() documentoPersonalData: { body: string },
    @UploadedFile() file: Express.Multer.File
  ) {
    try {
      const body: UpdateDocumentoPersonalDTO = JSON.parse(documentoPersonalData.body);
      let documentToChange = await this.documentoPersonalService.findOne(id);

      if (!documentToChange) {
        throw new HttpException('El documento no existe', HttpStatus.NOT_FOUND);
      }
      documentToChange = { ...documentToChange, ...body };
      const documentoPersonal = await this.documentoPersonalService.update(documentToChange, file);

      return { documentoPersonal };
    } catch (err) {
      throw new HttpException('Error al actualizar el documento Personal', HttpStatus.BAD_REQUEST);
    }
  }

  @Delete(':id')
  async delete(@Param('id') id: number): Promise<DeleteResult> {
    let documento = await this.documentoPersonalService.findOne(id);
    if (!documento) {
      throw new HttpException('El documento no existe', HttpStatus.BAD_REQUEST);
    }
    if (documento.tipoDocumentoPersonal.requerido === '1') {
      throw new HttpException('El documento es obligatorio', HttpStatus.BAD_REQUEST);
    }
    return this.documentoPersonalService.delete(documento);
  }
}
