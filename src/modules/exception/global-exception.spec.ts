import { GlobalException } from './global-exception';

describe('GlobalException', () => {
  it('should be defined', () => {
    expect(new GlobalException()).toBeDefined();
  });
});
