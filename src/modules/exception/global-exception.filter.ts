import { ArgumentsHost, Catch, ExceptionFilter } from '@nestjs/common';
import { GlobalException } from './global-exception';

@Catch(GlobalException)
export class GlobalExceptionFilter implements ExceptionFilter {
  catch(exception: GlobalException, host: ArgumentsHost) {

    const ctx = host.switchToHttp();
    const response = ctx.getResponse();

    response
      .status(500)
      .json({
        timestamp: new Date().toISOString(),
        error: exception.E_Type(),
        msg: exception.what(),
      });    
  }
}
