export class GlobalException extends Error {
    
    constructor(public readonly message = ``, public readonly type=0) {
        super();
    }

    public what(): string {
        return this.message;
    }

    public E_Type(): number {
        return this.type;
    }
}
