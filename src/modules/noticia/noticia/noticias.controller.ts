import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Query,
  Request,
  UploadedFile,
  UseGuards,
  UseInterceptors
} from '@nestjs/common';
import { NoticiasService } from './noticias.service';
import { FileInterceptor } from '@nestjs/platform-express';
import { Noticia } from '../../../models/noticias/noticia.entity';
import { Persona } from '../../../models/persona/persona.entity';
import { AuthGuard } from '@nestjs/passport';
import { NoticiaTipo } from '../../../enums/noticia.tipo.enum';
import { Pagination } from 'nestjs-typeorm-paginate';
import {
  ApiBearerAuth,
  ApiConsumes,
  ApiImplicitBody,
  ApiOperation,
  ApiResponse,
  ApiUseTags
} from '@nestjs/swagger';
import { NoticiaResponseDTO } from '../../../models/noticias/dto/noticia-response.dto';
import { CrearNoticiaDTO } from '../../../models/noticias/dto/crear-noticia.dto';
import { ActualizarNoticiaDTO } from '../../../models/noticias/dto/actualizarNoticia.dto';
@ApiBearerAuth()
@ApiUseTags('Noticia')
@Controller('noticias')
export class NoticiasController {
  constructor(private readonly noticiasService: NoticiasService) {}

  @UseGuards(AuthGuard('jwt'))
  @Get('all')
  async findAll(
    @Query('searchTerm') searchTerm: string,
    @Query('page') page = 1,
    @Query('limit') limit = 10,
    @Query('tipo') tipo: NoticiaTipo,
    @Query('fechaInicio') fechaInicio: string,
    @Query('fechaFin') fechaFin: string
  ): Promise<Pagination<Noticia>> {
    return this.noticiasService.findAll(tipo, searchTerm, fechaInicio, fechaFin, {
      page,
      limit
    });
  }

  @ApiOperation({ title: 'Obtener noticias general', description: 'Listar todas las noticias' })
  @ApiResponse({
    status: 200,
    description: 'Noticias registradas',
    type: NoticiaResponseDTO,
    isArray: true
  })
  @UseGuards(AuthGuard('jwt'))
  @Get('/:tipo/all')
  GetNoticias(@Param('tipo') tipo: string): Promise<Noticia[]> {
    return this.noticiasService.getNoticiasByTipo(tipo);
  }

  @ApiOperation({ title: 'Obtener noticia particular', description: 'Obtener noticia por ID' })
  @ApiResponse({
    status: 200,
    description: 'Noticia encontrada',
    type: NoticiaResponseDTO
  })
  @UseGuards(AuthGuard('jwt'))
  @Get(':id')
  GetNoticia(@Param('id') id: number): Promise<Noticia> {
    return this.noticiasService.getNoticiaById(id);
  }

  @ApiOperation({ title: 'Crear noticia', description: 'Creacion de nueva noticia' })
  @ApiResponse({
    status: 200,
    description: 'Nueva noticia registrada',
    type: NoticiaResponseDTO
  })
  @ApiConsumes('multipart/form-data')
  @ApiImplicitBody({ name: 'body', description: 'Datos de la noticia', type: CrearNoticiaDTO })
  @UseGuards(AuthGuard('jwt'))
  @Post('/create')
  @UseInterceptors(FileInterceptor('arch'))
  CrearNoticia(
    @Body() noticia: { body: string },
    @UploadedFile() arch: Express.Multer.File,
    @Request() { user }: { user: Persona }
  ): Promise<any> {
    let rest = JSON.parse(noticia.body);
    return this.noticiasService.crearNoticia({ ...rest, imagen: arch, persona: user });
  }

  @ApiOperation({ title: 'Eliminar noticia', description: 'Eliminación de noticia' })
  @ApiResponse({
    status: 200,
    description: 'Noticia eliminada',
    type: NoticiaResponseDTO
  })
  @UseGuards(AuthGuard('jwt'))
  @Delete('/:tipo/delete/:id')
  EliminarNoticia(@Param('tipo') tipo: string, @Param('id') id: number): Promise<any> {
    return this.noticiasService.EliminarNoticia(tipo, id);
  }

  @ApiOperation({ title: 'Modificar noticia', description: 'modificación de noticia' })
  @ApiResponse({
    status: 200,
    description: 'Noticia actualizada',
    type: NoticiaResponseDTO
  })
  @ApiConsumes('multipart/form-data')
  @ApiImplicitBody({
    name: 'body',
    description: 'Datos de actualización',
    type: ActualizarNoticiaDTO
  })
  @UseGuards(AuthGuard('jwt'))
  @Post('/update/:id')
  @UseInterceptors(FileInterceptor('arch'))
  async update(
    @Param('id') id: number,
    @Body() noticia: { body: string },
    @Request() { user }: { user: Persona },
    @UploadedFile() arch?: Express.Multer.File
  ): Promise<any> {
    if (noticia.body) {
      let noticiaToChange = await this.noticiasService.getNoticiaById(id);
      if (!noticiaToChange) {
        throw new HttpException('La noticia no existe', HttpStatus.NOT_FOUND);
      }

      let rest = JSON.parse(noticia.body);

      if (rest.tipo == NoticiaTipo.BANNER || rest.tipo == NoticiaTipo.EVENTO) {
        // Se desea cambiar la posicion de la noticia
        if (rest.posicion) {
          let allNoticias = await this.noticiasService.getNoticiasByTipo(noticiaToChange.tipo);
          // Caso. El usuario elige una posicion dejando posiciones de por medio, posiciones negativas o > 5.
          if (
            rest.posicion <= 0 ||
            rest.posicion > 5 ||
            rest.posicion > allNoticias.length + 1 ||
            (allNoticias.length == 1 && rest.posicion > allNoticias.length)
          ) {
            throw new HttpException(
              'No se pueden añadir noticias dejando posiciones de por medio o en posiciones mayores a 5',
              HttpStatus.BAD_REQUEST
            );
          }
          let toUpdateNoticias;
          let toUpdateNoticiasPromises;

          if (rest.posicion > noticiaToChange.posicion) {
            // RODAR IZQUIERDA
            toUpdateNoticias = allNoticias.filter(
              (noticia) =>
                noticia.posicion > noticiaToChange.posicion && noticia.posicion <= rest.posicion
            );

            toUpdateNoticiasPromises = toUpdateNoticias.map(
              (noticia) => this.noticiasService.save({ ...noticia, posicion: noticia.posicion - 1 })
              // this.guardarNoticia({ ...noticia, posicion: noticia.posicion - 1 })
            );
          } else {
            // RODAR DERECHA
            toUpdateNoticias = allNoticias.filter(
              (noticia) =>
                noticia.posicion >= rest.posicion && noticia.posicion < noticiaToChange.posicion
            );
            toUpdateNoticiasPromises = toUpdateNoticias.map(
              (noticia) => this.noticiasService.save({ ...noticia, posicion: noticia.posicion + 1 })
              //this.guardarNoticia({ ...noticia, posicion: noticia.posicion + 1 })
            );
          }

          await Promise.all(toUpdateNoticiasPromises);
        }
      }

      noticiaToChange = {
        ...noticiaToChange,
        titulo: rest.titulo,
        descripcion: rest.descripcion,
        posicion: rest.posicion,
        persona: user,
        resumen: rest.resumen
      };
      return this.noticiasService.actualizarNoticia(noticiaToChange, arch);
    } else {
      throw new HttpException('Los datos de la noticia son requeridos', HttpStatus.BAD_REQUEST);
    }
  }
}
