import { Req, Res, Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { Between, LessThanOrEqual, Like, MoreThanOrEqual, Repository } from 'typeorm';
import { CrearNoticiaDTO } from '../../../models/noticias/dto/crear-noticia.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Noticia } from '../../../models/noticias/noticia.entity';
import configService from '../../../config/config/config.service';
import { Aws3CntmService } from '../../aws3-cntm/aws3-cntm/aws3-cntm.service';
import { GuardarNoticiaDTO } from '../../../models/noticias/dto/guardar-noticia.dto';
import { NoticiaTipo } from '../../../enums/noticia.tipo.enum';
import { IPaginationOptions, paginate, Pagination } from 'nestjs-typeorm-paginate';

const AWS_S3_BUCKET_NAME = configService.get('AWS_S3_BUCKET_NAME');
@Injectable()
export class NoticiasService {
  constructor(
    @InjectRepository(Noticia)
    private readonly noticiasRepository: Repository<Noticia>,
    private readonly s3Service: Aws3CntmService
  ) {}

  findAll(
    tipo: NoticiaTipo,
    searchTerm: string,
    fechaInicio: string,
    fechaFin: string,
    options: IPaginationOptions
  ): Promise<Pagination<Noticia>> {
    console.log('fechaInicio', fechaInicio);
    console.log('fechaFin', fechaFin);
    console.log(searchTerm);
    console.log(tipo);

    const queryBuilder = this.noticiasRepository.createQueryBuilder('noticia');

    if (searchTerm) {
      queryBuilder.andWhere(`UPPER(noticia.titulo) LIKE UPPER('%${searchTerm}%')`);
    }
    if (tipo) {
      queryBuilder.andWhere('noticia.tipo = :tipo', { tipo });
    }
    if (fechaInicio) {
      const fechaInicioDate = new Date(fechaInicio);
      if (fechaFin) {
        const fechaFinDate = new Date(fechaFin);
        queryBuilder.andWhere('noticia.fechaPublicacion BETWEEN :startDate AND :endDate', {
          startDate: fechaInicioDate,
          endDate: fechaFinDate
        });
      } else {
        queryBuilder.andWhere('noticia.fechaPublicacion >= :fechaInicioDate', { fechaInicioDate });
      }
    } else if (fechaFin) {
      const fechaFinDate = new Date(fechaFin);
      queryBuilder.andWhere('noticia.fechaPublicacion <= :fechaFinDate', { fechaFinDate });
    }
    queryBuilder.orderBy('noticia.fechaPublicacion', 'DESC');
    queryBuilder.addOrderBy('noticia.tipo', 'ASC');

    return paginate(queryBuilder, options);

    /*.where(`noticia.titulo LIKE '%:searchterm%'`, { searchTerm })
    .andWhere('noticia.tipo === :tipo', { tipo })
    .andWhere('noticia.fechaPublicacion BETWEEN(:startDate, endDate)', {
      startDate: fechaInicioDate,
      endDate: fechaFinDate
    })
    .orderBy('noticia.fechaPublicacion', 'DESC');
    if (searchTerm && tipo && fechaInicio && fechaFin) {
      const fechaInicioDate = new Date(fechaInicio);
      const fechaFinDate = new Date(fechaFin);
      return paginate<Noticia>(queryBuilder, options);
    } else if (searchTerm && tipo && fechaInicio) {
      const fechaInicioDate = new Date(fechaInicio);
      return paginate<Noticia>(this.noticiasRepository, options, {
        where: {
          titulo: Like(`%${searchTerm}%`),
          tipo,
          fechaPublicacion: MoreThanOrEqual(fechaInicioDate)
        },
        order: { fechaPublicacion: 'DESC' }
      });
    } else if (searchTerm && tipo && fechaFin) {
      const fechaFinDate = new Date(fechaFin);
      return paginate<Noticia>(this.noticiasRepository, options, {
        where: {
          titulo: Like(`%${searchTerm}%`),
          tipo,
          fechaPublicacion: LessThanOrEqual(fechaFinDate)
        },
        order: { fechaPublicacion: 'DESC' }
      });
    } else if (searchTerm && fechaInicio && fechaFin) {
      const fechaInicioDate = new Date(fechaInicio);
      const fechaFinDate = new Date(fechaFin);
      return paginate<Noticia>(this.noticiasRepository, options, {
        where: {
          titulo: Like(`%${searchTerm}%`),
          fechaPublicacion: Between(fechaInicioDate, fechaFinDate)
        },
        order: { tipo: 'ASC', fechaPublicacion: 'DESC' }
      });
    } else if (tipo && fechaInicio && fechaFin) {
      const fechaInicioDate = new Date(fechaInicio);
      const fechaFinDate = new Date(fechaFin);
      return paginate<Noticia>(this.noticiasRepository, options, {
        where: {
          tipo,
          fechaPublicacion: Between(fechaInicioDate, fechaFinDate)
        },
        order: { fechaPublicacion: 'DESC' }
      });
    } else if (searchTerm && tipo) {
      return paginate<Noticia>(this.noticiasRepository, options, {
        where: {
          titulo: Like(`%${searchTerm}%`),
          tipo
        },
        order: { fechaPublicacion: 'DESC' }
      });
    } else if (searchTerm && fechaInicio) {
      const fechaInicioDate = new Date(fechaInicio);
      return paginate<Noticia>(this.noticiasRepository, options, {
        where: {
          titulo: Like(`%${searchTerm}%`),
          fechaPublicacion: MoreThanOrEqual(fechaInicioDate)
        },
        order: { tipo: 'ASC', fechaPublicacion: 'DESC' }
      });
    } else if (searchTerm && fechaFin) {
      const fechaFinDate = new Date(fechaFin);
      return paginate<Noticia>(this.noticiasRepository, options, {
        where: {
          titulo: Like(`%${searchTerm}%`),
          fechaPublicacion: LessThanOrEqual(fechaFinDate)
        },
        order: { tipo: 'ASC', fechaPublicacion: 'DESC' }
      });
    } else if (tipo && fechaInicio) {
      const fechaInicioDate = new Date(fechaInicio);
      return paginate<Noticia>(this.noticiasRepository, options, {
        where: {
          tipo,
          fechaPublicacion: MoreThanOrEqual(fechaInicioDate)
        },
        order: { fechaPublicacion: 'DESC' }
      });
    } else if (tipo && fechaFin) {
      const fechaFinDate = new Date(fechaFin);
      return paginate<Noticia>(this.noticiasRepository, options, {
        where: {
          tipo,
          fechaPublicacion: LessThanOrEqual(fechaFinDate)
        },
        order: { fechaPublicacion: 'DESC' }
      });
    } else if (fechaInicio && fechaFin) {
      const fechaInicioDate = new Date(fechaInicio);
      const fechaFinDate = new Date(fechaFin);
      return paginate<Noticia>(this.noticiasRepository, options, {
        where: {
          fechaPublicacion: Between(fechaInicioDate, fechaFinDate)
        },
        order: { tipo: 'ASC', fechaPublicacion: 'DESC' }
      });
    } else if (searchTerm) {
      return paginate<Noticia>(this.noticiasRepository, options, {
        where: {
          titulo: Like(`%${searchTerm}%`)
        },
        order: { tipo: 'ASC', fechaPublicacion: 'DESC' }
      });
    } else if (tipo) {
      return paginate<Noticia>(this.noticiasRepository, options, {
        where: {
          tipo
        },
        order: { fechaPublicacion: 'DESC' }
      });
    } else if (fechaInicio) {
      const fechaInicioDate = new Date(fechaInicio);
      console.log('ENTREEEEEEEEEEEEEEEEEEE');
      console.log(fechaInicioDate);
      return paginate<Noticia>(this.noticiasRepository, options, {
        where: {
          fechaPublicacion: MoreThanOrEqual(fechaInicioDate)
        },
        order: { tipo: 'ASC', fechaPublicacion: 'DESC' }
      });
    } else if (fechaFin) {
      const fechaFinDate = new Date(fechaFin);
      return paginate<Noticia>(this.noticiasRepository, options, {
        where: {
          fechaPublicacion: LessThanOrEqual(fechaFinDate)
        },
        order: { tipo: 'ASC', fechaPublicacion: 'DESC' }
      });
    } else {
      return paginate<Noticia>(this.noticiasRepository, options, {
        order: { tipo: 'ASC', fechaPublicacion: 'DESC' }
      });
    }*/
  }

  getNoticiasByTipo(tipo: string): Promise<Noticia[]> {
    return this.noticiasRepository.find({
      where: { tipo },
      order: { posicion: 'ASC' }
    });
  }

  save(noticia: Noticia) {
    return this.noticiasRepository.save(noticia);
  }

  guardarNoticia(noticia: GuardarNoticiaDTO) {
    return this.noticiasRepository.save(noticia);
  }

  async crearNoticia({
    tipo,
    posicion,
    titulo,
    descripcion,
    resumen,
    imagen,
    persona
  }: CrearNoticiaDTO) {
    let mediaExt = '';
    let urlImagen;
    let promesasArray = [];
    if (tipo === NoticiaTipo.BANNER || tipo === NoticiaTipo.EVENTO) {
      if (!posicion || !imagen) {
        throw new HttpException('Debe tener imagen y posicion', HttpStatus.BAD_REQUEST);
      }
      let allNoticias: any[] = await this.getNoticiasByTipo(tipo);
      if (allNoticias.length === 5) {
        throw new HttpException('Ya no se pueden crear más noticias', HttpStatus.BAD_REQUEST);
      }
      //  Caso 1. No estan todas las noticias, y la nueva se pone en una posicion existente ---> rodar las noticias a partir de esta posicion
      if (allNoticias.length < 5 && posicion <= allNoticias.length && posicion > 0) {
        promesasArray = allNoticias.map((noticia) => {
          if (noticia.posicion >= posicion) {
            noticia.posicion++;
            return this.guardarNoticia(noticia);
          }
        });
        await Promise.all(promesasArray);
      } else if ((allNoticias.length < 5 && posicion > allNoticias.length + 1) || posicion <= 0) {
        // Caso 2. No estan todas las noticias, y el usuario elige una posicion dejando posiciones de por medio.
        throw new HttpException(
          'No se pueden añadir noticias dejando posiciones de por medio',
          HttpStatus.BAD_REQUEST
        );
      }
    }
    const noticia = {
      tipo,
      posicion,
      titulo,
      descripcion,
      persona,
      resumen
    };
    let noticiaCreated = await this.guardarNoticia(noticia);
    mediaExt = imagen.originalname.replace(/(^.*\.)/gi, '.');
    urlImagen = `https://${AWS_S3_BUCKET_NAME}/complex/noticias/${noticiaCreated.id}${mediaExt}`;

    await this.s3Service.uploadNewsImage(noticiaCreated.id, imagen, mediaExt);
    noticiaCreated.imagen = urlImagen;
    noticiaCreated = await this.noticiasRepository.save(noticiaCreated);
    return { noticia: noticiaCreated };
  }

  getNoticiaById(id: number): Promise<Noticia> {
    return this.noticiasRepository.findOne(id, {
      relations: ['persona']
    });
  }

  async actualizarNoticia(noticiaActualizada: Noticia, imagen?: Express.Multer.File) {
    let mediaExt = '';
    let urlImagen;

    if (imagen) {
      console.log('TENGO IMAGEN');
      mediaExt = imagen.originalname.replace(/(^.*\.)/gi, '.');
      urlImagen = `https://${AWS_S3_BUCKET_NAME}/complex/noticias/${
        noticiaActualizada.id
      }${mediaExt}`;
      await this.s3Service.uploadNewsImage(noticiaActualizada.id, imagen, mediaExt);
      noticiaActualizada.imagen = urlImagen;
    }
    console.log(noticiaActualizada);
    await this.noticiasRepository.save(noticiaActualizada);

    return { noticia: noticiaActualizada };
  }

  async EliminarNoticia(tipo: string, id: number): Promise<any> {
    try {
      const existingNoticia = await this.getNoticiaById(id);
      await this.noticiasRepository.delete(id);

      if (tipo == NoticiaTipo.BANNER || tipo == NoticiaTipo.EVENTO) {
        const allNoticias = await this.getNoticiasByTipo(tipo);
        const actualizarNoticiasPromises = allNoticias.map((noticia) => {
          if (noticia.posicion > existingNoticia.posicion) {
            return this.guardarNoticia({ ...noticia, posicion: noticia.posicion - 1 });
          }
          return Promise.resolve(noticia);
        });

        await Promise.all(actualizarNoticiasPromises);
      }

      if (existingNoticia.imagen) await this.s3Service.deleteNewsImage(id);
      return Promise.resolve({ message: 'Ok' });
    } catch (error) {
      console.log(error);
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'No se ha encontrado la noticia o un error ha ocurrido'
        },
        HttpStatus.FORBIDDEN
      );
    }
  }
}
