import { Test, TestingModule } from "@nestjs/testing";
import { NoticiasController } from "./noticias.controller";
import { NoticiasService } from "./noticias.service";

describe('Noticia Controller', () => {
    let controller: NoticiasController;
    let noticiasService: NoticiasService;
    let NoticiasServiceMock: jest.Mock<Partial<NoticiasService>>;
  
  
    beforeEach(() => {
      NoticiasServiceMock = jest.fn<Partial<NoticiasService>, NoticiasService[]>(() => ({
        crearNoticia:jest.fn(),
      }));
    });
  
    beforeEach(async () => {
      const module: TestingModule = await Test.createTestingModule({
        controllers: [NoticiasController],
        providers: [{ provide: NoticiasService, useValue: new NoticiasServiceMock() }]
      }).compile();
      noticiasService = module.get<NoticiasService>(NoticiasService);
      controller = module.get<NoticiasController>(NoticiasController);
    });
  
    it('should be defined', () => {
      expect(controller).toBeDefined();
    });
});  