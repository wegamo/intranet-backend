import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Noticia } from '../../../models/noticias/noticia.entity';
import { DeleteResult, Repository } from 'typeorm';
import { NoticiasService } from './noticias.service';
import { Aws3CntmService } from '../../aws3-cntm/aws3-cntm/aws3-cntm.service';
import { SampleNoticia } from '../../../../test/ObjectSamples';
import { CrearNoticiaDTO } from '../../../models/noticias/dto/crear-noticia.dto';

describe('NoticiasService', () => {
  let service: NoticiasService;
  let RepositoryMock: jest.Mock;
  let repository: Repository<Noticia>;

  beforeEach(() => {
    RepositoryMock = jest.fn(() => ({
      find: jest.fn(),
      findOne: jest.fn(),
      save: jest.fn(),
      create: jest.fn(),
      update: jest.fn(),
      delete: jest.fn()
    }));
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        NoticiasService,
        { provide: getRepositoryToken(Noticia), useValue: new RepositoryMock() },
        Aws3CntmService
      ]
    }).compile();

    repository = module.get(getRepositoryToken(Noticia));
    service = module.get<NoticiasService>(NoticiasService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('findAll()', () => {
    let expectedNoticias: Noticia[];
    let result: Noticia[];

    describe('case: success', () => {
      describe('when there are rules', () => {
        beforeEach(async () => {
          expectedNoticias = [SampleNoticia];
          (repository.find as jest.Mock).mockResolvedValue(expectedNoticias);
          result = await service.GetNoticias(SampleNoticia.tipo);
        });

        it('should invoke repository.find()', () => {
          expect(repository.find).toHaveBeenCalledTimes(1);
        });

        it('should return an array of rules', () => {
          expect(result).toStrictEqual(expectedNoticias);
        });
      });
    });

    describe('case: failure', () => {
      describe('when there are not rules', () => {
        beforeEach(async () => {
          expectedNoticias = [];
          (repository.find as jest.Mock).mockResolvedValue(expectedNoticias);
          result = await service.GetNoticias(SampleNoticia.tipo);
        });

        it('should invoke repository.find()', () => {
          expect(repository.find).toHaveBeenCalledTimes(1);
        });

        it('should return an array empty', () => {
          expect(result).toStrictEqual(expectedNoticias);
        });
      });
    });
  });

  describe('findOne(id)', () => {
    let expectedNoticia: Noticia;
    let result: Noticia;

    describe('case: success', () => {
      describe('when a rule is found', () => {
        beforeEach(async () => {
          expectedNoticia = SampleNoticia;
          (repository.findOne as jest.Mock).mockResolvedValue(expectedNoticia);
          result = await service.GetNoticia(SampleNoticia.id);
        });

        it('should invoke repository.findOne()', () => {
          expect(repository.findOne).toHaveBeenCalledTimes(1);
          expect(repository.findOne).toHaveBeenCalledWith(SampleNoticia.id);
        });

        it('should return a rule', () => {
          expect(result).toStrictEqual(expectedNoticia);
        });
      });
    });

    describe('case: failure', () => {
      describe('when a rule is not found', () => {
        beforeEach(async () => {
          expectedNoticia = undefined;
          (repository.findOne as jest.Mock).mockResolvedValue(expectedNoticia);
          result = await service.GetNoticia(SampleNoticia.id);
        });

        it('should invoke repository.findOne()', () => {
          expect(repository.findOne).toHaveBeenCalledTimes(1);
          expect(repository.findOne).toHaveBeenCalledWith(SampleNoticia.id);
        });

        it('should return undefined', () => {
          expect(result).toStrictEqual(expectedNoticia);
        });
      });
    });
  });

  describe('create(Noticia)', () => {
    let body: CrearNoticiaDTO;

    describe('case: success', () => {
      const { ...rest } = SampleNoticia;
      let expectedNoticia: Noticia;
      let result: Noticia;

      describe('when a rule is created', () => {
        beforeEach(async () => {
          body = rest;
          expectedNoticia = SampleNoticia;
          (repository.save as jest.Mock).mockResolvedValue(expectedNoticia);
          result = await service.guardarNoticia(body);
        });

        it('should invoke repository.save()', () => {
          expect(repository.save).toHaveBeenCalledTimes(1);
          expect(repository.save).toHaveBeenCalledWith(body);
        });

        it('should create a rule', () => {
          expect(result).toStrictEqual(expectedNoticia);
        });
      });
    });

    describe('case: failure', () => {
      let expectedError: TypeError;

      describe('when a rule is not created', () => {
        beforeEach(async () => {
          body = undefined;
          expectedError = new TypeError();
          jest.spyOn(service, 'guardarNoticia').mockRejectedValue(expectedError);
        });

        it('should throw when the body is undefined', async () => {
          await expect(service.guardarNoticia(body)).rejects.toThrow(TypeError);
        });

        it('should not create a rule', () => {
          expect(repository.save).not.toHaveBeenCalled();
        });
      });
    });
  });
  /*
    describe('update(id, Noticia)', () => {
      let update: UpdateNoticiaDTO;
  
      describe('case: success', () => {
        let updateResult: UpdateResult | GlobalException;
        let result: UpdateResult | GlobalException;
  
        describe('when a rule is updated', () => {
          beforeEach(async () => {
            update = {
              nombre: 'Test',
              descripcion: 'Test'
            };
            updateResult = new UpdateResult();
            (repository.update as jest.Mock).mockResolvedValue(updateResult);
            result = await service.update(1, update);
          });
  
          it('should invoke repository.update()', () => {
            expect(repository.update).toHaveBeenCalledTimes(1);
            expect(repository.update).toHaveBeenCalledWith(1, update);
          });
  
          it('should update a rule', () => {
            expect(result).toStrictEqual(updateResult);
          });
        });
      });
  
      describe('case: failure', () => {
        let expectedError: TypeError;
  
        describe('when a rule is not updated', () => {
          beforeEach(async () => {
            expectedError = new TypeError();
            update = undefined;
            jest.spyOn(service, 'update').mockRejectedValue(expectedError);
          });
  
          it('should throw when the body is undefined', async () => {
            await expect(service.update(1, update)).rejects.toThrow(TypeError);
          });
  
          it('should not update a rule', () => {
            expect(repository.update).not.toHaveBeenCalled();
          });
        });
      });
    });
    */
  describe('delete(id)', () => {
    let deleteResult: DeleteResult;
    let result: DeleteResult;

    describe('case: success', () => {
      describe('when a rule is found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 1;
          (repository.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await service.deleteNoticia(SampleNoticia.id);
        });

        it('should invoke repository.delete()', () => {
          expect(repository.delete).toHaveBeenCalledTimes(1);
          expect(repository.delete).toHaveBeenCalledWith(SampleNoticia.id);
        });

        it('should delete a rule', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });

    describe('case: failure', () => {
      describe('when a rule is not found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 0;
          (repository.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await service.deleteNoticia(SampleNoticia.id);
        });

        it('should invoke repository.delete()', () => {
          expect(repository.delete).toHaveBeenCalledTimes(1);
          expect(repository.delete).toHaveBeenCalledWith(SampleNoticia.id);
        });

        it('should not delete a rule', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });
  });
});
