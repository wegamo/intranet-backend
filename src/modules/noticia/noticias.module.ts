import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Noticia } from '../../models/noticias/noticia.entity';
import { Aws3CntmModule } from '../aws3-cntm/aws3-cntm.module';
import { Aws3CntmService } from '../aws3-cntm/aws3-cntm/aws3-cntm.service';
import { NoticiasController } from './noticia/noticias.controller';
import { NoticiasService } from './noticia/noticias.service';

@Module({
  imports: [TypeOrmModule.forFeature([Noticia]), Aws3CntmModule],
  controllers: [NoticiasController],
  providers: [NoticiasService, Aws3CntmService],
  exports: [NoticiasService, TypeOrmModule.forFeature([Noticia])]
})
export class NoticiasModule {}
