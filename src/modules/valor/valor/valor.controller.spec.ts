import { Test, TestingModule } from '@nestjs/testing';
import { DeleteResult, UpdateResult } from 'typeorm';

import { CreateValorDTO } from '../../../models/valor/dto/create-valor.dto';
import { UpdateValorDTO } from '../../../models/valor/dto/update-valor.dto';
import { Valor } from '../../../models/valor/valor.entity';

import { ValorController } from './valor.controller';
import { ValorService } from './valor.service';

import { GlobalException } from './../../exception/global-exception';
import { ValorInt } from './../../dto_response/Valor_Payload';
import { SampleValor } from './../../../../test/ObjectSamples';


describe('Valor Controller', () => {
  let controller: ValorController;
  let ValorServiceMock: jest.Mock<Partial<ValorService>>;
  let valorService: ValorService;

  beforeEach(() => {
    ValorServiceMock = jest.fn<Partial<ValorService>, ValorService[]>(() => ({
      findAll: jest.fn(),
      findOne: jest.fn(),
      create: jest.fn(),
      delete: jest.fn(),
      update: jest.fn()
    }));
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ValorController],
      providers: [{ provide: ValorService, useValue: new ValorServiceMock() }]
    }).compile();

    valorService = module.get<ValorService>(ValorService);
    controller = module.get<ValorController>(ValorController);
  });

  describe('findAll()', () => {
    let expectedValores: Valor[];
    let result: Valor[];

    describe('case: success', () => {
      describe('when there are values', () => {
        beforeEach(async () => {
          expectedValores = [
            SampleValor
          ];
          (valorService.findAll as jest.Mock).mockResolvedValue(expectedValores);
          result = await controller.findAll();
        });

        it('should invoke ValorService.findAll()', () => {
          expect(valorService.findAll).toHaveBeenCalledTimes(1);
        });

        it('should return an array of values', () => {
          expect(result).toStrictEqual(expectedValores);
        });
      });
    });

    describe('case: failure', () => {
      describe('when there are not values', () => {
        beforeEach(async () => {
          expectedValores = [];
          (valorService.findAll as jest.Mock).mockResolvedValue(expectedValores);
          result = await controller.findAll();
        });

        it('should invoke ValorService.findAll()', () => {
          expect(valorService.findAll).toHaveBeenCalledTimes(1);
        });

        it('should return an array empty', () => {
          expect(result).toStrictEqual(expectedValores);
        });
      });
    });
  });

  describe('findOne(id)', () => {
    let expectedValor: Valor;
    let result: Valor;

    describe('case: success', () => {
      describe('when a value is found', () => {
        beforeEach(async () => {
          expectedValor = SampleValor;
          (valorService.findOne as jest.Mock).mockResolvedValue(expectedValor);
          result = await controller.findOne(1);
        });

        it('should invoke ValorService.findOne()', () => {
          expect(valorService.findOne).toHaveBeenCalledTimes(1);
          expect(valorService.findOne).toHaveBeenCalledWith(1);
        });

        it('should return a value', () => {
          expect(result).toStrictEqual(expectedValor);
        });
      });
    });

    describe('case: failure', () => {
      describe('when a value is not found', () => {
        beforeEach(async () => {
          expectedValor = undefined;
          (valorService.findOne as jest.Mock).mockResolvedValue(expectedValor);
          result = await controller.findOne(1);
        });

        it('should invoke ValorService.findOne()', () => {
          expect(valorService.findOne).toHaveBeenCalledTimes(1);
          expect(valorService.findOne).toHaveBeenCalledWith(1);
        });

        it('should return undefined', () => {
          expect(result).toStrictEqual(expectedValor);
        });
      });
    });
  });

  describe('create(valorData)', () => {
    const {id,...rest} = SampleValor;
    let body: CreateValorDTO;

    describe('case: success', () => {
      let expectedValor: Valor;
      let result: Valor;

      describe('when a value is created', () => {
        beforeEach(async () => {
          body = rest;
          expectedValor = SampleValor; 

          (valorService.create as jest.Mock).mockResolvedValue(expectedValor);
          result = await controller.create(body);
        });

        it('should invoke ValorService.create()', () => {
          expect(valorService.create).toHaveBeenCalledTimes(1);
          expect(valorService.create).toHaveBeenCalledWith(body);
        });

        it('should create a value', () => {
          expect(result).toStrictEqual(expectedValor);
        });
      });
    });

    describe('case: failure', () => {
      let expectedError: TypeError;

      describe('when a value is not created', () => {
        beforeEach(async () => {
          body = undefined;
          expectedError = new TypeError();
          jest.spyOn(controller, 'create').mockRejectedValue(expectedError);
        });

        it('should throw when the body is undefined', async () => {
          await expect(controller.create(body)).rejects.toThrow(TypeError);
        });

        it('should not create a value', () => {
          expect(valorService.create).not.toHaveBeenCalled();
        });
      });
    });
  });

  describe('update(id, valorData)', () => {
    let update: UpdateValorDTO;

    describe('case: success', () => {
      let updateResult: UpdateResult | GlobalException;
      let result: UpdateResult | GlobalException;

      describe('when a value is updated', () => {
        beforeEach(async () => {
          update = {
            nombre: 'Test'
          };
          updateResult = new UpdateResult();
          (valorService.update as jest.Mock).mockResolvedValue(updateResult);
          result = await controller.update(1, update);
        });

        it('should invoke ValorService.update()', () => {
          expect(valorService.update).toHaveBeenCalledTimes(1);
          expect(valorService.update).toHaveBeenCalledWith(1, update);
        });

        it('should update a value', () => {
          expect(result).toStrictEqual(updateResult);
        });
      });
    });

    describe('case: failure', () => {
      let expectedError: TypeError;

      describe('when a value is not updated', () => {
        beforeEach(async () => {
          expectedError = new TypeError();
          update = undefined;
          jest.spyOn(controller, 'update').mockRejectedValue(expectedError);
        });

        it('should throw when the body is undefined', async () => {
          await expect(controller.update(1, update)).rejects.toThrow(TypeError);
        });

        it('should not update a value', () => {
          expect(valorService.update).not.toHaveBeenCalled();
        });
      });
    });
  });

  describe('delete(id)', () => {
    let deleteResult: DeleteResult | GlobalException;
    let result: DeleteResult | GlobalException;

    describe('case: success', () => {
      describe('when a value is found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 1;
          (valorService.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await controller.delete(1);
        });

        it('should invoke ValorService.delete()', () => {
          expect(valorService.delete).toHaveBeenCalledTimes(1);
          expect(valorService.delete).toHaveBeenCalledWith(1);
        });

        it('should delete a value', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });

    describe('case: failure', () => {
      describe('when a value is not found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 0;
          (valorService.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await controller.delete(1);
        });

        it('should invoke ValorService.delete()', () => {
          expect(valorService.delete).toHaveBeenCalledTimes(1);
          expect(valorService.delete).toHaveBeenCalledWith(1);
        });

        it('should not delete a value', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });
  });
});
