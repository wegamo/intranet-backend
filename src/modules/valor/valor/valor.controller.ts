import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { DeleteResult, UpdateResult } from 'typeorm';

import { CreateValorDTO } from '../../../models/valor/dto/create-valor.dto';
import { UpdateValorDTO } from '../../../models/valor/dto/update-valor.dto';
import { Valor } from '../../../models/valor/valor.entity';

import { ValorService } from './valor.service';

@Controller('valor')
export class ValorController {
  constructor(private readonly valorService: ValorService) {}

  @Get()
  findAll(): Promise<Valor[]> {
    return this.valorService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: number): Promise<Valor> {
    return this.valorService.findOne(id);
  }

  @Post()
  async create(@Body() valorData: CreateValorDTO): Promise<Valor> {
    return this.valorService.create(valorData);
  }

  @Put(':id')
  async update(@Param('id') id: number, @Body() valorData: UpdateValorDTO): Promise<UpdateResult> {
    return this.valorService.update(id, valorData);
  }

  @Delete(':id')
  async delete(@Param('id') id: number): Promise<DeleteResult> {
    return this.valorService.delete(id);
  }
}
