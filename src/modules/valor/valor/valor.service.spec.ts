import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';

import { CreateValorDTO } from '../../../models/valor/dto/create-valor.dto';
import { UpdateValorDTO } from '../../../models/valor/dto/update-valor.dto';
import { Valor } from '../../../models/valor/valor.entity';

import { ValorService } from './valor.service';

import { GlobalException } from './../../exception/global-exception';
import { ValorInt } from './../../dto_response/Valor_Payload';
import { SampleValor } from './../../../../test/ObjectSamples';

describe('ValorService', () => {
  let service: ValorService;
  let RepositoryMock: jest.Mock;
  let repository: Repository<Valor>;

  beforeEach(() => {
    RepositoryMock = jest.fn(() => ({
      find: jest.fn(),
      findOne: jest.fn(),
      save: jest.fn(),
      update: jest.fn(),
      delete: jest.fn()
    }));
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ValorService,
        { provide: getRepositoryToken(Valor), useValue: new RepositoryMock() }
      ]
    }).compile();

    repository = module.get(getRepositoryToken(Valor));
    service = module.get<ValorService>(ValorService);
  });

  describe('findAll()', () => {
    let expectedValores: Valor[];
    let result: Valor[];

    describe('case: success', () => {
      describe('when there are values', () => {
        beforeEach(async () => {
          expectedValores = [
            SampleValor
          ];
          (repository.find as jest.Mock).mockResolvedValue(expectedValores);
          result = await service.findAll();
        });

        it('should invoke repository.find()', () => {
          expect(repository.find).toHaveBeenCalledTimes(1);
        });

        it('should return an array of values', () => {
          expect(result).toStrictEqual(expectedValores);
        });
      });
    });

    describe('case: failure', () => {
      describe('when there are not values', () => {
        beforeEach(async () => {
          expectedValores = [];
          (repository.find as jest.Mock).mockResolvedValue(expectedValores);
          result = await service.findAll();
        });

        it('should invoke repository.find()', () => {
          expect(repository.find).toHaveBeenCalledTimes(1);
        });

        it('should return an array empty', () => {
          expect(result).toStrictEqual(expectedValores);
        });
      });
    });
  });

  describe('findOne(id)', () => {
    let expectedValor: Valor;
    let result: Valor;

    describe('case: success', () => {
      describe('when a value is found', () => {
        beforeEach(async () => {
          expectedValor = SampleValor;
          (repository.findOne as jest.Mock).mockResolvedValue(expectedValor);
          result = await service.findOne(1);
        });

        it('should invoke repository.findOne()', () => {
          expect(repository.findOne).toHaveBeenCalledTimes(1);
          expect(repository.findOne).toHaveBeenCalledWith(1);
        });

        it('should return a value', () => {
          expect(result).toStrictEqual(expectedValor);
        });
      });
    });

    describe('case: failure', () => {
      describe('when a value is not found', () => {
        beforeEach(async () => {
          expectedValor = undefined;
          (repository.findOne as jest.Mock).mockResolvedValue(expectedValor);
          result = await service.findOne(1);
        });

        it('should invoke repository.findOne()', () => {
          expect(repository.findOne).toHaveBeenCalledTimes(1);
          expect(repository.findOne).toHaveBeenCalledWith(1);
        });

        it('should return undefined', () => {
          expect(result).toStrictEqual(expectedValor);
        });
      });
    });
  });

  describe('create(valor)', () => {
    const {id,...rest} = SampleValor;
    let body: CreateValorDTO;

    describe('case: success', () => {
      let expectedValor: Valor;
      let result: Valor;

      describe('when a value is created', () => {
        beforeEach(async () => {
          body = rest;
          expectedValor = SampleValor;
          (repository.save as jest.Mock).mockResolvedValue(expectedValor);
          result = await service.create(body);
        });

        it('should invoke repository.save()', () => {
          expect(repository.save).toHaveBeenCalledTimes(1);
          expect(repository.save).toHaveBeenCalledWith(body);
        });

        it('should create a value', () => {
          expect(result).toStrictEqual(expectedValor);
        });
      });
    });

    describe('case: failure', () => {
      let expectedError: TypeError;

      describe('when a value is not created', () => {
        beforeEach(async () => {
          body = undefined;
          expectedError = new TypeError();
          jest.spyOn(service, 'create').mockRejectedValue(expectedError);
        });

        it('should throw when the body is undefined', async () => {
          await expect(service.create(body)).rejects.toThrow(TypeError);
        });

        it('should not create a value', () => {
          expect(repository.save).not.toHaveBeenCalled();
        });
      });
    });
  });

  describe('update(id, valor)', () => {
    let update: UpdateValorDTO;

    describe('case: success', () => {
      let updateResult: UpdateResult | GlobalException;
      let result: UpdateResult | GlobalException;

      describe('when a value is updated', () => {
        beforeEach(async () => {
          update = {
            nombre: 'Test'
          };
          updateResult = new UpdateResult();
          (repository.update as jest.Mock).mockResolvedValue(updateResult);
          result = await service.update(1, update);
        });

        it('should invoke repository.update()', () => {
          expect(repository.update).toHaveBeenCalledTimes(1);
          expect(repository.update).toHaveBeenCalledWith(1, update);
        });

        it('should update a value', () => {
          expect(result).toStrictEqual(updateResult);
        });
      });
    });

    describe('case: failure', () => {
      let expectedError: TypeError;

      describe('when a value is not updated', () => {
        beforeEach(async () => {
          expectedError = new TypeError();
          update = undefined;
          jest.spyOn(service, 'update').mockRejectedValue(expectedError);
        });

        it('should throw when the body is undefined', async () => {
          await expect(service.update(1, update)).rejects.toThrow(TypeError);
        });

        it('should not update a value', () => {
          expect(repository.update).not.toHaveBeenCalled();
        });
      });
    });
  });

  describe('delete(id)', () => {
    let deleteResult: DeleteResult | GlobalException;
    let result: DeleteResult | GlobalException;

    describe('case: success', () => {
      describe('when a value is found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 1;
          (repository.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await service.delete(1);
        });

        it('should invoke repository.delete()', () => {
          expect(repository.delete).toHaveBeenCalledTimes(1);
          expect(repository.delete).toHaveBeenCalledWith(1);
        });

        it('should delete a value', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });

    describe('case: failure', () => {
      describe('when a value is not found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 0;
          (repository.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await service.delete(1);
        });

        it('should invoke repository.delete()', () => {
          expect(repository.delete).toHaveBeenCalledTimes(1);
          expect(repository.delete).toHaveBeenCalledWith(1);
        });

        it('should not delete a value', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });
  });
});
