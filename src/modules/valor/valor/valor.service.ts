import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';

import { CreateValorDTO } from '../../../models/valor/dto/create-valor.dto';
import { UpdateValorDTO } from '../../../models/valor/dto/update-valor.dto';
import { Valor } from '../../../models/valor/valor.entity';

@Injectable()
export class ValorService {
  constructor(
    @InjectRepository(Valor)
    private readonly valorRepository: Repository<Valor>
  ) {}

  async findAll(): Promise<Valor[]> {
    return await this.valorRepository.find();
  }

  async findOne(id: number): Promise<Valor> {
    return await this.valorRepository.findOne(id);
  }

  async create(valor: CreateValorDTO): Promise<Valor> {
    return await this.valorRepository.save(valor);
  }

  async update(id: number, valor: UpdateValorDTO): Promise<UpdateResult> {
    return await this.valorRepository.update(id, valor);
  }

  async delete(id: number): Promise<DeleteResult> {
    return await this.valorRepository.delete(id);
  }
}
