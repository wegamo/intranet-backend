import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { Valor } from '../../models/valor/valor.entity';

import { ValorController } from './valor/valor.controller';
import { ValorService } from './valor/valor.service';

@Module({
  imports: [TypeOrmModule.forFeature([Valor])],
  providers: [ValorService],
  controllers: [ValorController]
})
export class ValorModule {}
