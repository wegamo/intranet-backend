import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { HistDiasDispVacaciones } from '../../../models/hist-dias-disp-vacaciones/hist-dias-disp-vacaciones.entity';
import { CreateHistDiasDispVacacionesDTO } from '../../../models/hist-dias-disp-vacaciones/dto/create-hist-dias-disp-vacaciones.dto';

@Injectable()
export class HistDiasDispVacacionesService {
  constructor(
    @InjectRepository(HistDiasDispVacaciones)
    private readonly histDiasDispVacacionesRepository: Repository<HistDiasDispVacaciones>
  ) {}

  create(histDiasDisp: CreateHistDiasDispVacacionesDTO): Promise<HistDiasDispVacaciones> {
    return this.histDiasDispVacacionesRepository.save(histDiasDisp);
  }

  findLastest(id: number): Promise<HistDiasDispVacaciones> {
    return this.histDiasDispVacacionesRepository.findOne({
      where: { persona: id },
      order: { fechaSolicitud: 'DESC' }
    });
  }
}
