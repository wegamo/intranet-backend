import { Controller, Get, Request, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiUseTags } from '@nestjs/swagger';
import { HistDiasDispVacaciones } from '../../../models/hist-dias-disp-vacaciones/hist-dias-disp-vacaciones.entity';
import { Persona } from '../../../models/persona/persona.entity';
import { HistDiasDispVacacionesService } from './hist-dias-disp-vacaciones.service';

@ApiUseTags('Histórico de dias disponibles')
@Controller('hist-dias-disp-vacaciones')
export class HistDiasDispVacacionesController {
  constructor(private readonly histDiasDispVacacionesService: HistDiasDispVacacionesService) {}

  @UseGuards(AuthGuard('jwt'))
  @Get('available')
  findAvailableDays(@Request() { user }: { user: Persona }): Promise<HistDiasDispVacaciones> {
    return this.histDiasDispVacacionesService.findLastest(user.id);
  }
}
