import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { HistDiasDispVacacionesService } from './hist-dias-disp-vacaciones/hist-dias-disp-vacaciones.service';
import { HistDiasDispVacacionesController } from './hist-dias-disp-vacaciones/hist-dias-disp-vacaciones.controller';
import { HistDiasDispVacaciones } from '../../models/hist-dias-disp-vacaciones/hist-dias-disp-vacaciones.entity';

@Module({
  imports: [TypeOrmModule.forFeature([HistDiasDispVacaciones])],
  providers: [HistDiasDispVacacionesService],
  exports: [HistDiasDispVacacionesService],
  controllers: [HistDiasDispVacacionesController]
})
export class HistDiasDispVacacionesModule {}
