import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { HistCargo } from '../../models/hist-cargos/hist-cargos.entity';
import { HistCargosController } from './hist-cargos/hist-cargos.controller';
import { HistCargosService } from './hist-cargos/hist-cargos.service';

@Module({
  imports: [TypeOrmModule.forFeature([HistCargo])],
  controllers: [HistCargosController],
  providers: [HistCargosService],
  exports: [HistCargosService]
})
export class HistCargosModule {}
