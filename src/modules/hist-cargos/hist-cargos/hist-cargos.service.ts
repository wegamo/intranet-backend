import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateHistCargoDTO } from '../../../models/hist-cargos/dto/create-hist-cargo.dto';
import { HistCargo } from '../../../models/hist-cargos/hist-cargos.entity';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { UpdateHistCargoDTO } from '../../../models/hist-cargos/dto/update-hist-cargo.dto';

@Injectable()
export class HistCargosService {
  constructor(
    @InjectRepository(HistCargo) private readonly histCargoRepository: Repository<HistCargo>
  ) {}

  findOne(id: number): Promise<HistCargo> {
    return this.histCargoRepository.findOne(id);
  }

  async findByPersona(personaId: number): Promise<HistCargo[]> {
    return await this.histCargoRepository.find({ persona: { id: personaId } });
  }

  async create(histCargo: CreateHistCargoDTO): Promise<HistCargo> {
    return await this.histCargoRepository.save(histCargo);
  }

  update(histCargo: UpdateHistCargoDTO): Promise<HistCargo> {
    return this.histCargoRepository.save(histCargo);
  }

  async delete(id: number): Promise<DeleteResult> {
    try {
      return await this.histCargoRepository.delete(id);
    } catch (Err) {
      throw new HttpException(
        {
          status: HttpStatus.INTERNAL_SERVER_ERROR,
          error: 'Ha ocurrido un error al eliminar registro histórico (cargos)'
        },
        HttpStatus.INTERNAL_SERVER_ERROR
      );
    }
  }
}
