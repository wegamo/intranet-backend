import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Put,
  UseFilters
} from '@nestjs/common';
import { CreateHistCargoDTO } from '../../../models/hist-cargos/dto/create-hist-cargo.dto';
import { UpdateHistCargoDTO } from '../../../models/hist-cargos/dto/update-hist-cargo.dto';
import { HistCargo } from '../../../models/hist-cargos/hist-cargos.entity';
import { DeleteResult } from 'typeorm';
import { GlobalExceptionFilter } from './../../exception/global-exception.filter';
import { HistCargosService } from './hist-cargos.service';
import { ApiUseTags } from '@nestjs/swagger';

@ApiUseTags('Histórico de cargos')
@Controller('hist-cargos')
@UseFilters(new GlobalExceptionFilter())
export class HistCargosController {
  constructor(private readonly histCargosService: HistCargosService) {}

  @Post('create')
  async create(@Body() histCargo: CreateHistCargoDTO): Promise<HistCargo> {
    try {
      return await this.histCargosService.create(histCargo);
    } catch (err) {
      throw new HttpException('Error al Crear el Historico de Cargo', HttpStatus.BAD_REQUEST);
    }
  }

  @Put('update/:id')
  async update(
    @Param('id') id: number,
    @Body() histCargoData: UpdateHistCargoDTO
  ): Promise<HistCargo> {
    try {
      let histCargo = await this.histCargosService.findOne(id);
      if (!histCargo) {
        throw new HttpException('El Histórico de cargo no existe', HttpStatus.BAD_REQUEST);
      }
      histCargo = { ...histCargo, ...histCargoData };
      return await this.histCargosService.update(histCargo);
    } catch (err) {
      throw new HttpException('Error al Actualizar el Historico de Cargo', HttpStatus.BAD_REQUEST);
    }
  }

  @Delete(':id')
  async delete(@Param('id') id: number): Promise<DeleteResult> {
    let cargo = await this.histCargosService.findOne(id);
    if (!cargo) {
      throw new HttpException('El cargo no existe', HttpStatus.BAD_REQUEST);
    }
    return this.histCargosService.delete(cargo.id);
  }
}
