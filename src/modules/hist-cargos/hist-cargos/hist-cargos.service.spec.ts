import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { HistCargo } from '../../../models/hist-cargos/hist-cargos.entity';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { HistCargosService } from './hist-cargos.service';
import { CreateHistCargoDTO } from '../../../models/hist-cargos/dto/create-hist-cargo.dto';
import { SampleHistCargo } from '../../../../test/ObjectSamples';
import { UpdateHistCargoDTO } from '../../../models/hist-cargos/dto/update-hist-cargo.dto';

describe('HistCargosService', () => {
  let service: HistCargosService;
  let RepositoryMock: jest.Mock;
  let repository: Repository<HistCargo>;

  beforeEach(() => {
    RepositoryMock = jest.fn(() => ({
      find: jest.fn(),
      save: jest.fn(),
      delete: jest.fn(),
      update: jest.fn(),
      createQueryBuilder: jest.fn(() => ({
        take: jest.fn().mockReturnThis(),
      })),
    }));
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        HistCargosService,
        { provide: getRepositoryToken(HistCargo), useValue: new RepositoryMock() }
      ]
    }).compile();
    repository = module.get(getRepositoryToken(HistCargo));
    service = module.get<HistCargosService>(HistCargosService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('findAll()', () => {
    let expectedHistCargos: HistCargo[];
    let result: HistCargo[];

    describe('case : success', () => {
      describe('when there are cargos of a person', () => {
        beforeEach(async () => {
          expectedHistCargos = [SampleHistCargo];
          (repository.find as jest.Mock).mockResolvedValue(expectedHistCargos);
          result = await service.findByPersona(SampleHistCargo.persona.id);
        });

        it('should invoke repository.findByPersona()', () => {
          expect(repository.find).toHaveBeenCalledTimes(1);
        });

        it('should return an array of roles', () => {
          expect(result).toStrictEqual(expectedHistCargos);
        });
      });
    });

    describe('case : failure', () => {
      describe('when there are not cargos of a person', () => {
        beforeEach(async () => {
          expectedHistCargos = [];
          (repository.find as jest.Mock).mockResolvedValue(expectedHistCargos);
          result = await service.findByPersona(SampleHistCargo.persona.id);
        });

        it('should invoke repository.findByPersona()', () => {
          expect(repository.find).toHaveBeenCalledTimes(1);
        });

        it('should return an empty array', () => {
          expect(result).toStrictEqual(expectedHistCargos);
        });
      });
    });
  });

  describe('create(CreateHistCargoDTO)', () => {
    const { id, ...rest } = SampleHistCargo;
    let body: CreateHistCargoDTO;

    describe('case : success', () => {
      let expectedHistCargo: HistCargo;
      let result: HistCargo;

      describe('when an role is created', () => {
        beforeEach(async () => {
          body = rest;
          expectedHistCargo = SampleHistCargo;
          (repository.save as jest.Mock).mockResolvedValue(expectedHistCargo);
          result = await service.create(body);
        });

        it('should invoke repository.save()', () => {
          expect(repository.save).toHaveBeenCalledTimes(1);
          expect(repository.save).toHaveBeenCalledWith(body);
        });

        it('should create a role', () => {
          expect(result).toStrictEqual(expectedHistCargo);
        });
      });
    });

    describe('case : failure', () => {
      let expectedError: TypeError;
      let body: undefined;

      describe('when a role is not created', () => {
        beforeEach(async () => {
          body = undefined;
          expectedError = new TypeError();
          jest.spyOn(service, 'create').mockRejectedValue(expectedError);
        });

        it('it should throw when the body is undefined', async () => {
          await expect(service.create(body)).rejects.toThrow(TypeError);
        });

        it('should not create an role', () => {
          expect(repository.save).not.toHaveBeenCalled();
        });
      });
    });
  });

  describe('update(id, histCargo)', () => {
    let update: UpdateHistCargoDTO;

    describe('case: success', () => {
      let updateResult: HistCargo;
      let result: HistCargo;

      describe('when a company is updated', () => {
        beforeEach(async () => {
          update = {
            fechaInicio:SampleHistCargo.fechaInicio,
            cargo:SampleHistCargo.cargo,
            fechaFin: new Date()
          };
          //updateResult = new UpdateResult();
          (repository.save as jest.Mock).mockResolvedValue(updateResult);
          result = await service.update(update);
        });

        it('should invoke repository.update()', () => {
          expect(repository.save).toHaveBeenCalledTimes(1);
          expect(repository.save).toHaveBeenCalledWith(update);
        });

        it('should update a company', () => {
          expect(result).toStrictEqual(updateResult);
        });
      });
    });

    describe('case: failure', () => {
      let expectedError: TypeError;

      describe('when a company is not updated', () => {
        beforeEach(async () => {
          expectedError = new TypeError();
          update = undefined;
          jest.spyOn(service, 'update').mockRejectedValue(expectedError);
        });

        it('should throw when the body is undefined', async () => {
          await expect(service.update(update)).rejects.toThrow(TypeError);
        });

        it('should not update a company', () => {
          expect(repository.save).not.toHaveBeenCalled();
        });
      });
    });
  });

  describe('delete(id)', () => {
    let deleteResult: DeleteResult;
    let result: DeleteResult;

    describe('case : success', () => {
      describe('when an role is found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 1;
          (repository.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await service.delete(1);
        });

        it('should invoke repository.delete()', () => {
          expect(repository.delete).toHaveBeenCalledTimes(1);
          expect(repository.delete).toHaveBeenCalledWith(1);
        });

        it('should delete an role', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });

    describe('case : failure', () => {
      let expectedError: TypeError;

      describe('when a role is not found', () => {
        beforeEach(async () => {
          deleteResult = new DeleteResult();
          deleteResult.affected = 0;
          (repository.delete as jest.Mock).mockResolvedValue(deleteResult);
          result = await service.delete(1);
        });

        it('should invoke repository.delete()', () => {
          expect(repository.delete).toHaveBeenCalledTimes(1);
          expect(repository.delete).toHaveBeenCalledWith(1);
        });

        it('should not delete an role', () => {
          expect(result).toStrictEqual(deleteResult);
        });
      });
    });
  });
});
