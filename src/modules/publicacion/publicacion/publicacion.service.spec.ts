import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Publicacion } from '../../../models/publicacion/publicacion.entity';
import { Repository } from 'typeorm';
import { PublicacionService } from './publicacion.service';

describe('PublicacionService', () => {
  let service: PublicacionService;
  let RepositoryMock: jest.Mock;
  let repository: Repository<Publicacion>;

  beforeEach(() => {
    RepositoryMock = jest.fn(() => ({
      find: jest.fn(),
      findOne: jest.fn(),
      save: jest.fn(),
      update: jest.fn(),
      delete: jest.fn()
    }));
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        PublicacionService,
        { provide: getRepositoryToken(Publicacion), useValue: new RepositoryMock() },
      ]
    }).compile();

    repository = module.get(getRepositoryToken(Publicacion));
    service = module.get<PublicacionService>(PublicacionService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
