import {
  Body,
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Req,
  Res,
  UploadedFile,
  UseGuards,
  UseInterceptors
} from '@nestjs/common';
import { Response, Request } from 'express';
import { FileInterceptor } from '@nestjs/platform-express';
import { PersonaService } from '../../persona/persona/persona.service';
import { UsuarioPermisoService } from '../../usuario-permiso/usuario-permiso/usuario-permiso.service';
import { Aws3CntmService } from '../../aws3-cntm/aws3-cntm/aws3-cntm.service';
import { PublicacionService } from './publicacion.service';
import { Publicacion } from '../../../models/publicacion/publicacion.entity';
import { AwsStructure } from '../../aws3-cntm/aws3-cntm/aws.interface';
import { AuthGuard } from '@nestjs/passport';
import { TipoObjeto } from '../../../enums/tipo-objeto.enum';
import { Persona } from '../../../models/persona/persona.entity';
import { TipoPermiso } from '../../../enums/tipo-permiso.enum';
import { UsuarioPermiso } from '../../../models/usuario-permiso/usuario-permiso.entity';
import { DeleteResult } from 'typeorm';
import * as AWS from 'aws-sdk';
import {
  ApiBearerAuth,
  ApiConsumes,
  ApiImplicitBody,
  ApiOperation,
  ApiResponse,
  ApiUseTags
} from '@nestjs/swagger';
import { CreatePublicacionDTO } from '../../../models/publicacion/dto/create-publicacion.dto';
import { PublicacionResponseDTO } from '../../../models/publicacion/dto/publicacion-response.dto';
import { AreaService } from '../../area/area/area.service';
import { VerticalService } from '../../vertical/vertical/vertical.service';
import { Area } from '../../../models/area/area.entity';
import { Vertical } from '../../../models/vertical/vertical.entity';

@ApiBearerAuth()
@ApiUseTags('Publicaciones')
@Controller('publicacion')
export class PublicacionController {
  constructor(
    private readonly publicacionService: PublicacionService,
    private readonly s3Service: Aws3CntmService,
    private readonly usuarioPermisoService: UsuarioPermisoService,
    private readonly personaService: PersonaService,
    private readonly areaService: AreaService,
    private readonly verticalService: VerticalService,
  ) {}

  @ApiOperation({ title: 'Crear noticia', description: 'Creación de noticia' })
  @ApiResponse({
    status: 200,
    description: 'Noticia generada',
    type: PublicacionResponseDTO
  })
  @ApiConsumes('multipart/form-data')
  @ApiImplicitBody({
    name: 'body',
    description: 'Datos de la publicacion',
    type: CreatePublicacionDTO
  })
  @UseGuards(AuthGuard('jwt'))
  @UseInterceptors(FileInterceptor('file'))
  @Post('create')
  async create(
    @Body() publicacionData: { body: string },
    @UploadedFile() file: Express.Multer.File
  ): Promise<Publicacion> {
    try {
      let nuevaPublicacion: {
        parentKey: string;
        tipoObjeto: TipoObjeto;
        nombreObjeto: string;
        user: number;
        side:string;
        sectionId:number;
      } = JSON.parse(publicacionData.body);
      const { parentKey, tipoObjeto, nombreObjeto, side, sectionId } = nuevaPublicacion;
      let resPublicacion: Publicacion;
      let publicante: Persona = await this.personaService.findOne(nuevaPublicacion.user);
      let awsRes: { resp: AWS.S3.PutObjectOutput; nKey: string };
      awsRes = await this.s3Service.addAwsObject(
        nombreObjeto.toLowerCase().replace(/ /g,"-"),
        parentKey,
        file.originalname === '*' ? null : file
      );
      //INSTANCIAR BSD
      let targetArea: Area = null;
      let targetVertical: Vertical = null;

      if(side === 'T'){
        targetArea = await this.areaService.findById(sectionId);
      }  
      else if(side === 'V'){
        targetVertical = await this.verticalService.findById(sectionId)
      }

      let nPublicacion: CreatePublicacionDTO = {
        publicante: publicante,
        ubicacion: awsRes.nKey,
        tipoObjeto: tipoObjeto,
        nombreObjeto: nombreObjeto,
        actualizacion: `${Date.now()} ${publicante.primerNombre + ' ' + publicante.primerApellido}`,
        area: targetArea,
        vertical:targetVertical,
      }
    
      resPublicacion = await this.publicacionService.create(nPublicacion);
      let nPermiso: UsuarioPermiso = await this.usuarioPermisoService.create({
        tipoPermiso: TipoPermiso.READWRITE,
        persona: publicante,
        publicacion: resPublicacion
      });
      return resPublicacion;
    } catch (err) {
      throw new HttpException(
        `Error al Crear el publicación ${err.message}`,
        HttpStatus.INTERNAL_SERVER_ERROR
      );
    }
  }
  @ApiOperation({ title: 'Eliminar publicación', description: 'Eliminación de publicación' })
  @ApiResponse({
    status: 200,
    description: 'Preview de documento'
  })
  @ApiResponse({
    status: 406,
    description: 'Rechazo ante eliminación de archivo protegido'
  })
  @UseGuards(AuthGuard('jwt'))
  @Post(':id')
  async deletePublicacion(@Param('id') id: number): Promise<any> {
    try {
      let pubData: Publicacion = await this.publicacionService.findByID(id);
      if (pubData.tipoObjeto === 'A') {
        await this.s3Service.removeAwsObject(pubData.ubicacion);
      } else {
        throw new HttpException(
          `No esta permitido eliminar un directorio`,
          HttpStatus.NOT_ACCEPTABLE
        );
      }
      let delPermisos: DeleteResult = await this.usuarioPermisoService.deleteByFile(pubData.id);
      return await this.publicacionService.deleteByID(id);
    } catch (err) {
      throw new HttpException(
        `Error al eliminar publicación : ${err.message}`,
        HttpStatus.INTERNAL_SERVER_ERROR
      );
    }
  }

  @ApiOperation({
    title: 'Obtener estado de bucket',
    description: 'Obtención de publicaciones en bucket'
  })
  @ApiResponse({
    status: 200,
    description: 'Estructura de bucket'
  })
  
  @UseGuards(AuthGuard('jwt'))
  @Get()
  async getPublicaciones(): Promise<AwsStructure> {
    try {
      return await this.s3Service.getAwsStructure('documentos');
    } catch (err) {
      throw new HttpException(
        `Error al obtener publicaciones ${err.message}`,
        HttpStatus.INTERNAL_SERVER_ERROR
      );
    }
  }
  //SOLUCION BAJO ESTUDIO
  //@UseGuards(AuthGuard('jwt'))
  @ApiOperation({ title: 'Obtener preview', description: 'Obtener previe de documento' })
  @ApiResponse({
    status: 200,
    description: 'Preview de documento'
  })
  @Get('safe/:pId')
  async getSafePublicacion(
    @Param('pId') pId: number,
    @Res() res: Response,
    @Req() req: Request
  ): Promise<any> {
    try {
      let pubRes: Publicacion = await this.publicacionService.findByID(pId);
      let fileResp: AWS.S3.GetObjectOutput = await this.s3Service.getFromBucket(pubRes.ubicacion);
      //console.log(fileResp);
      let toHeaders = {
        'Content-Type': fileResp.ContentType,
        'Content-Length': fileResp.ContentLength
      };
      //console.log(toHeaders);
      res.set({
        ...toHeaders
      });
      res.send(fileResp.Body);
    } catch (err) {
      throw new HttpException(
        `Error al obtener publicaciones ${err.message}`,
        HttpStatus.INTERNAL_SERVER_ERROR
      );
    }
  }
}
