import { Test, TestingModule } from '@nestjs/testing';
import { PublicacionController } from './publicacion.controller';
import { PublicacionService } from './publicacion.service';

describe('Publicacion Controller', () => {
  let controller: PublicacionController;
  let PublicacionServiceMock: jest.Mock<Partial<PublicacionService>>;
  let publicacionService: PublicacionService;

  beforeEach(() => {
    PublicacionServiceMock = jest.fn<Partial<PublicacionService>, PublicacionService[]>(() => ({

    }));
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PublicacionController],
      providers: [{ provide: PublicacionService, useValue: new PublicacionServiceMock() }]
    }).compile();

    publicacionService = module.get<PublicacionService>(PublicacionService);
    controller = module.get<PublicacionController>(PublicacionController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
