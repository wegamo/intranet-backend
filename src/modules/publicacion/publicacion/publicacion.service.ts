import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Publicacion } from '../../../models/publicacion/publicacion.entity';
import { DeleteResult, Repository } from 'typeorm';
import { Aws3CntmService } from '../../aws3-cntm/aws3-cntm/aws3-cntm.service';
import configService from '../../../config/config/config.service';
import { UsuarioPermisoService } from '../../usuario-permiso/usuario-permiso/usuario-permiso.service';
import { CreatePublicacionDTO } from '../../../models/publicacion/dto/create-publicacion.dto';
import { PersonaService } from '../../persona/persona/persona.service';
const AWS_S3_BUCKET_NAME = configService.get('AWS_S3_BUCKET_NAME');
@Injectable()
export class PublicacionService {
    constructor(
        @InjectRepository(Publicacion) private readonly publicacionRepository: Repository<Publicacion>,
      ) {}
    async create(publicacionData:CreatePublicacionDTO):Promise<Publicacion> {
      return await this.publicacionRepository.save(publicacionData);
    }
    async deleteByID(publicacionId:number): Promise<DeleteResult> {
      return await this.publicacionRepository.delete(publicacionId)
    }
    async findByID(publicacionId:number): Promise<Publicacion> {
      return await this.publicacionRepository.findOne({where:{id:publicacionId},});
    }
    //{ current , childs }
    async findByCascade(publicacionId:number): Promise<Publicacion[]> {
      return await this.publicacionRepository.find({where:{id:publicacionId}})
    }
}
