import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Publicacion } from '../../models/publicacion/publicacion.entity';
import { AreaModule } from '../area/area.module';
import { AreaService } from '../area/area/area.service';
import { Aws3CntmModule } from '../aws3-cntm/aws3-cntm.module';
import { PersonaModule } from '../persona/persona.module';
import { PersonaService } from '../persona/persona/persona.service';
import { UsuarioPermisoModule } from '../usuario-permiso/usuario-permiso.module';
import { UsuarioPermisoService } from '../usuario-permiso/usuario-permiso/usuario-permiso.service';
import { VerticalModule } from '../vertical/vertical.module';
import { VerticalService } from '../vertical/vertical/vertical.service';
import { PublicacionController } from './publicacion/publicacion.controller';
import { PublicacionService } from './publicacion/publicacion.service';

@Module({
  imports:[
  TypeOrmModule.forFeature([Publicacion]),
  Aws3CntmModule,
  forwardRef(()=> UsuarioPermisoModule),
  forwardRef(() => PersonaModule),
  forwardRef(() => AreaModule),
  forwardRef(()=> VerticalModule)
  ],
  controllers: [PublicacionController],
  providers: [
    PublicacionService, 
    UsuarioPermisoService, 
    PersonaService, 
    AreaService, 
    VerticalService
  ],
  exports:[PublicacionService]
})
export class PublicacionModule {}
