import { Module } from '@nestjs/common';
import { Aws3CntmService } from './aws3-cntm/aws3-cntm.service';
import { Aws3CntmController } from './aws3-cntm/aws3-cntm.controller';

@Module({
  imports: [],
  providers: [Aws3CntmService],
  exports:[Aws3CntmService],
  controllers: [Aws3CntmController],
})
export class Aws3CntmModule {}
