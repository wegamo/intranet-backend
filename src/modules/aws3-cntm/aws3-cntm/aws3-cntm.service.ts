import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import * as AWS from 'aws-sdk';
import configService from './../../../config/config/config.service';
import * as fs from 'fs';
import * as path from 'path';

import {
  FileOutput,
  BucketFolder,
  AwsStructure,
  AwsComplexFolder,
  AWS_config,
  AwsNoticias,
  AwsNoticia
} from './aws.interface';

const AWS_S3_BUCKET_NAME = configService.get('AWS_S3_BUCKET_NAME');

AWS.config.update({
  accessKeyId: configService.get('AWS_ACCESS_KEY_ID'),
  secretAccessKey: configService.get('AWS_SECRET_ACCESS_KEY')
});

AWS.config.logger = console;
const s3 = new AWS.S3({
  apiVersion: '2006-03-01',
  region: 'us-east-1'
});

@Injectable()
export class Aws3CntmService {
  constructor() {}

  async removeAwsObject(_key: string): Promise<any> {
    let DelRes = [];
    DelRes.push(
      await s3
        .deleteObject({
          Bucket: configService.get('AWS_S3_BUCKET_NAME'),
          Key: _key
        })
        .promise()
    );
    return DelRes;
  }
  //OBLIGADO A REFACTORIZAR
  async addBasicObject(
    data: any,
    _key: string,
    parentKey: string
  ): Promise<AWS.S3.PutObjectOutput> {
    let res: AWS.S3.PutObjectOutput = await s3
      .putObject(
        {
          Bucket: configService.get('AWS_S3_BUCKET_NAME'),
          ACL: 'public-read-write',
          Key: `${parentKey}${_key}`,
          Body: data
        },
        (awsError, awsData) => {
          if (awsError) {
            throw new HttpException('Ha ocurrido un error en su solicitud', HttpStatus.BAD_REQUEST);
          } else {
            console.log(awsData);
          }
        }
      )
      .promise();
    return res;
  }

  async addAwsObject(
    _key: string,
    parentKey: string,
    file: any
  ): Promise<{ resp: AWS.S3.PutObjectOutput; nKey: string }> {
    let FData = [];
    let NewKey = '';

    if (file !== null) {
      NewKey = `${parentKey}${_key}`;
    } else {
      NewKey = `${parentKey}${_key}/`;
    }
    FData.push(
      await s3
        .putObject({
          Bucket: configService.get('AWS_S3_BUCKET_NAME'),
          ACL: 'public-read-write',
          Key: NewKey,
          Body: file ? file.buffer : null
        })
        .promise()
    );
    return { resp: FData[0], nKey: NewKey };
  }

  async getBasicObject(_key: string): Promise<any> {
    let res: AWS.S3.GetObjectOutput = await s3
      .getObject({
        Bucket: configService.get('AWS_S3_BUCKET_NAME'),
        Key: _key
      })
      .promise();
    return res;
  }

  async getNoticiaById(type: string, _id: number) {
    let res: AWS.S3.GetObjectOutput = await s3
      .getObject({
        Bucket: configService.get('AWS_S3_BUCKET_NAME'),
        Key: 'complex/noticias/AwsNoticias.json'
      })
      .promise();
    let resNoticias: AwsNoticias = <AwsNoticias>JSON.parse(res.Body.toString());
    let resNoticia: AwsNoticia;
    resNoticia =
      type === 'B'
        ? resNoticias.bannerNews.filter((b) => b.id === _id)[0]
        : resNoticias.basicNews.filter((n) => n.id === _id)[0];
    return resNoticia;
  }

  async CheckBucket(_prefix: string) {
    try {
      console.log(_prefix);
      let DataRes = await s3
        .listObjects({
          Bucket: configService.get('AWS_S3_BUCKET_NAME'),
          Prefix: _prefix
        })
        .promise();
      return DataRes;
    } catch (e) {
      throw new Error(`Could not retrieve files from S3: ${e.message}`);
    }
  }
  //FUNCION PARA ESTRUCTURADO RECURSIVO
  FolderNav = (
    splitDir: string[],
    innerFolder: { [prop: string]: BucketFolder },
    asType: number,
    self: AWS.S3.Object
  ) => {
    const index = splitDir[0];
    if (asType === 0) {
      if (splitDir.length > 1) {
        splitDir.shift();
        this.FolderNav(splitDir, innerFolder[index].sFolders, asType, self);
      } else {
        innerFolder[`${index}`] = {
          folderName: `${index}`,
          folderSize: self.Size,
          folderKey: self.Key,
          lastUpdate: self.LastModified,
          lastObtain: new Date(),
          leaf: false,
          folderObjects: [],
          folder: true,
          sFolders: {}
        };
      }
    } else if (asType === 1) {
      if (splitDir.length > 2) {
        splitDir.shift();
        this.FolderNav(splitDir, innerFolder[index].sFolders, asType, self);
      } else {
        try {
          innerFolder[`${index}`].leaf = true;
          innerFolder[`${index}`].folderObjects.push({
            fileName: splitDir[1],
            fileKey: self.Key,
            fileSize: self.Size,
            fileType: null,
            fileUpd: self.LastModified
          });
        } catch (e) {
          console.log(innerFolder);
          console.log(index, splitDir);
          console.log(e);
        }
      }
    }
  };
  //
  async getAwsStructure(_Key: string): Promise<AwsStructure> {
    try {
      let gFolder = await s3
        .listObjects({
          Bucket: AWS_S3_BUCKET_NAME,
          Prefix: `${_Key}`
        })
        .promise();
      let R_awsStructure: AwsStructure = {
        BucketStatus: {
          totalSize: null,
          lastUpdate: gFolder.Contents[0].LastModified,
          lastObtain: new Date()
        },
        bucketFolders: {}
      };
      gFolder.Contents.shift();
      let NavArray = [];
      for (var x of gFolder.Contents) {
        if (x.Key.endsWith('/')) {
          NavArray = x.Key.slice(0, -1)
            .toLowerCase()
            .replace(/ /g, '-')
            .split(/\//);
          NavArray.shift();
          this.FolderNav(NavArray, R_awsStructure.bucketFolders, 0, x);
        } else {
          NavArray = x.Key.replace(/ /g, '-')
            .toLowerCase()
            .split(/\//);
          NavArray.shift();
          if (NavArray.length > 1) {
            this.FolderNav(NavArray, R_awsStructure.bucketFolders, 1, x);
          }
        }
      }
      return R_awsStructure;
    } catch (e) {
      throw new Error(`Could not retrieve files from S3: ${e.message}`);
    }
  }

  async ListFolder(_key: string): Promise<AWS.S3.ObjectList> {
    try {
      const objs = await s3
        .listObjects({
          Bucket: AWS_S3_BUCKET_NAME,
          Prefix: `documentos/${_key}`
        })
        .promise();
      if (objs.$response.error) {
        console.log(objs.$response.error);
      }
      return objs.Contents;
    } catch (e) {
      throw new Error(`Could not retrieve files from S3: ${e.message}`);
    }
  }
  async paramAwsDelete(targetKey: string, asRoot: boolean): Promise<AWS.S3.DeleteObjectsOutput> {
    let DelKeys: { Key: string }[] = [];

    if (asRoot === true) {
      let ToDel = await s3
        .listObjects({
          Bucket: AWS_S3_BUCKET_NAME,
          Prefix: targetKey
        })
        .promise();

      ToDel.Contents.forEach((item, ind) => {
        DelKeys.push({ Key: item.Key });
      });

      DelKeys.sort((a, b) => {
        if (a.Key > b.Key) {
          return -1;
        }
        if (a.Key < b.Key) {
          return 1;
        }
        return 0;
      });
    }else {
      DelKeys.push({ Key: targetKey });
    }

    return await s3
      .deleteObjects({
        Bucket: configService.get('AWS_S3_BUCKET_NAME'),
        Delete: {
          Objects: DelKeys,
          Quiet: false
        }
      })
      .promise();
  }

  async checkStateAws(transv: string): Promise<any> {
    console.log('Getting docs from: ', transv);
    let SafeObjs = async (_prefix: string): Promise<AWS.S3.ObjectList> => {
      try {
        const objs = await s3
          .listObjects({
            Bucket: AWS_S3_BUCKET_NAME,
            Prefix: `documentos/${_prefix}`
          })
          .promise();
        if (objs.$response.error) {
          console.log(objs.$response.error);
        }
        return objs.Contents;
      } catch (e) {
        throw new Error(`Could not retrieve files from S3: ${e.message}`);
      }
    };
    let Bck = await SafeObjs(transv);
    let resRet = [];
    let actIndx = -1;
    console.time('Op');
    for (var x of Bck) {
      if (x.Key.endsWith('/')) {
        resRet.push({ folderName: x.Key, folderObjects: [] });
        actIndx++;
      } else {
        resRet[actIndx].folderObjects.push({
          fileKey: x.Key,
          fileSize: x.Size,
          Etag: x.ETag,
          downTag: `https://${AWS_S3_BUCKET_NAME}/${x.Key}`
        });
      }
    }
    resRet.shift();
    console.timeEnd('Op');
    return resRet;
  }

  async getFromBucket(aws: string): Promise<AWS.S3.GetObjectOutput> {
    const obj = await s3.getObject({ Bucket: AWS_S3_BUCKET_NAME, Key: aws }).promise();
    if (obj.$response.error) {
      throw new HttpException(`AWS ERR ${obj.$response.error}`, HttpStatus.CONFLICT);
    }
    return obj;
  }

  //  CURSOS
  uploadCourses(id, file, extension) {
    try {
      return s3
        .putObject({
          Bucket: AWS_S3_BUCKET_NAME,
          ACL: 'public-read-write',
          Key: `complex/cursos/curso_${id}${extension}`,
          Body: file.buffer
        })
        .promise();
    } catch (error) {
      console.log(error);
    }
  }
  deleteCourse(id, extension) {
    try {
      return s3
        .deleteObject({
          Bucket: AWS_S3_BUCKET_NAME,
          Key: `complex/cursos/curso_${id}.${extension}`
        })
        .promise();
    } catch (error) {
      console.log(error);
    }
  }

  // DOCUMENTOS PERSONALES
  async uploadPersonalDocuments(id, file, extension) {
    try {
      return await s3
        .putObject({
          Bucket: AWS_S3_BUCKET_NAME,
          ACL: 'public-read-write',
          Key: `complex/documentos-personales/documento_${id}${extension}`,
          Body: file.buffer
        })
        .promise();
    } catch (error) {
      console.log(error);
    }
  }
  deletePersonalDocument(id, extension) {
    try {
      return s3
        .deleteObject({
          Bucket: AWS_S3_BUCKET_NAME,
          Key: `complex/documentos-personales/documento_${id}.${extension}`
        })
        .promise();
    } catch (error) {
      console.log(error);
    }
  }

  // SOLICITUDES DE PRESTACIONES
  async uploadFirmaSolicitantePrestaciones(id, file, extension) {
    try {
      return await s3
        .putObject({
          Bucket: AWS_S3_BUCKET_NAME,
          ACL: 'public-read-write',
          Key: `complex/hist-prestaciones/firma-solicitante/prestaciones_firma_empleado_${id}${extension}`,
          Body: file.buffer
        })
        .promise();
    } catch (error) {
      console.log(error);
    }
  }

  async uploadFirmaThPrestaciones(id, file, extension) {
    try {
      return await s3
        .putObject({
          Bucket: AWS_S3_BUCKET_NAME,
          ACL: 'public-read-write',
          Key: `complex/hist-prestaciones/firma-th/prestaciones_firma_th_${id}${extension}`,
          Body: file.buffer
        })
        .promise();
    } catch (error) {
      console.log(error);
    }
  }

  async uploadPresupuestoPrestaciones(id, file, extension) {
    try {
      return await s3
        .putObject({
          Bucket: AWS_S3_BUCKET_NAME,
          ACL: 'public-read-write',
          Key: `complex/hist-prestaciones/presupuesto/prestaciones_presupuesto_${id}${extension}`,
          Body: file.buffer
        })
        .promise();
    } catch (error) {
      console.log(error);
    }
  }

  // VACACIONES
  async uploadFirmaSolicitanteVacaciones(id, file, extension) {
    try {
      return await s3
        .putObject({
          Bucket: AWS_S3_BUCKET_NAME,
          ACL: 'public-read-write',
          Key: `complex/hist-vacaciones/firma-solicitante/vacaciones_firma_empleado_${id}${extension}`,
          Body: file.buffer
        })
        .promise();
    } catch (error) {
      console.log(error);
    }
  }

  async uploadFirmaSupervisorVacaciones(id, file, extension) {
    try {
      return await s3
        .putObject({
          Bucket: AWS_S3_BUCKET_NAME,
          ACL: 'public-read-write',
          Key: `complex/hist-vacaciones/firma-supervisor/vacaciones_firma_supervisor_${id}${extension}`,
          Body: file.buffer
        })
        .promise();
    } catch (error) {
      console.log(error);
    }
  }

  async uploadFirmaThVacaciones(id, file, extension) {
    try {
      return await s3
        .putObject({
          Bucket: AWS_S3_BUCKET_NAME,
          ACL: 'public-read-write',
          Key: `complex/hist-vacaciones/firma-th/vacaciones_firma_th_${id}${extension}`,
          Body: file.buffer
        })
        .promise();
    } catch (error) {
      console.log(error);
    }
  }

  // AUSENCIAS
  async uploadFirmaSolicitanteAusencias(id, file, extension) {
    try {
      return await s3
        .putObject({
          Bucket: AWS_S3_BUCKET_NAME,
          ACL: 'public-read-write',
          Key: `complex/hist-ausencias/firma-solicitante/ausencia_firma_empleado_${id}${extension}`,
          Body: file.buffer
        })
        .promise();
    } catch (error) {
      console.log(error);
    }
  }

  async uploadFirmaSupervisorAusencias(id, file, extension) {
    try {
      return await s3
        .putObject({
          Bucket: AWS_S3_BUCKET_NAME,
          ACL: 'public-read-write',
          Key: `complex/hist-ausencias/firma-supervisor/ausencia_firma_supervisor_${id}${extension}`,
          Body: file.buffer
        })
        .promise();
    } catch (error) {
      console.log(error);
    }
  }

  async uploadFirmaThAusencias(id, file, extension) {
    try {
      return await s3
        .putObject({
          Bucket: AWS_S3_BUCKET_NAME,
          ACL: 'public-read-write',
          Key: `complex/hist-ausencias/firma-th/ausencia_firma_th_${id}${extension}`,
          Body: file.buffer
        })
        .promise();
    } catch (error) {
      console.log(error);
    }
  }

  //  NOTICIAS
  async uploadNewsImage(id, image, extension) {
    try {
      return await s3
        .putObject({
          Bucket: AWS_S3_BUCKET_NAME,
          ACL: 'public-read-write',
          Key: `complex/noticias/${id}${extension}`,
          Body: image.buffer
        })
        .promise();
    } catch (error) {
      console.log(error);
    }
  }
  async deleteNewsImage(id) {
    try {
      let noticiasToDelete = await s3
        .listObjects({
          Bucket: AWS_S3_BUCKET_NAME,
          Prefix: `complex/noticias/${id}`
        })
        .promise();
      const noticiasPromises = noticiasToDelete.Contents.map((noticia) =>
        s3
          .deleteObject({
            Bucket: AWS_S3_BUCKET_NAME,
            Key: noticia.Key
          })
          .promise()
      );
      await Promise.all(noticiasPromises);
    } catch (error) {
      console.log(error);
    }
  }
}
