import { Test, TestingModule } from '@nestjs/testing';
import { Aws3CntmController } from './aws3-cntm.controller';
import { Aws3CntmService } from './aws3-cntm.service';

describe('Aws3Cntm Controller', () => {
  let controller: Aws3CntmController;
  let Aws3CntmServiceMock: jest.Mock<Partial<Aws3CntmService>>;
  let aws3Service: Aws3CntmService;

  beforeEach(() => {
    Aws3CntmServiceMock = jest.fn<Partial<Aws3CntmService>, Aws3CntmService[]>(() => ({
    
    }));
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [Aws3CntmController],
      providers:[{ provide:Aws3CntmService, useValue: new Aws3CntmServiceMock()}]
    }).compile();
    aws3Service = module.get<Aws3CntmService>(Aws3CntmService);
    controller = module.get<Aws3CntmController>(Aws3CntmController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
