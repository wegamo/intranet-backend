export interface FileOutput {
    //sampleData:string, 
    //status:string, 
    stream: Buffer,
    FileName:string
}

export interface ObjectUpload {
  Obj?: File;
  type: string /*O,F*/;
  parentKey: string /*/*- */;
  objectKey: string /* *-/obj */;
  asName: string;
}

export interface BucketFile {
  fileName: string;
  fileKey: string;
  fileSize: number;
  fileType?: number;
  fileUpd: Date;
}

export interface BucketFolder {
  folderName: string;
  folderKey: string;
  folderSize: number;
  folderObjects?: BucketFile[];
  lastUpdate: Date;
  lastObtain: Date;
  leaf: boolean;
  folder: boolean;
  sFolders?: { [prop: string]: BucketFolder };
}

export interface AwsStructure {
  BucketStatus: {
    totalSize?: number;
    lastUpdate: Date;
    lastObtain: Date;
  };
  bucketFolders?: { [prop: string]: BucketFolder };
}

export interface AwsNoticia {
  posicion: number;
  titulo: string;
  descripcion: string;
  imagen: string;
  id: number;
}

export interface AwsNoticias {
  basicNews: AwsNoticia[];
  bannerNews: AwsNoticia[];
}
/*
export interface AwsBanner {
    config:{
        redir:String,
        ind:Number,
        n_date: Date,
        title:String,
        a_body:String,
        src:String | null,
    },
    media:File | null,
}

export interface AwsNew {
    config:{
        redir:String,
        ind:Number,
        n_date: Date,
        title:String,
        a_body:String,
        src:String | null,
    },
    media:File | null,
}*/

export interface AwsComplexFolder {
  o_key: String;
  s_data?: AwsComplexRes;
  childFolders?: { [prop: string]: AwsComplexFolder };
}

export interface AWS_config {
  redir: String | null;
  ind: Number;
  n_date: Date;
  title: String;
  a_body: String | null;
  src: String | null;
}

export interface AwsComplex {
  config: {
    redir: String | null;
    n_date: Date;
    title: String;
    a_body: String | null;
    src: String | null;
  };
  media: any | null;
  ind: Number | null;
}

export interface AwsComplexUpd {
  act: String;
  config: {
    redir: String | null;
    n_date: Date;
    title: String;
    a_body: String | null;
    src: String | null;
  };
  media: any | null;
  ind: Number | null;
}

export interface AwsComplexRes {
  o_key: String;
  content: {
    redir: String | null;
    title: String;
    ind: Number | null;
    a_body: String | null;
    media: String | null;
  };
}
/*
export interface BannerRes {
    redir:String,
    ind:Number,
    title:String,
    a_body:String
    media:String | null,
}

export interface NewRes {
    redir:String,
    ind:Number,
    title:String,
    a_body:String
    media:String | null,
}*/
