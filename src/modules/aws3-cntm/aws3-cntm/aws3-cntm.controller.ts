import { Controller, Get, HttpException, HttpStatus, Query, Res } from '@nestjs/common';
import { Aws3CntmService } from './aws3-cntm.service';
import { Response } from 'express';

import {
  FileOutput,
  BucketFolder,
  BucketFile,
  AwsStructure,
  AwsComplex,
  ObjectUpload,
  AwsComplexUpd
} from './aws.interface';
import { ApiUseTags } from '@nestjs/swagger';

@ApiUseTags('Aws-S3')
@Controller('aws3-cntm')
export class Aws3CntmController {
  constructor(private readonly _awsService: Aws3CntmService) {}
  /*
  @Post('/content')
  GetAwsStructure(@Body() _body: { _key: string }): Promise<any> {
    return this._awsService.getAwsStructure('');
  }*/
  /*
  @Post('/doc')
  async getFromAWS(
    @Body() awsdata: awsBody,
    @Res() res: Response,
    @Req() req: Request
  ): Promise<any> {
    try {
      let F_Resp: FileOutput = await this._awsService.getFromBucket(awsdata.aws);
      res.set({
        'Content-Type': 'application/pdf',
        'Content-Length': F_Resp.stream.length
      });
      res.sendFile(F_Resp.sampleData);
      console.log('File Serving Done...');
    } catch (e) {
      console.log(e);
      return { Test: 'FAILURE TESTING' };
    }
  }*/
  /*
  @Get('/get-noticia/:type/:id')
  async GetNoticiaById(@Param('type') type: string, @Param('id') id: string): Promise<any> {
    return await this._awsService.getNoticiaById(type, id);
  }*/

  /*@Get('/check')
  CheckFolder(): Promise<any> {
    return this._awsService.CheckBucket('complex/');
  }*/

  /*@Post('/check')
  CheckFolder(@Body() _body: { _prefix: string }): Promise<any> {
    console.log(_body);
    return this._awsService.CheckBucket(_body._prefix);
  }*/

  /*
  @Post('/del')
  DeleteObject(@Body() _body: { _key: string }): Promise<any> {
    return this._awsService.removeAwsObject(_body._key);
  }*/

  /*
  @Post('/upl-m')
  @UseInterceptors(FileInterceptor('arch'))
  UploadObjectM(@Body() _body, @UploadedFile() arch: any | null): Promise<any> {
    const nBody: ObjectUpload = JSON.parse(_body.body);
    console.log(nBody);
    return this._awsService.addAwsObject(nBody.objectKey, nBody.parentKey, arch);
  }*/

  //PARA CARPETAS
  /*
  @Post('/upl')
  UploadObject(@Body() _body: ObjectUpload): Promise<any> {
    return this._awsService.addAwsObject(_body.objectKey, _body.parentKey, null);
  }*/

  //EP : NOTICIAS
  /*@Post('/new')
  @UseInterceptors(FileInterceptor('arch'))
  UploadNew(@Body() _body: { body: string }, @UploadedFile() arch: any | null): Promise<any> {
    let rest = JSON.parse(_body.body);
    //rest = rest.body;
    const UplData: AwsComplex = {
      config: {
        redir: rest.config.redir,
        n_date: new Date(),
        title: rest.config.title,
        a_body: rest.config.a_body,
        src: rest.config.src
      },
      media: arch,
      ind: rest.ind
    };
    return this._awsService.UplComplex(UplData, 'news');
  }*/
  /*@Post('/complex')
  GetNews(@Body() _body: { _key: string }): Promise<any> {
    return this._awsService.getBasicObject(_body._key);
  }*/

  /*@Put('/complex-u')
    @UseInterceptors(FileInterceptor('arch'))
    UpdNew(@Body() _body:{body:string}, @UploadedFile() arch?: any) :Promise<any> {
      let rest = JSON.parse(_body.body);
      const UplData : AwsComplexUpd = { 
      act:'',
      config:{
        redir:rest.config.redir,
        n_date: new Date(),
        title:rest.config.title,
        a_body:rest.config.a_body,
        src:rest.config.src,
      }, media:arch,
         ind:rest.ind,
      }
      return this._awsService.UpdComplex();
    }*/

  /*@Post('/complex-d')
  DelNew(@Body() _body: { _key: string }): Promise<any> {
    return this._awsService.DelComplex(_body._key);
  }*/

  //EP : BANNERS
  /*@Post('/banner')
  @UseInterceptors(FileInterceptor('arch'))
  UploadBanner(@Body() _body: { body: string }, @UploadedFile() arch?: any): Promise<any> {
    let rest = JSON.parse(_body.body);
    //rest = rest.body;
    //console.log(rest);
    const UplData: AwsComplex = {
      config: {
        redir: rest.config.redir,
        n_date: new Date(),
        title: rest.config.title,
        a_body: rest.config.a_body,
        src: rest.config.src
      },
      media: arch,
      ind: rest.ind
    };
    return this._awsService.UplComplex(UplData, 'banners');
  }*/
  /*
  @Get('/banners')
  GetBanners(@Body() _body: { _key: string }): Promise<any> {
    return this._awsService.GetComplex();
  }*/

  @Get('/preview')
  async previewFile(@Query('url') url, @Res() res: Response): Promise<any> {
    try {
      const response = await this._awsService.getBasicObject(url);

      let toHeaders = {
        'Content-Type': response.ContentType,
        'Content-Length': response.ContentLength
      };
      res.set({
        ...toHeaders
      });
      res.send(response.Body);
    } catch (err) {
      throw new HttpException(
        `Error al obtener publicaciones ${err.message}`,
        HttpStatus.INTERNAL_SERVER_ERROR
      );
    }
  }
}
