import { Test, TestingModule } from '@nestjs/testing';
import { Aws3CntmService } from './aws3-cntm.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { TipoDocumentoPersonalService } from '../../tipo-documento-personal/tipo-documento-personal/tipoDocumentoPersonal.service';

describe('Aws3CntmService', () => {
  let service: Aws3CntmService;
  let documentoService: TipoDocumentoPersonalService;
  let DocumentoServiceMock: jest.Mock<Partial<TipoDocumentoPersonalService>>;
  //let RepositoryMock: jest.Mock;

  beforeEach(() => {
    DocumentoServiceMock = jest.fn(() => ({
      find: jest.fn(),
      findOne: jest.fn(),
      save: jest.fn(),
      update: jest.fn(),
      delete: jest.fn()
    }));
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        Aws3CntmService,
        { provide: TipoDocumentoPersonalService, useValue: new DocumentoServiceMock() }
      ]
    }).compile();

    service = module.get<Aws3CntmService>(Aws3CntmService);
    documentoService = module.get<TipoDocumentoPersonalService>(TipoDocumentoPersonalService);
  });
  //AWS STRUCTURE GET
  /*it('Should generate aws bucket structure', () => {

  });*/
  //AWS OBJECT UPLOAD
  /*it('Should return aws bucket object', () => {

  });*/
  //AWS OBJECT DELET
  /*it('Should delete aws bucket object', () => {

  });*/
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
