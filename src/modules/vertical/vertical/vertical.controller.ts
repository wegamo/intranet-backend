import { Body, Controller, Delete, Get, Param, Post, Put, UseGuards, UseInterceptors, UploadedFiles, HttpStatus, HttpException } from '@nestjs/common';
import { DeleteResult, UpdateResult } from 'typeorm';
import { CreateVerticalDTO } from '../../../models/vertical/dto/create-vertical.dto';
import { UpdateVerticalDTO } from '../../../models/vertical/dto/update-vertical.dto';
import { Vertical } from '../../../models/vertical/vertical.entity';
import { FilesInterceptor } from '@nestjs/platform-express';
import { VerticalService } from './vertical.service';
import { PublicacionService } from '../../publicacion/publicacion/publicacion.service';
import { UsuarioPermisoService } from '../../usuario-permiso/usuario-permiso/usuario-permiso.service';
import { Aws3CntmService } from '../../aws3-cntm/aws3-cntm/aws3-cntm.service';
import { AuthGuard } from '@nestjs/passport';
import { IVerticalType } from '../../../helpers/IVerticalType';
import { PersonaService } from '../../persona/persona/persona.service';
import { Persona } from '../../../models/persona/persona.entity';
import { Publicacion } from '../../../models/publicacion/publicacion.entity';
import { TipoObjeto } from '../../../enums/tipo-objeto.enum';
import { UsuarioPermiso } from '../../..//models/usuario-permiso/usuario-permiso.entity';
import { TipoPermiso } from '../../../enums/tipo-permiso.enum';
import { ApiBearerAuth, ApiConsumes, ApiImplicitBody, ApiOperation, ApiResponse, ApiUseTags } from '@nestjs/swagger';
import { InitVerticalDTO } from '../../../models/vertical/dto/init-vertical.dto';
import { EmpresaAreaEstatus } from '../../../enums/empresa-area.enum';

@ApiBearerAuth()
@ApiUseTags('Verticales')
@Controller('vertical')
export class VerticalController {
    constructor(
      private readonly verticalService: VerticalService,
      private readonly publicacionService: PublicacionService,
      private readonly s3Service: Aws3CntmService,
      private readonly usuarioPermisoService: UsuarioPermisoService,
      private readonly personaService: PersonaService,
    ) {}
  
  @ApiOperation({ title:'Obtener verticales', description:'Búsqueda general de verticales' })
  @ApiResponse({
    status: 200,
    description: 'Verticales registradas',
    type:IVerticalType,
    isArray:true,
  })
  @UseGuards(AuthGuard('jwt'))
  @Get()
  findAll(): Promise<IVerticalType[]> {
    try{
      return this.verticalService.findParsed();
    }
    catch(err){
      throw new HttpException(`Error al obtener Verticales ${err.message}`, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @ApiOperation({ title:'Obtener vertical', description:'Búsqueda de vertical según ID' })
  @ApiResponse({
    status: 200,
    description: 'Vertical registrada',
    type:IVerticalType,
  })
  @UseGuards(AuthGuard('jwt'))
  @Get('parsed')
  findParsed(): Promise<IVerticalType[]> {
    try{
      return this.verticalService.findParsedV2();
    }
    catch(err){
      throw new HttpException(`Error al obtener Verticales ${err.message}`, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
  
  
  @ApiOperation({ title:'Obtener verticales', description:'Obtención de verticales' })
  @ApiResponse({
    status: 200,
    description: 'Verticales registradas',
    type:IVerticalType,
    isArray:true
  })
  @UseGuards(AuthGuard('jwt'))
  @Get('id')
  findAllId(): Promise<Vertical[]> {
    try{
      return this.verticalService.findAllId();
    }
    catch(err){
      throw new HttpException(`Error al obtener Verticales ${err.message}`, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @ApiOperation({ title:'Obtener vertical', description:'Búsqueda de vertical según ID' })
  @ApiResponse({
    status: 200,
    description: 'Vertical registrada',
    type:IVerticalType,
  })
  @UseGuards(AuthGuard('jwt'))
  @Get(':id')
  findOne(@Param('id') id: number): Promise<Vertical> {
    try{
      return this.verticalService.findById(id);
    }
    catch(err){
      throw new HttpException(`Error al obtener Vertical ${err.message}`, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  
  @ApiConsumes('multipart/form-data')
  @ApiImplicitBody({ name: 'body', description: 'datos de la vertical', type:InitVerticalDTO  })
  @ApiOperation({ title:'Crear vertical', description:'Creación de vertical' })
  @ApiResponse({
    status: 200,
    description: 'Vertical registrada',
    type:IVerticalType,
  })
  @UseGuards(AuthGuard('jwt'))
  @Post()
  @UseInterceptors(FilesInterceptor('imgs',2))
  async create(
    @Body() VerticalData: { body: string },
    @UploadedFiles() imgs: Express.Multer.File[]): Promise<Vertical> {
    let VerticalDTO : CreateVerticalDTO;
    let img: Express.Multer.File;
    let imgSvg: Express.Multer.File;
    let espacioAws: string;
    let data : InitVerticalDTO = JSON.parse(VerticalData.body);
    let publicante : Persona = await this.personaService.findOne(data.user);
    try{
      VerticalDTO =  data.data;
      img = imgs[0];
      imgSvg = imgs[1];
      let toName: string = VerticalDTO.nombre.toLowerCase().replace(/ /g,"-");
      //let toName: string = VerticalDTO.nombre.replace(/ /g,"-");
      let espacioResp : { resp: AWS.S3.PutObjectOutput; nKey: string } = await  this.s3Service.addAwsObject(toName,'documentos/verticales/',null); 
      espacioAws  = espacioResp.nKey;
      let publicacionResp: Publicacion = await this.publicacionService.create({
        publicante:publicante, 
        ubicacion:espacioAws,
        tipoObjeto:TipoObjeto.CARPETA,
        nombreObjeto:toName,
        actualizacion:`${Date.now()} ${publicante.primerNombre+" "+publicante.primerApellido}`
      });
      let permisoResp: UsuarioPermiso = await this.usuarioPermisoService.create(
        {tipoPermiso:TipoPermiso.READWRITE,persona:publicante,publicacion:publicacionResp}
      );
      let imgResp:{ resp: AWS.S3.PutObjectOutput; nKey: string } = await  this.s3Service.addAwsObject(img.originalname,espacioAws,img); 
      let imgSvgResp:{ resp: AWS.S3.PutObjectOutput; nKey: string } = await  this.s3Service.addAwsObject(imgSvg.originalname,espacioAws,imgSvg);              
      return this.verticalService.create(VerticalDTO,espacioAws,img.originalname,imgSvg.originalname,);
    }
    catch(err){
      throw new HttpException(`Error al generar Vertical ${err.message}`, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @ApiOperation({ title:'Modificar vertical', description:'Modificación de vertical según ID' })
  @ApiResponse({
    status: 200,
    description: 'Vertical modificada',
    type:IVerticalType,
  })
  @UseGuards(AuthGuard('jwt'))
  @Put(':id')
  @UseInterceptors(FilesInterceptor('imgs'))
  async update(
    @Param('id') id: number,
    @Body() VerticalData: { body: string },
    @UploadedFiles() imgs: Express.Multer.File[]
  ): Promise<UpdateResult> {
    let VerticalDTO : UpdateVerticalDTO;
    let img: Express.Multer.File;
    let imgSvg: Express.Multer.File;
    let Vertical: Vertical = await this.verticalService.findById(id);
    let data : { data:UpdateVerticalDTO,user:number } = JSON.parse(VerticalData.body);
    try{
      VerticalDTO  = data.data;  
      img = imgs[0];
      imgSvg = imgs[1];
      if(img.originalname !== '*'){
        let prevImg:string = Vertical.img; 
        let imgResp:{ resp: AWS.S3.PutObjectOutput; nKey: string } = await  this.s3Service.addAwsObject(img.originalname,Vertical.espacioAws,img); 
        if(prevImg !== img.originalname){
          let multiDelete: AWS.S3.DeleteObjectsOutput = await  this.s3Service.paramAwsDelete(`${Vertical.espacioAws}${prevImg}`,false);
        }
      }
      if(imgSvg.originalname !== '*'){
        let prevImgSvg:string = Vertical.imgSvg; 
        let imgRespSvg:{ resp: AWS.S3.PutObjectOutput; nKey: string } = await  this.s3Service.addAwsObject(imgSvg.originalname,Vertical.espacioAws,imgSvg); 
        if(prevImgSvg !== imgSvg.originalname){
          let multiDelete: AWS.S3.DeleteObjectsOutput = await  this.s3Service.paramAwsDelete(`${Vertical.espacioAws}${prevImgSvg}`,false);
        }
      }
      let updResp: UpdateResult = await this.verticalService.update(Vertical,VerticalDTO);
      return updResp;
    }
    catch(err){
      throw new HttpException(`Error al actualizar Vertical ${err.message}`, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
  
  @ApiOperation({ title:'Deshabilitar vertical', description:'Deshabilitado de vertical' })
  @ApiResponse({
    status: 200,
    description: 'Vertical deshabilitada',
    type:IVerticalType,
  })
  @UseGuards(AuthGuard('jwt'))
  @Delete(':id')
  async delete(@Param('id') id: number): Promise<UpdateResult> {
    try{
      return await this.verticalService.updateState(id,EmpresaAreaEstatus.INACTIVA);
    }
    catch(err){
      throw new HttpException(`Error al eliminar Vertical ${err.message}`, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
  
  @ApiOperation({ title:'Eliminar vertical', description:'Eliminación completa de vertical'})
  @ApiResponse({
    status: 200,
    description: 'Vertical eliminada',
    type:IVerticalType,
  })
  @UseGuards(AuthGuard('jwt'))
  @Delete('del/:id')
  async trueDelete(@Param('id') id: number): Promise<DeleteResult> {
    try{
      let vertical: Vertical = await this.verticalService.findById(id);
      let deleteRes = await this.verticalService.delete(id);
      let multiDelete: AWS.S3.DeleteObjectsOutput = await  this.s3Service.paramAwsDelete(vertical.espacioAws,true);
      return deleteRes;
    }
    catch(err){
      throw new HttpException(`Error al eliminar Vertical ${err.message}`, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @ApiOperation({ title:'Habiltar vertical', description:'Habilitado de vertical'  })
  @ApiResponse({
    status: 200,
    description: 'Vertical habilitada',
    type:IVerticalType,
  })
  @UseGuards(AuthGuard('jwt'))
  @Put('enable/:id')
  async enableVertical(@Param('id') id: number): Promise<UpdateResult> {
    try{
      return await this.verticalService.updateState(id,EmpresaAreaEstatus.ACTIVA);
    }
    catch(err){
      throw new HttpException(`Error al habilitar Vertical ${err.message}`, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
