import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { CreateVerticalDTO } from '../../../models/vertical/dto/create-vertical.dto';
import { UpdateVerticalDTO } from '../../../models/vertical/dto/update-vertical.dto';
import { Vertical } from '../../../models/vertical/vertical.entity';
import { IVerticalType } from '../../../helpers/IVerticalType';
import { EmpresaAreaEstatus } from '../../../enums/empresa-area.enum';

@Injectable()
export class VerticalService {
  constructor(
    @InjectRepository(Vertical)
    private readonly verticalRepository: Repository<Vertical>
  ) {}

  findAll(): Promise<Vertical[]> {
    return this.verticalRepository.find();
  }

  parseVerticales (verticales: Vertical[]) : IVerticalType[] {
    let parsedRes: IVerticalType[] = verticales.map((item, ind) => {
      const { id, nombre, espacioAws, img, imgSvg, estatus, descripcion } = item;
      return {
        id,
        nombre,
        icono: img,
        descripcion,
        iconoSvg: imgSvg,
        espacio: espacioAws,
        estatus
      };
    });

    return parsedRes;
  }

  async findParsedV2(): Promise<IVerticalType[]> {
    let verticalRes: Vertical[] = await this.verticalRepository
      .createQueryBuilder('vertical')
      .distinctOn(["vertical.id"])
      .innerJoin('vertical.publicaciones', 'publicacion')
      .where("publicacion.tipoObjeto = 'A'")
      .orderBy('vertical.id', 'ASC')
      .getMany();
    return this.parseVerticales(verticalRes);
  }

  async findParsed(): Promise<IVerticalType[]> {
    let verticalRes : Vertical[] = await this.verticalRepository.find({order:{id:"ASC"}});
    return this.parseVerticales(verticalRes);
  }


  async findAllId(): Promise<Vertical[]> {
    const verticales: Vertical[] = await this.verticalRepository
      .createQueryBuilder()
      .select('id, nombre')
      .getRawMany();

    return verticales;
  }
  async findById(id: number): Promise<Vertical> {
    let getRes: Vertical = await this.verticalRepository.findOne({
      where: { id }
    });
    return getRes;
  }

  async create(
    vertical: CreateVerticalDTO,
    espacioAws: string,
    img: string,
    imgSvg: string
  ): Promise<Vertical> {
    return await this.verticalRepository.save({
      ...vertical,
      estatus: EmpresaAreaEstatus.ACTIVA,
      espacioAws,
      img,
      imgSvg
    });
  }

  async update(
    { id, ...prevVertical }: Vertical,
    verticalUpd: UpdateVerticalDTO
  ): Promise<UpdateResult> {
    let UpdRes: UpdateResult = await this.verticalRepository.update(
      { id: id },
      { ...prevVertical, ...verticalUpd }
    );
    return UpdRes;
  }

  async delete(id: number): Promise<DeleteResult> {
    let DelRes: DeleteResult = await this.verticalRepository.delete(id);
    return DelRes;
  }
  /*
      async disable(verticalId: number): Promise<UpdateResult> {
        //let area : Vertical = await this.verticalRepository.findOne({where:{id:verticalId}});
        return await this.verticalRepository.update({id:verticalId},{ estatus:EmpresaAreaEstatus.INACTIVA });
      }
    
      async enable(verticalId: number): Promise<UpdateResult> {
        //let vertical : Vertical = await this.verticalRepository.findOne({where:{id:verticalId}});
        return await this.verticalRepository.update({id:verticalId},{ estatus:EmpresaAreaEstatus.ACTIVA });
      }*/

  async updateState(verticalId: number, state: EmpresaAreaEstatus): Promise<UpdateResult> {
    //let vertical : Vertical = await this.verticalRepository.findOne({where:{id:verticalId}});
    return await this.verticalRepository.update({ id: verticalId }, { estatus: state });
  }
}
