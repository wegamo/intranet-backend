import { forwardRef, Module } from '@nestjs/common';
import { VerticalController } from './vertical/vertical.controller';
import { VerticalService } from './vertical/vertical.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Vertical } from '../../models/vertical/vertical.entity';
import { Aws3CntmModule } from '../aws3-cntm/aws3-cntm.module';
import { PublicacionModule } from '../publicacion/publicacion.module';
import { UsuarioPermisoModule } from '../usuario-permiso/usuario-permiso.module';
import { PersonaModule } from '../persona/persona.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Vertical]),
    forwardRef(()=> PublicacionModule),
    Aws3CntmModule,
    UsuarioPermisoModule,
    PersonaModule,
  ],
  controllers: [VerticalController],
  providers: [VerticalService]
})
export class VerticalModule {}
