import { Persona } from './../src/models/persona/persona.entity';
import { Empresa } from './../src/models/empresa/empresa.entity';
import { Rol } from './../src/models/rol/rol.entity';
import { Valor } from './../src/models/valor/valor.entity';
import { Norma } from './../src/models/norma/norma.entity';
import { Cargo } from './../src/models/cargo/cargo.entity';
import { TipoDocumentoPersonal } from '../src/models/tipo-documento-personal/tipoDocumentoPersonal.entity';
import { Area } from './../src/models/area/area.entity';
import { CategoriaNoticia } from './../src/models/categoria-noticia/categoria-noticia.entity';
import { Curso } from './../src/models/curso/curso.entity';
import { HistVacaciones } from './../src/models/hist-vacaciones/hist-vacaciones.entity';
import { HistPrestaciones } from './../src/models/hist-prestaciones/hist-prestaciones.entity';
import { Solicitud } from './../src/models/solicitud/solicitud.entity';
import { HistAusencias } from './../src/models/hist-ausencias/hist-ausencias.entity';
import { HistCargo } from '../src/models/hist-cargos/hist-cargos.entity';
import { TelefonoPersona } from '../src/models/telefono-persona/telefono-persona.entity';
import { TelefonoEmpresa } from '../src/models/telefono-empresa/telefono-empresa.entity';
import { MontoPrestaciones } from '../src/models/monto-disponible-prestaciones/monto-disponible-prestaciones.entity';
import { DocumentoPersonal } from '../src/models/documento-personal/documento-personal.entity';
import { PersonaEstatus } from '../src/enums/persona-estatus.enum';
import { SolicitudEstatus } from '../src/enums/solicitud-estatus.enum';
import { Noticia } from '../src/models/noticias/noticia.entity';
import { NoticiaTipo } from '../src/enums/noticia.tipo.enum';
import { genero } from '../src/enums/genero.enum';
import { Ausencia } from '../src/models/ausencia/ausencia.entity';
import { MotivoAusencia } from '../src/models/motivo-ausencia/motivoAusencia.entity';
import { TipoDescuento } from '../src/models/tipo-descuento/tipoDescuento.entity';
import { TipoGrado } from '../src/enums/tipo-grado.enum';
import { DocumentoPersonalEstatus } from '../src/enums/documento-personal-estatus.enum';
/*
export const SampleEstatus: Estatus = {
  id: 1,
  nombre: 'TestEstatus',
  tipo: 'EMPRESA'
};*/

export const SampleRol: Rol = {
  id: 1,
  nombre: 'ADMIN'
};

export const SampleEmpresa: Empresa = {
  id: 1,
  vision: 'TEST VISION',
  mision: 'TEST MISION',
  nombre: 'TestEmpresa',
  correo: 'SampleCorreo@Domain.com',
  estatus: 'Test Estatus',
  //estatus: SampleEstatus,
  //normas: [],
  telefonos: []
  //valores: []
};

export const SampleArea: Area = {
  id: 1,
  nombre: 'Testable Area',
  descripcion: 'Test Area'
};

export const SamplePersona: Persona = {
  id: 1,
  cedula: 'TESTCI',
  primerNombre: 'TestNombre',
  segundoNombre: '',
  primerApellido: 'TestApellido',
  segundoApellido: '',
  direccion: 'SampleDirection',
  imgPerfil: 'AWSSAMPLEDIR-smt.ext',
  estatus: PersonaEstatus.ACTIVO,
  estado: 'ESTADO-PAIS',
  pais: 'PAIS',
  genero: genero.MASCULINO,
  email: 'samplemail@domain.com',
  password: 'samplepassword',
  fechaNacimiento: new Date('1988-04-25'),
  fechaIngreso: new Date('2010-02-12'),
  telefonos: [],
  rol: SampleRol,
  empresa: SampleEmpresa
};

export const SampleTelefonoPersona: TelefonoPersona = {
  id: 1,
  numero: '501',
  persona: SamplePersona
};

export const SampleTelefonoEmpresa: TelefonoEmpresa = {
  id: 1,
  numero: '503',
  empresa: SampleEmpresa
};

export const SampleSolicitud: Solicitud = {
  id: 1,
  requiereAprobacion: true,
  descripcion: 'Descripcion Test Solicitud'
  //tipo_ausencia: false,
  //tipo_descuento: false,
  //solicitante: SamplePersona,
  //superior: null
};
export const SampleTipoDescuento: TipoDescuento = {
  id: 1,
  nombre: 'Nombre tipo descuento',
  ausencias: []
};
export const SampleMotivoAusencia: MotivoAusencia = {
  id: 1,
  nombre: 'Nombre motivo ausencia',
  ausencias: []
};
export const SampleAusencia: Ausencia = {
  id: 1,
  solicitud: SampleSolicitud,
  motivo: SampleMotivoAusencia,
  tipoDescuento: SampleTipoDescuento,
  histAusencias: []
};

export const SampleHistAusencias: HistAusencias = {
  id: 1,
  //motivo: 'Sample Motivo',
  estatus: SolicitudEstatus.PENDIENTE,
  firmaSolicitante: 'SAMPLEURLSIGN',
  fechaRevision: new Date(),
  fechaSolicitud: new Date(),
  fechaInicio: new Date(),
  fechaFin: new Date(),
  motivoRechazo: '',
  //solicitud: SampleSolicitud,
  ausencia: SampleAusencia,
  persona: SamplePersona
};

export const SampleHistVacaciones: HistVacaciones = {
  id: 1,
  diasPendienteDisfrute: 16,
  diasPendientePago: 8,
  cantidadDias: 8,
  estatus: SolicitudEstatus.PENDIENTE,
  fechaInicio: new Date('20/04/2021'),
  fechaFin: new Date('28/04/2021'),
  periodoPagado: 'SamplePeridoPagado',
  periodoDisfrute: 'SamplepPeriodoDisfrute',
  firmaSolicitante: 'SAMPLEURLSIGN',
  motivoRechazo: '',
  fechaRevision: new Date(),
  fechaSolicitud: new Date(),
  fechaExamenPreVacacional: new Date(),
  fechaReintegro: new Date('28/04/2021'),
  solicitud: SampleSolicitud,
  persona: SamplePersona
};

export const SampleHistPrestaciones: HistPrestaciones = {
  id: 1,
  montoSolicitado: '100',
  acumulado75: '75',
  motivo: 'Sample Motivo',
  estatus: SolicitudEstatus.PENDIENTE,
  firmaSolicitante: 'SAMPLEURLSIGN',
  fechaRevision: new Date(),
  fechaSolicitud: new Date(),
  solicitud: SampleSolicitud,
  persona: SamplePersona,
  presupuesto: 'AWS-presupuestofile'
};

export const SampleMontoPrestaciones: MontoPrestaciones = {
  id: 1,
  estatus: SolicitudEstatus.PENDIENTE,
  fechaSolicitud: new Date(),
  persona: SamplePersona
};

export const SampleCurso: Curso = {
  id: 1,
  nombre: 'Test Curso',
  institucion: 'Test Institucion',
  tipoGrado: TipoGrado.CURSO,
  //CONSIDERAR API EXTERNA
  pais: 'Test Pais',
  ciudad: 'Test Ciudad',
  certificado: 'SampleURL',
  fechaInicio: new Date('2020-04-02'),
  fechaFin: new Date('2020-06-03'),
  persona: SamplePersona
};
export const SampleTipoDocumentoPersonal: TipoDocumentoPersonal = {
  id: 1,
  nombre: 'Titulo Universitario',
  requerido: 'O'
  //obligatorio: true
  //url: 'test-url-aws',
  //tipo: 'DOCUMENTO'
};

export const SampleDocumentoPersonal: DocumentoPersonal = {
  id: 1,
  archivo: 'test-url-aws',
  estatus: DocumentoPersonalEstatus.LISTO,
  //documento: SampleDocumento,
  persona: SamplePersona,
  tipoDocumentoPersonal: SampleTipoDocumentoPersonal
};
/*
export const SampleUsuario = {
  id: 1,
  nombre: 'Test',
  password: 'Test',
  rol: SampleRol
};*/

export const SampleNorma: Norma = {
  id: 1,
  nombre: 'TestNorma',
  tipo: 'VESTIMENTA',
  descripcion: 'Test Descripcion'
  //empresa: SampleEmpresa
};

export const SampleValor: Valor = {
  id: 1,
  nombre: 'TestValor',
  descripcion: 'Test Descripcion'
  //empresa: SampleEmpresa
};

export const SampleCargo: Cargo = {
  id: 1,
  nombre: 'TEST NOMBRE CARGO',
  //departamento: 'DEP PRUEBAS',
  area: SampleArea,
  responsabilidades: 'Test responsabilidades'
};

export const SampleCategoriaNoticia: CategoriaNoticia = {
  id: 1,
  nombre: 'test categoria noticia',
  noticias: []
};

export const SampleNoticia: Noticia = {
  id: 'SAMPLEIDNOTICIA',
  tipo: NoticiaTipo.BANNER,
  titulo: 'Titulo Noticia',
  descripcion: '',
  posicion: 1,
  imagen: 'SAMPLEURLIMGAWS',
  persona: SamplePersona,
  categoria: SampleCategoriaNoticia,
  fechaPublicacion: new Date()
};
export const SampleHistCargo: HistCargo = {
  id: 1,
  fechaInicio: new Date('2010-05-03'),
  cargo: SampleCargo,
  fechaFin: null,
  persona: SamplePersona
};
/*
export const SampleDocumento: DocumentoPersonal = {
  id: 1,
  //nombre: 'TEST DOCUMENTO',
  //requerido: 'obligatorio'
  //url: 'test-url-aws',
  //tipo: 'DOCUMENTO'
};
*/
export const SampleEmail = {
  email: 'test@domain.com',
  username: 'test',
  password: 'test'
};
